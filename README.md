# Implementation of segmentation and registration in Julia

Authors: **Jan Kybic**, Martin Dolejsi

**source:** https://gitlab.fel.cvut.cz/biomedical-imaging-algorithms/segregjl

**python version:** https://gitlab.fel.cvut.cz/biomedical-imaging-algorithms/segreg

## Abstract

It is known that image registration is mostly driven by image edges. We have taken this idea to the extreme. In segmented images, we ignore the interior of the components and focus on their boundaries only. Furthermore, by assuming spatial compactness of the components, the similarity criterion can be approximated by sampling only a small number of points on the normals passing through a sparse set of keypoints. This leads to an order-of-magnitude speed advantage in comparison with classical registration algorithms. Surprisingly, despite the crude approximation, the accuracy is comparable. By virtue of the segmentation and by using a suitable similarity criterion such as mutual information on labels, the method can handle large appearance differences and large variability in the segmentations. The segmentation does not need not be perfectly coherent between images and over-segmentation is acceptable. We demonstrate the performance of the method on a range of different datasets, including histological slices and Drosophila imaginal discs, using rigid transformations.

---

## Using benchmark script

see sample images and landmarks in folder samples
in the same folder is an Ipython notebook for visualise result
