#!/usr/bin/python
# Example of calling Julia from Python

import scipy
import scipy.ndimage
import scipy.misc
import numpy as np
import pylab
import time

def showoverlay(img1,img2):
  s=img1.shape
  ss=img1.shape+(1,)
  c=np.concatenate((np.reshape(img1,ss),np.reshape(img2,ss),np.zeros(ss,dtype=img2.dtype)),axis=2)
  pylab.imshow(c)
  return

print("Starting Julia")
import julia
j=julia.Julia()
#j.eval('push!(LOAD_PATH,"/home/kybic/work/segregjl/")')
j.eval('push!(LOAD_PATH,".")')
j.eval("import register_patterns")
from julia import register_patterns as rp

print("Reading images")
pylab.ion() # interactive mode 
img1=scipy.misc.imread("imgs/reference.png")[:,:,1] 
img2=scipy.misc.imread("imgs/fuzzyInput.png")[:,:,1]
img2=scipy.ndimage.shift(img2,[5,0],order=1,mode='nearest')
img2=scipy.ndimage.rotate(img2,10,reshape=False,order=1,mode='nearest')
pylab.figure(1)
showoverlay(img1,img2)
#pylab.imshow(img1-img2)
pylab.title('overlay before')

print("Registering")
t1=time.time()
d=rp.register_pattern(img1,img2,1e-9) ;
# deform img2
warped=np.array(rp.deform_patterns(img1,img2,d),dtype=img2.dtype) ; # for some reason, the 'array' is not automatic
t2=time.time()-t1
print("Registration took ",t2)
print("Calculating inverse transform ")
invdef=rp.inverse_transformation(d,warped,img2)
print("Inverse calculated")
# deform image img1
invwarped=np.array(rp.deform_patterns(img2,img1,invdef),dtype=img2.dtype)
print("Image deformed")
pylab.figure(2)
pylab.imshow(warped)
pylab.title('warped')
pylab.figure(3)
showoverlay(img1,warped)
pylab.title('overlay after')

pylab.figure(4)
showoverlay(invwarped,img2)
pylab.title('overlay after inv')
print("Waiting")
pylab.pause(10)
