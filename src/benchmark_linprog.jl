# This is intended as a standalone script to be called by the registration evaluation framework
#
# It will be called like:
#   julia benchmark_linprog.jl moving_image reference_image output_dir parameter_file reference_image_landmarks
#
# At the moment, it performs one fixed experiment 
#
# Jan Kybic, 2016

using benchmark_register

function main()
    if length(ARGS) == 5
        register(ARGS[1], ARGS[2], ARGS[3], ARGS[4], ARGS[5])
    else
        register(ARGS[1], ARGS[2], ARGS[3], ARGS[4])
    end
end

#function mean_corner_val(img)
#    return (img[1,1]+img[1,end]+img[end,end],img[end,1])/4
#end

import benchmark_register

benchmark_register.register("imgs/case03-5-he-small.png","imgs/case03-3-psap-small.png",
                            "tmp/",landmarks="tmp/Case003_HE.txt")
