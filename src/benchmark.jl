# Test the registration on Flagship data
# Martin Dolejsi, September 2015
println("Including packages...")
# cd("c:/Code/segregjl/")       #this is for testing on my computer only
include("batchfunctions.jl")

if length(ARGS) == 5
    batchfunctions.test_register_flagship_script(ARGS[1], ARGS[2], ARGS[3], ARGS[4], ARGS[5])
else
    batchfunctions.test_register_flagship_script(ARGS[1], ARGS[2], ARGS[3], ARGS[4])
end
# julia benchmark.jl "c:/Code/segregjl/imgs/case03-3-psap-small.png" "c:/Code/segregjl/imgs/case03-5-he-small.png" "c:/Temp/ress/" "c:/Code/segregjl/params.txt" "M:/microscopy/Flagship/landmarks-MultiGeneSignatureMaps/scale-5pc/case03-5-he.txt"
# julia benchmark.jl moving_image static_image output_dir parameters static_image_landmarks
