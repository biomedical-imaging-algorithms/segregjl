"""
Coarse segmentation registration. That is, rigid registration with multiple initializations.
Used as a baseline to check other registration methods or as the first step.
"""
module coarsereg

using NLopt
using ..mil
using ..segimgtools
using ..deformation

export register_rigid, apply_transform, register_rigid_global, tovector,
       RigidRegistrationResult2D,
       register_rigid_global2,
       register_rigid_multiscale

abstract type RegistrationResult end

mutable struct RigidRegistrationResult2D <: RegistrationResult
    # the parameter vector is encoded as [ angle shiftx shifty ]
    theta::Vector{Float64}
    function RigidRegistrationResult2D(theta)
        @assert(length(theta)==3)
        return new(theta)
    end
end

function tovector(t::RigidRegistrationResult2D)
    return t.theta
end

function upscale(theta::RigidRegistrationResult2D)
    # change parameters when the image size changes
    t=theta.theta
    return RigidRegistrationResult2D([t[1];2*t[2];2*t[3]])
end

function apply_transform(g,theta::RigidRegistrationResult2D,outpsize)
    return deformation.transform_rigid2D(g,outpsize,theta.theta[1],theta.theta[2:3])
end

function register_rigid(f::Array{T,2},g::Array{T,2},k;x0=[0.;0.;0.],final=true) where {T}
  # rigidly (shift+rotation) register two images, returning a RigidRegistrationResult
  # f is fixed, g is being deformed. k is the number of classes
  # for the moment, we are making a specialized 2D version
  # if x0 is given, it is the starting point
  count=0  
  function crit(theta,grad)
      @assert(length(grad)==0) # we do not provide a gradient
      count+=1
      warped=deformation.transform_rigid2D(g,size(f),theta[1],theta[2:3])
      #warped=apply_transform(g,theta,size(f))
      m=calculateMIL(f,warped,k)
      #println("crit count=$count m=$m")
      return m
  end
  dof=3 # 
  opt=Opt(:LN_BOBYQA,dof)
  opt.max_objective=crit  
  opt.lower_bounds=[-200.,-max(size(f,2),size(g,2))/2,-max(size(f,1),size(g,1))/2]
  opt.upper_bounds=[200.,max(size(f,2),size(g,2))/2,max(size(f,1),size(g,1))/2]
  #max_objective!(opt,crit)
  #lower_bounds!(opt,[-200.,-max(size(f,2),size(g,2))/2,-max(size(f,1),size(g,1))/2])
  #upper_bounds!(opt,[200.,max(size(f,2),size(g,2))/2,max(size(f,1),size(g,1))/2])

  if final
      opt.xtol_abs=[0.5;0.5;0.5]
      opt.maxeval=1000
      opt.initial_step=[1.;1.;1.]
      #xtol_abs!(opt,[0.5;0.5;0.5]) 
      #maxeval!(opt,1000) # at most 1000 evaluations
      #initial_step!(opt,[1.;1.;1.]) # initial step
  else
      opt.xtol_abs=[10;5;5]
      opt.maxeval=100
      opt.initial_step=[1.;1.;1.]
      #xtol_abs!(opt,[10;5;5]) 
      #maxeval!(opt,100)
      #initial_step!(opt,[1.;1.;1.]) # initial step
  end
  m0=crit(x0,[])
  println("initial criterion=$m0")
  (optf,optx,ret)=optimize(opt,x0)
  println("register_rigid: Optimization finished after $count iterations, crit=$optf")
  return RigidRegistrationResult2D(optx),optf  
end

function register_rigid_global(f::Array{T,2},g::Array{T,2},k::Int;niter=30) where {T}
    # as register_rigid but tries several different starting points
    bestval=-Inf
    bestx=[0;0;0]
    bounds=[360.,max(size(f,2),size(g,2)),max(size(f,1),size(g,1))]
    for i=1:niter
        println("register_rigid_global: iteration $i started")
        x0=rand(3).*bounds-0.5*bounds
        (xopt,val)=register_rigid(f,g,k,x0=x0,final=false)
        #println("register_rigid_global: iteration $i value=$val")
        if val>bestval
            bestval=val ; bestx=xopt
        end
    end
    println("register_rigid_global: final optimization started, bestval=$bestval")
    xopt,val=register_rigid(f,g,k,x0=tovector(bestx),final=true)
    println("register_rigid_global: final val=$val")
    return xopt,val
end

function register_rigid_random(f::Array{T,2},g::Array{T,2},k;niter=1000) where {T}
    # try 'niter' random parameter values and report the best
    function crit(theta)
      warped=deformation.transform_rigid2D(g,size(f),theta[1],theta[2:3])
      return calculateMIL(f,warped,k)
    end
    bestval=-Inf
    bestx=[0;0;0]
    bounds=[360.,max(size(f,2),size(g,2))/2,max(size(f,1),size(g,1))/2]
    println("register_rigid_global: niter=$niter started")
    for i=1:niter
        #println("register_rigid_global: iteration $i started")
        x0=rand(3).*bounds-0.5*bounds
        val=crit(x0)
        #println("register_rigid_global: iteration $i value=$val")
        if val>bestval
            bestval=val ; bestx=x0
        end
    end
    return RigidRegistrationResult2D(bestx),bestval
end
    
function register_rigid_global2(f::Array{T,2},g::Array{T,2},k;niter=1000) where {T}
    # as register_rigid_global but does not optimize initial guesses
    # the crit function could be factorized out
    bestx::RigidRegistrationResult2D,bestval=register_rigid_random(f,g,k,niter=niter)
    println("register_rigid_global: final optimization started, bestval=$bestval")
    xopt,val=register_rigid(f,g,k,x0=tovector(bestx),final=true)
    println("register_rigid_global: final val=$val")
    return xopt,val
end

function register_rigid_multiscale(f::Array{T,2},g::Array{T,2},k;minsize=32) where {T}
    xopt,val=register_rigid_multiscale_inner(f,g,k,minsize=minsize)
    return register_rigid(f,g,k,x0=tovector(xopt),final=true)
end
    
function register_rigid_multiscale_inner(f::Array{T,2},g::Array{T,2},k;minsize=64) where {T}
    s=minimum(size(f))
    println("register multiscale called with size(f)=$(size(f))")
    if s<=minsize
        xopt,val=register_rigid_random(f,g,k)
    else
        # image is too big, reduce it
        fr=downsample_majority(f)
        gr=downsample_majority(g)
        xopt,val=register_rigid_multiscale_inner(fr,gr,k;minsize=minsize)
        println("register_multiscale coarser level returns val=$val")
        # upscale and perform local optimization
        x0=tovector(upscale(xopt))
        xopt,val=register_rigid(f,g,k,x0=x0,final=false)
        println("register_multiscale local registration size(f)=$(size(f)) val=$val")
    end
    return xopt,val
end
        

end # module coarsereg
