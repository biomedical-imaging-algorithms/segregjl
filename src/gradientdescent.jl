"""gradient descent implementation

Martin Dolejsi, September 2015
"""
module gradientdescent

export gradient_descent

using ..Options

include("debugassert.jl")
switchasserts(false)
include("debugprint.jl")
switchprinting(true)

"""
    gradient_descent(crit, xstart, initstep, minstep, maxiter, opts)
Performs gradient descent on the ctiterion crit, returns optimal parameter vector `xopt`
and coresponding criterion value `fopt`
# Arguments
* `crit::Function`: called `m = crit(x, grad)` returns criterion `m` and fills the gradient
    `grad` in `x`. If `grad` empty `crit()` should only return `m`.
* `xstart`: starting point for the descent.
* `initstep`: initial step of the descent next evaluated point will be `x - grad * initStep` or
    `x - grad / sqrt(sum(grad.^2)) * initStep` if optional parameter `normalizeGrad` is `true`.
* `minstep`: stoping condition. Iteration stops when step size is smaler than `minstep`.
* `maxiter`: stoping condition. Iteration stops when number of iteration reaches `maxiter`.
# Optional arguments in `opts`
* `minstepMul`: If optimization doesn't make any descent before `minstep` is reached, `minstep`
    is set to `minstep * minstepMul` and iteration continues. This is done just once if 
    `minstepMulRepeat`is `false`, if  `minstepMulRepeat` is `true` `minstep` is multilyied until
    maxiter is reached.
* 'stepMul': If the actual step generates descent, step is increased to 'step * stepMul' to insrease
    the speed.
* `stepDiv`: If there is do descent with actual step, optimization continues with `step = step / stepDiv`.
* `normalizeGrad`: If true, gradient is normalized to length 1 in each iteration.
"""
function gradient_descent(crit::Function, xstart, initstep, minstep::Number, maxiter::Integer, opts::Opts)
    @defaults opts normalizeGrad = false minstepMul = 0.001 stepMul = 1.5 stepDiv = 2. minstepMulRepeat = false
    @debugassert(minstepMul < 1)    #it is not a good idea to increase the minimum step when actual step is alredy smaller
    step = initstep * stepDiv       # step is divided by stepDiv in the inner while
    iter = 0
    grad = copy(xstart)
    tempx = copy(xstart)
    m = crit(xstart, Float64[])
    mlast = Inf
    stepPerformed = false
    while !stepPerformed            # if there is no descent, try to modify the parameters
        if iter != 0                # do not modify parameters in first round
            minstep *= minstepMul
            mlast = Inf
            m = crit(xstart, Float64[])
            if !minstepMulRepeat
                stepPerformed = true
            elseif minstep == 0     # if the minstep should be lowered repeatedly avoid underflow
                break
            end
        end
        while iter < maxiter && m < mlast && step > minstep
            iter += 1
            mlast = m
            crit(tempx, grad)       # find gradiend
            if normalizeGrad
                grad /= sqrt(sum(grad.^2))
            end
            while m >= mlast && step > minstep  # find step size
                step /= stepDiv
                m = crit(tempx - grad * step, Float64[])
            end
            if m < mlast            # apply the step
                @debugprintln_with_color(:magenta, "Iter = $iter Crit = $m, actual step = $step")
                tempx -= grad * step
                stepPerformed = true
            else
                @debugprintln_with_color(:magenta, "Iter = $iter Crit = $mlast, actual step = $step")
                m = mlast
            end
            step *= stepMul * stepDiv   #set the initial step for the next iteration
        end
    end
    return (tempx, m)
end

"""
    greedy_descent(crit, xstart, initstep, minstep, maxiter, opts)
Performs greedy descent on the ctiterion crit, returns optimal parameter vector `xopt` and
coresponding criterion value `fopt`.
Function evaluate crit in 5 points (`x - initStep:initStep/2:x + initstep`) for every dimension,
and choses the best result in every iteration.
"""
function greedy_descent(crit::Function, xstart, initstep, minstep::Number, maxiter::Integer, opts)
    @defaults opts stepMul = 2 stepDiv = 2.
    iter = 0
    aval = Inf
    minval = Inf
    step = initstep
    minshift = 0
    x = copy(xstart)
    xa = copy(x)
    grad = Float64[]
    while iter < maxiter && step > minstep
        iter += 1
        minparam = -1
        minshift = 0
        range = collect(-step:step/2:step)
        for param = 1:length(xstart)
            copy!(xa, x)
            for shift in range
                xa[param] = x[param] + shift
                aval = crit(xa, grad)
                if aval < minval
                    minval = aval
                    minparam = param
                    minshift = shift
                end
            end
        end
        if minparam > 0 && minshift != 0
            x[minparam] += minshift
            if abs(minshift) == step
                step *= stepMul
            end
            
        else
            step /= stepDiv
        end
        @debugprintln_with_color(:magenta, "Iter = $iter Crit = $minval, actual step = $step")
    end
    return (x, crit(x, Float64[]))
end

end     #module
