"""
Multivariate Gaussian distributions for segmented images
"""
module gaussian

export GParams, dimension, imgnormapprox, evalnormapprox, evalimgnormapprox, product
export transform, entropy, entropy_of_transformed

"""
Parameters of a normal distribution
"""
struct GParams
    """w::Float64: weight"""
    w::Float64
    """mu::Vector{Float64}: mean"""
    mu::Vector{Float64}
    """C::Matrix{Float64}: covariance matrix C"""
    C::Matrix{Float64}

    function GParams(w,mu::Vector{Float64},C::Array)
        d=length(mu)
        @assert(size(C,1)==d && size(C,2)==d)
        @assert w>=0.
        return new(w,mu,C)
    end
end

# the methods of GParams

"""
    dimension(g)
Return number of dimensions of `g`.
"""
dimension(g::GParams) = length(g.mu)

"""
    imgnormapprox(p, k)
Calculate the mean and variance of all classes in `p`.
Returns the parameters of the Gaussians, `Array{GParams}`,
`p` should contain numbers between 1 and `k`.
"""
function imgnormapprox(p::Array{T,N},k::Integer) where {T,N}
    @assert(ndims(p)==N)
    sizep=size(p)
    n=length(p)
    # first calculate the mean
    m=zeros(N,k)
    cnt=zeros(Int,k)
    for i=1:n # for all pixels
        c=p[i]
        @assert(c>=1 && c<=k)
        #        m[:,c]+=[ind2sub(sizep,i)...] # increment sum, beware of the order of indices
        m[:,c]+=[Tuple(CartesianIndices(sizep)[i])...] # increment sum, beware of the order of indices        
        cnt[c]+=1       # increment counter
    end
    for i=1:k # normalize for each class
        m[:,i]./=cnt[i]
    end
    # calculate the covariance matrix here
    C=zeros(N,N,k)
    for i=1:n # for all pixels
        c=p[i] # the values have been tested above
        x=[Tuple(CartesianIndices(sizep)[i])...]-m[:,c] # difference wrt of the mean
        C[:,:,c]+=x*x'
    end
    for i=1:k # normalize for each class
        C[:,:,i]./=cnt[i]
    end
    # create the parameters
    return [ GParams(cnt[i],m[:,i],C[:,:,i]) for i=1:k ]
end

"""
    evalnormapprox(g, x)
Evaluate the normal approximation described by `g` at point `x`.
"""
function evalnormapprox(g::GParams,x::Vector{Float64})
    d=dimension(g)
    @assert length(x)==d
    if g.w==0.
        return 0.
    end
    return g.w*((2pi)^d*det(g.C))^(-1/2)*exp(-0.5*dot(x,inv(g.C)*x))
end

"""
    evalimgnormapprox(sizep, g)
Evaluate the normal approximation calculated by `imgnormapprox`
for class described by parameters `g` for an image of dimension `sizep`.
"""
function evalimgnormapprox(sizep,g::GParams)
    y=zeros(sizep)
    d=length(sizep) # dimension
    invC=inv(g.C)
    detC=det(g.C)
    @assert (d==dimension(g))
    # precalculate constant
    c=g.w*((2pi)^d*detC)^(-1/2)
    
    mu=g.mu
    for i=1:length(y)
        x=[Tuple(CartesianIndices(sizep)[i])...]-mu
        y[i]=c*exp(-0.5*dot(x,invC*x))
    end
    return y
end

"""
    productintegral(g, h)
Integral of the product of two Gaussians `g` and `h` is calculated by evaluating a normal pdf.
see Matrix cookbook.
"""
function productintegral(g::GParams,h::GParams)
    gh=GParams(g.w*h.w,g.mu,g.C+h.C)
    return evalnormapprox(gh,h.mu)
end

"""
    normalize_columns(A)
Scale each column so that it sums to 1. This fails if some column contains only zeros.
"""
function normalize_columns(A)
    return A./sum(A,1)
end

"""
    entropy(gs, hs)
Joint entropy of two discrete probability fields. Each component `k` of `gs` and `hs`
describes the probability of a classes `k` in space.
This is an approximation, since the approximated probabilities do not sum to 1  
"""
function entropy(gs::Array{GParams},hs::Array{GParams})
    k=length(gs)
    @assert length(hs)==k
    # calculate the approximate joint probability matrix
    P=zeros(k,k)
    for i=1:k
        for j=1:k
            P[i,j]=productintegral(gs[i],hs[j])
        end
    end
    println("P=",P)
    # normalize the joint probability matrix
    P/=sum(P) ;
    println("normalized P=",P)
    # calculate entropy
    H=0.
    for i=1:k
        for j=1:k
            p=P[i,j]
            H+=p>0. ? -p*log(p) : 0. # just to deal with log(0)
        end
    end
    return H

end

"""
    transform(g, A, t)
Given a normal distribution ``p(x)`` described by parameters `g`
find parameters of a distribution ``p(y)=p(x)`` with ``y=Ax+t``.
"""
function transform(g::GParams,A::Matrix{Float64},t::Vector{Float64})
    return GParams(g.w,A*g.mu+t,A*g.C*A')
end

function transform(gs::Array{GParams},A::Matrix{Float64},t::Vector{Float64})
    return map(g->transform(g,A,t),gs)
end

"""
    entropy_of_transformed(gs, hs, A, t)
Calculate the entropy between the class probability field described by `gs`
and an affinely transformed version of `hs`.
"""
function entropy_of_transformed(gs::Vector{GParams},hs::Vector{GParams},A::Matrix{Float64},t::Vector{Float64})
    return entropy(gs,transform(hs,A,t))
end

end
    
    
    
