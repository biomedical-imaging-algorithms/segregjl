"""
Image similarity criterion--sum of squared differences (SSD).

Represent criteria ``\\sum_i \\phi(f_i,g_i)``,

where ``\\phi(f_i,g_i) = -(f_i-g_i)^2``

``f_i``, ``g_i`` are pixel values.

!!! note

    Negative SSD is computed to respect the protocol defined in [`criterion`](@ref).
"""
module ssd

using ..criterion
using Images
using ColorTypes

export SSDParameters, SSDState

#-------------- implement the Criterion protocol
"""
There are no parameters for the SSD criterion, just used for dispatching.
"""
struct SSDParameters <: CriterionParameters end

"""
There is no state for the SSD criterion, just used for dispatching.
"""
struct SSDState <: CriterionState{SSDParameters} end

"""
    criterion_init(f, g, p)
Initialize the criterion state given two images `p`, `q` and parameters `p`.
"""
function criterion.criterion_init(f::AbstractArray{T,N}, g::AbstractArray{T,N}, p::SSDParameters,eqclass=nothing) where {T,N}
    SSDState()
end

"""
    criterion_eval(s, pf, pg)
Return criterion value ``\\phi(f_i,g_i)`` for two pixels `pf`, `pg` and criterion state `s`.
"""
function criterion.criterion_eval(s::SSDState,f::T,g::T) where {T<:Number}
    - (float(f)-float(g))^2 # to avoid overflow and maintain stability, work with Float64
end

#evaluate SSD criterion for two RGB pixel values
function criterion.criterion_eval(s::SSDState,f::T,g::T) where {T<:AbstractRGB}
    - ((float(red(f))-float(red(g)))^2+(float(green(f))-float(green(g)))^2+(float(blue(f))-float(blue(g)))^2)
end

#evaluate the SSD criterion for two grayscale pixels
function criterion.criterion_eval(s::SSDState,f::T,g::T) where {T<:Gray}
    - (float(f)-float(g))^2 # to avoid overflow and maintain stability, work with Float64
end
#-------------- implement the Criterion protocol

"""
    ssd_val{T,N}(img1, img2)
Return SSD of two images with the same size.
"""
function ssd_val(img1::AbstractArray{T,N}, img2::AbstractArray{T,N}) where {T,N}
    if size(img1) != size(img2)
        error("Image sizes should be the same")
    end
    if T <: RGB
        error("Image should be one channel.")
    end
    ssdval=0.
    for i=1:length(img1)
        ssdval += (Float64(img1[i]) - Float64(img2[i])) ^ 2
    end
    return ssdval
    end
    
    
end #module


