"""
Image similarity criterion---mutual information (MI) for multichannel images, based on 
a Gaussian approximation


see criterion.jl for the interface
"""
module gaussianMI

import Images
import ..deformation
import ..segimgtools
using LinearAlgebra
using ..criterion
using ..Options

# for testing only
#import ImageView    
#import Winston

const Image = segimgtools.Image


export GaussianMIParameters, GaussianMIState
export test_GaussianMI


# here we implement the Criterion protocol. This criterion has no parameters

struct GaussianMIParameters <: CriterionParameters end

struct GaussianMIState <: CriterionState{GaussianMIParameters}
    df::Int                 # dimensionality of f
    dg::Int                 # dimensionality of g
    mf::Vector{Float64}     # mean of f
    mg::Vector{Float64}     # mean of g
    invcovfg::Matrix{Float64} # inverse covariance matrix of fg 
    invcovf::Matrix{Float64}  # inverse covariance matrix of f 
    invcovg::Matrix{Float64}  # inverse covariance matrix of g
    c::Float64                # additive constant
end

""" Calculate log(det(a)) via eigendecomposition, ignoring small eigenvalues """
function robust_logdet(a)
    vals,vecs=eigen(a)
    mineig=1e-10*maximum(abs.(vals)) # threshold for eigenvalues
    function clip(x)
        return abs(x)>mineig ? log(x) : 0.
    end
    return sum(map(clip,vals))
end

function criterion.criterion_init(f::AbstractArray{T,N}, g::AbstractArray{T,N},p::GaussianMIParameters) where {T,N}
  npix=length(f) # number of pixels
  @assert(npix==length(g)) # the images must have the same size                       
  df=length(segimgtools.pixel_to_vector(f[1])) # dimensionality
  dg=length(segimgtools.pixel_to_vector(g[1]))
  dfg=df+dg                       
  mf=zeros(Float64,df)
  mg=zeros(Float64,dg)
  covfg=zeros(Float64,(dfg,dfg))                        
  for i in 1:npix
    fv=segimgtools.pixel_to_vector(f[i])
    gv=segimgtools.pixel_to_vector(g[i])
    mf .+= fv
    mg .+= gv
  end                     
  mf ./= npix  ;  mg ./= npix                     
  for i in 1:npix
    fv=segimgtools.pixel_to_vector(f[i]) .- mf
    gv=segimgtools.pixel_to_vector(g[i]) .- mg
    fg=vcat(fv,gv)
    covfg .+= fg .* fg'
  end
  covfg ./= npix
  covf = @view covfg[1:df,1:df]
  covg = @view covfg[df+1:df+dg,df+1:df+dg]
  #c=0.5 * log( det(covf) * det(covg) / det(covfg) )  
  c=0.5 * ( robust_logdet(covf) + robust_logdet(covg) - robust_logdet(covfg)) 
  #  @show npix det(covf) det(covg) det(covfg) c  
  #@show npix covfg covf covg
  @assert(!isnan(c))
  # using a pseudoinverse to handle singular cases    
  return GaussianMIState(df,dg,mf,mg,pinv(covfg),pinv(covf),pinv(covg),c)
end # end of criterion_init
# See e.g. https://www.math.nyu.edu/faculty/kleeman/infolect7.pdf

""" evaluate a bilinear form for the same argument   """                       
bilinear(M,v) = v' * M * v
                         
                         
""" Calculate criterion contribution for pixel values pf, pg """
function criterion.criterion_eval(s::GaussianMIState,pf,pg)
  fv=segimgtools.pixel_to_vector(pf) .- s.mf
  gv=segimgtools.pixel_to_vector(pg) .- s.mg
  return s.c + 0.5 * ( bilinear(s.invcovf,fv)+bilinear(s.invcovg,gv)-bilinear(s.invcovfg,vcat(fv,gv)))
end                         

function calculateGaussianMI(s::GaussianMIState,f::AbstractArray, g::AbstractArray) 
    @assert(size(f)==size(g))
    r= 0.
    for i in eachindex(f)
        r += criterion_eval(s,f[i],g[i])
    end
    return r
end
                         
                         
# ----------------------------

function test_GaussianMI()
    ImageView.closeall()
    # test mutual information criterion
    img2=Images.load("imgs/simple.png")
    angle=10. ; shift=[0.;0.]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift,@options default=img2[1,1])
    ImageView.imshow(img1)
    ts=linspace(-30,30,50)
    ms=zeros(size(ts))
    ms2=zeros(size(ts))
    mbest=-Inf
    abest=0.
    s=criterion_init(img1,img2,GaussianMIParameters())
    for i in 1:length(ts)
        a=ts[i] 
        img2t=deformation.transform_rigid2D(img2,size(img2),a,shift)
        #s=criterion_init(img1,img2,GaussianMIParameters())
        m=calculateGaussianMI(s,img1,img2t)
        if m>mbest 
            mbest=m
            abest=a
        end
        ms[i]=m
        println("angle=$a mi=$m")
    end
    for i in 1:length(ts)
        a=ts[i] 
        img2t=deformation.transform_rigid2D(img2,size(img2),a,shift)
        s=criterion_init(img1,img2,GaussianMIParameters())
        m=calculateGaussianMI(s,img1,img2t)
        ms2[i]=m
        println("angle=$a mi2=$m")
    end
    tw = Winston.plot(ts,ms,"b-",ts,ms2,"g-")
    Base.display(tw)
    Winston.xlabel("angle") ; Winston.ylabel("MIL")
    println("best angle = $abest mil=$mbest")
end


end # module                         
