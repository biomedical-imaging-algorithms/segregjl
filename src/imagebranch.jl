const colorpkgs = ["ColorTypes", "Colors", "ColorVectorSpace"]
const imgpkgs = ["ImageCore", "ImageFiltering", "ImageAxes", "ImageMetadata"]
const extrapkgs = ["ImageMagick", "NRRD", "Netpbm", "ImageView"]

filterinstalled(pkgs) = filter(pkg -> Pkg.installed(pkg) != nothing, pkgs)
function checkoutbranch(pkgs, branchname)
    for pkg in pkgs
        Pkg.checkout(pkg, branchname)
    end
    nothing
end
function fetchall(pkgs)
    ipkgs = filterinstalled(pkgs)
    for pkg in ipkgs
        repo = LibGit2.GitRepo(Pkg.dir(pkg))
        LibGit2.fetch(repo)
    end
    nothing
end

function imagebranch(branchname)
    if branchname == "release"
        Pkg.free(filterinstalled(["FixedPointNumbers"; "Images"; colorpkgs; imgpkgs; extrapkgs]))
    elseif branchname == "images-next"
        Pkg.free(["FixedPointNumbers"; colorpkgs])
        fetchall([imgpkgs; extrapkgs])
        Pkg.checkout("Images", "images-next")
        checkoutbranch(imgpkgs, "master")
        checkoutbranch(filterinstalled(extrapkgs), branchname)
    elseif branchname == "fixed-renaming"
        Pkg.checkout("FixedPointNumbers", "teh/compact_printing")
        fetchall([imgpkgs; extrapkgs])
        Pkg.checkout("Images", "fixed-renaming")
        checkoutbranch(filterinstalled([colorpkgs; imgpkgs; extrapkgs]), branchname)
    else
        error("$branchname not recognized")
    end
    nothing
end
