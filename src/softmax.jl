"""
Softmax segmentation used in segmentation--registration alternating loop.

Reimplemented from python.
Martin Dolejsi, Jan Kybic, July 2015
"""
module softmax

# import deformation
import Images
#import ImageView
import NLopt
using ..segimgtools
using ..mil

export set_a!, train_classifier!, eval_probability, hard_classify

include("debugassert.jl")
switchasserts(true)
include("debugprint.jl")
switchprinting(false)

const eps = 1e-300

#--------------=Single Softmax classifier=----------------------
mutable struct SoftMaxClassifier
    a::Matrix{Float64}      # parameter vector
    order::Int              # order of aproximation
    nclass::Int             # number of classes
    nfeat::Int              # number of features (length of the feature vector)
    # mina::Vector{Float64} # limit for optimization
    # maxa::Vector{Float64} # limit for optimization
    
    function SoftMaxClassifier(order::Int, nclass::Int, nfeat::Int)
        klen = segimgtools.kernelize_size(nfeat, order)
        a = randn(nclass, klen) * 1e-16 #initial value
        new(a, order, nclass, nfeat)
    end
end

function set_a!(classifier::SoftMaxClassifier, av::Vector{Float64})
    klen = segimgtools.kernelize_size(classifier.nfeat, classifier.order)
    @debugassert(length(av) == klen * classifier.nclass)
    classifier.a = reshape(av, classifier.nclass, klen)
    return nothing
end

function set_a!(classifier::SoftMaxClassifier, a::Array{Float64, 2})
    klen = segimgtools.kernelize_size(classifier.nfeat, classifier.order)
    @debugassert(size(a) == (classifier.nclass, klen))
    classifier.a = a
    return nothing
end

@inbounds function init!(classifier::SoftMaxClassifier)
    randn!(classifier.a)
    for i = 1:length(classifier.a)
        classifier.a[i] * 1e-16
    end
    return nothing
end

get_av(classifier::SoftMaxClassifier) = reshape(classifier.a, length(classifier.a))
# Returns the a matrix as parameter vector av

function eval_probability_core!(fi::Vector{T}, f::Array{U, 2}, v::Vector{T}, classifier::SoftMaxClassifier, e::Vector{T}, z::Array{T, 2}, i::Int, ii::Int) where {T,U}
   
    for j = 1:classifier.nfeat
        fi[j] = f[j,i]
    end
    segimgtools.kernelize!(v, fi, classifier.order)
    BLAS.gemm!('N', 'N', 1., classifier.a, v, 0., e)    # matrix multip
    for j = 1:length(e)
        e[j] = exp(e[j])
    end
    segimgtools.nan_to_num!(e)    # to handle infinities
    se = sum(e)
    if se == 0  #avoid division by zero
        for j = 1:length(e)
            z[ii,j] = 1. / classifier.nclass# get probabilities
        end
    else
        for j = 1:length(e)
            z[ii,j] = e[j] / se           # get probabilities
        end
    end
    return nothing
end
        
        
#TODO incorporate the beta param from classifier_cython.pyx
function softmax_crit(classifier::SoftMaxClassifier, trdata::Array{Tuple{Array{Float64, 1}, Array{Float64, 2}}, 1})
    s = 0.                          # here we sum the results
    sumw = 0.
    v = zeros(segimgtools.kernelize_size(classifier.nfeat, classifier.order))
    fi = Array(Float64, classifier.nfeat)
    gradt=zeros(size(classifier.a)) # to sum the gradient
    z = Array(Float64, 1, classifier.nclass)
    e = zeros(Float64, classifier.nclass)
    for k in 1:classifier.nclass    # k is the class number
        w, f = trdata[k]            # weight and features
        n = size(f, 2)
        @debugassert(size(f, 1) == classifier.nfeat)

        for i = 1:n                 # for all training samples
            eval_probability_core!(fi, f, v, classifier, e, z, i, 1)
            s -= w[i] * log(z[k] + eps)
            z[1, k] -= 1
            sumw += w[i]
            BLAS.gemm!('T', 'T', w[i], z, v, 1.0, gradt)
        end
    end
    return s / sumw, gradt / sumw
end

function train_classifier!(classifier::SoftMaxClassifier, trdata::Array{Tuple{Array{Float64, 1}, Array{Float64, 2}}, 1}, lmbd::Float64 = .01, xtol_abs::Float64 = .001, initialstep::Float64 = 1., neval::Int = 1000)
    
    @debugassert(classifier.nclass == length(trdata))       #number of classes
    klen = segimgtools.kernelize_size(classifier.nfeat, classifier.order)
    count = 0

    function crit!(av, grad)
    # av is a vector of length number of clases * kernel dimension
    # that will be reshaped to a matrix of coefficients a[classifier.nclass,kdim].
    # Returns function value and fills grad
        count += 1
        set_a!(classifier::SoftMaxClassifier, av)
        s, gradt = softmax_crit(classifier, trdata::Array{Tuple{Array{Float64, 1}, Array{Float64, 2}}, 1})
        # println(s + lmbd / (2. * classifier.nclass) * sum(classifier.a .^ 2))
        if grad != []
            grad[:] = reshape(gradt + classifier.a * (lmbd / classifier.nclass), length(classifier.a))
            # gg = segimgtools.numgrad(x->begin set_a!(classifier, x) ; s,g = softmax_crit(classifier, trdata); s; end,av,0.0001)
            # println((gg-grad)./gg)
            # println(gg)
        end
        return s + lmbd / (2. * classifier.nclass) * sum(classifier.a .^ 2)
    end

    ndof = classifier.nclass * klen
    # opt=NLopt.Opt(:LN_NEWUOA,ndof)
    opt=NLopt.Opt(:LD_MMA, ndof)        # we would like to use LBFGS but it occassionaly fails for unknown reasons
    NLopt.min_objective!(opt,crit!)
    NLopt.xtol_abs!(opt, fill(xtol_abs, ndof))
    NLopt.maxeval!(opt, neval)                          # at most "neval" evaluations
    NLopt.initial_step!(opt, fill(initialstep, ndof))   # initial step
    tic()
    (optf,opta,ret)=NLopt.optimize(opt, get_av(classifier))
    set_a!(classifier, opta)
    @debugprintln_with_color(:green, "Soft_max learning: Optimization finished after $(toq())s and $count iterations, crit=$optf")
    return optf
end

function eval_probability!(z::Array{T,2}, classifier::SoftMaxClassifier, f::Array{U,2}, n::Int) where {T,U}
# Evaluate class probabilities for given feature vectors
    v = zeros(T, segimgtools.kernelize_size(classifier.nfeat, classifier.order))
    e = zeros(T, classifier.nclass)
    fi = Array(T, classifier.nfeat)

    for i = 1:n
        eval_probability_core!(fi, f, v, classifier, e, z, i, i)
    end
    return nothing
end

function eval_probability(classifier::SoftMaxClassifier, f::Array{T,2}) where {T}
# Evaluate class probabilities for given feature vectors
    n = size(f, 2)
    @debugassert(size(f, 1) == classifier.nfeat)
    z = zeros(Float64, n, classifier.nclass)
    eval_probability!(z, classifier, f, n)
    return z
end

function hard_classify!(label::Array{U,1}, z::Array{Float64,2}, classifier::SoftMaxClassifier, f::Array{T,2}) where {T,U}
# Fill label with the most likely class for each feature vector
    n = size(f, 2)
    eval_probability!(z, classifier, f, n)
    lbest = 0
    zbest::Float64 = -Inf
    for i = 1:size(z, 1)
        zbest = -Inf
        for j = 1:size(z, 2)
            if z[i, j] > zbest
                lbest = j
                zbest = z[i, j]
            end
        end
        label[i] = lbest
    end
end

function hard_classify!(label::Array{U,1}, classifier::SoftMaxClassifier, f::Array{T,2}) where {T,U}
# Fill label with the most likely class for each feature vector
    z = zeros(Float64, size(f, 2), classifier.nclass)
    hard_classify!(label, z, classifier, f)
end

function hard_classify(classifier::SoftMaxClassifier, f::Array{T,2}) where {T}
# Return the most likely class for each feature vector
    label = zeros(UInt8, size(f, 2))
    hard_classify!(label, classifier, f)
    return label
end


#--------------=Two images softmax classifier=----------------
mutable struct SoftMaxTwo
    class::Array{SoftMaxClassifier}
    
    function SoftMaxTwo(order::Int, nclass::Int, nfeat::Int)
        new([SoftMaxClassifier(order, nclass, nfeat), SoftMaxClassifier(order, nclass, nfeat)])
    end
end

function init!(classifier::SoftMaxTwo)
    init!(classifier.class[1])
    init!(classifier.class[2])
    return nothing
end

function set_a!(classtwo::SoftMaxTwo, av::Vector{Float64})
# Fills the parameter matrices of both classifiers
    klen = segimgtools.kernelize_size(classtwo.class[1].nfeat, classtwo.class[1].order)
    @debugassert(length(av) == 2 * klen * classtwo.class[1].nclass)
    classtwo.class[1].a = reshape(av[1:klen * classtwo.class[1].nclass], classtwo.class[1].nclass, klen)
    classtwo.class[2].a = reshape(av[klen * classtwo.class[1].nclass + 1:end], classtwo.class[2].nclass, klen)
    return nothing
end

get_av(classtwo::SoftMaxTwo) = [reshape(classtwo.class[1].a, length(classtwo.class[1].a)); reshape(classtwo.class[2].a, length(classtwo.class[2].a))]
# Returns all the matrices a as a parameter vector av

function init_softmaxtwo_random!(classtwo::SoftMaxTwo, si::Array{segimgtools.SuperpixelImage, 1}, sfeatures::Array{Array{T, 2}, 1}, radiusmm::Float64) where {T}
# Initialize SoftMaxTwo classifier using random points
    @debugassert(classtwo.class[1].nclass == classtwo.class[2].nclass)
    minsizemm = min(.5 * Images.pixelspacing(si[1]), .5 * Images.pixelspacing(si[1]))
    maxsizemm = max(([size(si[1])...] - .5) .* Images.pixelspacing(si[1]), ([size(si[2])...] - .5) .* Images.pixelspacing(si[1]))
    #generate random points (centers)
    centersmm::Array{Float64} = [rand(minsizemm[k]:maxsizemm[k]) for k = 1:ndims(si[1]), j = 1:classtwo.class[1].nclass]
    trcol = Tuple{Array{Float64,1}, Array{Float64,2}}[]
    for nimg = 1:2                          #for each image
        #find superpixels close to the center
        closesuperpixels::Array{Dict{segimgtools.SuperpixelType, Int64}} = [segimgtools.superpixels_in_range(si[nimg], centersmm[:,j], radiusmm) for j = 1:classtwo.class[1].nclass]
        #init training data vector
        tr::Array{Tuple{Array{Float64, 1}, Array{Float64, 2}}, 1} = [(Array(Float64, length(closesuperpixels[i]))::Array{Float64, 1}, Array(Float64, (classtwo.class[1].nfeat, length(closesuperpixels[i])))::Array{Float64,2}) for i=1:classtwo.class[1].nclass] #initialize training data
        for j = 1:classtwo.class[nimg].nclass
            #alloc arrays for class j
            pointn = 1
            #fill the training data
            for item in closesuperpixels[j]
                tr[j][1][pointn] = item[2]
                for i = 1:classtwo.class[1].nfeat
                    tr[j][2][i, pointn] = sfeatures[nimg][i, item[1]]
                end
                pointn += 1
            end
        end
        train_classifier!(classtwo.class[nimg], tr)
        trcol = [trcol; tr]
    end
    return trcol
end

function mutual_inf!(grad::Array{Float64,1}, classtwo::SoftMaxTwo, spoverlap::Array{Int,2}, sfeatures::Array{Array{T, 2}, 1}, z::Array{Array{U, 2}, 1}, lmbd_mi::Float64 = 0.001) where {T,U}
# Calculate mutual information criterion and its gradient, if required.
    for i = 1:length(z)
      eval_probability!(z[i], classtwo.class[i], sfeatures[i], size(sfeatures[i], 2))
    end

    totpix, P = mil.assemble_probability_matrix(classtwo.class[1].nclass, spoverlap, z)
    if grad == []
        gradP = []
    else
        gradP = zeros(Float64, size(P))
    end
    mi = mil.calculate_MIL_or_grad!(true, gradP, P)

    av = get_av(classtwo)
    crit = -mi + lmbd_mi / (2. * classtwo.class[1].nclass) * sum(av .^ 2)
    if grad == []
        return crit
    end
    
    # the minus sign is compensated by the minus sign at return
    gradt = -lmbd_mi / classtwo.class[1].nclass * reshape(av, classtwo.class[1].nclass, segimgtools.kernelize_size(classtwo.class[1].nfeat, classtwo.class[1].order), 2)
    mil.mutual_inf_gradient!(classtwo.class[1].nclass, spoverlap, z, sfeatures, P, gradP, gradt, totpix, classtwo.class[1].order)
    grad[:] = -reshape(gradt, length(gradt))
    return crit
end
        
function train_classifier!(classtwo::SoftMaxTwo, si::Array{segimgtools.SuperpixelImage}, sfeatures::Array{Array{T, 2}, 1}, spoverlap::Array{Int,2}, barrier::Bool, trcol::Array{Tuple{Array{Float64,1}, Array{Float64,2}}}, lmbd_mi::Float64 = 0.0001, xtol_abs::Float64 = .001, initialstep::Float64 = 1., neval::Int = 1000) where {T}
    @debugassert(classtwo.class[1].nclass * 2 == length(trcol))       #number of classes
    klen = segimgtools.kernelize_size(classtwo.class[1].nfeat, classtwo.class[1].order)
    global z = [Array(Float64, size(sfeatures[i], 2), classtwo.class[i].nclass) for i = 1:2]
    count = 0

    function crit!(av, grad)
    # av is a vector of length number of length nclass * segimgtools.kernelize_size * 2
    # that will be reshaped into a matrices of coefficients a[nclass,kdim].
    # Returns function value and fills grad
        count += 1
        set_a!(classtwo, av)

        J = mutual_inf!(grad, classtwo, spoverlap, sfeatures::Array{Array{T, 2}, 1}, z::Array{Array{Float64, 2}, 1}, lmbd_mi)
        # if grad != []
            # gg = segimgtools.numgrad(x->begin set_a!(classtwo, x) ; s = mutual_inf!([], classtwo, spoverlap, sfeatures, z, lmbd_mi); end,av,0.0001)
            # println((gg-grad)./gg)
            # println(gg)
            # @bp
        # end
        if !barrier
            return J
        end
        Js1, g1 = softmax_crit(classtwo.class[1], trcol[1:classtwo.class[1].nclass])
        Js2, g2 = softmax_crit(classtwo.class[2], trcol[classtwo.class[1].nclass + 1:end])
        grad[:] += [g1[:], g2[:]]
        return J + Js1 + Js2
    end
    ndof = classtwo.class[1].nclass * klen * 2
    # opt=NLopt.Opt(:LN_NEWUOA,ndof)
    opt=NLopt.Opt(:LD_MMA, ndof)        # we would like to use LBFGS but it occassionaly fails for unknown reasons
    NLopt.min_objective!(opt, crit!)
    NLopt.xtol_abs!(opt, fill(xtol_abs, ndof))
    NLopt.maxeval!(opt, neval)                          # at most "neval" evaluations
    NLopt.initial_step!(opt, fill(initialstep, ndof))   # initial step
    grad = Array(Float64, size(get_av(classtwo)))
    tic()
    (optf,opta,ret)=NLopt.optimize(opt, get_av(classtwo))
    set_a!(classtwo, opta)
    @debugprintln_with_color(:green, "Soft_max_two learning: Optimization finished after $(toq())s and $count iterations, crit=$optf")
    return optf
end

function init_softmaxtwo_random_optimize!(classtwo::SoftMaxTwo, si::Array{segimgtools.SuperpixelImage}, sfeatures::Array{Array{T, 2}, 1}, spoverlap::Array{Int,2}, radiusmm::Float64, barrier::Bool = false, lmbd_mi::Float64 = 0.001, xtol_abs::Float64 = 0.001) where {T}
# Generates random initialization and optimize classifiers to get maximal MI
    trcol = init_softmaxtwo_random!(classtwo, si, sfeatures, radiusmm)
    return train_classifier!(classtwo, si, sfeatures, spoverlap, barrier, trcol, lmbd_mi, xtol_abs, .1, 200)
end    

function check_classifier(classtwo::SoftMaxTwo, sfeatures::Array{Array{T, 2}, 1}) where {T}
#check if there are at least 2 classes in every image
    #first image
    bOK1 = false
    feat = Array(T, size(sfeatures[1], 1), 1)
    z = Array(Float64, 1, classtwo.class[1].nclass)
    val = Array(Int, 1)
    val1 = Array(Int, 1)
    for j = 1:length(feat)
        feat[j, 1] = sfeatures[1][j, 1]
    end
    hard_classify!(val, z, classtwo.class[1], feat)        #reference value
    val1 = zeros(val)
    for i = 2:size(sfeatures[1], 2)
        for j = 1:length(feat)
            feat[j, 1] = sfeatures[1][j,i]
        end
        hard_classify!(val1, z, classtwo.class[1], feat)
        if  val1[1] != val[1]
            bOK1 = true
            break
        end
    end
    #second image
    bOK2 = false
    for j = 1:length(feat)
        feat[j, 1] = sfeatures[2][j,1]
    end
    hard_classify!(val, z, classtwo.class[2], feat)        #reference value
    for i = 2:size(sfeatures[2],2)
        for j = 1:length(feat)
            feat[j, 1] = sfeatures[2][j,i]
        end
        hard_classify!(val1, z, classtwo.class[2], feat)
        if val1[1] != val[1]
            bOK2 = true
            break
        end
    end
    return bOK1 && bOK2
end

function init_softmaxtwo_random_optimize_repeat!(classtwo::SoftMaxTwo, si::Array{segimgtools.SuperpixelImage, 1}, sfeatures::Array{Array{T, 2}, 1}, spoverlap::Array{Int,2}, radiusmm::Float64, barrier::Bool = false, ninits::Int = 1, lmbd_mi::Float64 = 0.001, xtol_abs::Float64 = 0.001) where {T}
# Repeat init_softmaxtwo_random() and mutual information criterion optimization ninits times
# and choose the best result
    bestcrit = typemax(T); besta = get_av(classtwo)
    for i = 1:ninits
        bOK = false
        while !bOK
            # init!(classtwo)
            crit = init_softmaxtwo_random_optimize!(classtwo, si, sfeatures, spoverlap, radiusmm, barrier, lmbd_mi, xtol_abs)
            bOK = check_classifier(classtwo, sfeatures)
            if crit < bestcrit && bOK
                bestcrit = crit
                besta = get_av(classtwo)
            end
            # assig = zeros(length(sfeatures[1]))
            # assig = softmax.hard_classify(classtwo.class[1], sfeatures[1])
            # p1 = Images.Image(assig[si[1]], spatialorder=Images.spatialorder(si[1]), pixelspacing=Images.pixelspacing(si[1])))
            # assig = zeros(length(sfeatures[2]))
            # assig = softmax.hard_classify(classtwo.class[2], sfeatures[2])
            # p2 = Images.Image(assig[si[2]], spatialorder=Images.spatialorder(si[2]), pixelspacing=Images.pixelspacing(si[2]))
            # ImageView.view(p1/maximum(p1))
            # ImageView.view(p2/maximum(p2))
            # @bp
        end
    end
    set_a!(classtwo, besta)
    return bestcrit
end

end #module softmax
