"""
Registration of segmented and unsegmented images based on approximating
the criterion by sampling along normals from keypoints.

Jan Kybic, Martin Dolejsi, 2014-2016
"""
module finereg

#import ImageMagick
import Images
import ImageFiltering
#import ImageView
using ..segimgtools
using Base.Cartesian
#using Base.Collections
#using gradientdescent
using ..criterion      # exports criterion_eval, criterion_init
using ..mil                # adds methods to criterion_eval, criterion_init
#using eqcrit             # also exports criterion_eval, criterion_init
using ..ssd                # also exports criterion_eval, criterion_init     
using ..softmax
using NLopt
using ..coarsereg
using Colors
import ..deformation
import ..SLICsuperpixels
using ..Options
using ..keypoints  # we want to add methods
import Base.getindex # we want to add methods
import Base.length   # we want to add methods
import ..optimizer
import ..admm
using ..regularization
using LinearAlgebra
# using FastAnonymous
#using Winston

export register_fast, register_fast_multiscale, register_fast_multiscaleHB
export segment_register_fast_multiscale
include("debugassert.jl")
switchasserts(true)
include("debugprint.jl")
#switchprinting(false)
switchprinting(true)

"""
    transform_keypoints(kpts, ndef)
Return an array of `SimpleKeypoints` obtained by transforming `kpts` by `ndef`. The deformation
is considered as identity if `ndef` is `nothing`.
"""
function transform_keypoints(kpts::Vector{K},ndef::Union{D,Nothing}) where {K<:AbstractKeypoint,D<:deformation.AbstractDeformation}
    nkpts=length(kpts)
    kptsnew=Array{SimpleKeypoint}(undef,nkpts)
    if ndef===nothing || deformation.is_identity(ndef) # get(ndef)
        # transformation is identity, just copy
        for i in 1:length(kpts)
            kptsnew[i]=SimpleKeypoint(kpts[i])
        end
    else
        def=ndef # def=get(ndef)
        for i in 1:length(kpts)
            kptsnew[i]=SimpleKeypoint(deformation.transform_point(def,kpts[i].pos),
                                      deformation.transform_normal(def,kpts[i].normal,kpts[i].pos))
        end
    end
    return kptsnew
end


"""
The type `Contributions` is produced by `precalculate_contributions` or
`precalculate_contributions_pwl`and contains preprocessed point contributions,
allowing to evaluate the criterion for any displacement.
It should support methods `get_hmax`, `good_approx`, `get_npix`.
"""
abstract type Contributions end

"""
Contributions represented as a matrix, precalculated for a set of (integer) shifts
"""
mutable struct ContributionsSampled <: Contributions
    """`D::Matrix{Float64}`: precalculated contibutions for each `keypoint` and shift"""
    D::Matrix{Float64}
    """`maxD::Float64`: precalculated maximum of `D`"""
    maxD::Float64
    """`npix::Int`: number of pixels in the image"""
    npix::Int 
end    

function length(contribs::ContributionsSampled)
    size(contribs.D,2)
end

"""
    getindex(contribs, i)
Return contributions for `i`-th keypoint. For use with eval_contribution.
"""
function getindex(contribs::ContributionsSampled,i::Integer)
    view(contribs.D,:,i) # this used to be 'sub', probably obsolete
end

"""
    get_hmax(c)
Return maximal precomputed shift.
""" 
function get_hmax(c::ContributionsSampled)
    # needs to be consistent with hmax_to_maxdist
    # TODO: factorize out, avoid the duplicity
    div(size(c.D,1),2)
end

"""
    get_npix(c)
Return number of pixels in the image with keypoints
"""
function get_npix(c::ContributionsSampled)
    c.npix
end

"""
    good_approx(h, c)
Return true if a normal projection normalized by the normal length (shift) `h` of a point
is approximated well by contributions `c`.
"""
function good_approx(h::Float64,c::Contributions)
    hmax=get_hmax(c)
    # We want the result to be at least at one sampled point from the end.
    !(h >= hmax - 1 || h <= -hmax + 1)
end
    
"""
    eval_contribution(h1, contribs, i, barrier = true)
Given a precalculated contribution of `i`-th keypoint and a shift 'h1' along normal
for that keypoint, return the contribution of that keypoint to the criteiron.
For `barrier` = `true` apply a barier function if the displacement is outside the precalculated values.
"""
function eval_contribution(h1::Float64,contribs::ContributionsSampled,i::Int,barrier=true)
# TODO: This is a simplified version of eval_contribution_and_derivative. It might be possible to write the code just once using macros. 
    hmaxd=size(contribs.D,1)
    hmax=get_hmax(contribs) # maximum precalculated displacement
    @debugassert(2*hmax+1==hmaxd)
    h = h1 + hmax + 1 # h is an index to the "D" vector
    # handle the out-of-bounds cases
    if h >= 2 * hmax + 1
        if barrier
            critval = contribs.D[2 * hmax + 1, i] - contribs.maxD*(h1-hmax)^2 # barrier function 
        else
            critval = contribs.D[2*hmax+1,i]
        end
    elseif h < 1
        if barrier
            critval = contribs.D[1,i]-contribs.maxD*(h1+hmax)^2
        else
            critval = D[1,i]
        end
    else
        # Linear interpolation if within the precalculated values
        hf = floor(Int,h); hd = h - hf
        critval = contribs.D[hf,i] * (1 - hd) + contribs.D[hf+1,i] * hd
    end
    return critval::Float64
end


"""
    eval_contribution_and_derivative(h1, contribs, i, barrier = true)
like `eval_contribution` but returns also contributin derivative with respect to shift `h1`.
"""
function eval_contribution_and_derivative(h1::Float64,contribs::ContributionsSampled,i::Int,barrier=true)
    hmaxd=size(contribs.D,1)
    hmax=get_hmax(contribs) # maximum precalculated displacement
    @debugassert(2*hmax+1==hmaxd)
    h = h1 + hmax + 1 # h is an index to the "D" vector
    # handle the out-of-bounds cases
    if h >= 2 * hmax + 1
        if barrier
            critval = contribs.D[2 * hmax + 1, i] - contribs.maxD*(h1-hmax)^2 # barrier function 
            dval = -2*contribs.maxD*(h1-hmax)
        else
            critval = contribs.D[2*hmax+1,i]
            dval = 0.
        end
    elseif h < 1
        if barrier
            critval = contribs.D[1,i]-contribs.maxD*(h1+hmax)^2
            dval = -2*contribs.maxD*(h1+hmax)
        else
            critval = contribs.D[1,i]
            dval = 0.
        end
    else
        # Linear interpolation if within the precalculated values
        hf = floor(Int,h); hd = h - hf
        critval = contribs.D[hf,i] * (1 - hd) + contribs.D[hf+1,i] * hd
        if hd < 1e-3 && hf > 1
            dval = (contribs.D[hf+1,i] - contribs.D[hf-1,i])/2
        else
            dval = contribs.D[hf+1,i] - contribs.D[hf,i]
        end
    end
    return critval::Float64, dval
end


"""
    normal_to_criterion_contributions!(critstate, kptf, v, D, kpind, hmax)
Given a vector of sampled pixel values `v` of length 2*maxdist, representing displacements
``-d_{max}+0.5,-d_{max}+1.5,\\ldots,-0.5,0.5\\ldots,d_{max}-1.5,d_{max}-0.5`` fill one column of the
matrix storing the criterion contribution precalculated for all displacements ``-h_{max}\\ldots h_{max}``
"""
function normal_to_criterion_contributions!(critstate::CriterionState, 
                                               kptf::TwoclassKeypoint,v::Vector{T},
                                               D::AbstractArray{Float64,2},kpind::Int,hmax) where {T}
    @inline ff(ii, id) = id[ii+maxdist] 
    
    maxdist=hmax_to_maxdist(hmax, TwoclassKeypoint) #d_{max}
    @debugassert(size(D,1)==2*hmax+1)
    @debugassert(length(v)==2*maxdist)

    #@debugprintln("normal_to_criterion_contributions started")
    # preallocate cumulative sums. If allocation is a problem, we will factorize it out
    ipos=Array{Float64}(undef,2*maxdist)
    ineg=Array{Float64}(undef,2*maxdist)
    cpos=kptf.classpos
    cneg=kptf.classneg
    sump=0.
    sumn=0.
    @inbounds for j=1:2*maxdist
        sump+=criterion_eval(critstate,cpos,v[j]) ; ipos[j]=sump
        sumn+=criterion_eval(critstate,cneg,v[j]) ; ineg[j]=sumn
    end
    # @show ipos ineg
    # unrolling the loop for j=-hmax
    @inbounds D[1,kpind] = kptf.count*( ipos[0 + maxdist] - ipos[-hmax + maxdist]+ ineg[-hmax + maxdist] )
    for j=-hmax+1:hmax
        @inbounds   D[j+hmax+1,kpind] = kptf.count*( ipos[j + hmax+maxdist] - ipos[j+maxdist]+ ineg[j+maxdist]-ineg[j-hmax+maxdist])
    end
end    

# an implementation for SampledKeypoint.
function normal_to_criterion_contributions!(critstate::CriterionState,
                                               kptf::SampledKeypoint{K},v::Vector{T},
                                               D::AbstractArray{Float64,2},kpind::Int,hmax::Integer) where {T,K}

    #@debugprintln("normal_to_criterion_contributions for SampledKeypoints started")

    maxdist=hmax_to_maxdist_moving(hmax,SampledKeypoint{K})
    @debugassert(size(D,1)==2*hmax+1)
    @debugassert(length(kptf.pixels)==2*hmax)
    @debugassert(length(v)==2*maxdist)

    #@show critstate
    for ofs=-hmax:hmax 
        # calculate criterion for offset ofs
        z::Float64=0.
        for i=1:2*hmax # index into kptf.pixels
            k=i+hmax+ofs
            @debugassert(k>=1 && k<=2maxdist)
            z+=criterion_eval(critstate,kptf.pixels[i],v[k]) # sum criterion contribution
            #@show kptf.pixels[i] v[k] z
            #@assert(!isnan(z))
        end
        D[ofs+hmax+1,kpind]=z*kptf.count
    end
end

"""
    precalculate_contributions{T}(g::Images.Image{T}, kptsf, kptsg, critstate, hmax; defaultval = zero(T))
Return `ContributionsSampled` with the criterion precomputated for all the shifts at
all the keypoints. The normal values are sampled up to distance `maxdist` (``d_{max}``).
It should work for any criterion as long as `criterion_eval` used in `normal_to_criterion_contribution`
is implemented.
"""
function precalculate_contributions(g::Image{T},kptsf::Vector{K},
                           kptsg::Vector{SimpleKeypoint}, critstate::CriterionState,
                           hmax::Integer; defaultval=zero(T)) where {K<:AbstractKeypoint,T}
    n=length(kptsf)
    @assert(length(kptsg)==n)
    maxdist=hmax_to_maxdist(hmax,K) # maximum displacement
    D=zeros(Float64,2*hmax+1,n)
    numoutside=0
    maxdistmov = hmax_to_maxdist_moving(hmax,K)
    v = zeros(T, 2 * maxdistmov)
    pxsp = Images.pixelspacing(g)
    # process all keypoints
    for i=1:n
        # sampled pixel values along the normal
        if sample_normal!(g,kptsg[i],maxdistmov,v,pxsp,defaultval)
            normal_to_criterion_contributions!(critstate,kptsf[i],v,D,i,hmax)
        else
            numoutside+=1
        end    
    end
    if numoutside > 0
        @debugprintln_with_color(:magenta, "Unable to sample normal for $numoutside Keypoints outside of the image.")
    end
    maxD=maximum(abs.(D))
    return ContributionsSampled(D,maxD,length(g))
end


"""
Parameters of piecewise linear function ``f(x)``

``f(x)=\\begin{cases}\\begin{array}{cl} u_0+u_+ ( x - x0 ) & \\textrm{if }x > x_0 \\\\ u_0+u_- ( x0 - x ) & \\textrm{if } x <= x0\\end{array}\\end{cases}``

``u_+>0, u_->0``.

NOTE: the constant parameter ``u_0`` is not necessary for the optimization and could be omitted.
"""
struct PiecewiseLinearParams
    """u0::Float64: ``u_0``"""
    u0::Float64
    """uplus::Float64: ``u_+``"""
    uplus::Float64
    """uminus::Float64: ``u_-``"""
    uminus::Float64
    """x0::Float64: ``x_0``"""
    x0::Float64
end


"""
Contributions approximated by a piecewise linear convex function.
Each individual contribution ``f(x)``, where ``x`` is the shift along the normal, is
approximated by [`PiecewiseLinearParams`](@ref)
"""
mutable struct ContributionsPiecewiseLinear <: Contributions
    contribs::Vector{PiecewiseLinearParams}
    npix::Integer # number of pixels
    hmax::Float64 # maximum displacement for which the approximation is valid
end    

function length(c::ContributionsPiecewiseLinear)
    length(c.contribs)
end

function get_npix(c::ContributionsPiecewiseLinear)
    c.npix
end

function getindex(c::ContributionsPiecewiseLinear, i::Integer)
    c.contribs[i]
end

function get_hmax(c::ContributionsPiecewiseLinear)
    # needs to be consistent with hmax_to_maxdist
    # TODO: factorize out, avoid the duplicity
    c.hmax
end

"""
    eval_pwl(h, c)
Return ``f(h)`` of piecewise linear function with parameters `c`.
"""
function eval_pwl(h::Float64,c::PiecewiseLinearParams)
    d=h-c.x0
    if d>0.
        c.u0 + c.uplus * d
    else
        c.u0 - c.uminus * d
    end
end

"""
    eval_pwl_and_derivative(h, c)
Return ``f(h)`` and its derivatin with respect to h of piecewise linear function with parameters `c`.
"""
function eval_pwl_and_derivative(h::Float64,c::PiecewiseLinearParams)
    d=h-c.x0
    if d>0.
        c.u0 + c.uplus * d, c.uplus
    else
        c.u0 - c.uminus * d, -c.uminus
    end
end



# eval_contribution implementation for ContributionsPiecewiseLinear. 'barrier' is not used,
# sign change to undo a sign change in eval_contributions
function eval_contribution(h::Float64,c::ContributionsPiecewiseLinear,i::Int,barrier = false)
    -eval_pwl(h,c[i]) 
end

# eval_contribution_and_derivative implementation for ContributionsPiecewiseLinear. 'barrier' is not used,
# sign change to undo a sign change in eval_contributions
function eval_contribution_and_derivative(h::Float64,c::ContributionsPiecewiseLinear,i::Int,barrier = false)
    val,der=eval_pwl_and_derivative(h,c[i])
    return -val,-der
end


"""
    sumquad(n)
Return 
``\\begin{cases}\\begin{array}{cl} \\sum_{i=1}^n i^2 & \\textrm{for }n > 0 \\\\ 0 & \\textrm{othrtwise.}\\end{array}\\end{cases}``
"""
function sumquad(n::Integer)
    n>0 ? div(((3+2n)*n+1)*n,6) : 0
end
    
"""
    linfit(f, xmax)
Return the coefficient ``a`` of the least squares approximation ``ax \\approx f(x)``,
minimizing sum of squared differences at ``x=1,2,\\ldots,x_{max}``
"""
function linfit(f,xmax::Integer)
    #@debugassert(xmax>=1)
    tot=0.
    for i=1:xmax
        tot+=f(i)*i
    end
    tot/float(sumquad(xmax))
end


"""
    piecewiseLinear_approximate(f, hmax)
Given a function ``f(x)`` that can be evaluated for integers ``-h_{max},\\ldots,h_{max}``,
return `PiecewiseLinearParams` that approximate ``f(x)`` well. We shall guarantee that the
approximation is convex."""
function piecewiseLinear_approximate(f,hmax::Integer)
    @debugassert(hmax>0)
    x0,u0=argminval(f,range(-hmax,2hmax+1))
    #fplus= @anon x -> f(x-x0)-u0
    fplus= x -> f(x0+x)-u0
    uplus= x0 < hmax ? linfit( fplus, hmax-x0) : NaN 
    #fminus=@anon x -> f(x0-x)-u0
    fminus= x -> f(x0-x)-u0
    uminus= x0>-hmax ? linfit( fminus, hmax+x0) : -uplus
    if isnan(uplus)
        uplus=-uminus
    end
    @debugassert(!isnan(uplus) && !isnan(uminus))
    PiecewiseLinearParams(u0,uplus,uminus,x0)
end

"""
    linfit_plus(d, x0, hmax, xmax, u0)
Return ``u_+`` used in `piecewiseLinearParams`.
"""
function linfit_plus(d,x0,hmax,xmax::Integer,u0)
    #@debugassert(xmax>=1)
    tot=0.
    for i=1:xmax
        tot+=(-d[x0+i+hmax+1]-u0)*i
    end
    tot/float(sumquad(xmax))
end

"""
    linfit_minus(d, x0, hmax, xmax, u0)
Return ``u_-`` used in `piecewiseLinearParams`.
"""
function linfit_minus(d,x0,hmax,xmax::Integer,u0)
    tot=0.
    for i=1:xmax
        tot+=(-d[x0-i+hmax+1]-u0)*i
    end
    tot/float(sumquad(xmax))
end


"""
    piecewiseLinear_approximate_fast(d, hmax)
Specialized version of `piecewiseLinear_approximate`, avoiding calling a function. `d` is a
vector of values calculated by `normal_to_criterion_contributions!`
"""
function piecewiseLinear_approximate_fast(d,hmax::Integer)
    @debugassert(hmax>0)
    x0,u0=argminval(h->-d[h+hmax+1],range(-hmax,length=2hmax+1))
    #u0,x0=findmin(map(h->-d[h+hmax+1],range(-hmax,length=2hmax+1)))
    uplus= x0 < hmax ? linfit_plus( d, x0, hmax,hmax-x0,u0) : NaN 
    uminus= x0>-hmax ? linfit_minus( d, x0, hmax, hmax+x0,u0) : -uplus
    if isnan(uplus)
        uplus=-uminus
    end
    if isnan(uplus) || isnan(uminus)
        #@show d u0 x0 hmax uplus uminus
        x0,u0=argminval(h->-d[h+hmax+1],range(-hmax,length=2hmax+1))
        #u0,x0=findmin(map(h->-d[h+hmax+1],range(-hmax,length=2hmax+1)))
        #@show x0 u0
    end
    @debugassert(!isnan(uplus) && !isnan(uminus))
    PiecewiseLinearParams(u0,uplus,uminus,x0)
end


"""
    precalculate_contributions_pwl{T}(g::Images.Image{T}, kptsf, kptsg, critstate, hmax; defaultval = zero(T))
Like `precalculate_contributions` but returns `ContributionsPiecewiseLinear`
"""
function precalculate_contributions_pwl(g::Image{T},kptsf::Vector{K},
                           kptsg::Vector{SimpleKeypoint}, critstate::CriterionState,
                           hmax::Integer; defaultval::T=zero(T)) where {K<:AbstractKeypoint,T}
    @debugprintln("precalculate_contributions_pwl")
    n=length(kptsf)
    @assert(length(kptsg)==n)
    maxdist=hmax_to_maxdist(hmax,K) # maximum displacement
    pwls=Array{PiecewiseLinearParams}(undef,n)
    d=zeros(Float64,2*hmax+1,1)
    numoutside::Integer=0
    maxdist=hmax_to_maxdist_moving(hmax,K)
    c::Vector{T}=fill(defaultval,2*maxdist)
    pxsp = Images.pixelspacing(g)
    # process all keypoints
    #@debugprintln("precalculate_contributions $(length(kptsf[1].pixels)) hmax=$(hmax)")

    for i=1:n
        # sampled pixel values along the normal
        if (sample_normal!(g,kptsg[i],maxdist,c,pxsp,defaultval))
            normal_to_criterion_contributions!(critstate,kptsf[i],c,d,1,hmax)
            #if isnan(d[1])
            #    @show d c
            #    @debugassert(false)
            #end
            pwls[i]=piecewiseLinear_approximate_fast(d,hmax)
        else
            # default contribution is a constant zero
            pwls[i]=PiecewiseLinearParams(0.,0.,0.,0.)
            numoutside+=1
        end    
    end
    if numoutside > 0
        @debugprintln_with_color(:magenta, "Unable to sample normal for $numoutside keypoints outside of the image.")
    end
    return ContributionsPiecewiseLinear(pwls,length(g),hmax)
end


"""
    eval_contributions(theta, def, kptsf, kptsg, contributions, grad)
Return the criterion value for a given parameter `theta`. If `length(grad) > 0`,
function also stores the criterion gradient to `grad`. `def` is the deformation,
`kptsf` are keypoints in the fixed image (f), `contributions` contains precalculated information
from the moving image (g). The sign is reversed.
"""
function eval_contributions(theta::Vector{Float64},
              def::deformation.AbstractDeformation,kptsf::Vector{K},
              kptsg::Vector{L}, contributions::Contributions, grad::Vector{Float64}) where {K<:AbstractKeypoint,L<:AbstractKeypoint}
    # @debugprintln("Starting eval_contributions")
    deformation.set_theta!(def, theta)
    dograd::Bool = length(grad) > 0
    if dograd
        fill!(grad, 0.)
    end
    n=length(kptsf)                       # number of points
    y=zeros(Float64,length(kptsf[1].pos)) # coordinates of the transformed fixed image landmarks
    m::Float64=0.                         # criterion
    # go over all keypoints
    for i=1:n
        # transform the point from the fixed image
        deformation.transform_point_kpt!(def, y, kptsf, i)            #fast evaluation 
        # deformation.transform_point!(def, kptsf[i].pos, y)          #slow evaluation
        # project onto the normal, in normal units
        for j=1:length(y)
            y[j] -= kptsg[i].pos[j]        #subtract inplace. If the subtraction is done as y[:] -= kptsg[i].pos julia allocates.
        end
        h1 = dot(y, kptsg[i].normal) / norm2(kptsg[i].normal)  # project onto normal
        if dograd
            m1,dval=eval_contribution_and_derivative(h1,contributions,i)
            #println("i=$i kf=$(kptsf[i].pos) kg=$(kptsg[i].pos) kn=$(kptsg[i].normal) h1=$(h1) m1=$m1 dval=$dval")
            # multiply 'dval' by partial derivative of shift wrt parameters
            deformation.add_grad_contrib_kpt!(grad, dval, def, kptsf, kptsg, i)             #fast evaluation
            # deformation.add_grad_contrib!(grad, dval, kptsg[i].normal, def, kptsf[i].pos) #slow evaluation 
        else
            m1=eval_contribution(h1,contributions,i)
        end
        m -= m1
    end
    m /= float(contributions.npix)
    if dograd
        grad[:] /= - contributions.npix
        # g1 = segimgtools.numgradc(th->eval_contributions(th,def,kptsf,kptsg,contributions,Float64[]), theta, 1e-12)
        # g2 = segimgtools.numgrad(th->eval_contributions(th,def,kptsf,kptsg,contributions,Float64[]), theta, 1e-12)
        # println(grad)
        # println(((grad - g1) ./ grad))
        # println(((grad - g2) ./ grad))
        # @bp
    end
    return m
end

"""
    eval_regularization(theta, def, regparams, grad)
Evaluate regularization for a given parameter `theta`.
`def` is the deformation, `regparams` defines regularization type and its parameters.
"""
function eval_regularization(theta::Vector{Float64}, def::deformation.AbstractDeformation,
                            regparams::regularization.AbstractRegularizationParams, grad::Vector{Float64})
    # @debugprintln("Starting eval_regularization")
    deformation.set_theta!(def, theta)
    return regularization.regularize!(def, regparams, grad)
end
    
"""
    check_if_approx_good(y, kptg, contributions)
Return `true` if point `y` is well approximated by a normal defined by a keypoint `kptg`
and precalculated approximations `contributions`.
"""
function check_if_approx_good(y::Vector{Float64},kptg::AbstractKeypoint,contributions::Contributions)
    # calculate relative squared distance between 'y' and the transformed keypoint
    # Euclidean norm of the difference
    ltvec::Float64 = dist2(y,kptg.pos)
    nn2 = norm2(kptg.normal)
    ltvec /= nn2               # relative distance squared

    h = dot(y-kptg.pos, kptg.normal) / nn2             # project onto normal, in normal units

    #the approximation along the normal is not good,
    # possibly because the normal has not been sampled far enough
    ( good_approx(h,contributions)                
    # or if the angle to the normal is too large (more than acos(1/3) \approx 70 degrees)
    # Ignore points closer than sqrt(2) normals from the landmark
    && !(ltvec > 2 && ltvec > (h * 3)^2) )
end

"""
    check_sampling(kptsf, kptsg, contributions, newdef, relnumbadapprox = 0.1)
Check whether the criterion approximation by normal sampling is sufficiently accurate fir given deformation
or if the normals should be resampled.
# Arguments
* `kptsf<:Vector{AbstractKeyoint}`: keypoints in the reference image
* `kptsg<:Vector{AbstractKeyoint}`: transformed keypoints used to sample the normals
* `contributions::Contributions`: precalculated contributions
* `newdef::AbstractDeformation`: new transformation to be checked.
* `relnumbadapprox`: relative number of keypoints that are allowed to be 'bad' (default 0.1)

If more than 'relnumbadapprox' points `kptsf` deformed by `def` are not well approximated
by an approximation defined by `kptsg` and `contributions`, return `false` otherwise `true`.
"""
function check_sampling(kptsf::Vector{K}, kptsg::Vector{L},
                        contributions::Contributions, newdef::deformation.AbstractDeformation,
                        relnumbadapprox = 0.1) where {K<:AbstractKeypoint,L<:AbstractKeypoint}
    y = zeros(Float64,length(kptsf[1].pos)) # temporary for position check
    numbad::Integer=0
    n=length(kptsf)
    for i=1:n
        # transform the point from the fixed image to 'y' using deformation 'newdef'
        deformation.transform_point_kpt!(newdef, y, kptsf, i)           #fast evaluation 
        # deformation.transform_point!(newdef, kptsf[i].pos, y)         #slow evaluation
        if !check_if_approx_good(y,kptsg[i],contributions)
            numbad+=1
        end
    end
    numbad < relnumbadapprox*n
end

"""
    make_applyAAt(def, kptsf, kptsg)
Create the functions suitable to be used as applyA and applyAt for the admm,
and vector `c`.
"""
function make_applyAAt(def::deformation.AbstractDeformation,
                kptsf::Vector{K},kptsg::Vector{L}) where {K<:AbstractKeypoint,L<:AbstractKeypoint}
    n=length(kptsf)
    d = deformation.copy(def) # to avoid overwriting the parameter
    # nd=length(kptsf[1].pos)
    nx=length(deformation.get_theta(d))
    rn=[ 1. / norm2(kptsg[i].normal) for i=1:n ]
    function applyA(theta)
        Atheta=zeros(Float64,n)
        deformation.set_theta!(d, theta)
        y=zeros(Float64,length(kptsf[1].pos)) # coordinates of the transformed fixed image landmarks
        yt=zeros(Float64,length(kptsf[1].pos)) # coordinates of the transformed fixed image landmarks
        for i=1:n
            # transform the point from the fixed image
            deformation.transform_point_kpt!(d, y, kptsf, i)    #fast evaluation
            # deformation.transform_point!(d, kptsf[i].pos, y)    #slow evaluation
            deformation.transform_initial!(yt, d, kptsf, i)
            for j = 1:length(y)
                y[j] -= yt[j]
            end
            # TODO: optimize more by incorporating rn into the scalar product vector
            # project onto the normal, in normal units
            Atheta[i] = dot(y, kptsg[i].normal) * rn[i]   # project onto normal
        end
        return Atheta
    end # applyA
    # calculate the constant shift
    c=zeros(Float64,n)
    for i=1:n
        # project onto normal and add initial deformation back, note: c= - dot...
        c[i]=-dot(kptsg[i].pos - deformation.transform_initial(d, kptsf, i), kptsg[i].normal) * rn[i]
    end
    # apply transpose
    function applyAt(y)
        x=zeros(Float64,nx)
        for i=1:n
            deformation.add_grad_contrib_kpt!(x, y[i], d, kptsf, kptsg, i)
            # deformation.add_grad_contrib!(x, y[i], kptsg[i].normal, d, kptsf[i].pos) #slow evaluation 
        end
        return x
    end

    return applyA,applyAt,c
end

"""
    testApplyAt(applyA, applyAt, theta0)
Verify whether applyA and applyAt really give a transpose
"""                
function testApplyAt(applyA,applyAt,theta0)
    nx=length(theta0)
    ny=length(applyA(theta0))
    A=zeros(Float64,ny,nx)
    At=zeros(Float64,nx,ny)
    for i=1:nx
        x=zeros(Float64,nx)
        x[i]=1.0
        A[:,i]=applyA(x)
    end
    for i=1:ny
        y=zeros(Float64,ny)
        y[i]=1.0
        At[:,i]=applyAt(y)
    end
    # @show A
    # @show At
    Att=copy(transpose(At))
    @show maximum(abs(A-Att))
    @show sum(abs(A-Att))/length(A)
    @show indmax(abs(A-Att))
end

"""
    make_optim_Di(contributions)
Return a function to evaluate particular contribution, and second one finding the minimum
of a particular contribution plus a quadratic term.
The minimization simply tries all options. Sign needs to be changed.

To be used in the ADMM optimization.

!!! warning

    The function is implemented for `ContributionsSampled` only.
"""
function make_optim_Di(contributions::ContributionsSampled)
    hmax=get_hmax(contributions)
    r=linspace(-hmax,hmax,2hmax+1)
    function eval_Di(i,h)
        -eval_contribution(h,contributions,i)
    end
    function optim_Di(i,w2,w1;opts...)
        function interpol_Di(h,h1,y1,h2,y2)
            # interpolate a function value 'f' between two sampled ones given D_i
            h2h1=h2-h1 # should be equal to 1.
            ((h-h1)*y2+(h2-h)*y1)/h2h1 + w2*h^2+w1*h
        end
        function refine_Di(h1,y1,h2,y2)
            # find a minimum argument of 'f' given values y1=D_i(h1), y2=D_i(h2)
            hopt=0.5/w2*((y1-y2)/(h2-h1)-w1)
            segimgtools.clip(hopt,h1,h2)
        end
        
        #this is possible and elegant, but slower than->
        # evalf= h -> eval_Di(i,h)+w2*h^2+w1*h
        # h2=argmin(evalf,r)   # integer solution to be refined
        #--->THIS
        h2=-Inf ; bestval=Inf
        for x in r
            y=-eval_contribution(x,contributions,i)+w2*x^2+w1*x
            if y<bestval
                bestval=y ; h2=x
            end
        end
        
        # TODO: optimize by testing the derivative first
        y2=-eval_contribution(h2,contributions,i) #it is possible to use eval_Di(i,h2), but it is slower
        do1=h2>-hmax+0.5 # robust test for rint>-hmax
        do3=h2< hmax-0.5 # robust test for rint<hmax
        if  do1
            h1=h2-1.
            y1=-eval_contribution(h1,contributions,i) #it is possible to use eval_Di(i,h1), but it is slower
            h1r=refine_Di(h1,y1,h2,y2)
            if !do3 return h1r end
        end

        if do3
            h3=h2+1.
            y3=-eval_contribution(h3,contributions,i) #it is possible to use eval_Di(i,h3), but it is slower
            h3r=refine_Di(h2,y2,h3,y3)
            if !do1 return h3r end
        end

        # choose the minimum of the two intervals
        y1r=interpol_Di(h1r,h1,y1,h2,y2)
        y3r=interpol_Di(h3r,h2,y2,h3,y3)
        hopt=y1r < y3r ? h1r : h3r
        hstp=1e-1
        # @debugassert(evalf(hopt)<=evalf(hopt+hstp) && evalf(hopt)<=evalf(hopt-hstp))
        return hopt
    end
    return eval_Di,optim_Di
end

"""
    register_fast(f, g, kpts, def, critstate, optimizer_params, regularization_params, opts)
Given two images and the keypoints in the refernce image, register them.
# Arguments
* `f::Images.Image`: reference image.
* `g::Images.Image`: moving image.
* `kpts<:Vector{AbstractKeypoint}`: keypoints from base image.
* `def::AbstractDeformation`: initial deformation.
* `critstate::CriterionState`: defines the criterion used for optimization.
    For optimizers that need only a function value or a derivative any criterion can be used.
    For `OptimizerADMMS`, use only the criterion in a special additive form (see module [admm](@ref))
* `optimizer_params::AbstractOptimizer`: defines the optimizer. For `OptimizerADMMS` only
    deformalions linear wrt parameter vector, and criterion in aditive form can be used.
    For other optimizers use any deformation/criterion.
* `regularization_params::AbstractRegularizationParams`: defines regularization (use `NoneRegularizationParams` for no regularization).
* `opts::Opts`: aditional parameters

The reference image `f` is used only for visualization, if ever.
""" 
function register_fast(f::Image,g::Image,kpts::Vector{K},
                       def::deformation.AbstractDeformation, critstate::CriterionState,
                       optimizer_params::optimizer.AbstractOptimizer,
                       regularization_params::regularization.AbstractRegularizationParams, opts::Opts) where {K<:AbstractKeypoint}
    @defaults opts lbound=deformation.lower_bound(def) ubound=deformation.upper_bound(def) hmax=10 resample = false defaultval=zero(g[1]) maxResample = 20 pwl=false
    @assert(isa(optimizer_params,optimizer.AbstractOptimizer) || isa(optimizer_params,admm.OptimizerADMMS))
    # The current implementation of ADMM needs the deformation to be linear in its arguments
    @assert(!isa(optimizer_params,admm.OptimizerADMMS) || deformation.is_linear(def))
    badApprox = true # true if resampling of the normals is needed
    newdef = deformation.copy(def)
    deformation.set_keypoints!(newdef, kpts)
    # img1kpts=keypoints.create_image_with_keypoints(f, kpts, critstate.k;llen= hmax)
    # img1kpts=keypoints.create_image_with_keypoints(f, kpts;llen= hmax) #rgb
    # ImageView.view(img1kpts,name="with keypoints")
    iternum=1
    if isa(optimizer_params,admm.OptimizerADMMS) # ADMM
        applyRtR = regularization.make_applyRtR(newdef, regularization_params)
    end
    while badApprox && maxResample >= 1
        maxResample -= 1
        # Sampling normals
        theta0=deformation.get_theta(newdef)
        kptsg=transform_keypoints(kpts,newdef)

        if pwl
            contributions=precalculate_contributions_pwl(g,kpts,kptsg,critstate,hmax;defaultval=defaultval)
        else
            contributions=precalculate_contributions(g,kpts,kptsg,critstate,hmax;defaultval=defaultval)
        end
        # optimize
        if isa(optimizer_params,admm.OptimizerADMMS) # ADMM
            applyA,applyAt,c=make_applyAAt(newdef,kpts,kptsg)
            # testApplyAt(applyA,applyAt,theta0)
            # @bp
            eval_Di,optim_Di=make_optim_Di(contributions)
            optx=admm.admmss_optimize(eval_Di,optim_Di,applyA,applyAt,applyRtR,theta0,c;opts=optimizer_params)
        else # standard multidimensional optimization
            optx=optimizer.optimize(optimizer_params,
                                (thetaA,gradA) -> eval_contributions(thetaA,newdef,kpts,kptsg,contributions,gradA),
                                (thetaB,gradB) -> eval_regularization(thetaB, newdef, regularization_params, gradB),
                                theta0,lbound,ubound)
        end
        deformation.set_theta!(newdef, optx) # the resulting transformation of this iteration
        badApprox = resample
        if resample
            badApprox = !check_sampling(kpts, kptsg, contributions, newdef)
            if badApprox
                @debugprintln_with_color(:magenta, "Assumptions not satisfied, resampling needed.")
            end
        end
        # optsv = @options pixelspacing = Images.pixelspacing(f)
        # warped=deformation.transform_image_nn(newdef,g,size(f),optsv)
        # ImageView.view(deformation.overlay(f/maximum(f),warped/maximum(warped)),name="overlay after #$iternum")   #gray/segm.
        # ImageView.view(deformation.overlay(f,warped),name="overlay after #$iternum") #RGB

        iternum+=1
    end # end while
    if maxResample < 1
        @debugprintln_with_color(:red, "Resampling limit reached, the procedure probably does not converge.")
    end
    # optsv = @options pixelspacing = Images.pixelspacing(f) default = f[1]
    # warped=deformation.transform_image_nn(newdef,g,size(f),optsv)
    # maf = maximum(f); mag = maximum(g); maw = maximum(warped); mif = minimum(f); mig = minimum(g); miw = minimum(warped)
    # ImageView.view(deformation.overlay((f - mif)/(maf - mif),(g - mig)/(mag - mig)),name="overlay before")
    # ImageView.view(deformation.overlay((f - mif)/(maf - mif),(warped - miw)/(maw - miw)),name="overlay after $(iternum - 1) iterations")
    @check_used opts
    return newdef
end

"""
    resolve_large_rotation!(g, f, def, k)
Try all -90:90:180 degree rotations, pick the one with the largest MIL criterion.

!!! warning

    The function is implemented for `RigidDeformation2D` only.
"""
function resolve_large_rotation!(g::Image, f::Image, def::deformation.RigidDeformation2D, k)
    defn = deformation.copy(def)
    crit = -Inf
    alpha = 0
    
    for alphat = -90:90:180
        defn.theta[1] = alphat
        deformation.set_theta!(defn, defn.theta)
        g1 = deformation.transform_image_nn(defn, g, size(g), @options)
        critt = mil.calculateMIL(f, g1, k)
        if crit < critt
            alpha = alphat
            crit = critt
        end
    end
    def.maxtheta[1] = def.maxtheta[1] - def.theta[1] + alpha
    def.mintheta[1] = def.mintheta[1] - def.theta[1] + alpha
    def.theta[1] = alpha
    deformation.set_theta!(def, def.theta)
    
    return nothing
end


"""
    reduce_image(f, fac = Int[])
Reduce_image to half the size.
Label images are reduced by majority voting, other images by linear interpolation
"""
function reduce_image(f::Image{LabelType,N}, fac::Vector{Int} = Int[]) where {N}
    downsample_majority(f, fac)
end

function reduce_image(f::Image{T,N}, fac::Vector{Int} = Int[]) where {T,N}
    reduce_image_lin(f, fac)
end

"""
    reduce_image_lin(f, fac = Int[])
Reduce_image to half the size using linear interpolation.
"""
@generated function reduce_image_lin(f::Image{T,N}, fac::Vector{Int} = Int[]) where {T,N}
quote
    if fac == []
        r = get_resample_factor(Images.pixelspacing(f))
    else
        r = fac
    end
    @nexprs $N j->(s_j = Integer(div(size(f, j), r[j]))) # new image size
    s = @ntuple $N s
    g = with_pixelspacing(similar(f, s),Images.pixelspacing(f) .* r)
    identity_deformation=deformation.create_identity([s...],
       $( if :( $N )==2
           quote deformation.AffineDeformation2D end
       else quote deformation.AffineDeformation3D end end ) )
    sigma=ones(Float64,$N)*1.0
    #fs=convert(Image{eltype(f)},Images.imfilter_gaussian(f,sigma))
    fs=convert(Image{eltype(f)},ImageFiltering.imfilter(f,ImageFiltering.Kernel.gaussian(sigma)))
    # print("g=",g,"fs=",fs)
    deformation.transform_image_lin!(identity_deformation,g,fs,@options)
    return g
end
end #quote

"""
    smallest_downscaled_dimension(sg, willbedownscaled)
Find the minimum dimension, which will be downsampled.
"""
function smallest_downscaled_dimension(sg,willbedownscaled)
    s = Inf
    for i = 1:length(willbedownscaled)
        if willbedownscaled[i] > 1 && sg[i] < s
            s = sg[i]
        end
    end
    return s
end
    
"""
    register_fast_multiscale(f, g, kpts, def, nhood, critparams, optimizer_params, regularization_params, opts)
Register two images `f` and `g`  using `register_fast` in a multiscale fashion.
Downsample images until `minsize` is reached, and keypoints until `minKP` is reached.
"""
function register_fast_multiscale(f::Image, g::Image, kpts::AbstractVector{K},def::deformation.AbstractDeformation, nhood::Array{Set{T}}, critparams::CPType,
                                         optimizer_params::optimizer.AbstractOptimizer, regularization_params::regularization.AbstractRegularizationParams, opts::Opts) where {T,CPType<:CriterionParameters,K<:AbstractKeypoint}
    @defaults opts minsize = 128 minKP = typemax(Int) largerotation = true pwl=false
    # TODO: find something more general and flexible to determine the downsampling schedule

    lKP = length(kpts)
    @debugprintln("Register multiscale called with image sizes $(size(f)) $(size(g)) and $lKP keypoints.")
    
    rsf1, rsf2 = segimgtools.get_resample_factor(Images.pixelspacing(f), Images.pixelspacing(g))
    sf = size(f)
    sg = size(g)
    s = min(smallest_downscaled_dimension(sf, rsf1), smallest_downscaled_dimension(sg, rsf2))

    if s <= minsize && lKP <= minKP
        if largerotation
            @debugtime( resolve_large_rotation!(g, f, def, k), "Resolving large rotations:")
        end
        r = deformation.get_theta(def)
        @debugprintln("Register multiscale innermost started. Starting point: ")#$(convert(Array{Float32},r))")
        # initialize criterion
        critstate::CriterionState{CPType}=criterion_init(f,g,critparams)
        dnew= @debugtimeline( register_fast(f,g, kpts, def, critstate, optimizer_params, regularization_params, opts), "Register_fast:")
        r = deformation.get_theta(dnew)
        # @debugprintln_with_color(:green, "Registration result: $(convert(Array{Float32},r))")
    else
        # image is too big, reduce it
        redFac1 = ones(length(size(f)))
        redFac2 = ones(length(size(g)))
        if s > minsize
            redFac1 = rsf1
            redFac2 = rsf2
        end
        # subsample keypoints if there are too many
        subsample_keypoints=true
        if lKP > minKP
            kptsn, nhoodn = subsample_keypoints_equidistant(kpts, nhood, redFac2)
            lKPn = length(kptsn)
        else
            subsample_keypoints=false
        end

        # if there are too few after subsampling, just convert the coordinates
        if !subsample_keypoints || lKPn < 5 || lKPn < minKP / 3
            kptsn::Array{K} = [reduce_keypoint(kpts[i], redFac2) for i = 1:length(kpts)]
            nhoodn = copy(nhood)
        end

        
        if length(kpts) == length(kptsn) && s <= minsize
            @debugprintln("No further subsampling or image reduction possible.")
            d = deformation.copy(def) # Set initial deformation to def and register outside the if
            if largerotation
                @debugtime( resolve_large_rotation!(g, f, def, k), "Resolving large rotations:")
            end
        else
            # coarse level registration
            if maximum(redFac1) > 1
                fn = reduce_image(f, round.(Int, redFac1))
            else
                fn = f
            end
            if maximum(redFac2) > 1
                gn = reduce_image(g, round.(Int, redFac2))
            else
                gn = g
            end
            resample_normals!(fn, kptsn) # resample normals if needed on scale change
            d = register_fast_multiscale(fn, gn, kptsn, def, nhoodn, critparams, optimizer_params, regularization_params, opts)
        end
        # perform local optimization
        # initialize criterion
        warped=deformation.transform_image_nn(d,g,size(f),@options)
        critstate=criterion_init(f,warped,critparams)
        r = deformation.get_theta(d)
        @debugprintln("Local registration started: size(g)=$(size(g)), length(kpts)=$lKP. Starting point: ")#$(convert(Array{Float32},r))")
        #warped=deformation.transform_image_nn(d,g,size(f),@options)
        #ImageView.view(segimgtools.overlay(f,warped),name="overlay before local at scale=$(size(f))") 
        dnew=@debugtimeline( register_fast(f,g, kpts, d, critstate, optimizer_params, regularization_params, opts),"register_fast local:")
        r=deformation.get_theta(dnew)
        # @debugprintln_with_color(:green, "Register_fast_multiscale result: $(convert(Array{Float32},r))")
        # @bp
    end
    # warped=deformation.transform_image_nn(dnew, g, size(g), @options)
    #ImageView.view(segimgtools.overlay(f,warped),name="overlay after at scale=$(size(f))") 
   return dnew
end

"""
    register_fast_multiscaleHB(f, g, kpts, def, nhood, critparams, optimizer_params, regularization_params, Rfac, Bfac, opts, Rlev = 1)
Register two images `f` and `g` using `register_fast` in a multiscale fashion with `HierarchicalDeformation`.
`Rfac` is the vector defining resampling of the image, each element in Rfac defines number of image
down-samplings for the next recursion level, elements of `Bfac` define shift in hierarchical deformation,
`Rfac` and `Bfac` should be the same size, `Rlev` is level of recursion (index into `Rfac` and `Bfac`)

E.g. `Rfac = [0,1,1]` defines 4 levels of image pyramid, where the first 2 are the same,
`Bfac = [1,0,0]` defines 2 level optimization of hierarchical transformation
(2 level `HierarchicalDeformation`should be passed as an argument). when used together the optimization
will be:
1. image size 1/4, deformation level 2
2. image size 1/2, deformation level 2
3. image size 1, deformation level 2
4. image size 1, deformation level 1
"""
function register_fast_multiscaleHB(f::Image, g::Image, kpts::AbstractVector{K},def::deformation.HierarchicalDeformation, nhood::Array{Set{T}}, critparams::CPType,
                                         optimizer_params::optimizer.AbstractOptimizer, regularization_params::regularization.AbstractRegularizationParams, Rfac::Vector{Int}, Bfac::Vector{Int}, opts::Opts, Rlev::Int = 1) where {T,CPType<:CriterionParameters,K<:AbstractKeypoint}
    # TODO: eliminate equivalent_classes, this is not a general thing, put it in the parameters
    @defaults opts minKP = typemax(Int) largerotation = true pwl=false equivalentclasses=Array{Int}(undef,0,0)
    # register_fast_multiscale recursively calls register_fast, in order to predefine schedule.

    Blev = 1
    for i = 1:Rlev-1
        Blev += Bfac[i]
    end
    lKP = length(kpts)
    if Rlev > length(Rfac)
        # last recursion level
        if largerotation
            @debugtime( resolve_large_rotation!(g, f, def, k), "Resolving large rotations:")
        end
        r = deformation.get_theta(def)
        @debugprintln("Register multiscale innermost started. Image sizes: $(size(f)), $(size(g)), transformation level $Blev, kpts = $(length(kpts))")
        # initialize criterion
        critstate::CriterionState{CPType}=criterion_init(f,g,critparams,equivalentclasses)
        deformation.set_level!(def, Blev)
        dnew= @debugtimeline( register_fast(f,g, kpts, def, critstate, optimizer_params, regularization_params, opts), "Register_fast:")
        #r = deformation.get_theta(dnew)
        # @debugprintln_with_color(:green, "Registration result: $(convert(Array{Float32},r))")
    else
        # reduce image
        redFac1 = ones(ndims(f))
        redFac2 = ones(ndims(g))
        pxsp1 = copy(Images.pixelspacing(f))
        pxsp2 = copy(Images.pixelspacing(g))
        for Rft = 1:Rfac[Rlev]  # convert scalar resample factor to vector one
            rf1, rf2 = segimgtools.get_resample_factor(pxsp1, pxsp2)
            redFac1 .*= rf1
            redFac2 .*= rf2
        end

        # subsample keypoints if there are too many
        subsample_keypoints=true
        mm = maximum(max(redFac1,redFac2))
        if lKP > minKP && mm > 1
            kptsn, nhoodn = subsample_keypoints_equidistant(kpts, nhood, redFac2)
            lKPn = length(kptsn)
        else
            subsample_keypoints=false
        end

        # if there are too few after subsampling, just convert the coordinates
        if !subsample_keypoints || lKPn < 5 || lKPn < minKP / 3
            kptsn::Array{K} = [reduce_keypoint(kpts[i], redFac2) for i = 1:length(kpts)]
            nhoodn = copy(nhood)
        end

        if maximum(redFac1) > 1
            fn = reduce_image(f, round(Int, redFac1))
        else
            fn = f
        end
        if maximum(redFac2) > 1
            gn = reduce_image(g, round(Int, redFac2))
        else
            gn = g
        end
        if mm > 1
            resample_normals!(fn, kptsn)
        end
        d = register_fast_multiscaleHB(fn, gn, kptsn, def, nhoodn, critparams, optimizer_params, regularization_params, Rfac, Bfac, opts, Rlev + 1)

        # perform local optimization
        # initialize criterion
        critstate=criterion_init(f,g,critparams,equivalentclasses)
        @debugprintln("Local registration started: Image sizes: $(size(f)), $(size(g)), transformation level $Blev, kpts = $(length(kpts))")
        deformation.set_level!(d, Blev)
        dnew=@debugtimeline( register_fast(f,g, kpts, d, critstate, optimizer_params, regularization_params, opts),"register_fast local:")
        r=deformation.get_theta(dnew)
        # @debugprintln_with_color(:green, "Register_fast_multiscale result: $(convert(Array{Float32},r))")
    end
    # warped=deformation.transform_image_nn(dnew, g, size(g), @options)
    #ImageView.view(segimgtools.overlay(f,warped),name="overlay after at scale=$(size(f))")
    deformation.set_level!(dnew, 0)
    return dnew
end

"""
    segment_register_local(img2, img1, def, critparams, optimizer_params, regularization_params, spedge, opts)
Actual segmentation folowed by registration performed on each scale in `segment_register_fast_multiscale`.
"""
function segment_register_local(img2::Image{T,N}, img1::Image{T,N},
                                     def::deformation.AbstractDeformation, critparams::CPT,
                                     optimizer_params::optimizer.AbstractOptimizer,
                                     regularization_params::regularization.AbstractRegularizationParams,
                                     spedge::Number, opts::Opts) where {T,N,CPT<:CriterionParameters}
# Suport function for segment_register_fast_multiscale() performs segmentation and optimization
    @defaults opts order = 1 regular = 20 minvol = [] ninit = 10
    spvol = spedge ^ N
    #find superpixels and features in img2
    s2l, count2 = @debugtime( SLICsuperpixels.slicSize(img2, spvol, regular, false, UInt16, minvol), "SLIC superpixels img2:")
    s2f = SLICsuperpixels.getMeans(s2l, img2, count2)
    s2f = [ones(1, count2); s2f]
# mt = s2f[2,:];
# m2 = Images.Image(mt[s2l], spatialorder=Images.spatialorder(s2l), pixelspacing=Images.pixelspacing(s2l))
# m2 = m2-minimum(m2)
# ImageView.view(m2/maximum(m2))

    #find superpixels and features in img1
    s1l, count1 = @debugtime( SLICsuperpixels.slicSize(img1, spvol, regular, false, UInt16, minvol), "SLIC superpixels img1:")
    s1f = SLICsuperpixels.getMeans(s1l, img1, count1)
    s1f = [ones(1, count1); s1f]
# println("Superpixel count = $([Float64(count1), Float64(count2)]))")

# mt = s1f[2,:];
# m1 = Images.Image(mt[s1l], spatialorder=Images.spatialorder(s1l), pixelspacing=Images.pixelspacing(s1l)) 
# m1 = m1-minimum(m1)
# ImageView.view(m1/maximum(m1))

    s2lt = @debugtime( deformation.transform_image_nn(def, s2l, size(s1l), @options default = 0 pxspacing = Images.pixelspacing(s1l)), "Image deformation:")
# mt = s2f[2,:];
# m1 = Images.Image(mt[s2lt], spatialorder=Images.spatialorder(s2lt), pixelspacing=Images.pixelspacing(s2lt)) 
# m1 = m1-minimum(m1)
# ImageView.view(m1/maximum(m1))
    distmm = max(spedge * 3., minimum(size(img2)) / 10)
    overlap = segimgtools.segmentation_overlap_array(s1l, s2lt)
    classtwo = softmax.SoftMaxTwo(order, critparams.k, size(s1f,1))    #init classifier
    J = @debugtime( softmax.init_softmaxtwo_random_optimize_repeat!(classtwo, segimgtools.SuperpixelImage[s1l, s2lt], Array{Float64, ndims(s2f)}[s1f, s2f], overlap, distmm, false, ninit, 0.001, 0.001), "SoftMax learning:")
    assig1 = softmax.hard_classify(classtwo.class[1], s1f)
    p1 = Image(assig1[s1l], spatialorder=Images.spatialorder(s1l), pixelspacing=Images.pixelspacing(s1l)) 
    assig2 = softmax.hard_classify(classtwo.class[2], s2f)
    p2 = Image(assig2[s2l], spatialorder=Images.spatialorder(s2l), pixelspacing=Images.pixelspacing(s2l)) 

    kpts, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(p1, spedge, minimum(Images.pixelspacing(p1))),"find_keypoints_and_neighbors:")
# println("Number of keypoints $(length(kpts))")
# ikp = create_image_with_keypoints(p1,s1l,kpts,critparams.k)
# ImageView.view(ikp)
# ImageView.view(p1/maximum(p1))
# ImageView.view(p2/maximum(p2))

    critstate::CriterionState{CPT}=criterion_init(p1, p2, critparams)
    dnew= @debugtimeline( register_fast(p1, p2, kpts, def, critstate, optimizer_params, regularization_params, opts), "Register_fast:")
    return dnew
end

"""
    segment_register_fast_multiscale(img2, img1, def, critparams, optimizer_params, regularization_params, spedge, opts)
Perform joint segmentation and registration if two images. Segmentation using softmax,
registration by calling `register_fast`.
"""
function segment_register_fast_multiscale(
         img2::Image{T,N}, img1::Image{T,N}, def::U, critparams::CPT,
         optimizer_params::optimizer.AbstractOptimizer, regularization_params::regularization.AbstractRegularizationParams,
         spedge::Number, opts::Opts) where {T,N,U<:deformation.AbstractDeformation,CPT<:CriterionParameters}
    @defaults opts minsize = 128

    # find dimensions to downsample (tatget image should be as isotropic as posible for downscale factor 2 and pixel size should be as close as possible among the images)
    redFac1, redFac2 = segimgtools.get_resample_factor(Images.pixelspacing(img1), Images.pixelspacing(img2))

    s = Inf
    s2 = [size(img2)...]
    s1 = [size(img1)...]
    for i = 1:length(redFac1)
        # find minimal size among these selected for downsampling
        if redFac1[i] > 1 && s1[i] < s
            s = s1[i]
        end
        if redFac2[i] > 1 && s2[i] < s
            s = s2[i]
        end
    end
    d = deformation.copy(def)
    @debugprintln("Register multiscale called with image sizes static-$(size(img1)), moving-$(size(img2)).")
    if s <= minsize
        r = deformation.get_theta(def)
        @debugprintln("Segmentation - register multiscale innermost started.")
        dnew = segment_register_local(img2, img1, def, critparams, optimizer_params, regularization_params, spedge, opts)
        r = deformation.get_theta(dnew)
    else
        # image is too big, reduce it
        # TODO - this does not seem to be very robust, does not use a standard interface
        if N ==3
            dsub = deformation.create_identity(s2, deformation.RigidDeformation3D)
        else
            dsub = deformation.create_identity(s2, deformation.RigidDeformation2D)
        end
        s1n = ntuple(k->(floor(Int64, s1[k] ./redFac1[k])), N)
        img1n = Images.copyproperties(img1, Array(eltype(img1), s1n))
        img1n["pixelspacing"] = Images.pixelspacing(img1n) .* redFac1
        deformation.transform_image_lin!(dsub, img1n, img1, @options)

        s2n = ntuple(k->(floor(Int64, s2[k] ./redFac2[k])), N)
        img2n = Images.copyproperties(img2, Array(T, s2n))
        img2n["pixelspacing"] = Images.pixelspacing(img2n) .* redFac2
        deformation.transform_image_lin!(dsub, img2n, img2, @options)
        
        spedgen = floor(Int, spedge * prod(min(redFac1,redFac2))^(1/N))

        d = segment_register_fast_multiscale(img2n, img1n, def, critparams, optimizer_params, regularization_params, spedgen, opts)

        # perform local optimization
        r = deformation.get_theta(d)
        @debugprintln("Local segmentation - registration started: size(img1)=$(size(img1)), size(img2)=$(size(img2))")
        dnew = segment_register_local(img2, img1, d, critparams, optimizer_params, regularization_params, spedge, opts)
        r=deformation.get_theta(dnew)
    end
    return dnew
end



end # module
