"""
Julia reimplementation of SLIC superpixels. This module doesn't return the same
results as the original C++ library, because of the different conversion to Lab and some
minor bug fixes.

Martin Dolejsi, November 2014
"""
module SLICsuperpixels

using Images
using Colors
using Base.Cartesian

#using debugassert
include("debugassert.jl")
switchasserts(false)

export slic, slicSize, getMeans, getNeighbors, slicMeans, slicSizeMeans

"""
    getNhood(dims)
Returns `dims` x `2dims` matrix specifying relative coords of neighbor pixels.
"""
function getNhood(dims)
    if dims == 2                    #this is to mimic original C++ code
        return Int[-1 0 1 0; 0 -1 0 1]
    else
        nhood = zeros(Int, dims, dims*2)
        fi = 1
        for i in 1:dims
            nhood[i, fi] = 1
            nhood[i, fi + 1] = -1
            fi += 2
        end
        return nhood
    end
end

ceil_int(x)=ceil(Int,x)
ceil_int_tuple(t)=map(ceil_int,t)

"""
    ensureConectivity(labels, step, minCSize) 
Merge superpixels smaller than `minCSize` with neighbor superpixel. Relable the 
raw superpixels, so no superpixel consist of more than one connected pomponent.
Returns the relabeled superpixel image and number of superpixels. `step` (initial
distance of superpixel seeds) is used to estimate the size of a temporary buffer.
"""
@generated function ensureConectivity(labels::AbstractArray{T,N}, step, minCSize)  where {T,N}
quote
    imDim = ndims(labels)
    imSize = [size(labels)...]
    istep = ceil_int_tuple(step)
    qsize = 1
    @nexprs $N j->(qsize *= istep[j] + 1)
    qsize *= 2 ^ $N
    
    nlabels = zeros(T, size(labels))
    queue = Array{Int32}(undef,imDim, qsize)
    nlabel = one(T)
    adjlabel = one(T)
    nhood = getNhood(imDim)
    @nloops $N i labels begin
        if (@nref $N nlabels i) == 0
            (@nref $N nlabels i) = nlabel
            @nexprs $N j->(queue[j,1] = i_j)
            # find any nhood label
            for npN in 1:size(nhood,2)
                @nexprs $N j->(n_j = queue[j,1] + nhood[j,npN])
                if (@nall $N j->(n_j >= 1)) && (@nall $N j->(n_j <= imSize[j]))
                    if (@nref $N nlabels n) != 0
                        @inbounds adjlabel = (@nref $N nlabels n)
                        #break
                    end
                end
            end
            count = 1
            qi = 1
            # grow actual point (queue[:,qi])
            while qi <= count
                for npN in 1:size(nhood,2)
                    @nexprs $N j->(n_j = queue[j,qi] + nhood[j,npN])
                    if (@nall $N j->(n_j >= 1)) && (@nall $N j->(n_j <= imSize[j]))
                        if (@nref $N nlabels n) == 0 && (@nref $N labels i) == (@nref $N labels n)
                            count += 1
                            @debugassert(count <= qsize)
                            @nexprs $N j->(queue[j, count] = n_j)
                            @inbounds (@nref $N nlabels n) = nlabel
                        end
                    end
                end
                qi += 1
            end
            # remove component < minCSize
            if count <= minCSize
                for k in 1:count
                    @nexprs $N j->(n_j = queue[j,k])
                    @inbounds (@nref $N nlabels n) = adjlabel
                end
                nlabel -= one(T)
            end
            @debugassert(nlabel < typemax(T))
            nlabel += one(T)
        end
    end
    #    return copyproperties(labels, nlabels), nlabel - one(T)
    return nlabels, nlabel - one(T)
end
end

"""
getMeansCore!(labels, spC, img, counts)
Update mean intensity (color) of each superpixel.
"""
@generated function getMeansCore!(labels::AbstractArray{T,N}, spC::Array{V,2}, img::AbstractArray{U,N}, counts) where {T,N,U,V}
quote
    @nloops $N i labels begin
        @inbounds begin
            @inbounds label = @nref $N labels i
            @inbounds counts[label] += 1
            @inbounds spC[1, label] += @nref $N img i
        end
    end
    return nothing
end
end

#------------------------------------------------------------------------------
@generated function getMeansCore!(labels::AbstractArray{T,N}, spC::Array{V,2}, img::AbstractArray{Lab{U},N}, counts) where {T,N,U,V}
quote
    @nloops $N i labels begin
        @inbounds begin
            tlab = @nref $N img i
            label = @nref $N labels i
            counts[label] += 1
            spC[1, label] += tlab.l
            spC[2, label] += tlab.a
            spC[3, label] += tlab.b
        end
    end
    return nothing
end
end

#------------------------------------------------------------------------------
@generated function getMeansCore!(labels::AbstractArray{T,N}, spC::Array{V,2}, img::AbstractArray{RGB{U},N}, counts) where {T,N,U,V}
quote
    @nloops $N i labels begin
        @inbounds begin
            trgb = @nref $N img i
            label = @nref $N labels i
            counts[label] += 1
            spC[1, label] += trgb.r
            spC[2, label] += trgb.g
            spC[3, label] += trgb.b
        end
    end
    return nothing
end
end

#------------------------------------------------------------------------------
@generated function getMeansCore!(labels::AbstractArray{T,N}, spC::Array{V,2}, img::AbstractArray{Gray{U},N}, counts) where {T,N,U,V}
quote
    @nloops $N i labels begin
        @inbounds begin
            tval = @nref $N img i
            label = @nref $N labels i
            counts[label] += 1
            spC[1, label] += tval.val
        end
    end
    return nothing
end
end

"""
    updateMeansCore!(labels, spP, spC, img, counts)
Generic core of `updateMeans!(...)`.
"""
@generated  function updateMeansCore!(labels::AbstractArray{T,N}, spP::Array{Float64,2}, spC::Array{Float64,2}, img::AbstractArray{U,N}, counts) where {T,N,U}
quote
    getMeansCore!(labels, spC, img, counts)
    @nloops $N i labels begin
        @inbounds label = @nref $N labels i
        @nexprs $N j->(spP[j, label] += i_j)
    end
    return nothing
end
end

"""
    updateMeans!(labels, spP, spC, img)
Update superpixel positions `spP` and superpixel mean intensities (colors) `spC`
to be mean positions and colors of image points assigned to the particular superpixel in `labels`.
"""
function updateMeans!(labels::AbstractArray{T,N}, spP::Array{Float64,2}, spC::Array{Float64,2}, img::AbstractArray{U,N}) where {T,N,U}
    counts = zeros(UInt32, size(spC, 2))
    fill!(spC,0)
    fill!(spP,0)

    updateMeansCore!(labels, spP, spC, img, counts)

    for i = 1:length(counts)
        if counts[i] > 0
            for j = 1: size(spC, 1)
                @inbounds spC[j,i] /= counts[i]
            end
            for j = 1: size(spP, 1)
                @inbounds spP[j,i] /= counts[i]
            end
        end
    end
    return nothing
end

"""
    getColDim(a)
Return number of componnents (chanels) for pixel of given color type.
# Example

```jldoctest
julia> SLICsuperpixels.getColDim(RGB(1,1,1))
3

julia> SLICsuperpixels.getColDim(Gray(1))
1
```
"""
function getColDim(a::RGB{T}) where {T}
    return 3
end

#------------------------------------------------------------------------------
function getColDim(a::Lab{T}) where {T}
    return 3
end

#------------------------------------------------------------------------------
function getColDim(a)
    return 1
end

"""
    getMeans{T,N,U}(labels, img, nLables)
Return vector of mean superpixel intensities (colors).
"""
function getMeans(labels::AbstractArray{T,N}, img::AbstractArray{U,N}, nLables) where {T,N,U}
    # Computes mean intensity in superpixels

    counts = zeros(UInt32, nLables)
    spC = zeros(Float32, getColDim(img[1]), nLables)
    
    getMeansCore!(labels, spC, img, counts)
    broadcast!(/, spC, spC, counts')
    return spC
end

"""
    getDistSq(SeedPoint, ImagePoint)
Return square distance for different color types.
"""
getDistSq(SeedPoint, ImagePoint) = (SeedPoint[1] - ImagePoint) ^ 2

#------------------------------------------------------------------------------
# Computes square distance of color for Gray image
getDistSq(SeedPoint, ImagePoint::Gray{T}) where {T} = 3 * 128 * 128 * (SeedPoint[1] - ImagePoint.val) ^ 2 

#------------------------------------------------------------------------------
function getDistSq(SeedPoint, ImagePoint::RGB{T}) where {T}
    # Computes square distance of color for RGB image

    @inbounds begin
        distSq = (SeedPoint[1] - ImagePoint.r) ^ 2
        distSq += (SeedPoint[2] - ImagePoint.g) ^ 2
        distSq += (SeedPoint[3] - ImagePoint.b) ^ 2
    end
    distSq *= 128 * 128 
    return distSq
end

#------------------------------------------------------------------------------
function getDistSq(SeedPoint, ImagePoint::Lab{T}) where {T}
    # Computes square distance of color for Lab image

    @inbounds begin
        distSq = (SeedPoint[1] - ImagePoint.l) ^ 2
        distSq += (SeedPoint[2] - ImagePoint.a) ^ 2
        distSq += (SeedPoint[3] - ImagePoint.b) ^ 2
    end
    return distSq
end

"""
    assignPixels!(dist, labels, spP, spC, img, step, imPxSize, invwt)
For each superpixel seed `spP[spN]` finds the closest points in `spP[spN] - roundStep : spP[spN] + roundStep`
and set its label to `spN`.
"""
@generated  function assignPixels!(dist::Array{T,N}, labels, spP::Array{Float64,2}, spC::Array{Float64,2}, img, step, imPxSize, invwt) where {T,N}
quote
    spP1 = zeros(Float64, $N)
    spPlb = zeros(Int64, $N)
    spPub = zeros(Int64, $N)
    colSize = size(spC)
    spC0 = zeros(eltype(spC), colSize[1])
    a=0
    # for each superpixel
    for spN in 1:colSize[2]
        @nexprs $N j->(spP1[j] = spP[j, spN])
        for j = 1:colSize[1]
            spC0[j] = spC[j, spN]
        end

        # compute the limits
        @nexprs $N j->(spPlb[j] = floor(Int64, spP1[j] - step[j]))
        @nexprs $N j->(spPlb[j] = max(1, spPlb[j]))
        @nexprs $N j->(spPub[j] = ceil(Int64, spP1[j] + step[j]))
        @nexprs $N j->(spPub[j] = min(size(dist, j), spPub[j]))

        @nloops $N i d->(spPlb[d]:spPub[d]) begin
            # color distance
            @inbounds adist = getDistSq(spC0, @nref $N img i)
            # add spatial distance
            @nexprs $N j->(adist += invwt * ((spP1[j] - i_j) * imPxSize[j]) ^ 2)
            if adist < @nref $N dist i
                @inbounds begin
                    (@nref $N dist i) = adist
                    (@nref $N labels i) = spN
                end
            end
        end
    end
    return nothing
end
end

"""
    iterate!(img, spP, spC, step, imPxSize, invwt, tData)
The main execution function. Do all the iterations of k-means and return raw superpixel label image.
"""
function iterate!(img, spP::Array{Float64,2}, spC::Array{Float64,2}, step, imPxSize, invwt, tData::DataType)
    # Perform the restricted k-means iteration and returs labeled image

    imSize = size(img)
    spCnt = size(spP, 2)
    if tData == Any
        tData = UInt8
        if (spCnt > 2 ^ 32 - 1)
            tData = UInt64
        elseif (spCnt > 2 ^ 16 - 1)
        elseif (spCnt > 2 ^ 8 - 1)
            tData = UInt16
        end
    end
    @debugassert((tData == UInt8 && (spCnt <= 2 ^ 8 - 1)) || (tData == UInt16 && (spCnt <= 2 ^ 16 - 1)) || (tData == UInt32 && (spCnt <= 2 ^ 32 - 1)) || (tData == UInt64 && (spCnt <= 2. ^ 64 - 1)))
    #imSP = copyproperties(img, Array(tData, imSize))
    #imSP["colorspace"] = "Gray"
    imSP=Array{tData}(undef,imSize)
    distance = Array{eltype(spC)}(undef,imSize)
    for iterN in 1:10
        fill!(distance, floatmax(eltype(distance)))
        assignPixels!(distance, imSP, spP, spC, img, step, imPxSize, invwt)
        updateMeans!(imSP, spP, spC, img)
    end
    return imSP
end

"""
    initSPColorCore!(img, outarr, spP)
Compute the initial mean superpixel intensities (colors) for different color spaces.
"""
@generated function initSPColorCore!(img::Array{RGB{T},N}, outarr::Array{Float64,2}, spP::Array{Int,2}) where {T,N}
quote
    @inbounds begin
        for i in 1:size(spP, 2)
            trgb = (@nref $N img j->(spP[j, i]))
            outarr[1,i] = trgb.r
            outarr[2,i] = trgb.g
            outarr[3,i] = trgb.b
        end
    end
end
end

#------------------------------------------------------------------------------
@generated function initSPColorCore!(img::Array{Lab{T},N}, outarr::Array{Float64,2}, spP::Array{Int,2}) where {T,N}
quote
    @inbounds begin
        for i in 1:size(spP, 2)
            tlab = (@nref $N img j->(spP[j, i]))
            outarr[1,i] = tlab.l
            outarr[2,i] = tlab.a
            outarr[3,i] = tlab.b
        end
    end
end
end

#------------------------------------------------------------------------------
@generated function initSPColorCore!(img::Array{Gray{T},N}, outarr::Array{Float64,2}, spP::Array{Int,2}) where {T,N}
quote
    @inbounds begin
        for i in 1:size(spP, 2)
            outarr[1,i] = (@nref $N img j->(spP[j, i]))
        end
    end
end
end

#------------------------------------------------------------------------------
@generated function initSPColorCore!(img::Array{T,N}, outarr::Array{Float64,2}, spP::Array{Int,2}) where {T,N}
quote
    @inbounds begin
        for i in 1:size(spP, 2)
            outarr[1, i] = (@nref $N img j->(spP[j, i]))
        end
    end
end
end

"""
    initSPColor(img, spP)
Initializes superpixel mean intensities (colors).
"""
function initSPColor(img::Array{T,N}, spP::Array{Float64,2}) where {T,N}
    spPi = round.(Int, spP)
    outarr = Array{Float64}(undef,getColDim(img[1]), size(spPi, 2))
    initSPColorCore!(img, outarr, spPi)
    return outarr
end

"""
    initSPPosition(step, imSize, imDims)
Initialize the superpixel mean positions.
"""
function initSPPosition(step, imSize, imDims)
    s2 = step ./ 2. .- .5
    pc = [collect(Float64, s2[i]:step[i]:imSize[i]) for i = 1:imDims] #initial center coordinates can be off grid
    eachRp = 1
    seqRp = 1.
    cnt = Array{Int64}(undef,imDims)
    for i in 1:imDims
        cnt[i] = length(pc[i])
        seqRp *= cnt[i]
    end

    seeds = Array{Float64}(undef,imDims, round(Int, seqRp))
    for dimN in 1:imDims
        inx = 1;
        seqRp /= cnt[dimN]
        for seqN in 1:seqRp
            for arrN in 1:size(pc[dimN],1)
                for eachN in 1:eachRp
                    seeds[dimN,inx] = pc[dimN][arrN]
                    inx += 1
                end
            end
        end
        eachRp *= cnt[dimN]
    end
    return round.(seeds)
end



"""
    slicExe{T,N}(img, countSP, compact, minsize, outtype)
Returns the image containing superpixel labels. Actual entry point for the method
"""
function slicExe(img::AbstractArray{T,N}, countSP, compact, minsize, outtype::DataType) where {T,N}
    imSize = [size(img)...]
    imDims = ndims(img)
    imPxSize = pixelspacing(img)
    invwt = round((prod(imSize .* imPxSize) / countSP) ^ (1 / imDims))
    step = invwt ./ imPxSize
    invwt = (compact / round((prod(imSize .* imPxSize / maximum(imPxSize)) / countSP) ^ (1 / imDims))) ^ 2
    
    # Initialize
    spP = initSPPosition(step, imSize, imDims)
    spC = initSPColor(img, spP)
    
    # Do the k-means
    imSP = iterate!(img, spP, spC, step, imPxSize, invwt, outtype)
    
    # Remove small superpixels
    if minsize == []
        return ensureConectivity(imSP, step, prod(step) / 4)
    else
        return ensureConectivity(imSP, step, minsize)
    end
end

"""
    slic(img, countSP, compact, bLab = true, outtype = Any, minsize = [])
Return superpixel label image (each pixel labeled with superpixel number) and actual
number of the superpixels.

# Arguments
* `img::AbstractArray`: input image
* `countSP`: desired number of the superpixels. Note that there will be slightly less superpixels in the output.
* `compact`: regularization high compactnes will produce square superpixels try to start with `compact = 10`
* `bLab::Bool`: set `false` if ihe superpixels should not be computed in Lab colorspace.
* `outtype`: inner type of superpixel image. If `Any` the smallest type that can hold desired number of superpixels is selected.
* `minsize`: mimimal size of superpixel in pixels, smaller superpixels will be merged with neighbors. If `minsize == []` it is set to 1/4 of the initial superpixel size
"""
function slic(img::AbstractArray, countSP, compact, bLab::Bool = true, outtype::DataType = Any, minsize = [])
    # Returns the image containing superpixel labels
    
    # It is a bit tricky to find, if the image is grayscale
    #if eltype(img) ==ColorTypes.Gray{FixedPointNumbers.N0f8}
    #    print(typeof(img))
    #    img255 = reinterpret(UInt8,img)
    #    return slicExe(img255, countSP, compact, minsize, outtype)
    #end
    if eltype(img) == Gray || eltype(img) == Int16 || issubtype(eltype(img),Gray)
        img=map(Float32,img)
        img255 = img - minimum(img)
        img255 /= maximum(img255) / 255
        return slicExe(img255, countSP, compact, minsize, outtype)
    end
    if eltype(img) == UInt16
        img255 = map(Float32, img) / maximum(img) * 255
        return slicExe(img255, countSP, compact, minsize, outtype)
    end

    if bLab
        imgLab = convert(AbstractArray{Lab}, map(Float32, img)) #Python wraper uses bgr image
        return slicExe(imgLab, countSP, compact, minsize, outtype)
    else
        return slicExe(img, countSP, compact, minsize, outtype)
    end
end

#------------------------------------------------------------------------------
function slic(img::AbstractArray{Lab{T},N}, countSP, compact, bLab::Bool = true, outtype::DataType = Any, minsize = []) where {T,N}
    # Returns the image containing superpixel labels 

    return slicExe(float32.(img), countSP, compact, minsize, outtype)
end

#------------------------------------------------------------------------------
function slic(img::AbstractArray{RGB{T},N}, countSP, compact, bLab::Bool = true, outtype::DataType = Any, minsize = []) where {T,N}
    # Returns the image containing superpixel labels 

    if bLab
        imgLab = convert(Array{Lab}, float32.(img)) #Python wraper uses bgr image
        return slicExe(imgLab, countSP, compact, minsize, outtype)
    else
        return slicExe(img, countSP, compact, minsize, outtype)
    end
end

"""
    slicSize(img, sizeSP, compact, bLab = true, outtype = Any, minsize = [])
Returns the same as [`slic`](@ref), but the size (area or volume in native units e.g. mm) should be provided
instead of the number of the superpixels.

"""
function slicSize(img, sizeSP, compact, bLab::Bool = true, outtype::DataType = Any, minsize = [])
    # Returns the image containing superpixel labels
    
    return slic(img, round(prod([size(img)...] .* pixelspacing(img)) / sizeSP), compact, bLab, outtype, minsize)
end

"""
    slicMeans(img, sizeSP, compact, bLab = true, outtype = Any, minsize = [])
Returns the same as [`slic`](@ref), plus the vector of mean intensity values for each superpixel.
"""
function slicMeans(img::AbstractArray, countSP, compact, bLab::Bool = true, outtype::DataType = Any, minsize = [])
    slabels, count = SLICsuperpixels.slic(img, countSP, compact, bLab, outtype)
    sfeatures = SLICsuperpixels.getMeans(slabels, img, count)
    return slabels, sfeatures, count
end

"""
    slicSizeMeans(img, sizeSP, compact, bLab = true, outtype = Any, minsize = [])
Returns the same as `slicSize`, plus the vector of mean intensity values for each superpixel.
"""
function slicSizeMeans(img::AbstractArray, sizeSP, compact, bLab::Bool = true, outtype::DataType = Any, minsize = [])
    slabels, count = SLICsuperpixels.slicSize(img, sizeSP, compact, bLab, outtype)
    sfeatures = SLICsuperpixels.getMeans(slabels, img, count)
    return slabels, sfeatures, count
end

"""
    getNeighbors(imgSP, count)
Returns `nhood::Array`. Each `nhood[i]` contains `Set` of all the neighbor superpixels.
"""
@generated function getNeighbors(imgSP::AbstractArray{T,N}, count) where {T,N}
    # Returns neighbours for each supervoxel
quote
    nhood = Set{T}[Set{T}() for i=1:count]
    @nloops $N i d->(1:size(imgSP, d) - 1) begin
        for j = 1:$N
            shift = zeros(T,$N)
            shift[j] = 1
            @nexprs $N k->(np_k = i_k + shift[k])
            push!(nhood[(@nref $N imgSP i)],(@nref $N imgSP np))
            push!(nhood[(@nref $N imgSP np)],(@nref $N imgSP i))
        end
    end
    return nhood
end
end

end #module
