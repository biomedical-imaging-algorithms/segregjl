"""
Image similarity criterion--mutual information (MIL).

Represent criteria ``\\sum_i \\phi(f_i,g_i)``,

where ``\\phi(f_i,g_i) = p(f_i,g_i)log\\frac{p(f_i,g_i)}{p(f_i)p(g_i)}``

``f_i``, ``g_i`` are pixel values.
"""
module mil

using Base.Cartesian
import ..segimgtools
import Images
using ..criterion

export MILState, MILParameters
export calculateMIL, entropy

include("debugassert.jl")
switchasserts(true)

const Image = segimgtools.Image
const Histogram = Matrix{Int64}

"""
    entropy{T}(p)
Calculate an entropy from a 1D or 2D histogram.
"""
function entropy(p::Array{T}) where T
    sump=sum(p)
    H=0.
    for i=1:length(p)
        if p[i]>zero(T)
            pp=p[i]/sump
            H+=pp*log(pp)
        end
    end
    return -H
end

"""
    joint_histogram(f, g, k)
Given two images of the same size containing integers 0 ``\\ldots`` k, calculate
their joint histogram `k` ``\\times`` `k`. The value `0` means 'unknown' and such data points
are ignored.
"""
function joint_histogram(f::AbstractArray{T1,N},g::AbstractArray{T2,N},k::Integer) where {T1<:Integer,T2<:Integer,N}
    @debugassert(size(f)==size(g))
    @assert(k>0 && k<256)
    n=length(f) # number of pixels
    p=zeros(Int64,k,k) # JK: corrected "ones" to "zeros"
    for i=1:n
        cf=f[i] ; cg=g[i]
        if (cf>0) && (cg>0)
            @debugassert(cf>=1) ; @debugassert(cf<=k)
            @debugassert(cg>=1) ; @debugassert(cg<=k)
            p[cf,cg]+=1
        end
    end
    return p::Histogram
end

"""
    jh_ds_core!(p, f, g, sf, sg, pxspf, pxspg)
Generic part of `joint_histogram`, returns the estimate of histogram for
image with different sizes.
"""
@generated function jh_ds_core!(p, f::Image{T,N}, g::Image{T,N}, sf, sg, pxspf, pxspg) where {T,N}
    quote
    @nloops $N xg d->(1:sg[d]) begin
        @nexprs $N d->(a_d = (xg_d - 0.5) * pxspg[d] / pxspf[d] + 0.5)
        @nexprs $N d->(xf_d = round(Int,a_d))
        if (@nall $N j->(xf_j >= 1)) && (@nall $N j->(xf_j <= sf[j]))
            vf = (@nref $N f xf)
            vg = (@nref $N g xg)
            if vf > 0 && vg > 0
                p[vf,vg] += 1
            end
        end
    end
    return nothing
  end
end

function joint_histogram(f::segimgtools.LabelImage,g::segimgtools.LabelImage,k::Integer)
    # given two images of the same size containing integers 0..k, calculate
    # their joint histogram k x k. The value '0' means 'unknown' and such data points
    # are ignored.
    @assert(k>0 && k<256)
    n=length(f) # number of pixels
    p=zeros(Int64,k,k)
    sf = [size(f)...]
    pxspf = Images.pixelspacing(f)
    sg = [size(g)...]
    pxspg = Images.pixelspacing(g)
    if (sg != sf) || (minimum(min(pxspf, pxspg) ./ max(pxspf, pxspg)) < .9)
        jh_ds_core!(p, f, g, sf, sg, pxspf, pxspg)
    else
        for i=1:n
            cf=f[i] ; cg=g[i]
            if (cf>0) && (cg>0)
                @debugassert(cf>=1) ; @debugassert(cf<=k)
                @debugassert(cg>=1) ; @debugassert(cg<=k)
                p[cf,cg]+=1
            end
        end
    end
    return p::Histogram
end

"""
    calculateMIL(f, g, k)
Given two discrete N dimensional images `f`, `g` with values 1 ``\\ldots`` `k`
calculate the Mutual information on labels (MIL). Images `f`, `g` can also
contain a special value '0' meaning "unknown class"
"""
function calculateMIL(f,g,k::Integer)

    p=joint_histogram(f,g,k)
    # calculate mutual information
    return entropy(sum(p,dims=2))+entropy(sum(p,dims=1))-entropy(p)
end

const epsilon=eps(1.0) # to avoid logarithm of zero

"""
    MIL_weights(f, g, k, clabels = Array(Int, 0, 0))
Given two discrete images with values 0 ``\\ldots`` `k`
calculate the MIL contribution weights

``w(f_i,g_i)=log(P(f_i,g_i)/(P(f_i)*P(g_i)))``

where P(f_i,g_i) is the global joint class probality matrix.
"""
function MIL_weights(f::segimgtools.LabelImage,g::segimgtools.LabelImage,k::Int,clabels = Array{Int}(undef, 0, 0)::Array{Int, 2})
    if clabels == Array{Int}(undef,0, 0)
        h=mil.joint_histogram(f,g,k)
    else
        h = ones(Float64, k, k)
        for i=1:size(clabels, 2)
            h[clabels[1,i], clabels[2,i]] = clabels[3,i]
        end
    end
    totcnt=sum(h)
    pf=(sum(h,dims=2) .+ epsilon)/totcnt # marginal probability, size (3,1)
    pg=(sum(h,dims=1) .+ epsilon)/totcnt # size (1,3)
    pfg=(h .+ epsilon) ./ totcnt  # avoid logarithm of zero
    w=log.( pfg ./ (pf*pg) ) # elementwise division
    return w::Matrix{Float64}
end

#-------------- implement the Criterion protocol
"""
Parameter of the MIL criterion is the number of classes 1>`k`<256.
"""
struct MILParameters <: CriterionParameters
    k::Int
    function MILParameters(k::Int)
        @assert(k>1)
        @assert(k<256)
        new(k)
    end
end

"""
Weight matrix `w` and number of classes `k` are `MILstate`.
"""
struct MILState <: CriterionState{MILParameters}
    k::Int # we could point to the MILParameters instead
    w::Matrix{Float64} # matrix calculated by MIL_weights
end

"""
    criterion_init(f, g, p)
Initialize the criterion state given two images `f`, `g` and parameters `p`.
"""
function criterion.criterion_init(f::segimgtools.LabelImage, g::segimgtools.LabelImage, p::MILParameters, clabels = Array{Int}(undef,0, 0)::Array{Int, 2})
        MILState(p.k,MIL_weights(f,g,p.k,clabels))
end

"""
    criterion_eval(s, pf, pg)
Return criterion value ``\\phi(f_i,g_i)`` for two pixels `pf`, `pg` and criterion state `s`.
"""
function criterion.criterion_eval(s::MILState, pf::segimgtools.LabelType, pg::segimgtools.LabelType)
    # @debugassert(pf>=1 && pf<=s.k)
    # @debugassert(pg>=1 && pg<=s.k)
    # TODO: add @inbounds when it seems safe
    (pf > 0 && pg > 0 ) ? s.w[pf,pg] : 0.
end
#-------------- end criterion protocol


#--------------=for segmentation purposes=------------------
"""
    assemble_probability_matrix(ncls, spoverlap, z)
Given soft clasifications z1, z2 and superpixels overlaps `overlap`
creates probability cross-matrix for MIL comutation.
"""
function assemble_probability_matrix(ncls, spoverlap, z)
    P = zeros(ncls, ncls)
    # it is faster to normalize after
    totpix = 0
    for i = 1:size(spoverlap ,1)
        for j = 1:ncls
            for k = 1:ncls
                P[j,k] += spoverlap[i, 3] * z[1][spoverlap[i, 1], j] * z[2][spoverlap[i, 2], k]
            end
        end
        totpix += spoverlap[i, 3]
    end
    
    @debugassert(totpix > 0)
    P = P / totpix
    return totpix, P
end

"""
    calculate_MIL_or_grad!(bmil, grad, P)
`P` is the cross-probability matrix if `bmil == false` or `grad == []` do not compute it.
"""
function calculate_MIL_or_grad!(bmil::Bool, grad::Array, P::Array{Float64,2})
    # @debugassert(abs(sum(P)-1.) < 1e-3)
    eps = 1e-6
    Ps = size(P)
    Pl = length(P)
    Pi = zeros(Float64, Ps[1], 1)
    Pj = zeros(Float64, 1, Ps[2])
    
    for i = 1:Ps[2]
        for j = 1:Ps[1]
            Pi[j] += P[j, i] + eps / Pl
            Pj[i] += P[j, i] + eps / Pl
        end
    end
    
    logP = Array{Float64}(default, Ps)
    BLAS.gemm!('N', 'N', 1., Pi, Pj, 0., logP)    # matrix multip Pi * Pj
    for i = 1:Pl
        logP[i] = log((P[i] + eps / Pl) / logP[i])
    end
    segimgtools.nan_to_num!(logP)

    mil=0.
    if bmil == true
        for i = 1:Pl
            mil += P[i] * logP[i]
        end
    end
    if grad != []
        grad[:] = logP - 1
    end
    return mil
end

"""
    mutual_inf_gradient!(ncls, spoverlap, z, sfeatures, P, gradP, grad, totpix, order)
Helper function for mutual_inf. grad is modified in place.
"""
function mutual_inf_gradient!(ncls::Int, spoverlap::Array{Int,2}, z::Array{Array{U,2}, 1}, sfeatures::Array{Array{T,2}, 1}, P, gradP, grad, totpix::Int, order::Int) where {T,U}
    fdim = size(sfeatures[1], 1)
    v1 = Array{T}(default, (segimgtools.kernelize_size(fdim, order)))
    v2 = Array{T}(default, (segimgtools.kernelize_size(fdim, order)))

    sf = Array{T}(default, size(sfeatures[1], 1))
    z1q1 = Array{T}(default, size(z[1], 2))
    z2q2 = Array{T}(default, size(z[2], 2))

    @inbounds for i = 1:size(spoverlap ,1)
        for j = 1:length(sf)
            sf[j] = sfeatures[1][j, spoverlap[i, 1]]
        end
        segimgtools.kernelize!(v1, sf, order)
        for j = 1:length(sf)
            sf[j] = sfeatures[2][j, spoverlap[i, 2]]
        end
        segimgtools.kernelize!(v2, sf, order)

        for j = 1:length(z1q1)
            z1q1[j] = z[1][spoverlap[i, 1], j]
            z2q2[j] = z[2][spoverlap[i, 2], j]
        end

        wtt = spoverlap[i, 3] / totpix
        for k = 1:ncls
            for l = 1:ncls
                # for the first classifier parameters
                wt = wtt * gradP[k,l]
                w = wt * z2q2[l]
                for kk = 1:ncls
                    ww = w * z1q1[k] * ((k == kk) ? 1 - z1q1[kk] : - z1q1[kk])
                    for j = 1:fdim
                        grad[kk, j, 1] += ww * v1[j]
                    end
                end
                # for the second classifier parameters
                w = wt * z1q1[k]
                for ll = 1:ncls
                    ww = w * z2q2[l] * ((l==ll) ? 1 - z2q2[ll] : - z2q2[ll])
                    for j = 1:fdim
                        grad[ll, j, 2] += ww * v2[j]
                    end
                end
            end
        end
    end
    return nothing
end

                        
end # module
