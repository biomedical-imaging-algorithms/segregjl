"""
Registration using linear programming

Jan Kybic, 2016-2017
"""
module linprog_register

import Images
import MathProgBase                  # for linear programming
import GLPK
#import GLPKMathProgInterface         # could be replaced by another solver
import Gurobi
using JuMP
import ImageView

using ..keypoints
using ..finereg
import ..deformation
using ..criterion # exports criterion_eval, criterion_init
import ..ssd
using ..segimgtools
using ..Options
#using MicroLogging
using LinearAlgebra
using Logging

export register_fast_linprog_multiscale, register_fast_linprog

include("debugprint.jl")
switchprinting(true)
include("debugassert.jl")
switchasserts(true)

#configure_logging(min_level=:debug)




## """
##     linprog_optimize_hmax(def,kptsf,kptsg,contribs,theta_0)

## find the deformation parameters using linear programming
##         def - deformation
##         kptsf - keypoints in image f
##         kptsg - keypoints in image g
##         contribs - criterion contributions calculated using `precalculate_contributions_pwl`
##         theta_0 - initial value of the deformation function

## returns the optimum value for 'theta'

## it is further assumed that `set_keypoints!(def,kpts)` was called
## the keypoints kptsg were already deformed with theta_0

## This version limits the maximum displacement at keypoints to `hmax`. Not fully tested.
## """
## function linprog_optimize_hmax{K<:AbstractKeypoint}(def::deformation.AbstractDeformation,
##             kptsf::Vector{K},kptsg::Vector{SimpleKeypoint},
##             contribs::finereg.ContributionsPiecewiseLinear,
##             theta_0::Vector{Float64})

## @assert(deformation.is_linear(def))
## hmax=finereg.get_hmax(contribs) # maximum allowed displacement
## ntheta=length(theta_0)
## nkpts=length(kptsf)
## dim=length(kptsf[1].pos) # dimension
## # assemble the linear program in the form    min c^T x     such that Ax <= b
## #the linear program to be solved has unknowns x=[D_i \theta_i], where D_i is the
## #criterion contribution  at keypoint 'i'.
## n=nkpts+ntheta  # number of unknowns
## m=4nkpts        # number of constraints
## a=spzeros(Float64,m,n) # sparse matrix
## b=[ zeros(Float64,2nkpts) ; (-hmax)*ones(Float64,2nkpts) ] # TODO: optimize for speed
## c=[ ones(Float64,nkpts) ; zeros(Float64,ntheta) ]


## y=zeros(Float64,length(kptsf[1].pos)) # temporary vector
## dim=length(kptsf[1].pos) # dimension of the space
## # go over all keypoints to assemble the matrices
## for i=1:nkpts
##     # first calculate \phi_0(x_i)-T_0(x_i)
##     deformation.transform_initial!(y,def,kptsf[i].pos)
##     broadcast!(-,y,y,kptsg[i].pos) #subtract y-=kptsg[i].pos but in place
##     # take a dot product with \tilde{m}_i
##     mtilde=kptsg[i].normal / norm2(kptsg[i].normal)
##     q=dot(y,mtilde) # transformed and rescaled normal
##     ci=contribs[i]

##     qmxi=q-ci.x0
##     uplus=ci.uplus ; uminus=ci.uminus
##     b[2i-1]=qmxi*uplus   # RHS of the constraints
##     b[2i]= -qmxi*uminus
##     b[2nkpts+2i-1]-=q
##     b[2nkpts+2i]+=q
##     a[2i-1,i]=1.0
##     a[2i,i]=1.0
##     # go over all basis functions for this keypoint
##     for (j,val) in deformation.coefficients_iterator(def,kptsf[i].pos)
##         for d=1:dim # dimension
##             jj=nkpts+j*dim+d
##             mval=val*mtilde[d]
##             a[2i-1,jj]=-uplus*mval
##             a[2i,jj]=+uminus*mval
##             a[2nkpts+2i-1,jj]=mval
##             a[2nkpts+2i,jj]=-mval
##         end
##     end
## end # loop for i
## #println("b=",b)
## #println("c=",c)
## #println("a=",full(a))
## #solver=GLPKMathProgInterface.GLPKSolverLP(method=:Simplex, presolve=false)
## solver=GLPKMathProgInterface.GLPKSolverLP(method=:InteriorPoint)
## lpsol=@debugtime(MathProgBase.linprog(c,a,'>',b,-Inf,Inf,solver),"Solving linear program:")
## if lpsol.status!=:Optimal
##     error("Linear program solver returned $(lpsol.status)")
## end
## xopt=lpsol.sol[nkpts+1:end]
## #println("xopt=",xopt)
## return xopt # return the new theta
## end # end of linprog_optimize_hmax


## """
##     linprog_optimize(def,kptsf,kptsg,contribs,theta_0,[lambda])

## find the deformation parameters using linear programming
##         def - deformation
##         kptsf - keypoints in image f
##         kptsg - keypoints in image g
##         contribs - criterion contributions calculated using `precalculate_contributions_pwl`
##         theta_0 - initial value of the deformation function
##         lambda - regularization weight, it can be "nothing", a scalar,
##                  or a vector of the same size as the deformation parameters `theta`

## returns the optimum value for 'theta', minimizing

## \sum_i D_i + \sum_j \lambda_j \theta_j

## it is further assumed that `set_keypoints!(def,kpts)` was called
## the keypoints kptsg were already deformed with theta_0

## No explicit limit on the maximum displacement
## """
## function linprog_optimize{K<:AbstractKeypoint}(def::deformation.AbstractDeformation,
##             kptsf::Vector{K},kptsg::Vector{SimpleKeypoint},
##             contribs::finereg.ContributionsPiecewiseLinear,
##             theta_0::Vector{Float64};
##             lambda=nothing)

## @assert(deformation.is_linear(def))
## ntheta=length(theta_0)
## nkpts=length(kptsf)
## dim=length(kptsf[1].pos) # dimension
## # assemble the linear program in the form    min c^T x     such that Ax >= b
## #the linear program to be solved has unknowns x=[D_i \theta_i z_i], where D_i is the
## #criterion contribution  at keypoint 'i' and z_i=abs \theta_i  (if regularization is needed)
## n=nkpts+ntheta+ntheta*(lambda!=nothing)  # number of unknowns
## m=2nkpts+2ntheta*(lambda!=nothing)        # number of constraints
## a=spzeros(Float64,m,n) # sparse matrix
## b=zeros(Float64,m)
## if lambda==nothing
##     c=[ ones(Float64,nkpts) ; zeros(Float64,ntheta) ]
## elseif isa(lambda,Array)
##     @debugassert(length(size(lambda))==1 && size(lambda)==ntheta && isa(lambda,Array{Float64}))
##     c= [ ones(Float64,nkpts) ; zeros(Float64,ntheta) ; lambda ]
## else
##     @debugassert(isa(lambda,Number))
##     c= [ ones(Float64,nkpts) ; zeros(Float64,ntheta) ; ones(Float64,ntheta)*lambda ]
## end

## if lambda==nothing
##     lbnd=[ zeros(Float64,nkpts) ; -Inf*ones(Float64,ntheta) ]
## else
##     lbnd=[ zeros(Float64,nkpts) ; -Inf*ones(Float64,ntheta) ; zeros(Float64,ntheta)    ]
## end
    
## y=zeros(Float64,length(kptsf[1].pos)) # temporary vector
## dim=length(kptsf[1].pos) # dimension of the space
## # go over all keypoints to assemble the matrices
## #println("dim=$dim nkpts=$nkpts prodbeta=$(size(def.prodbeta)) x0=", contribs[1].x0, " uplus=", contribs[1].uplus, " uminus=",contribs[1].uminus, " u0=",contribs[1].u0)
## for i=1:nkpts
##     # first calculate \phi_0(x_i)-T_0(x_i)
##     deformation.transform_initial!(y,def,kptsf[i].pos)
##     broadcast!(-,y,y,kptsg[i].pos) #subtract y-=kptsg[i].pos but in place
##     # take a dot product with \tilde{m}_i
##     mtilde=kptsg[i].normal / norm2(kptsg[i].normal)
##     q=dot(y,mtilde) # transformed and rescaled normal
##     ci=contribs[i]

##     qmxi=q-ci.x0
##     uplus=ci.uplus ; uminus=ci.uminus
##     b[2i-1]=qmxi*uplus   # RHS of the constraints
##     b[2i]= -qmxi*uminus
##     a[2i-1,i]=1.0
##     a[2i,i]=1.0
##     # go over all basis functions for this keypoint
##     # TODO: refactor so that coefficients_iterator_keypoint does not need kptsf
##     # for (j,val) in deformation.coefficients_iterator(def,kptsf[i].pos)
## #    println("i=$i")
##     for (j,val) in deformation.coefficients_iterator_keypoint(def,i,kptsf)
## #        println("   j=$j val=$val")
##         for d=1:dim # dimension
##             jj=nkpts+j*dim+d
##             mval=val*mtilde[d]
##             a[2i-1,jj]=-uplus*mval
##             a[2i,jj]=+uminus*mval
##         end
##     end
## end # loop for i

## # now the regularization
## if lambda!=nothing
##     for i=1:ntheta
##         a[2nkpts+2i-1,nkpts+i]=-1.          # encode z_i-theta_i>=0.
##         a[2nkpts+2i-1,nkpts+ntheta+i]=1.
##         a[2nkpts+2i,nkpts+i]=1.             # encode z_i+theta_i>=0.
##         a[2nkpts+2i,nkpts+ntheta+i]=1.
##     end
## end
## ## println("b=",b)
## ## println("c=",c)
## ## #display(full(a))
## ## print("a[1]=",a[1,:])
## ## print("a[2]=",a[2,:])
## ## print("a[13]=",a[13,:])
## ## print("a[14]=",a[14,:])
## #solver=GLPKMathProgInterface.GLPKSolverLP(method=:Simplex, presolve=false,msg_lev=GLPK.MSG_ON)
## solver=GLPKMathProgInterface.GLPKSolverLP(method=:Simplex, presolve=false)
## #solver=GLPKMathProgInterface.GLPKSolverLP(method=:InteriorPoint, presolve=true)
## lpsol=@debugtime(MathProgBase.linprog(c,a,'>',b,lbnd,Inf,solver),"Solving linear program:")
## if lpsol.status==:Unbounded
##     print("Unbounded ray=",lpsol.attrs.unboundedray)
## end
## if lpsol.status!=:Optimal
##     error("Linear program solver returned $(lpsol.status)")
## end
## #res=a*lpsol.sol-b
## #println("constraints: ",minimum(res),a*lpsol.sol-b)
## #println("constraints: ",minimum(res))
## #xopt=copy(lpsol.sol)
## #xopt[1:nkpts]+=1e-3
## #res=a*xopt-b
## #println("constraints: ",minimum(res),a*lpsol.sol-b)
## theta_opt=lpsol.sol[nkpts+1:nkpts+ntheta]
## #println("Optimal x=",lpsol.sol)
## #println("Optimal theta=",theta_opt)
## if lambda==nothing
##     println("Criterion value=",lpsol.sol)
## else
##     println("Criterion value=",lpsol.objval, " data part=",sum(lpsol.sol[1:nkpts]), "  regularization=",
##     dot(c[nkpts+ntheta+1:end],lpsol.sol[nkpts+ntheta+1:end]))
## end
## #crit_exp=finereg.eval_contributions(theta_opt,def,kptsf,kptsg,contribs,Float64[])
## #crit=lpsol.objval
## #for i in 1:nkpts
## #    crit+=contribs[i].u0
## #end
## #crit/=float(contribs.npix)
## #println("crit_exp=",crit_exp, " crit=",crit)
## #println("Criterion=",crit)
## return theta_opt # return the new theta
## end # end of linprog_optimize


"""
    linprog_optimize_model(def,kptsf,kptsg,contribs,theta_0,[lambda],[hmax][fdiffreg])
Linprog_optimize_model is a reimplementation of linprog_optimize using the JuMP modeling language.
Find the deformation parameters using linear programming.
#Arguments
* `def` - deformation
* `kptsf` - keypoints in image f
* `kptsg` - keypoints in image g
* `contribs` - criterion contributions calculated using `precalculate_contributions_pwl`
* `theta_0` - initial value of the deformation function
* `lambda` - regularization weight, it can be "nothing", a scalar,
         or a vector of the same size as the deformation parameters `theta`
* `hmax` - maximum displacement in multiples of the keypoint normals
* `fdiffreg` - regularization for finite difference l1 penalty

returns the optimum value for `theta`, minimizing

normfactor*``\\sum_i D_i + \\sum_j \\lambda_j |\\theta_j| + fdiffreg \\sum_ik | \\Delta_x \\theta_ik | + | \\Delta_y \\theta_ik |``

where ``D_i`` are the landmark contributions,
``\\Delta`` is the finite difference operator.

it is further assumed that `set_keypoints!(def,kpts)` was called
the keypoints kptsg were already deformed with theta_0

Maximum displacement is limited to `hmax`*|keypoint normal|. We shall assume
that all keypoint normals have the same size.

`fdiffreg` and `lambda` regularization and maximum displacement `hmax` is only supported for B-splines deformations.
"""
function linprog_optimize_model(def::deformation.AbstractDeformation,
       kptsf::Vector{K},kptsg::Vector{SimpleKeypoint},
       contribs::finereg.ContributionsPiecewiseLinear, theta_0::Vector{Float64},
       normfactor::Float64;
       lambda=nothing,hmax=nothing,fdiffreg=nothing) where {K<:AbstractKeypoint}
#@assert(!isa(def,deformation.HierarchicalDeformation))
@assert(deformation.is_linear(def))

ntheta=length(theta_0) # number of deformation parameters
nkpts=length(kptsf) # number of keypoints - measurement points
dim=length(kptsf[1].pos) # dimension

if !isa(isa(def,deformation.HierarchicalDeformation) ? def.def[def.actualLevel] : def,
        deformation.BsplineDeformation)
    @info("linprog_optimize_model: Regularization and maximum displacement limitation is ignored for a non-bspline deformation.")
    hmax=nothing ; fdiffreg=nothing ; lambda=nothing
end
    
#solver=GLPKMathProgInterface.GLPKSolverLP(method=:Simplex, presolve=false)
#solver=GLPKMathProgInterface.GLPKSolverLP(method=:InteriorPoint)
#solver=Gurobi.GurobiSolver(Method=2,OutputFlag=0) # interior point method
#solver=Gurobi.GurobiSolver(Method=1,OutputFlag=0) # simplex method
#m=Model(solver=solver)
#m=Model(with_optimizer(GLPK.Optimizer))
#m=Model(with_optimizer(Gurobi.Optimizer,OutputFlag=0))
m=Model(optimizer_with_attributes(Gurobi.Optimizer, "OutputFlag" => 0))
#m=Model(with_optimizer(Gurobi.Optimizer,Method=2))
@variable(m,D[1:nkpts]>=0)       # criterion contribution at keypoint i 
#@variable(m,vtheta[1:ntheta]) # deformation parameters   
# bounds for parameter values
@variable(m,vtheta[1:ntheta])
if hmax!=nothing 
   kpnorm=keypoints.keypoint_normal_norm(kptsf) # size of the keypoint normal
   hmaxinf=hmax/sqrt(dim)*kpnorm  # infinity norm bound to guarantee l2-norm to be bound by hmax
   # @info("kpnorm=$kpnorm hmaxinf=$hmaxinf") 
   # println("kpnorm=$kpnorm hmaxinf=$hmaxinf") 
   #@debug "kpnorm=$kpnorm hmaxinf=$hmaxinf"
   for i=1:ntheta
       @constraint(m,theta_0[i]-hmaxinf<=vtheta[i]) # Gurobi does not support two-sided constraints
       @constraint(m,vtheta[i]<=hmaxinf+theta_0[i])
   end
end

@expression(m,criterion,sum(normfactor*D[i] for i=1:nkpts)) # data part of the criterion
    
# regularization - l1 penalty on the coefficients    
if lambda!=nothing
    @variable(m,z[1:ntheta]>=0)
    if isa(lambda,Array)
        @debugassert(length(size(lambda))==1 && size(lambda)==ntheta && isa(lambda,Array{Float64}))
        @expression(m,lambda_regularization,sum(z[j]*(lambda[j]/nkpts) for j=1:ntheta))
    else
        @debugassert(isa(lambda,Number))
        @expression(m,lambda_regularization,sum(z[j]*(lambda/nkpts) for j=1:ntheta))
    end
    add_to_expression!(criterion,lambda_regularization)
end
dim=length(kptsf[1].pos) # dimension of the space

# l1 regularization on the finite differences    
if fdiffreg!=nothing
      thetasize=size(deformation.get_theta_mat(def))
      @debugassert(dim==thetasize[1])
      if dim==2 
          numtdif=(thetasize[2]-1)*thetasize[3]+(thetasize[3]-1)*thetasize[2]
          @variable(m,tdif[1:dim,1:numtdif])
          tdifi=1 ; ti=0
          for j=1:thetasize[3]
              for i=1:thetasize[2]
                  if i<thetasize[2] # vertical differences
                      for k=1:dim
                          @constraint(m,vtheta[ti+k]-vtheta[ti+k+1]<=tdif[tdifi])
                          @constraint(m,vtheta[ti+k+1]-vtheta[ti+k]<=tdif[tdifi])
                          tdifi+=1
                      end # k
                  end
                  if j<thetasize[3] # horizontal differences
                      for k=1:dim
                          @constraint(m,vtheta[ti+k]-vtheta[ti+k+thetasize[2]]<=tdif[tdifi])
                          @constraint(m,vtheta[ti+k+thetasize[2]]-vtheta[ti+k]<=tdif[tdifi])
                          tdifi+=1
                      end # k
                  end
                  ti+=dim
              end
          end    
    #  elseif dim==3
    #      @variable(m,tdif[1:dim,thetasize[2]-1,thetasize[3]-1,thetasize[4]-1)
      else
          error("unsupported dimension $dim")
      end
      @expression(m,fdiffreg_regularization,(fdiffreg/nkpts)*sum(tdif[i] for i=1:numtdif))  
      add_to_expression!(criterion,fdiffreg_regularization)
end

@objective(m,Min,criterion)

y=zeros(Float64,length(kptsf[1].pos)) # temporary vector
# go over all keypoints to assemble the constraints
for i=1:nkpts
    # first calculate \phi_0(x_i)-T_0(x_i)
    #deformation.transform_initial!(y,def,kptsf[i].pos)
    deformation.transform_initial!(y,def,kptsf,i) # initial position of kpt i
    broadcast!(-,y,y,kptsg[i].pos) #subtract y-=kptsg[i].pos but in place
    # take a dot product with \tilde{m}_i
    mtilde=kptsg[i].normal / norm2(kptsg[i].normal)
    q=dot(y,mtilde) # transformed and rescaled normal
    ci=contribs[i]

    qmxi=q-ci.x0
    uplus=ci.uplus ; uminus=ci.uminus


    #affplus=AffExpr(D[i]) # building affine expressions step by step
    #affminus=AffExpr(D[i]) # building affine expressions step by step
    affplus=AffExpr(0.,D[i] => 1.) # building affine expressions step by step
    affminus=AffExpr(0.,D[i] => 1.) # building affine expressions step by step

    
    for (j,val) in deformation.coefficients_iterator_keypoint(def,i,kptsf)
        for d=1:dim     # dimension
            jj=j*dim+d  # index of the variable
            mval=val*mtilde[d]
            add_to_expression!(affplus,-uplus*mval,vtheta[jj])
            add_to_expression!(affminus,uminus*mval,vtheta[jj])
        end
    end
    @constraint(m,affplus>=qmxi*uplus)
    @constraint(m,affminus>=-qmxi*uminus)
end # for i   

# now the regularization
if lambda!=nothing
  for i=1:ntheta
    @constraint(m,z[i]-vtheta[i]>=0.)
    @constraint(m,z[i]+vtheta[i]>=0.)
  end
end    

#lpsol=@debugtime(solve(m),"Solving linear program")
lpsol=@debugtime(optimize!(m),"Solving linear program")
theta_opt=value.(vtheta)    
#if lambda==nothing
#    println("Criterion value=",getobjectivevalue(m))
#else
    objval=objective_value(m)
    datapart=normfactor*sum(value.(D))
    println("Criterion value=",objval, " data part=",datapart, "  regularization=", objval-datapart)
#end

return theta_opt
end # linprog_optimize_model

# version for HierarchicalDeformation
#function linprog_optimize_model{K<:AbstractKeypoint}(def::deformation.HierarchicalDeformation,
#       kptsf::Vector{K},kptsg::Vector{SimpleKeypoint},
#       contribs::finereg.ContributionsPiecewiseLinear, theta_0::Vector{Float64},
#       normfactor::Float64;
#       lambda=nothing,hmax=nothing,fdiffreg=nothing)
#  linprog_optimize_model(def.def[def.actualLevel],kptsf,kptsg,contribs,theta_0,normfactor;
#                        lambda=lambda,hmax=hmax,fdiffreg=fdiffreg)
#end

"""
    register_fast_linprog(f,g,kpts,def,critstate,opts)
This is a simplified variant of `finereg:register_fast` using linear programming.
'f' and 'g' are the segmented images, 'f' is only used for visualization, if ever.
'def' is the initial deformation, 'crit' is the criterion.
Recognized Options `opts` are
* `hmax` - maximum normal displacement, defaults to 10 pixels
* `defaultval` - default value for pixels outside the image, defaults to 0
* `resample` - whether to resample the normals if the transformation changes significantly, defaults to `false`
* `maxResample` - how many times to resample the normal, defaults to 20
* `lambda` - l1 regularization weight, passed on to linprog_optimize. It can be "nothing", a scalar, or a vector of the same size as the deformation parameters `theta`
"""
function register_fast_linprog(f::Image,g::Image,
                                                    kptsf::Vector{K},def::deformation.AbstractDeformation,
                                                    critstate::CriterionState,
                                                    opts::Opts) where {K<:AbstractKeypoint}
  @assert(!isa(def,deformation.HierarchicalDeformation))
  register_fast_linprog_onelevel(f,g,kptsf,def,critstate,opts)
end

# for hierarchical deformation, this only optimizes one level at a time
function register_fast_linprog_onelevel(f::Image,g::Image,
                                                    kptsf::Vector{K},def::deformation.AbstractDeformation,
                                                    critstate::CriterionState,
                                                    opts::Opts) where {K<:AbstractKeypoint}
 @defaults opts hmax=10 resample=false defaultval=zero(g[1]) maxResample = 20 lambda=nothing fdiffreg=nothing
 @assert(deformation.is_linear(def))
 badApprox = true # true if resampling of the normals is needed
 newdef = deformation.copy(def) # newdef is to be optimized
 #@info("register_fast_linprog_onelevel lambda=$(lambda) fdiffreg=$(fdiffreg) pxsize=$(Images.pixelspacing(f))")
 @debugtimeline(deformation.set_keypoints!(newdef, kptsf),"onelevel set_keypoints:") # precalculation based on fixed image keypoints
 defaultval=convert(eltype(g),defaultval)   
 iternum=1
 while badApprox && maxResample >= 1
        maxResample -= 1
        # Sampling normals
        theta0=deformation.get_theta(newdef)
     #kptsg=finereg.transform_keypoints(kptsf,Nullable(newdef))
        kptsg=finereg.transform_keypoints(kptsf,newdef)
        #kptsg=@debugtimeline(finereg.transform_keypoints(kptsf,Nullable(newdef)),"onelevel transform_keypoints")
        #println("onelevel: level=$(def.actualLevel) iternum=$(iternum) theta0=$(theta0)")
        if isa(def,deformation.HierarchicalDeformation)
            @info("register_fast_linprog_onelevel: level=$(def.actualLevel) iternum=$(iternum)")
        end
        contribs= finereg.precalculate_contributions_pwl(g,kptsf,kptsg,critstate,hmax;defaultval=defaultval)
        #contribs=@debugtimeline(finereg.precalculate_contributions_pwl(g,kptsf,kptsg,critstate,hmax;defaultval=defaultval),
#                             "precalculate_contributions_pwl")
        normfactor=1. / (contribs.npix*prod(Images.pixelspacing(f)))
        #optx=linprog_optimize(newdef,kptsf,kptsg,contribs,theta0,lambda=lambda)
        #@info("theta before: $theta0")
        optx=linprog_optimize_model(newdef,kptsf,kptsg,contribs,theta0,normfactor,
                                    lambda=lambda,hmax=hmax,fdiffreg=fdiffreg)
        #@info("theta after: $optx")
        #optx=linprog_optimize_model(newdef,kptsf,kptsg,contribs,theta0,lambda=lambda,hmax=nothing)
        deformation.set_theta!(newdef, optx) # the resulting transformation of this iteration
        kptsgg=finereg.transform_keypoints(kptsf,newdef)
        #kptsgg=@debugtimeline(finereg.transform_keypoints(kptsf,Nullable(newdef)),"onelevel transform_keypoints2")
        #deformation.set_theta!(newdef, theta0)
        badApprox = resample # check if resampling is needed
        if resample
            badApprox = !finereg.check_sampling(kptsf, kptsg, contribs, newdef)
            #badApprox = @debugtimeline(!finereg.check_sampling(kptsf, kptsg, contribs, newdef),"onelevel check_sampling")
            if badApprox
                @debugprintln_with_color(:magenta, "Assumptions not satisfied, resampling needed.")
            end
        end
        iternum+=1
 end # end while
 if maxResample < 1
     @debugprintln_with_color(:red, "Resampling limit reached, the procedure probably does not converge.")
 end
 #@check_used opts
 return newdef
end # register_fast_linprog

# ---------------------------------------------------------------------------------------------------
"""
    reduce_images(f, g, minsize)
Reduce image sizes `f`, `g` twice in some dimensions, so that the pixelsizes are roughly the same,
as long as the longest dimension is at least minsize. If no reduction is possible,
set reduced_images to false and return the original images.

fn,gn,redfac1,redfac2,reduced_images=reduce_images(f,g,minsize)

r,2 = vectors of the reduction factors
"""
function reduce_images(f::Image, g::Image, minsize)

   pxspf = Images.pixelspacing(f) ; pxspg = Images.pixelspacing(g)
   szf = Images.size(f) ; szg = Images.size(g) 
   minpxsp= min(minimum(pxspf),minimum(pxspg)) # minimum pixel spacing
   dims=length(pxspf)
   @assert(dims==length(pxspg)) 
   redfacf = ones(Int,dims)
   redfacg = ones(Int,dims)
   targetspacing=1.414*minpxsp
   # downsample in any dimension, where the pixelspacing is smaller than targetspacing 
    if minimum(szf)>=2minsize
    for i = 1:dims
       if pxspf[i]<=targetspacing 
           redfacf[i]=2 
       end
   end
   end
   if minimum(szg)>=2minsize
       for i = 1:dims
           if pxspg[i]<=targetspacing && szg[i] >= 2minsize
               redfacg[i]=2
           end
       end
   end
   reduced_images= max(maximum(redfacf),maximum(redfacg))>1
   if reduced_images
       f=finereg.reduce_image(f,redfacf)
       g=finereg.reduce_image(g,redfacg)
   end
   return f,g,redfacf,redfacg,reduced_images
end

"""
    reduce_keypoints(kpts, nhood, minKP, redfacg)
Reduce the number of keypoints
"""
function reduce_keypoints(kpts::AbstractVector{K},
     nhood::Array{Set{T}},minKP::Integer,redfacg::Array{Int}) where {K<:AbstractKeypoint,T<:Integer}

   # subsample keypoints if there are too many
   reduced_keypoints=false
   if length(kpts)>minKP
       kptsn, nhoodn = subsample_keypoints_equidistant(kpts, nhood, redfacg)
       reduced_keypoints = true
   end

   # if there are too few keypoints after subsampling, just convert the coordinates
   if !reduced_keypoints || length(kptsn)<5 || length(kptsn)<minKP/3
       kptsn::Array{K} = [reduce_keypoint(kpts[i], redfacg) for i = 1:length(kpts)]
       nhoodn = copy(nhood)
   end    
   reduced_keypoints = reduced_keypoints && (length(kptsn) != length(kpts)) #if keypoint subsampling fails
   return kptsn,nhoodn,reduced_keypoints    
end
    
"""
    register_fast_linprog_multiscale(f, g, kpts, def, nhood, critparams, opts)
Register_fast_multiscale_linprog - register two images `f` and `g` using `register_fast_linprog`
Recursively reduce the size of the images and the number of keypoints to obtain coarse scale approximation
Images bigger than 'maxsize' are not optimized locally.
"""
function register_fast_linprog_multiscale(
  f::Image, g::Image, kpts::AbstractVector{K},def::deformation.AbstractDeformation,
  nhood::Array{Set{T}}, critparams::CPType, opts::Opts) where {T,CPType<:CriterionParameters,K<:AbstractKeypoint}
  @defaults opts minsize = 128 minKP = typemax(Int) display=false hmax=10 maxsize=2048

  nkpts = length(kpts)
  @info "Register multiscale called with image sizes $(size(f)) $(size(g)), pixelspacing $(Images.pixelspacing(f)) $(Images.pixelspacing(g)) and $nkpts keypoints hmax=$hmax type=$(eltype(f)) $(eltype(g))."

  @assert(nkpts>0)  
  fn,gn,redfacf,redfacg,reduced_images=reduce_images(f,g,minsize)
  kptsn,nhoodn,reduced_keypoints=reduce_keypoints(kpts,nhood,minKP,redfacg)    

  if (reduced_images || reduced_keypoints) # something has been reduced
      resample_normals!(fn, kptsn) 
      d=register_fast_linprog_multiscale(fn,gn,kptsn,def,nhoodn,critparams,opts) # recursive call
  else
      d=def
      @info("Register_fast_linprog_multiscale innermost.")
  end
  if min(minimum(Images.size(f)),minimum(Images.size(g)))>maxsize
      @info("Image size exceeds 'maxsize', skipping local optimization.")
      return d
  end
  warped=deformation.transform_image_nn(d,g,size(f),@options)
  @info("Local registration started: sizef=$(size(f)) size(g)=$(size(g)), pixelspacing $(Images.pixelspacing(f)) $(Images.pixelspacing(g)), length(kpts)=$nkpts.")
  critstate=criterion_init(f,warped,critparams) # initialize criterion
  if display
   fkpts=keypoints.create_image_with_keypoints(copy(f),kptsn,llen=hmax,bsize=1)
   ImageView.imshow(fkpts,name="keypoints before local at scale=$(size(f))") 
   println("f size=$(size(f)) spacing=$(Images.pixelspacing(f)); g size=$(size(g)) spacing=$(Images.pixelspacing(g)) ;z warped size=$(size(warped)) spacing=$(Images.pixelspacing(warped))")   
   ImageView.imshow(deformation.overlay(f,warped),name="overlay before local at scale=$(size(f))") 
  end
  dnew=@debugtimeline( register_fast_linprog(f,g, kpts, d, critstate, opts),"register_fast_linprog local:")
  if display
   warped=deformation.transform_image_nn(dnew, g, size(f), @options)
   ImageView.imshow(deformation.overlay(f,warped),name="overlay after at scale=$(size(f))") 
  end
  return dnew
end     

##########################################################################################

"""
    is_suitable_level(f, def, knot_spacing)
Auxiliary function for register_fast_linprog_hierarchical. Returns 'true' if 'def'
is not a B-spline deformation or if the distance between knots is at least 'knot_spacing' pixels
"""
function is_suitable_level(f::Image,def::deformation.AbstractDeformation,knot_spacing)
  if !isa(def,deformation.BsplineDeformation)  return true end
  k=def.h ./ Images.pixelspacing(f)
  @info("is_suitable_level: knot spacing = $k")
  return minimum(k)>=knot_spacing
end
    
"""
    register_fast_linprog(f, g, kptsf, def::HierarchicalDeformation, critstate, opts)
Like `register_fast_linprog` but for HierarchicalDeformation.
The deformation are suppossed to be applied from the last.
We shall optimize deformation parameters from the coarsest level.

There is a new 'opt' parameter 'knot_spacing', which is the minimum number
of pixels per B-spline knot, we will not optimize B-spline deformations which are too fine.
"""
function register_fast_linprog(f::Image,g::Image,
                                                    kptsf::Vector{K},def::deformation.HierarchicalDeformation,
                                                    critstate::CriterionState,
                                                    opts::Opts) where {K<:AbstractKeypoint}
# TODO: create and use a more abstract interface, if ever there is another HierarchicalDeformation
  @defaults opts hmax=10 resample=false defaultval=zero(g[1]) maxResample = 20 lambda=nothing fdiffreg=nothing knot_spacing=8 start_with_last_level=false
  level=def.actualLevel
  if level==0 || start_with_last_level
    level=length(def.def)
    deformation.set_level!(def,level) # set to the last level
  end
    
  while true
    @info("register_fast_linprog_hierarchical: level=$level.")
    #warped=deformation.transform_image_lin(def, g, size(g), @options)
    #ImageView.imshow(deformation.overlay(f,warped),name="hierarchical before level=$(level)") 
    def=register_fast_linprog_onelevel(f,g,kptsf,def,critstate,opts)
    #warped=deformation.transform_image_lin(def, g, size(g), @options)
    #ImageView.imshow(deformation.overlay(f,warped),name="hierarchical after level=$(level)") 
    if level==1 || !is_suitable_level(f,def.def[level-1],knot_spacing)
        return def
    end
    level-=1
    deformation.set_level!(def,level) # set a finer level
  end
end

    
##########################################################################################


end # module linprog_register
