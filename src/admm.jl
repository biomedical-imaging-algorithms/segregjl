""" Implementation of the Alternating Direction Method of Multipliers

"""
module admm

#using Devectorize
import ..optimizer
#using Debug
#import Base.LinAlg.BLAS.axpy!
import LinearAlgebra.BLAS.axpy!

include("debugassert.jl")
switchasserts(false)
include("debugprint.jl")
switchprinting(false)


export admmss_optimize, cg_linear_solve, OptimizerADMMS

"""
     cg_linear_solve(applyA, b; reltol = 1e-3, abstol = 1e-12, maxiter = 1000)
Solve a system of equations ``Ax=b`` iteratively using a conjugate gradient method,
`applyA(x)` = ``Ax``

"""
function cg_linear_solve(applyA::Function,b::Vector{Float64},x0::Vector{Float64};
                         reltol=1e-3,abstol=1e-12,maxiter=1000)
    n=length(x0)
    @assert(length(b)==n)
    x=copy(x0)
    r= b- applyA(x0) # initial residual
    p=copy(r)
    rsold=dot(r,r)
    stol::Float64=max(abstol,reltol*norm(b,Inf))
    itnum::Int=1
    nr=norm(r,Inf)
    #@debugprintln("cg_linear_solve: initial residual $(nr), limit=$stol")
    while (nr>stol && itnum<maxiter)
        itnum+=1
        #@debugprintln("cg_linear_solve: iter=$itnum nr=$nr")
        Ap=applyA(p)
        alpha=rsold/dot(p,Ap)
        axpy!(alpha,p,x)   #x=x+alpha*p
        axpy!(-alpha,Ap,r) #r=r-alpha*Ap
        rsnew=dot(r,r)
        betacoef=rsnew/rsold  # there might be another formula
        p[:]=r+betacoef.*p
        rsold=rsnew
        nr=norm(r,Inf)
    end
    # rr=b-applyA(x)
    # @debugprintln("cg_linear_solve: iter=$itnum, final residual |r|=$nr |rr|=$(norm(rr,Inf)) ")
    return x
end

# parameters for admmss_optimize
mutable struct OptimizerADMMS <: optimizer.AbstractOptimizer
    maxiter::Integer
    errprim::Float64
    errdual::Float64
    ji_opts::Dict{Symbol,Any} # to be passed to optim_ji
    cg_opts::Dict{Symbol,Any} # cg_opts - to be passed to cg_linear_solve
    monitor::Bool
    rho::Float64
    errdy::Float64
end

function OptimizerADMMS(;maxiter=100,errprim=1e-3,errdual=1e-3,errdy=0.,monitor=false,rho=1.,
                           ji_opts=Dict(:reltol => 1e-3, :abstol => 1e-12, :maxiter =>100),
                            cg_opts=Dict(:reltol => 1e-12, :abstol => 1e-12, :maxiter =>100))

    OptimizerADMMS(maxiter, errprim, errdual, ji_opts, cg_opts, monitor,rho,errdy)
end


"""
    admmss_optimize(eval_ji, optim_ji, applyA, applyAt, applyRtR, x0, c; opts=OptimizerADMMS())

The single split ADMM solves a problem of finding a vector ``x`` minimizing
a criterion ``J`` in the form

``J = \\sum_{i=1}^N J_i(y_i) + ||R x||^2``

such that ``A x + c = y``,

where ``Rx`` is regularization.

# Arguments
* `eval_ji::Function`: called as `eval_ji(i,y_i)`, returns ``J_i(y_i)``. It is only used
    for monitoring the progress if `opts.monitor` is true, it is not called otherwise.
* `optim_ji::Function`: called as `optim_ji(i,w2,w1;ji_opts...)` returns ``y^*``
    where ``y^* =\\textrm{arg min}_y J_i(y)+w_2 y^2+w_1 y``. Optional `ji_opts` may
    contain additional parameters such as `reltol`, `abstol`, `numitera`
    Function to optimize individual ``J_i``
* `applyA::Function`: called as `applyA(x)`, returns ``A x``
* `applyAt::Function`: called as `applyAt(y)`, returns ``A^T y``
* `applyRtR::Function`: called as `applyRtR(x)`, returns ``R^T R x``
* `x0::Vector{Float64}`: initial value of ``x``

and optional parameters `opts` with fields
* `maxiter`: maximum number of iterations (default 1000)
* `errprim`: absolute ``l_{inf}`` error of the primal residual (default 1e-3), i.e. error of ``|Ax-y|_{inf}``
* `errdual`: absolute ``l_{inf}`` error of the dual   residual (default 1e-3), i.e. ``\\rho * |\\Delta y|_{inf}``
* `ji_opts`: to be passed to `optim_ji`
* `cg_opts`: to be passed to `cg_linear_solve`
* `monitor`: monitor the progress by evaluating the criterion during the optimization (default false)
"""
function admmss_optimize(eval_ji::Function,optim_ji::Function,applyA::Function,applyAt::Function,applyRtR::Function,
                         x0::Vector{Float64}, c::Vector{Float64};
                         opts::OptimizerADMMS=OptimizerADMMS())
    nx=length(x0)
    y=applyA(x0)+c
    ny=length(y)
    itnum=1
    rho=opts.rho
    lambda=zeros(Float64,ny)
    z=y # z=A*x0+c
    r=zeros(Float64,ny)  # z-y
    nr=0.
    mu=10. # const declaration deprecated
    tau=2. # const declaration deprecated
    x=x0

    # for debugging, print the current criterion
    function print_criterion(prefix,x,y,lambda)
        if opts.monitor
            Jd=sum(map(i->eval_ji(i,y[i]),1:ny)) # data part of the criterion
            Jm=sum(map(i->(.5*rho*y[i]^2-(rho*z[i]+lambda[i])*y[i]),1:ny)) #Jd+Jm is optimized in the first step
            r=applyA(x)+c-y
            rt=x'*applyRtR(x)
            Jr=0.5*rho*dot(r,r)+rt[1]#avoid Jr to be vector
            Jc=dot(lambda,r)    # constraint part
            Jl=Jd+Jr+Jc         # full lagrangian
            println(prefix,"Jd=$(Float32(Jd)) Jr=$(Float32(Jr)) Jc=$(Float32(Jc)) Jl=$(Float32(Jl)) Jd+Jm=$(Float32(Jd+Jm))")
        end
    end

    while itnum<=opts.maxiter
        print_criterion("admm initial        ",x,y,lambda) 
        # first substep, optimize wrt each y[i] separately
        w2=0.5*rho
        yold=copy(y)
        ff = true
        for i=1:ny
            w1=-(rho*z[i]+lambda[i])
            y[i]=optim_ji(i,w2,w1;opts.ji_opts...)
        end
        print_criterion("admm optimized y    ",x,y,lambda)

        # dual residual and its norms
        s=rho*applyAt(y-yold)
        dy=norm(y-yold,Inf)
        ns=norm(s,Inf) ;  ns2=norm(s,2)
        
        # second substep, optimize wrt x
        b=applyAt(y-c-lambda.*(1. / rho))
        applyAtAR= xv -> applyAt(applyA(xv)) + 2*applyRtR(xv)/rho
        x=cg_linear_solve(applyAtAR,b,x;opts.cg_opts...)
        print_criterion("admm optimized x    ",x,y,lambda) 

        # calculate primal residual and its norms
        z=applyA(x)+c
        r=z-y       
        nr=norm(r,Inf) ; nr2=norm(r,2)

        # update Langrange multipliers
        lambda+=rho*r
        print_criterion("admm updated lambda ",x,y,lambda) 

        @debugprintln("admmss_optimize: iter=$itnum |nr|=$nr |ns|=$ns |dy|=$dy |nr2|=$nr2 |ns2|=$ns2")
        # Automatic setting of rho
#        if nr2 > mu * ns2
        if nr > mu * dy
            rho*=tau
            @debugprintln("admmss_optimize: increasing rho to $rho.")
        end
#        if ns2 > mu * nr2
        if dy > mu * nr
             rho*=1. / tau
            @debugprintln("admmss_optimize: decreasing rho to $rho.")
        end

        itnum+=1
        if nr<opts.errprim && ( ns<opts.errdual  || dy < opts.errdy )
#        if nr<opts.errprim && dy < opts.errdy 
            @debugprintln("admmss_optimize: converged")
            break
        end
    end
    return x
end

# testing is in test_admm.jl

end     #module
 
