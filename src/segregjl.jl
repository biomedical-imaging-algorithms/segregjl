"""
Algorithms for registration of segmented images in Julia, based on out articles 

Pinheiro M. A. and Kybic J.. "Incremental B-spline deformation model for geometric graph matching." ISBI, pp. 1079-1082, April 2018.
Kybic J. and Borovec J.. "Fast registration by boundary sampling and linear programming." MICCAI, vol. 11070, pp. 783-791, 2018.
Kybic Jan, Dolejší Martin and Borovec Jiří. "Fast registration of segmented images by normal sampling." CVPRW: BioImage Computing Workshop, pp. 11-19, 6 2015.
Jan Kybic and Jiri Borovec. "Automatic simultaneous segmentation and fast registration of histological images." International Symposium on Biomedical Imaging (ISBI), pp. 774-777, April-May 2014.


Jan Kybic
"""
module segregjl
  
include("Options.jl")
using .Options

using MicroLogging
using Logging
using Colors
#using DelimitedFiles
using Statistics

include("criterion.jl")
using .criterion
include("ssd.jl")
include("SLICsuperpixels.jl")
include("segimgtools.jl")
include("bsplines.jl")
include("keypoints.jl")
include("deformation.jl")
include("optimizer.jl")
include("regularization.jl")
include("mil.jl")
include("gaussianMI.jl")
include("softmax.jl")
include("coarsereg.jl")
include("admm.jl")
include("finereg.jl")
include("linprog_register.jl")

using .linprog_register

export Options, linprog_register, keypoints, deformation, segimgtools, mil, finereg, SLICsuperpixels
export gaussianMI, coarsereg

end
