"""
Image registration by criterion minimization

Jan Kybic, November 2014
"""
module register

import deformation
import Images
import NLopt
import mil
import segimgtools

using Options

#include("debugassert.jl")
#switchasserts(true)

"""
    register_images(f, g, crit, def, opts)
Find deformation parameters minimizing a criterion.
* crit(Image,Image)->Float64
* starting at deformation "def".
* Gradient-less method is used with absolute tolerand xtol_abs,
initial step size 1.0; defaultval is used to fill in missing values in the transformed image.
"""
function register_images{T,N}(
        f::Images.Image{T,N},g::Images.Image{T,N},
        crit::Function,def::deformation.AbstractDeformation, opts::Opts)
  @defaults opts xtol_abs=0.1 initialstep=1. neval=1000 lbound=deformation.lower_bound(def) ubound=deformation.upper_bound(def)
  # Todo: optimize for speed
  cnt=0
  d=deformation.copy(def) # to avoid overwriting the parameter  
  function critwarped(theta,grad)
      @assert(length(grad)==0) # we do not provide a gradient
      cnt+=1
      deformation.set_theta!(d,theta)
      warped=deformation.transform_image_nn(d,g,size(f),opts)
      m=crit(f,warped)
      # println("crit count=$cnt m=$m")
      return m
  end
  ndof=length(deformation.get_theta(d)) # number of degrees of freedom
  opt=NLopt.Opt(:LN_BOBYQA,ndof)
  #opt=NLopt.Opt(:LN_COBYLA,ndof)
  #opt=NLopt.Opt(:LN_NELDERMEAD,ndof)
  #opt=NLopt.Opt(:LN_NEWUOA,ndof)
  NLopt.min_objective!(opt,critwarped)
  NLopt.lower_bounds!(opt,lbound)
  NLopt.upper_bounds!(opt,ubound)

  NLopt.xtol_abs!(opt,xtol_abs*ones(Float64,ndof))
  NLopt.maxeval!(opt,neval) # at most 1000 evaluations
  NLopt.initial_step!(opt,1.*ones(Float64,ndof)) # initial step
  (optf,optx,ret)=NLopt.optimize(opt,deformation.get_theta(d))
  println("Register.register: Optimization finished after $cnt iterations, crit=$optf")
  deformation.set_theta!(d,optx)
  @check_used opts
  return d
end

"""
    register_rigid_2D_MIL(f, g, k, opts)
Rigidly register two 2D images by minimization of the MIL criterion.
"""
function register_rigid_2D_MIL{T,N}(f::Images.Image{T,N},g::Images.Image{T,N},
                    k::Integer, opts::Opts)
   # println("register_rigid_2D k=$k maxf=$(maximum(f)) maxg=$(maximum(g))") 
   crit = (fi,gi) -> - mil.calculateMIL(fi,gi,k)
   def=deformation.create_identity([size(f)...] .* Images.pixelspacing(f), deformation.RigidDeformation2D)
   d_opt=register_images(f,g,crit,def,opts)
   return d_opt
end

"""
    register_images_multiscale(f, g, crit, def, opts)
Register two images like register_images but multiscale.
There is an additional option minsize, the minimum size (smaller dimension) to apply multiresolution.
"""
function register_images_multiscale{T,N}(f::Images.Image{T,N},g::Images.Image{T,N},
        crit::Function,def::deformation.AbstractDeformation, opts::Opts)
    @defaults opts minsize=64
    s=min(minimum(size(f)),minimum(size(g)))
    println("register multiscale called with size(f)=$(size(f)) size(g)=$(size(g)).")
    if s<=minsize
        r=deformation.get_theta(def)
        println("register multiscale innermost started. Starting point: $r")
        dnew=register_images(f,g,crit,def,opts)
        r=deformation.get_theta(dnew)
        println("register multiscale innermost finished. Registration result: $r")
        return dnew
    else
        # image is too big, reduce it
        fr=segimgtools.downsample_majority(f)
        gr=segimgtools.downsample_majority(g)
        # coarse level registration
        d=register_images_multiscale(fr,gr,crit,def,opts)
        # perform local optimization
        r=deformation.get_theta(d)
        println("register_multiscale local registration started size(f)=$(size(f)). Starting point: $r")
        dnew=register_images(f,g,crit,d,opts)
        r=deformation.get_theta(dnew)
        println("register_multiscale local registration finished size(f)=$(size(f)). Registration result: $r")
        return dnew
    end
    # @check_used opts
end
        
"""
    register_rigid_2D_MIL_multiscale(f, g, k, opts)
Rigidly register two 2D images by minimization of the MIL criterion.
"""      
function register_rigid_2D_MIL_multiscale{T,N}(f::Images.Image{T,N},g::Images.Image{T,N},
                    k::Integer, opts::Opts)
   # println("register_rigid_2D k=$k maxf=$(maximum(f)) maxg=$(maximum(g))") 
   crit = (fi,gi) -> - mil.calculateMIL(fi,gi,k)
   def=deformation.create_identity([size(f)...] .* Images.pixelspacing(f),deformation.RigidDeformation2D)
   d_opt=register_images_multiscale(f,g,crit,def,opts)
   return d_opt
end


end # module register
                       
