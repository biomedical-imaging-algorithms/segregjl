"""
Read Metaimage (mhd raw) files these are the two part files header.mhd + image.raw (usually).
Not all the features of metaimage format are implemented (e.g. datatypes, RGB...)

Martin Dolejsi, November 2015

The text file parsing comes from Brandon K. Miller
https://github.com/brandonkmiller/ConfParser.jl/blob/master/src/ConfParser.jl
"""
module readMetaimage

import Images
include("debugprint.jl")
switchprinting(true)

"""
    read_header(filename)
Read the header file `filename`. Return `Dict`: "param mame"->"value".
"""
function read_header(filename::AbstractString)
    # read the header file and parse the relevant items into a dictionary
    fh = open(filename, "r")
    data = Dict{AbstractString,AbstractString}()
    for line in eachline(fh)
         # skip empty lines
        if (ismatch(r"^\s*\n", line))
            continue
        end
        line = chomp(line) # trim trailing  whitespace
        # parse key/value
        m = match(r"^\s*([^=]*?)\s*=\s*(.*?)\s*$",line)
        if (m != nothing)
            key::String, value::String = m.captures
            data[key]=value
        else
            error("invalid syntax on line: \"$(line)\"")
        end
    end
    return data
end

"""
    read_data(filename)
Read the header file `filename` or `filename`.mhd or `filename`.hdr and image data.
The safest way how to call the function is to use the full name of the header file, and include
the "ElementDataFile" field in the header file. Return an image.
"""
function read_data(filename::AbstractString)
    @debugprintln("Reading $filename")
    if !isfile(filename)
        @debugprintln_with_color(:red, "File $filename doesn't exist trying $(filename).mhd")
        filename = string(filename, ".mhd")
    end
    if !isfile(filename)
        @debugprintln_with_color(:red, "File $filename doesn't exist trying $(filename[1:end-3])hdr")
        filename = string(filename[1:end-3], "hdr")
    end
    d=read_header(filename)
    ndims = parse(Int, d["NDims"])
    assert(ndims == 3) # only 3D supported for the moment
    # extract image size
    sz = Array(Int, ndims)
    szstr = split(d["DimSize"]," ")
    for i = 1:ndims
        sz[i] = parse(Int, szstr[i])
    end
    # extract voxel dimensions
    pixdim = float(split(d["ElementSpacing"], " "))
    # voxel datatype
    typestr = d["ElementType"]
    if typestr == "MET_SHORT"
        eltype = Int16
    elseif typestr == "MET_CHAR"
        eltype = Int8
    elseif typestr == "MET_UCHAR"
        eltype = UInt8
    else
        error("Type $typestr not supported, add new elseif here")
    end
    # byteorder
    byteorder = 0
    if haskey(d, "BinaryDataByteOrderMSB")
        if d["BinaryDataByteOrderMSB"] == "True"
            byteorder = 1
        end
    end
    # find data lile name
    if haskey(d, "ElementDataFile")
        directory, _ = splitdir(filename)
        datafile = joinpath(directory, d["ElementDataFile"])
    elseif filename[end-2:end] == "mhd"
        datafile = string(filename[1:end-3], "raw")
        if !isfile(datafile)
            @debugprintln_with_color(:red, "File $datafile doesn't exist trying $(dfilename[1:end-3])img")
            datafile = string(filename[1:end-3], "img")
        end
    elseif filename[end-2:end] == "hdr"
        datafile = string(filename[1:end-3], "raw")
        if !isfile(datafile)
            @debugprintln_with_color(:red, "File $datafile doesn't exist trying $(dfilename[1:end-3])img")
            datafile = string(filename[1:end-3], "img")
        end
    else
        datafile = string(filename, ".raw")
        if !isfile(datafile)
            @debugprintln_with_color(:red, "File $datafile doesn't exist trying $(filename).img")
            datafile = string(filename, ".img")
        end
    end
    # read the data
    datah = open(datafile, "r")
    data = read(datah, eltype, prod(sz))
    close(datah)
    # if bigendian convert
    if byteorder == 1 && (eltype != Int8 || eltype != UInt8)
        bytes=reinterpret(UInt8, data)
        for i=1:prod(sz)
            tmp = bytes[2*i]
            bytes[2*i] = bytes[2*i-1]
            bytes[2*i-1] = tmp
        end
    end
    data=reshape(data,sz...)
    # convert to grayscale image
    img=Images.grayim(data)
    img["spatialorder"]=["x", "y", "z"]
    img["pixelspacing"]=pixdim
    return img
end

end
