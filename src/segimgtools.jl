"""
Tools for working with segmented images.
"""
module segimgtools

export create_rigid_transform, rotation_matrix
export downsample_majority, get_resample_factor
export larger_size, extend_to_size, mosaic
export classimage2rgb,boundary_pixels!,draw_line!,draw_box!,draw_superpixels!
export Image,LabelType,LabelImage
export img2array, array2img, kmeans_segmentation
export make_segmentation_binary
export clip,clip_zero_one, norm2, dist2, sqr
export argmin, argminval, with_pixelspacing, similar_with_pixelspacing
export @get_with_default

using Base.Cartesian
using Images
using Colors
using ColorTypes
using FixedPointNumbers
using Clustering
using ..SLICsuperpixels
#using MicroLogging
using Logging
import AxisArrays

#disable_logging(Logging.BelowMinLevel) # or Debug or Info
#configure_logging(min_level=:debug)

import OffsetArrays

include("debugassert.jl")
switchasserts(false)
include("debugprint.jl")
switchprinting(false)

const Image = AbstractArray
const LabelType  = UInt8
const LabelImage  = Image{LabelType}
const LabelPixelType = Normed{UInt8,8}
const SuperpixelType = UInt16
const SuperpixelArray = Array{SuperpixelType}
const SuperpixelImage = Image{SuperpixelType}

"""
    prototype_segmentation(slabels, sfeatures, prototypes)
Segment superpixels by nearest neighbor into k-classes, returning labels
per superpixel and also per pixels. 
"""
function prototype_segmentation(slabels::SuperpixelImage, sfeatures::Array, prototypes::Array)
    @debugassert (length(prototypes)<256)
    # p = Image(Array(LabelType, size(slabels)),spatialorder=Images.spatialorder(slabels))
    assignments = Array(UInt8, size(sfeatures, 2))
    for iinx = 1:size(sfeatures, 2)
        dist2 = 1e7
        for pinx = 1:size(prototypes, 2)
            distt2 = (sfeatures[1, iinx] - prototypes[1, pinx]) ^ 2
            for i = 2:size(prototypes, 1)
                distt2 += (sfeatures[i, iinx] - prototypes[i, pinx]) ^ 2
            end
            if distt2 < dist2
                assignments[iinx] = pinx
                dist2 = distt2
            end
        end
    end
    #p=Images.Image(assignments[slabels],spatialorder=Images.spatialorder(slabels))
    #return assignments, p
    return assignments, assignments[slabels]
end

"""
    kmeans_segmentation(slabels, sfeatures, init)
Segment superpixels by k-means into k-classes, returning labels
per superpixel and also per pixels. `init` is used for k-means initialization.
"""
function kmeans_segmentation(slabels::SuperpixelImage, sfeatures::Array{T}, init::Array) where T
    tic()
    i = 1
    prototypes = convert(Array{T}, init)
    r0=kmeans!(sfeatures, prototypes)
   
    @debugprintln("k-means took $(toq()) s, cost $(r0.totalcost)")
    if !r0.converged
        @debugprintln_with_color(:red ,"k-means did not converge.")
        return Int64[],Image[]
    end
    # get pixelwise segmentation for display
    c=convert(Vector{LabelType},r0.assignments)
    #p=Images.Image(c[slabels],spatialorder=Images.spatialorder(slabels))
    #return r0.assignments,p::LabelImage
    return r0.assignments,c[slabels]::LabelImage
end

"""
    kmeans_segmentation(slabels, sfeatures, k, iter = 1)
Segment superpixels by k-means into k-classes, returning labels
per superpixel and also per pixels. kmeans is initialized randomly.
"""
function kmeans_segmentation(slabels::SuperpixelImage, sfeatures::Array, k::Integer, iter::Integer = 1)
    @assert(k<256)
    #tic()
    i = 1
    r0=kmeans(sfeatures,k; init = :kmpp, display=:none)
    while i < iter
        r=kmeans(sfeatures,k; init = :kmpp, display=:none)
        if !r0.converged
            r0 = r
        elseif r0.converged && r.totalcost < r0.totalcost
            r0 = r
        end
        i += 1
    end
    @debugprintln("k-means took $(toq()) s, cost $(r0.totalcost)")
    if !r0.converged
        @debugprintln_with_color(:red ,"k-means did not converge.")
        return Int64[],Image[]
    end
    # get pixelwise segmentation for display
    c=convert(Vector{LabelType},r0.assignments)
    #p=Images.Image(c[slabels], spatialorder = copy(Images.spatialorder(slabels)), pixelspacing = copy(Images.pixelspacing(slabels)))
    #return r0.assignments,p::LabelImage
    return r0.assignments,c[slabels]::LabelImage
end

"""
    slic_kmeans(img, spcount, compact, k, outtype = Any, bLab = true, kmeansrep = 1)
Call [`SLICsuperpixels.slicMeans`](@ref) and [`kmeans_segmentation`](@ref) return everything.
"""
function slic_kmeans(img, spcount, compact, k, outtype::DataType = UInt16, bLab = true, kmeansrep = 1)
    labels, features, count = SLICsuperpixels.slicMeans(img, spcount, compact, bLab, outtype)
    assignments, k_labels = kmeans_segmentation(labels, features, k, kmeansrep)
    return labels, features, count, assignments, k_labels
end

"""
    slic_size_kmeans(img, spsize, compact, k, outtype = Any, bLab = true, kmeansrep = 1)
Call [`SLICsuperpixels.slicSizeMeans`](@ref) and [`kmeans_segmentation`](@ref) return everything.
"""
function slic_size_kmeans(img, spsize, compact, k, outtype::DataType = UInt16, bLab = true, kmeansrep = 1)
    @info "Calculating superpixels"
    labels, features, count = SLICsuperpixels.slicSizeMeans(img, spsize, compact, bLab, outtype)
    @info "Calculating kmeans"
    assignments, k_labels = kmeans_segmentation(labels, features, k, kmeansrep)
    return labels, features, count, assignments, k_labels
end

"""
    img2array(img)
Convert a colour RGB image to an UInt8 array
"""
function img2array(img)
    return reinterpret(UInt8,convert(Array,separate(img)))
end

"""
    array2img(img)
Convert an UInt8 array to a color RGB image.
"""
function array2img(img)
    return convert(Image{RGB}, reinterpret(Colors.Ufixed8, img))
end

"""
    majority(x)
Return the most frequent component of the array.
If no parameter has the majority, the largest is returned.
Sort the array and find the longest run.
"""
function majority(x::Vector)
    lx=length(x)
    @assert(lx>0)
    sort!(x)
    runval=x[1]
    runlen=1 ;
    bestrunval=runval ; bestrunlen=runlen ;
    for i=2:lx
        if x[i]==runval
            runlen+=1
            if runlen>bestrunlen
                bestrunlen=runlen ; bestrunval=runval
            end
        else
            runval=x[i] ; runlen=1
        end
    end
    return bestrunval
end

function downsample_majority(f::Array{T,2}) where T
    # create an image half the size of the original one, each new pixel will be the majority vote of the four old ones
    #println("specialized_downsample_majority for arrays")
    sy=div(size(f,1),2) # new image size
    sx=div(size(f,2),2)
    g=zeros(UInt8,(sy,sx))
    for ix=1:sx
        for iy=1:sy
            g[iy,ix]=majority(reshape(f[iy*2-1:iy*2,ix*2-1:ix*2],4))
        end
    end
    return g
end

"""Heuristic to find which dimensions to downsample to get similar pixelspacing acros dimensions"""
function get_resample_dims(pxsp)
    minpxsp = minimum(pxsp)
    r = Int64[]
    for i = 1:length(pxsp)
        if (minpxsp / pxsp[i]) >= .75
            r = [r; i] # Q:some better way to append?
        end
    end
    return r
end

"""Heuristic to find which dimensions to downsample to get similar pixelspacing acros images and dimensions"""
function get_resample_dims(pxsp1, pxsp2)
    minpxsp = min(minimum(pxsp1), minimum(pxsp2))
    r1 = Int64[]
    r2 = Int64[]
    for i = 1:length(pxsp1)
        if (minpxsp / pxsp2[i]) >= .75
            r2 = [r2; i]
        end
        if (minpxsp / pxsp1[i]) >= .75
            r1 = [r1; i]
        end
    end
    return r1, r2
end

function get_resample_factor(pxsp)
    minpxsp = minimum(pxsp)
    r = ones(Int64, size(pxsp))
    inds = get_resample_dims(pxsp)
    r[inds] = 2
    return r
end

function get_resample_factor(pxsp1, pxsp2)
    #r1 = ones(Int64, size(pxsp1))
    #r2 = ones(Int64, size(pxsp2))
    r1 = ones(Int64, length(pxsp1))
    r2 = ones(Int64, length(pxsp2))
    inds1, inds2 = get_resample_dims(pxsp1, pxsp2)
    r1[inds1] = 2
    r2[inds2] = 2
    return r1, r2
end

function majority4(v::Vector{T}) where T
    # return the most frequent element of a four element vector v
    # if several elements have the same frequence, return any of them
    @debugassert(length(v)==4)
    c1=(v[1]==v[2])+(v[1]==v[3])+(v[1]==v[4])
    if c1==3 return v[1] end # all elements are the same, the most frequent case
    c2=(v[2]==v[1])+(v[2]==v[3])+(v[2]==v[4])
    c3=(v[3]==v[1])+(v[3]==v[2])+(v[3]==v[4])
    c4=(v[4]==v[1])+(v[4]==v[2])+(v[4]==v[3])
    # now find the maximum of c1,c2,c3,c4 and return the appropriate one
    if c1>c2
        if c3>c4 # candidates are c1 and c3
            if c1>c3 return v[1] else return v[3] end
        else     # candidates are c1 and c4
            if c1>c4 return v[1] else return v[4] end
        end
    else 
        if c3>c4 # candidates are c2 and c3
            if c2>c3 return v[2] else return v[3] end
        else # candidates are c2 and c4
            if c2>c4 return v[2] else return v[4] end
        end
    end
end


# Images.Image{T,N} 
@generated function downsample_majority(f::Image{T,N}, fac::Vector{Int} = Int[]) where {T,N}
    # create an image half the size of the original one, each new pixel will be the majority vote of the four old ones
quote
    if fac == []
        r = get_resample_factor(Images.pixelspacing(f))
    else
        r = fac
    end

    @nexprs $N j->(s_j = Integer(div(size(f, j), r[j]))) # new image size
    s = @ntuple $N s
    g = similar_with_pixelspacing(f, s,pixelspacing(f) .* r)
    ex = 0
    @nexprs $N j->(ex += r[j] - 1)
    nhood = Array(T, 2 ^ Integer(ex))
    @nloops $N i g begin
        inx = 1
        @nloops $N j d->(r[d] == 2 ? (2 * i_d - 1:2 * i_d) : i_d) begin
            nhood[inx] = (@nref $N f j)
            inx += 1
        end
        (@nref $N g i) = majority(nhood)
    end
    return g
end
end

function downsample_majority(f::Image{T,2}, fac::Vector{Int} = Int[]) where T
    # create an image half the size of the original one, each new pixel will be the majority vote of the four old ones
    if fac == []
        r = get_resample_factor(Images.pixelspacing(f))
    else
        r = fac
    end
    sy=div(size(f,1),2) # new image size
    sx=div(size(f,2),2)
    g = similar_with_pixelspacing(f, (sy,sx),pixelspacing(f) .* 2) # g=zeros(UInt8,(sy,sx))
    v=zeros(T,4)
    @inbounds  @simd for ix=1:sx
        @inbounds @simd for iy=1:sy
            # g[iy,ix]=majority(reshape(f[iy*2-1:iy*2,ix*2-1:ix*2],4))
            # specializing majority for 4 elements
            iy2=iy*2 ; ix2=ix*2
            v[1]=f[iy2-1,ix2-1]
            v[2]=f[iy2-1,ix2]
            v[3]=f[iy2,ix2-1]
            v[4]=f[iy2,ix2]
            g[iy,ix]=majority4(v)
        end
    end
    return g
end

function rotation_matrix(degrees)
    # create a 2D rotation matrix given an anticlockwise angle in degrees
    f=pi*degrees/180.
    cf=cos(f) ; sf=sin(f)
    return [ cf sf ; -sf cf ]
end

function create_rigid_transform(imgsize,degrees,scale=1.0)
    # imgsize is an image size
    # shift vector is given in [y x] order
    @assert length(imgsize)==2 # 2D only for the moment
    c= 0.5*([ imgsize[1] ; imgsize[2] ].+1)  # center coordinates
    A=scale*rotation_matrix(degrees)
    return (A,c-A*c)
end

# TODO: get rid of the if and find out how to write this function generically
function larger_size(size1,size2)
    # return the larger of the sizes
    if length([size1...]) == 2
        return @ntuple 2 k->max(size1[k],size2[k])
    elseif length([size1...]) == 3
        return @ntuple 3 k->max(size1[k],size2[k])
    end
    error("Unsuported dimension")
end

# function extend_to_size(img::Image,sz)
#     # extend image img (represented by an array) to size sz
    
#     s=size(img)
#     if s==sz
#         return img #no operation
#     end
#     sa=[s...] ; sza=[sz...] # tuple to array
#     @assert all(sa.<=sza)
#     prepad=div(sza-sa,2)
#     postpad=sza-sa-prepad
#     r=padarray(img,prepad,postpad,"value",zero(eltype(img)))
#     @assert size(r)==sz
#     return r
# end

remove_offset(a :: OffsetArrays.OffsetArray) = a.parent
remove_offset(a :: AbstractArray) = a

function extend_to_size(img::Image, sz, outsideval=zero(eltype(img)))
    # extend image img to size sz
    s = size(img)
    if s == sz
        return img #no operation
    end
    sa = [s...] ; sza = [sz...] # tuple to array
    @debugassert all(sa .<= sza)
#    if (pre && post)
        prepad=div.(sza - sa, 2)
        postpad=sza - sa - prepad
    # elseif pre
    #     prepad = sza - sa
    #     postpad = zeros(prepad)
    # else
    #     postpad = sza - sa
    #     prepad = zeros(postpad)
    # end
    #r=padarray(img, prepad, postpad, "value", outsideval)
    r=remove_offset(ImageFiltering.padarray(img,ImageFiltering.Fill(outsideval,prepad,postpad)))
    @debugassert size(r)==sz
    return r # Images.copyproperties(img,r) 
end

#function extend_to_size(img::Image, sz)
#    # extend image img to size sz
#    return extend_to_size(img, sz, zero(eltype(img)))
#end


function combine_images(i1::AbstractArray{T},i2::AbstractArray{T}) where T<:Integer
    println("combine_images for Integers ",T)
#    if length(size(i1))==2
#        return i1.+i2
#    else
        return div.(i1.+i2,2)
#    end
end

function combine_images(i1::AbstractArray{T},i2::AbstractArray{T}) where T<:ColorTypes.Gray
#    println("combine_images for Integers ",T)
#    if length(size(i1))==2
#        return i1.+i2
    #    else
    return Images.colorview(ColorTypes.RGB,i1,i2,Images.zeroarray)
end



function combine_images(i1::AbstractArray{T},i2::AbstractArray{T}) where T<:Number
    println("combine_images for Numbers ",T)
    half=convert(T,0.5)
    return half .* ( i1.+i2 )
#        return i1.+i2
#    else
#        return div(i1.+i2,2)
#    end
end



const ColorType{T} = RGB{T}
const ColorImgType{T,N} = Image{ColorType{T},N}
const GrayImgType{T,N} = Image{Gray{T},N}

function mean_color(x::ColorType{T},y::ColorType{T}) where T
    return ColorType{T}(0.5*x.r+0.5*y.r,0.5*x.g+0.5*y.g,0.5*x.b+0.5*y.b)
end

# function combine_images{T,N}(f::Image{T,N},g::Image{T,N})
#     @assert(size(f)==size(g))
#     # @bp
#     # r = Images.Image(Array(RGB{T},size(f)), {"spatialorder"=>spatialorder(f), "pixelspacing"=>pixelspacing(f)})
#     # for i=1:length(f)
#         # r[i]=RGB(f[i].val, g[i].val, 0)
#     # end
#     # return r
#     return Image(Images.Overlay((f,g), (RGB(1,0,0), RGB(0,1,0))), Dict{Any,Any}("spatialorder"=>spatialorder(f), "pixelspacing"=>pixelspacing(f)))
# end

function combine_images(f::Image{LabelType,N},g::Image{LabelType,N}) where N
    # try to guess if f,g are label images
    #println("combine_images for LabelType called")
    kf=maximum(f) ; kg=maximum(g)
    if kf<=10 && kg<=10
        # println("converting to RGB")
        return combine_images(classimage2rgb(f,kf),classimage2rgb(g,kg))
    end
    return combine_images(reinterpret(LabelPixelType,f),reinterpret(LabelPixelType,g))
end        
        
function combine_images(f::ColorImgType{T,N},g::ColorImgType{T,N}) where {T,N}
    @assert(size(f)==size(g))
    #println("combine_images for ColorImgType ", T)
    r=Images.similar(f)
    for i=1:length(f)
        r[i]=mean_color(f[i],g[i])
    end
    return r
end

function overlay_same_pixelspacing(img1,img2)
    s = larger_size(size(img1),size(img2))
    ie1 = extend_to_size(img1, s, zero(eltype(img1)))
    ie2 = extend_to_size(img2, s, zero(eltype(img2)))
    return combine_images(ie1,ie2)
end

"""
    mosaic(img1, img2, ssizei)
Return chessbord mosaic of `img1` and `img2`. `ssizei` is a size of one patch in px.
"""
@generated function mosaic(img1::Image{T,N}, img2::Image{T,N}, ssizei) where {T,N}
quote
    if length(ssizei) == 1
        ssize = ones($N) * ssizei
    else
        ssize = ssizei
    end
    pos = Array(Bool, $N)
    img3 = similar(img1)
    @nloops $N i img1 begin
        pod = true
        @nexprs $N j->(pos[j] = isodd(round(Int, div(i_j, ssize[j]))))
        @nexprs $N j->(pod = pos[j] ? pod : !pod)
        if pod#all(pos) || all(!pos)
            (@nref $N img3 i) = (@nref $N img2 i)
        else
            (@nref $N img3 i) = (@nref $N img1 i)
        end
    end
    return img3
end
end

"""
    classimage2rgb{(img, k)
Given a class image with values ``0,1,\\ldots k``,
create an RGB UInt8 image of the same size for visualization, mapping class to
colors. 0 always maps to white.
"""
function classimage2rgb(img::Image{T,N},k::Integer) where {T<:Integer,N}
    @assert(maximum(img)>=0)
    @assert(minimum(img)<=k)
    # first prepare the color table
    trgb=zeros(RGB{FixedPointNumbers.Normed{UInt8,8}},k+1)
    trgb[1] = RGB(1, 1, 1) # 0 maps to white
    for i=1:k
        trgb[i + 1] = convert(RGB, HSV(360 / k * i, 1., 1.))
    end
    return trgb[img .+ 1]
#    iout = Images.copyproperties(img, trgb[img + 1])
#    iout["colorspace"] = "sRGB"
#   return iout
end

# JK: the following no longer works: failing with "generated function body is not pure"
#     making a specialized version below
# @generated function draw_superpixels!{T,U,N}(img::Image{T,N}, p::Image{U,N}, bndcolor)
# quote
#     f(crd) = (@nref $N img i->(crd[i])) = bndcolor
#     boundary_pixels!(f, p)
# end
# end

function draw_superpixels!(img::Image{T,2}, p::Image{U,2}, bndcolor) where {T,U}
    f(crd) = ( img[crd[1],crd[2]]=bndcolor )
    boundary_pixels!(f, p)
end
    
function boundary_pixels(a::Matrix)
    # given an image, find boundaries between areas of different values
    # and return their coordinates (y,x) using the iterator protocol
    # the boundaries (which are between pixels) are shifted to lower coordinates
    ny=size(a,1) ; nx=size(a,2)
    for ix=1:nx-1
        for iy=1:ny-1
            a0=a[iy,ix]
            if a0!=a[iy,ix+1] || a0!=a[iy+1,ix] || a0!=a[iy+1,ix+1]
                produce(iy,ix)
            end
        end
    end
end


function boundary_pixels!(f::Function, img::Image{T,2}) where T
    # given an image, find boundaries between areas of different values
    # and calls f(y,x) for each of the pixels.
    # the boundaries (which are between pixels) are shifted to lower coordinates
    crd = Array{Int}(2)
    for crd[2]=1:size(img, 2) - 1
        for crd[1]=1:size(img, 1) - 1
            if (   img[crd[1], crd[2]] != img[crd[1], crd[2] + 1]
                || img[crd[1], crd[2]] != img[crd[1] + 1, crd[2]]
                || img[crd[1], crd[2]] != img[crd[1] + 1, crd[2] + 1])
                f(crd)
            end
        end
    end
end


function boundary_pixels!(f,img::Image{T,3}) where T
    # given an image, find boundaries between areas of different values
    # and calls f(y, x, z) for each of the pixels.
    # the boundaries (which are between pixels) are shifted to lower coordinates
    crd = Array(Int, 3)
    for crd[3]=1:size(img, 3) - 1
        for crd[2]=1:size(img, 2) - 1
            for crd[1]=1:size(img, 1) - 1
                if (   img[crd[1], crd[2], crd[3]] != img[crd[1] + 1, crd[2], crd[3]]
                    || img[crd[1], crd[2], crd[3]] != img[crd[1], crd[2] + 1, crd[3]]
                    || img[crd[1], crd[2], crd[3]] != img[crd[1] + 1, crd[2] + 1, crd[3]]
                    || img[crd[1], crd[2], crd[3]] != img[crd[1], crd[2], crd[3] + 1]
                    || img[crd[1], crd[2], crd[3]] != img[crd[1] + 1, crd[2], crd[3] + 1]
                    || img[crd[1], crd[2], crd[3]] != img[crd[1], crd[2] + 1, crd[3] + 1]
                    || img[crd[1], crd[2], crd[3]] != img[crd[1] + 1, crd[2] + 1, crd[3] + 1])
                    f(crd)
                end
            end
        end
    end
end

sqr = x -> x*x # square a number

const black = colorant"black"
const white = colorant"white"
const gray = colorant"gray"

@generated function draw_box!(img::Image{T,N}, x0, h=3, col=black) where {T,N}
    # set pixels in a circle of radius h around (x0,y0) to black
    # in an RGB image "img"
    # @assert(ndims(img)==3 && size(img,3)==3 )
quote
    if eltype(img) != typeof(col)
        col = minimum(img)
    end
    n=size(img)
    h2=h*h
    @nloops $N i d->max(1,min(n[d],x0[d]-h)):max(1,min(n[d],x0[d]+h)) begin
        sum2 = 0
        @nexprs $N j->(sum2 += sqr(i_j-x0[j]))
        if sum2 <= h2
            (@nref $N img i)=col
        end
    end
end    
end

@generated function draw_line!(img::Image{T,N}, x0, v, hmin=-6, hmax=6, thickness = 1, col=white) where {T,N}
    # sets pixels on a one-pixel wide line from (x0,y0)+hmin*v to
    # (x0,y0)+hmax*v to color "col" (white by default)
quote
    if eltype(img) != typeof(col)
        col = maximum(img)
    end
    n=size(img)
    thick = round(thickness)
    thick = (max(thick, 1.) - 1) / 2
    @nloops $N i d->(-thick:0.5:thick) begin
        for h=hmin:hmax
            @nexprs $N j->(x_j = round(Int,x0[j] + i_j + v[j] * h))
            if (@nall $N j->(x_j >= 1)) && (@nall $N j->(x_j <= n[j]))
                (@nref $N img x) = col
            end
        end
    end
end
end

function assertxyorder(img::Image)    
  # Make sure that an image is in the x,y spatial order
  sp=img.properties["spatialorder"]
  assert(sp[1]=="x" && sp[2]=="y")
end

function numgrad(f,x0::Vector{Float64},step=1e-3)
    # numerical finite difference derivative
    d=length(x0)
    g=zeros(Float64,d)
    y0=f(x0)
    for i=1:d
        x=copy(x0)
        x[i]+=step
        y=f(x)
        g[i]=(y-y0)/step
    end
    return g
end

function numgradc(f,x0::Vector{Float64},step=1e-3)
    # numerical finite difference derivative
    d=length(x0)
    g=zeros(Float64,d)
    for i=1:d
        x=copy(x0)
        x[i]-=step/2
        y0=f(x)
        x=copy(x0)
        x[i]+=step/2
        y=f(x)
        
        g[i]=(y-y0)/step
    end
    return g
end

function numJacobian(f,x0::Vector{Float64},step)
    # numerical Jacobian for a vector function f
    y0=f(x0)::Vector{Float64}
    n=length(y0) ; m=length(x0)
    J=zeros(Float64,n,m)
    for i=1:n
        J[i,:]=numgrad(x->f(x)[i],x0,step)
    end
    return J
end

"""
    clip(x, minx, maxx)
Clip value x between minx and maxx.
"""
function clip(x::T, minx::T, maxx::T) where {T<:Number}
    @debugassert(minx<=maxx)
    if x>maxx
        return maxx
    elseif x<minx
        return minx
    else
        return x
    end
end

"""
    clip_zero_one(x)
Clip value x between 0 and 1
"""
function clip_zero_one(x::T) where {T<:Number}
    return clip(x,zero(T),one(T))
end


#Converts segmentation to binary one. Background-0 is found as the majority of the corner points
function make_segmentation_binary(img::Image{T,2}) where T
    outim = Image(zeros(UInt8, size(img)))
    bglabel = majority([img[1, 1], img[1, end], img[end, 1], img[end, end]])
    for i= 1:length(img)
        if img[i] != bglabel
            outim[i] = 1
        end
    end
    return outim
end

#Returns image with contour defined as value change in seg, resamples date if desired
function create_image_with_contour(img::Image{T,2}, seg::Image{U,2}, contval::T, approxsize::Integer = 0) where {T,U}
    if approxsize == 0 || 1.5 * approxsize >= maximum(size(img))
        imgout = copy(img)
        segout = seg
    else
        imgout = restrict(img)
        segout = downsample_majority(seg)
        while 1.5 * approxsize <= maximum(size(imgout))
            imgout = restrict(img)
            segout = downsample_majority(seg)
        end
    end
    boundary_pixels!(segout) do y,x # mark boundaries gray
        imgout[y,x,:] = contval
    end
    return imgout
end

#Returns image with contour defined as value change in seg, resamples date if desired
function create_image_with_contour(img::Image{T,2}, seg::Image{U,2}, approxsize::Integer = 0) where {T,U}
    return create_image_with_contour(img, seg, zero(typeof(img[1])), approxsize)
end

function kernelize_size(fdim::Integer, order::Integer)
# Given order and feature vector length returns length of the kernelized featore vector
    if order == 1
        return fdim
    elseif order == 2
        return div(fdim * (fdim + 1), 2)
    elseif order == 3
        return div(fdim * (fdim + 1) * (fdim + 2), 6)
    else
        error("kernelize: Unsupported order")
    end
end

function kernelize(v::Vector, order::Integer)
# Given a vector, apply a polynomial "kernel" of a given order to it.
# We assume that v[1]=1.
    n = length(v)
    y = zeros(kernelize_size(n, order))
    kernelize!(y, v, order)
    return y
end

function kernelize!(y::Vector, v::Array, order::Integer)
# Given a vector, apply a polynomial "kernel" of a given order to it. and put the result in y
# We assume that v[1]=1.
    @debugassert(v[1] == 1)

    if order == 1
        for i = 1:length(v)
            y[i] = v[i]
        end
    elseif order == 2
        l = 1
        for i = 1:length(v)
            for j = i:length(v)
                y[l] = v[i] * v[j]
                l += 1
            end
        end
        @debugassert(l == length(y) + 1)
    elseif order == 3
        l = 1
        for i = 1:length(v)
            for j = i:length(v)
                for k = j:length(v)
                    y[l] = v[i] * v[j] * v[k]
                    l += 1
                end
            end
        end
        @debugassert(l == length(y) + 1)
    else
        error("kernelize: Unsupported order")
    end
    return nothing
end

#Returns Dict of overlaping labels (key as tuple) and area of the overlap in pixels (value)
#TODO segmentations of different size and/or pixel size
@generated function segmentation_overlap_dict(s1::Image{T,N}, s2::Image{T,N}) where {T,N}
quote
    lablabcnt = Dict{Tuple{T, T}, Int}()
    #TODO enable prelocation
    #sizehint!(lablabcnt, napprax)
    @nloops $N i s1 begin
        if (@nref $N s1 i)!=0 && (@nref $N s2 i) != 0
            lp = ((@nref $N s1 i), (@nref $N s2 i))
            cnt = get(lablabcnt, lp, 0) + 1
            setindex!(lablabcnt, cnt, lp)
        end
    end
    return lablabcnt
end
end

#Returns array of overlaping labels first two items and area of the overlap in pixels third item
function segmentation_overlap_array(s1::Image{T,N}, s2::Image{T,N}) where {T,N}
    oDict = segmentation_overlap_dict(s1, s2)
    sArr = Array(Int, length(oDict), 3)
    i = 1
    for it in oDict
        sArr[i, 1] = it[1][1]
        sArr[i, 2] = it[1][2]
        sArr[i, 3] = it[2]
        i += 1
    end
    return sArr
end

@generated function superpixels_in_range(si::Image{SuperpixelType,N}, centermm::Vector{Float64}, rmm::Number) where N
#find all superpixels closer than rmm (from centermm)
quote
    labcnt = Dict{SuperpixelType, Int64}()
    pxsp::Array{Float64, 1} = copy(Images.pixelspacing(si))
    centerpx = centermm ./ pxsp + .5
    rpx = rmm./ pxsp
    rmm2 = rmm * rmm
    lowlim = Array(Int,$N)
    uplim = Array(Int,$N)
    for i = 1:$N
        lowlim[i] = max(1, round(Int, centerpx[i] - rpx[i]))
        uplim[i] = min(size(si, i), round(Int, centerpx[i] + rpx[i]))
    end
    @nloops $N i d->(lowlim[d]:uplim[d]) begin
        distmm2 = 0.
        @nexprs $N k->(distmm2 += ((centerpx[k] - i_k - .5) * pxsp[k]) ^ 2)
        if distmm2 < rmm2 && (@nref $N si i) != 0
            cnt = get(labcnt, (@nref $N si i), 0) + 1
            setindex!(labcnt, cnt, (@nref $N si i))
        end
    end
    return labcnt
end
end

function nan_to_num!(e::Array{T,N}) where {T,N}
# replaces all NaNs with 0, -Inf with -maxfloat and Inf with maxfloat
    for j = 1:length(e)
        if isnan(e[j])
            e[j] = zero(T)
        elseif isinf(e[j])
            e[j] = sign(e[j]) * prevfloat(typemax(T))
        end
    end
end

function norm2(v::Vector{T}) where {T<:Number}
    #BLAS.nrm2(v)
    s=zero(T)
    for x in v
        s+=x*x
    end
    #reduce(:+,map v do x->x*x)
    return s
end

function dist2(v::Vector{T},w::Vector{T}) where {T<:Number}
    s::T=zero(T)
    n=length(v)
    @debugassert(length(w)==n)
    for i=1:n
        val::T=v[i]-w[i]
        s+=val*val
    end
    return s
end

""" find a minimum of a function 'f' over a sequence 'seq', returns the argument.
    The sequence must not be empty """                
function argmin(f,seq)
    bestx=first(seq) ; bestval=f(bestx)
    for x in seq
        y=f(x)
        if y<bestval
            bestval=y ; bestx=x
        end
    end
    return bestx
end

function argminval(f,seq)
    bestx=-Inf ; bestval=Inf
    for x in seq
        y=f(x)
        if y<bestval
            bestval=y ; bestx=x
        end
    end
    return bestx,bestval
end

function quantize(img::Image, quantQ::Int)
    @assert(quantQ > 0 && quantQ < 256)
    img = copyproperties(img, convert(Array{Float64}, data(img)))
    img -= minimum(img)
    img /= maximum(img) / (quantQ - 1)
    img += 1
    return copyproperties(img, round(UInt8, data(img)))
end

"""
    map_class!(img:, f)
Maps img values according to the function `f`.
"""
function map_class!(img::LabelImage, f::Function)
    for i = 1 :length(img)
        img[i] = f(img[i])
    end
end

"""
    mask_image!(img, mask, maskval, newval, background)
Set all the pixels in `img` to `newval` if coresponding pixel of mask == `maskval`.
Other pixels are set to `background`
"""
function mask_image!(img::Image{T1,N}, mask::Image{T2,N}, maskval, newval, background) where {T1,T2,N}
    for i = 1 :length(img)
        if mask[i] == maskval
            img[i] = newval
        else
            img[i] = background
        end
    end
end

function mask_image!(img::Image{T1,N}, mask::Array{T2,N}, maskval, newval, background) where {T1,T2,N}
    for i = 1 :length(img)
        if mask[i] == maskval
            img[i] = newval
        else
            img[i] = background
        end
    end
end

"""
    mask_image!(img, mask, maskval, background)
Set all the pixels in `img` to `background` if coresponding pixel of mask != `maskval`.
Other pixels are not changed.
"""
function mask_image!(img::Image{T1,N}, mask::Image{T2,N}, maskval, background) where {T1,T2,N}
    for i = 1 :length(img)
        if mask[i] != maskval
            img[i] = background
        end
    end
end

# function mask_image!{T1,T2,N}(img::Image{T1,N}, mask::Array{T2,N}, maskval, background)
#     for i = 1 :length(img)
#         if mask[i] != maskval
#             img[i] = background
#         end
#     end
# end

"""
    object_centroid(p, thr)
Finds centroid of the pixels > 0.
"""
function object_centroid(p::Image{T,2}, thr=zero(eltype(p))) where {T}
    cnt = 0
    mpos = zeros(Float64, 2)
    for y = 1:size(p, 2)
        for x = 1:size(p, 1)
            if p[x, y] > thr
                mpos[1] += x
                mpos[2] += y
                cnt += 1
            end
        end
    end
    if cnt==0
        @error "No pixels over threshold $(thr)"
    end
    return mpos./cnt
end

"""
    object_cov_mat(p, cog, thr)
Computes the covariance matrix of the objects vith intensity > `thr`,
`cog` is the object centroid obtained from `object_centroid` function.
"""
function object_cov_mat(p::Image{T,2}, cog, thr=zero(eltype(p))) where {T}
    cm = zeros(Float64, 2, 2)
    cnt = 0
    for y = 1:size(p, 2)
        for x = 1:size(p, 1)
            if p[x, y] > thr
                cm[1, 1] += (x - cog[1])^2
                cm[2, 1] += (x - cog[1]) * (y - cog[2])
                cm[2, 2] += (y - cog[2])^2
                cnt += 1
            end
        end 
    end
    cm[1, 2] = cm[2, 1]
    return cm ./ cnt
end

"""
    get_rot(H)
Computes the rotation matrix from the covariance martix `H` using svd.
"""
function get_rot(H)
    u,s,v = svd(H)
    R = v*u'
    if det(R) < -.5 #if R is reflection
        v = v * [1 0; 0 -1]
        R = v*u'
    end
    return R
end

"""
    `with_pixelspacing(img,p)`

    Given an AbstractArray `img` of dimension `d`, create AxisArray sharing the same data but with pixelspacing set
    to `p` (of length `d`)
"""
function with_pixelspacing(img::AbstractArray{T,N},p) where {T,N}
    @assert length(p)==N
    return AxisArrays.AxisArray(img,axisnames(img),to_tuple(p))
end

""" convert array to tuple if needed """
to_tuple(t::T) where T <: Tuple = t
to_tuple(t::AbstractArray{T,1}) where T = tuple(t...)

""" default axis names """
axisnames(img::AbstractArray{T,1}) where T = (:x) 
axisnames(img::AbstractArray{T,2}) where T = (:x,:y) 
axisnames(img::AbstractArray{T,3}) where T = (:x,:y,:z) 
axisnames(img::AbstractArray{T,5}) where T = (:x,:y,:z,:t) 

""" `similar_with_pixelspacing(img,outpsize,[pixelspacing])` creates an unitialized image of the same type and with the same pixelspacing as `img`, but with given size `outpsize` in pixels. Pixelspacing can be altered by an optional argument """
function similar_with_pixelspacing(img::AbstractArray,outpsize,pixspc=pixelspacing(img))
    return with_pixelspacing(similar(img,outpsize),pixspc)
end



# convert a pixel value (usually scalar grayscale  or RGB color) to a vector
pixel_to_vector(x::Images.Gray)=float(x)

pixel_to_vector(x::T) where T<:Number =float(x)

pixel_to_vector(x::T) where T<:Images.RGB = [ x.r, x.g, x.b ]

""" get_with_default(x,d) is a macro, returning 'x' unless 'x===nothing' when it returns 'd' """
macro get_with_default(x,d)
    return quote
        r=$(esc(x))
        ((r!==nothing) ? r : $(esc(d)))
    end
end


end # module

