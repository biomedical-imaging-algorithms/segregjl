"""
!!! warning "Obsolete"
    This module is obsolete, use more generic [readMetaimage](@ref). The parameters have to be changed.

read RIRE image files these are the two part files header.ascii + image.bin
see http://www.insight-journal.org/rire/data_format.php

Jan Kybic, June 2015

the text file parsing comes from Brandon K. Miller
https://github.com/brandonkmiller/ConfParser.jl/blob/master/src/ConfParser.jl
"""
module readRIRE

import Images
import ImageView

function read_header(filename::AbstractString)
    # read the "header.ascii" file and parse the relevant items into a dictionary
    # println("readRIRE: Opening file ", filename)
    fh=open(filename,"r")
    data=Dict{AbstractString,AbstractString}()
    for line in eachline(fh)
         # skip empty lines
        if (ismatch(r"^\s*\n", line))
            continue
        end
        line = chomp(line) # trim trailing  whitespace
        # parse key/value
        m = match(r"^\s*([^:=]*?)\s*:=\s*(.*?)\s*$",line)
        if (m != nothing)
            key::String, value::String = m.captures
            data[key]=value
        else
            error("invalid syntax on line: \"$(line)\"")
        end
    end
    return data
end

function read_data(directory::AbstractString)
    # in 'directory', read 'header.ascii' and 'image.bin'
    # return an image
    warn("This method is obsolete, use readMetaimage.read_data.")
    println("readRIRE: Reading data from directory ", directory)
    d=read_header(joinpath(expanduser(directory),"header.ascii"))
    assert(parse(Int,d["Image dimensions"])==3) # only 3D supported for the moment
    # extract image size
    sz=[parse(Int,d["Columns"]),parse(Int,d["Rows"]),parse(Int,d["Slices"])]
    # extract voxel dimensions
    ps=split(d["Pixel size"],":")
    pixdim=[ float(ps[1]), float(ps[2]), float(d["Slice thickness"]) ]
    # bits per voxel
    bpv=parse(Int, d["Bits allocated"])
    # now read the binary data
    datafn=joinpath(expanduser(directory),"image.bin")
    datah=open(datafn,"r")
    if bpv==16
        data=read(datah,Int16,prod(sz))
        # convert from little-endian
        bytes=reinterpret(UInt8,data)
        for i=1:prod(sz)
            tmp=bytes[2*i] ; bytes[2*i]=bytes[2*i-1] ; bytes[2*i-1]=tmp
            # dataata[i]=reinterpret(Int16,uint16(bytes[2*i-1])<<8 | uint16(bytes[2*i]))
        end
        data=reshape(data,sz...)
    elseif bpv==8
        data=read(datah,Int8,sz...)
    else
        error("Unsupported number of bits per voxel $(bpv)")
    end
    close(datah)
    # convert to grayscale image
    img=Images.grayim(data)
    img["spatialorder"]=["x", "y", "z"]
    img["pixelspacing"]=pixdim
    return img
end

function test_read_data()
    v=readRIRE.read_data("~/data/Medical/RIRE/training_001/ct")
    println(v)
    vf=float(v) ; vf-=minimum(vf) ; vf/=maximum(vf) ;
    #ImageView.view(vf)
    ImageView.view(vf,xy=["x","z","y"])
end

end
