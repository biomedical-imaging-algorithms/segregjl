""" Module called from benchmark_linprog

    Jan Kybic, 2016
"""

module benchmark_register

import Images
#import ImageView
using Options
import keypoints
import deformation
import ssd

using linprog_register
using segimgtools

#include("debugprint.jl")
#switchprinting(true)



function register(movimgname,refimgname,outputdir; paramfile="params_linprog.txt",landmarks=nothing)
    hmax=8 ; kptn=500 ; spacing=10 ; display=false ; sigma=[1.;1.]    
    #prepare destination folder
    if !isdir(outputdir)
        mkdir(outputdir)
    end

    # NOTE: ignore the parameter file for the moment

    # read images
    refimg=Images.load(refimgname)
    movimg=Images.load(movimgname)
    #if display
    #    ImageView.view(refimg, name = "static image")
    #    ImageView.view(movimg, name = "moving image")
    #end
    print("refimg size=$(size(refimg)) movimg size=$(size(movimg)) hmax=$hmax")
    t0=time()
    kptsimple=keypoints.find_keypoints_from_gradients(refimg;spacing=spacing,number=kptn,
                                                                     sigma=sigma)
    nhood=keypoints.keypoint_neighbors(kptsimple)
    kptsampled=keypoints.create_sampled_keypoints(refimg,kptsimple,hmax;spacing=float(spacing))
    #if display
    #    img1kpts=keypoints.create_image_with_keypoints(copy(refimg),kptsimple)
    #    ImageView.view(img1kpts,name="refimg with keypoints")
    #    ImageView.view(deformation.overlay(refimg,movimg),name="overlay before")
    #end
    isize=[size(movimg)...]
    imgsize=isize .* Images.pixelspacing(movimg) # image size in physical units
    def = deformation.HierarchicalDeformation([
       deformation.BsplineDeformation(3,imgsize, [32,32]), 
       deformation.BsplineDeformation(3,imgsize, [16,16]), 
       deformation.BsplineDeformation(3,imgsize, [8,8]), 
       deformation.create_identity(imgsize, deformation.AffineDeformation2D)
       ])
    t1=time()
    critparams=ssd.SSDParameters()
    crit_state=ssd.criterion_init(refimg,movimg,critparams)
    opts=@options hmax=hmax resample=true fdiffreg=nothing lambda=1e-3 maxResample=5 minKP=500 display=false maxsize=50000 minsize=128 knot_spacing=8 start_with_last_level=false
    d=register_fast_linprog_multiscale(refimg,movimg,kptsampled,def,nhood,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()
    timeTot=t2-t0
    println("Registration finished, time=$timeTot")
    #if display
        warped=deformation.transform_image_lin(d,movimg,size(refimg),@options)
    #    ImageView.view(warped,name="warped")
    #    ImageView.view(deformation.overlay(refimg,warped),name="overlay after")
    #end
    println("Creating output...")
    y  = zeros(Float64, 2)
    if landmarks != nothing
        fstr = open(landmarks, "r")
        landreg = readdlm(fstr, Float64; skipstart = 2)
        close(fstr)
        fstr = open(string(outputdir, "landmarks.txt"), "w")
        write(fstr, "point\n")
        print_shortest(fstr, size(landreg, 1))
        write(fstr, "\n")
        for poin = 1:size(landreg, 1)
            deformation.transform_point!(d, vec(landreg[poin, :]), y)
            print_shortest(fstr, y[1])
            write(fstr, " ")
            print_shortest(fstr, y[2])
            write(fstr, "\n")
        end
        close(fstr)
    end
    fstr = open(string(outputdir, "time.txt"), "w")
    print_shortest(fstr, timeTot)
    close(fstr)
    Images.save(string(outputdir, "registered.png"),warped)
    Images.save(string(outputdir, "overlay_after.png"),deformation.overlay(refimg,warped))
end    

function test_register()
    benchmark_register.register("imgs/case03-5-he-small.png","imgs/case03-3-psap-small.png",
                            "tmp/",landmarks="tmp/Case003_HE.txt")
end

end
