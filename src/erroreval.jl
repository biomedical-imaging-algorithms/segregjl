"""
Registration error/accuracy evaluation for different datasets.
"""
module erroreval

using Images
using deformation
using readMetaimage

export measure_error_RIRE, measure_error_POPI, measure_error_NIREP

include("debugassert.jl")
switchasserts(true)

function landmark_distance_RIRE{T,N}(land, img1::Images.Image{T,N}, img2::Images.Image{T,N}, d::deformation.AbstractDeformation)
    dist0mm = 0.
    dist0px = 0.
    distmm = 0.
    distpx = 0.

    println("Landmarks differences after registration and distance before->after")
    for i = 1:size(land,1)
    #file origin =[0,0,0] our origin[1,1,1]*pixelspacing(image)
        l1 = vec(land[i,5:7]) + Images.pixelspacing(img1) / 2 #second image
        l2 = vec(land[i,2:4]) + Images.pixelspacing(img2) / 2 #ct
        l3 = zeros(size(l1))
        dist0mm += sqrt(sum(((l1-l2)).^2))
        dist0px += sqrt(sum(((l1-l2) ./ Images.pixelspacing(img2)).^2))
        deformation.transform_point!(d,l1,l3)
        distmm += sqrt(sum(((l3-l2)).^2))
        distpx += sqrt(sum(((l3-l2) ./ Images.pixelspacing(img2)).^2))
        println("$(l3-l2), distance = $(sqrt(sum(((l1-l2)).^2))) -> $(sqrt(sum(((l3-l2)).^2)))")
    end
    println("Mean distance: $(dist0mm/size(land,1))->$(distmm/size(land,1)) mm, $(dist0px/size(land,1))->$(distpx/size(land,1)) px")
    return distmm/size(land,1), distpx/size(land,1)
end
    
"""
    measure_error_RIRE(img1, img2, d, landmarks)
Return mean distance of landmarks in px and mm after registration.

`landmarks` is file with landmarks provided in RIRE.
"""
function measure_error_RIRE{T,N}(img1::Images.Image{T,N}, img2::Images.Image{T,N}, d::deformation.AbstractDeformation, landmarks::AbstractString)
    land = readdlm(landmarks, ',', '\n'; skipstart = 1)
    return landmark_distance_RIRE(land, img1, img2, d)
end

"""
    measure_error_RIRE(img1, img2, d, angle, shift)
Return mean distance of virtual landmarks in px and mm after registration.

`angle` and `shift` are parameters of artificial rigid transformation. This mimics real landmarks
and real transformation.
"""
function measure_error_RIRE{T,N}(img1::Images.Image{T,N}, img2::Images.Image{T,N}, d::deformation.AbstractDeformation, angle::Array, shift::Array)
    # # measure the error for RIRE data
    t0=deformation.create_identity([size(img2)...] .* Images.pixelspacing(img2), deformation.RigidDeformation3D)
    deformation.set_theta!(t0,vcat(angle,shift))
    mul1 = ([size(img1)...] - 1) .* Images.pixelspacing(img1)
    land = zeros(Float64, 8, 7)
    land[1,1] = 1; land[1,5:7] = [0,0,0] .* mul1
    land[2,1] = 2; land[2,5:7] = [1,0,0] .* mul1
    land[3,1] = 3; land[3,5:7] = [0,1,0] .* mul1
    land[4,1] = 4; land[4,5:7] = [1,1,0] .* mul1
    land[5,1] = 5; land[5,5:7] = [0,0,1] .* mul1
    land[6,1] = 6; land[6,5:7] = [1,0,1] .* mul1
    land[7,1] = 7; land[7,5:7] = [0,1,1] .* mul1
    land[8,1] = 8; land[8,5:7] = [1,1,1] .* mul1
    for i = 1:size(land,1)
        l1 = vec(land[i,5:7]) + Images.pixelspacing(img1) / 2
        land[i, 2:4] = deformation.transform_point(t0, l1) - Images.pixelspacing(img1) / 2
    end
    return landmark_distance_RIRE(land, img1, img2, d)
end

#returns landmark coordinates in mm, image befins at 0,0,0 first pixel is at pixelspacing/2
function read_landmarks_POPI(landf, imgf)
    d=readMetaimage.read_header(imgf)
    szstr = split(d["Offset"]," ")
    pixdim = float(split(d["ElementSpacing"], " "))
    origin = Array(Float64, length(pixdim))
    
    for i = 1:3
        origin[i] = parse(Float64, szstr[i])
    end
    lp = readdlm(landf, ' ', '\n'; skipstart = 0)
    if typeof(lp) != Array{Float64, 2}
        lp = readdlm(landf, '\t', '\n'; skipstart = 0)
    end
    pxsp12 = pixdim / 2
    for j = 1:size(lp,2)
        for i = 1:size(lp,1)
            lp[i,j] = lp[i,j] - origin[j] + pxsp12[j]
        end
    end
    return lp, pixdim
end

"""
    measure_error_POPI(d, land1, land2, imgf1, imgf2)
Return mean distance of landmarks in px and mm after registration.

`land1`, `land2` are files with landmarks provided in POPI, `imgf1` and `imgf1` are image files.
"""
function measure_error_POPI(d::deformation.AbstractDeformation, land1, land2, imgf1, imgf2)
    lp1, pixdim1 = read_landmarks_POPI(land1, imgf1)
    lp2, pixdim2 = read_landmarks_POPI(land2, imgf2)

    dist0mm = 0.
    dist0px = 0.
    distmm = 0.
    distpx = 0.

    println("Landmarks differences after registration and distance before->after")
    for i = 1:size(lp1,1)
        l1 = vec(lp1[i,:]) #second image
        l2 = vec(lp2[i,:])
        l3 = zeros(size(l1))
        deformation.transform_point!(d, l1, l3)
        dist0mm += sqrt(sum(((l1-l2)).^2))
        dist0px += sqrt(sum(((l1 ./ pixdim1 - l2 ./ pixdim2)).^2))
        distmm += sqrt(sum(((l3-l2)).^2))
        distpx += sqrt(sum(((l3-l2) ./ pixdim2).^2))
        println("$(l3-l2), distance = $(sqrt(sum(((l1-l2)).^2))) -> $(sqrt(sum(((l3-l2)).^2)))")
    end
    println("Mean distance: $(dist0mm/size(lp1,1))->$(distmm/size(lp1,1)) mm, $(dist0px/size(lp1,1))->$(distpx/size(lp1,1)) px")
    return distmm/size(lp1,1), distpx/size(lp1,1)
end

"""
    measure_error_NIREP{T,N}(p1, p2, preg)
Return vector of region overlaps for all labels and overlap for the brain (everything except background)
"""
function measure_error_NIREP{T,N}(p1::Images.Image{T,N}, p2::Images.Image{T,N}, preg::Images.Image{T,N})
    # # measure the error for NIREP data
    
    maxcl = max(maximum(p1),maximum(preg))
    robef = Array(Float64, maxcl+1)
    roaft = Array(Float64, maxcl+1)
    roaft_brain = 0
    roaft_brain_cnt = 0
    for cl = 0:maxcl
        unb = 0
        intb = 0
        una = 0
        inta = 0
        for i = 1:length(p1)
            if (p1[i] == cl) | (p2[i] == cl)
                unb += 1
                if (p1[i] == cl) & (p2[i] == cl)
                    intb += 1
                end
            end
            if (p1[i] == cl) | (preg[i] == cl)
                una += 1
                if (p1[i] == cl) & (preg[i] == cl)
                    inta += 1
                end
            end
        end
        if cl != 0
            roaft_brain += inta
            roaft_brain_cnt += una
        end
        robef[cl+1] = intb/unb
        roaft[cl+1] = inta/una
        println("Relative overlap of segment $cl = $(robef[cl+1]) -> $(roaft[cl+1])")
    end
    return roaft, roaft_brain / roaft_brain_cnt
end

end #module
