"""
Geometric deformation and image warping in Julia.
Transformatios available both for 2D and 3D:
* rigid
* rigid + scale
* affine
* bspline of any order
* hierarchical that can consist of any transformations mentioned above.

Jan Kybic, Martin Dolejsi 2014 - 2016
"""
module deformation

import Images
import IterativeSolvers
using ImageView
using ..segimgtools
using ..Options
using Base.Cartesian
using FixedPointNumbers
using ColorTypes
using ..bsplines
using ..keypoints
# using MicroLogging
using Logging


using NLopt
#import Base.LinAlg.BLAS.axpy!

include("debugassert.jl")
switchasserts(true)
include("debugprint.jl")
switchprinting(true)

export overlay
export AbstractDeformation, AffineDeformation2D, ScaleDeformation2D, RigidDeformation2D
export AffineDeformation3D, RigidDeformation3D
export BsplineDeformation, HierarchicalDeformation

const degprad = pi / 180.

#Overlay comes here to avoid circular references
function overlay(img1,img2)
    ps1 = Images.pixelspacing(img1)
    ps2 = Images.pixelspacing(img2)
    ie1 = img1
    ie2 = img2
    if ps1 != ps2
        minps = min(ps1, ps2)
        ie1 = resample_to_pixelspacing(img1, minps)
        ie2 = resample_to_pixelspacing(img2, minps)
    end
    return segimgtools.overlay_same_pixelspacing(ie1,ie2)
end

#----------------------AbstractDeformation--------------------------
"""
AbstractDeformation represents the deformation function and parameters.
All defirmations should be subclass of AbstractDeformation.

We almost always use the x, y (row-major) spatial order, but some functions
can handle any spatial order.
"""
abstract type AbstractDeformation end

"""
    set_random!(c)
Set the deformation parameters randomly.
"""
function set_random!(c::AbstractDeformation)
    
    lb=lower_bound(c)
    ub=upper_bound(c)
    theta=lb+(ub-lb)*rand(length(lb))
    set_theta!(c,theta)
end

"""
    transform_point(c, inp)
Return the point `inp` transformed by `c`.

``y=C(x)``
"""
function transform_point end

function transform_point(c::T, inp::Vector{Float64}) where {T<:AbstractDeformation}
    outp = similar(inp)
    transform_point!(c, inp, outp) # this should be implemented for each class
    return outp
end

"""
    transform_point_relative!(c, inp, outp, x0)
Save the relative transformation of `inp` + `x0` to outp.

``y=C(x)-x+x_0``
"""
function transform_point_relative! end

function transform_point_relative!(c::T, inp::Vector{Float64}, outp::Vector{Float64}, x0::Vector{Float64}) where {T<:AbstractDeformation}
    #transform_point!(c, inp, outp)
    #for i = 1:length(outp)
    #    outp[i] += x0[i] - inp[i]
    #end
    tmp=transform_point(c,inp)
    for i = 1:length(outp)
        outp[i] = tmp[i]+x0[i] - inp[i]
    end
    return nothing
end

"""
    transform_normal(c, inv, inp)
Return the vector `inv` located in `inp` transformed by `c`.

``\\mathbf{v}=C(\\mathbf{u})``
!!! note
    The vector `inv` (``\\mathbf{u}``) represents a surface normal and it is always connected
    to a keypoint. So the vector starts at `inp` (``x``) and points towards
    `inp + inv` (``x+\\mathbf{u}``).
"""
function transform_normal end

function transform_normal(c::T, inv::Vector{Float64}, inp::Vector{Float64}) where {T<:AbstractDeformation}
    outv = similar(inv)
    transform_normal!(c, inv, outv, inp) # this should be implemented for each class
    return outv
end

"""
    transform_normal_relative!(c, inv, outv, inp, n0)
Finds the normal vector coordinate change inplace and add n0.

``\\mathbf{v}=C(\\mathbf{u})-\\mathbf{u} + \\mathbf{n_0}``
"""
function transform_normal_relative! end

function transform_normal_relative!(c::T, inv::Vector{Float64}, outv::Vector{Float64}, inp::Vector{Float64}, n0::Vector{Float64}) where {T<:AbstractDeformation}
    transform_normal!(c, inv, outv, inp)
    for i = 1: length(outv)
        outv[i] += n0[i] - inv[i]
    end
end

# version calling transform_point!() for compatibility
@inline transform_point_kpt!(c::T, outp::Vector{Float64}, kpts::Vector{U}, kptin::Int) where {T<:AbstractDeformation,U<:keypoints.AbstractKeypoint} =
            transform_point!(c, kpts[kptin].pos, outp)

# Transform keypoint i of pos inp using c with the data precomputed by set_keypoints!()
@inline transform_point_relative_kpt!(c::T, outp::Vector{Float64}, kpts::Vector{U}, kptin::Int, x0::Vector{Float64}) where {T<:AbstractDeformation,U<:keypoints.AbstractKeypoint} =
            transform_point_relative!(c, kpts[kptin].pos, outp, x0)

# adds contribution of one keypoint to gradient vector
function add_grad_contrib!(grad::AbstractArray{Float64,1}, dval::Float64, normal::Vector{Float64}, def::T, pos::Vector{Float64}) where {T<:AbstractDeformation}
    @debugprintln_with_color(:red, "Generic version of add_grad_contrib! is used. This could slow down the process")
    dif = deformation.get_jacobian(def, pos)
    grad[:] += dval*reshape(transpose(normal)*dif,length(grad)) / (norm2(normal))# modify in place!
    return nothing
end

"""
    is_linear(c)
Return if the transformation is a linear function of its arguments.
"""
function is_linear end

# By default it is not
@inline is_linear(c::AbstractDeformation) = false

"""
     set_level!(c, level)
Set the level for multi level deformations, used only in [`HierarchicalDeformation`](@ref).
"""
function set_level! end

# Used only in Hierarchical deformation (HierarchicalDeformation)
@inline set_level!(c::AbstractDeformation, level::Int) = nothing

"""
    get_valid_level(c)
Return the level for multi level deformations, used only in [`HierarchicalDeformation`](@ref).
"""
function get_valid_level end

#Used only in Hierarchical deformation (HierarchicalDeformation)
@inline get_valid_level(c::AbstractDeformation) = 1

# void version for compatibility
@inline set_keypoints!(c::D, kpts::Vector{T}) where {D<:AbstractDeformation,T<:keypoints.AbstractKeypoint} = nothing

"""
    transform_initial(c, kpts, i)
Return the constant element ``\\phi_0(x)`` of transform point function. This is suitable only when
``\\psi(z)`` (normal displacement) is a linear function of theta i.e. `is_linear()` returns `true`.
"""
function transform_initial end

function transform_initial(c::D, kpts::Vector{T}, i::Int) where {D<:AbstractDeformation, T<:keypoints.AbstractKeypoint}
    @debugassert(is_linear(c))
    return zeros(size(kpts[i].pos))
end

"""
    transform_initial!(y, c, kpts, i)
Inplace version of `transform_initial`. Result is stored in `y`.
"""
function transform_initial! end

function transform_initial!(y, c::D, kpts::Vector{T}, i::Int) where {D<:AbstractDeformation, T<:keypoints.AbstractKeypoint}
    @debugassert(is_linear(c))
    fill!(y, 0)
    return nothing
end

function transform_initial!(y, def::D, v::Vector{Float64}) where {D<:AbstractDeformation}
    @debugassert(is_linear(def))
    fill!(y, 0)
    return nothing
end


"""
    get_diff_regularization_matrix_RtR(c, mu)
Return ``R^TR`` where ``R=\\mu D`` and ``D`` is difference matrix of size length(theta)
``\\times`` length(theta)
"""
function get_diff_regularization_matrix_RtR end

function get_diff_regularization_matrix_RtR(c::D, mu::Float64) where {D<:AbstractDeformation}
    if mu == 0
        lt = length(get_theta(c))
        return spzeros(Float64, lt, lt)
    end
    return mu*mu*speye(Float64,length(get_theta(c)))
end

"""
    transform_image_nn!(c, imgout, imgin, opts)
Transform `imgin` by deformation `c`, put the result into `imgout`.
Use nearest neigbor interpolation. Out of bounds pixels are set to `default` value.
!!! warning
    Only 2D and 3D images supported.
"""
function transform_image_nn! end

## # this generic function transforms an image using NN interpolation
## # out of bounds pixels are set to default value
## @ngenerate N Image{T,N} function transform_image_nn!{T,N}(c::AbstractDeformation,imgout::Image{T,N},
##                                  imgin::Image{T,N},opts::Options)
##     @defaults opts default=zero(T)

##     sizein = size(imgin)
##     xin=zeros(Float64, N)        # point coordinates to imgin
##     xout=zeros(Float64, N)       # point coordinates to imgout
##     y=zeros(Int64, N)            # integer point coordinates to imgin
##     @nloops N i imgout begin   # for all pixels in output image
##         @nexprs N j->(xout[j] = (i_j - 0.5) * Images.pixelspacing(imgout)[j])
##         transform_point!(c, xout, xin) # store transformed point to xin
##         @nexprs N j->(y_j = integer(xin[j] / Images.pixelspacing(imgin)[j] + 0.5))
##         if (@nall N j->(y_j >= 1)) && (@nall N j->(y_j <= sizein[j]))
##             @inbounds (@nref N imgout i) = @nref N imgin y
##         else
##             @inbounds (@nref N imgout i) = default
##         end
##     end    
##     return imgout
## end

# explicit 2D version of NN interpolation
#function transform_image_nn!{T}(c::AbstractDeformation,imgout::Image{T,2},
#                                 imgin::Image{T,2},opts::Options)
@fastmath function transform_image_nn!(c::AbstractDeformation,imgout::Image{T,2}, 
                                 imgin::Image{T,2},opts::Opts) where {T}
    @defaults opts default=zero(T)
    #default=zero(T)
    #println("explicit 2D version of transform_image_nn!")
    sizein = size(imgin)
    sizeout = size(imgout)
    xin=zeros(Float64, 2)        # point coordinates to imgin
    xout=zeros(Float64, 2)       # point coordinates to imgout
    is1=float(Images.pixelspacing(imgout)[1])
    is2=float(Images.pixelspacing(imgout)[2])
    #it is important to use proper pixelspacing (images can have diferent pixel size)
    is1r::Float64=1. / float(Images.pixelspacing(imgin)[1])
    is2r::Float64=1. / float(Images.pixelspacing(imgin)[2])
    y1::Int=1 ; y2::Int=1 
    imgoutd=imgout.data
    for i2=1:sizeout[2]
        for i1=1:sizeout[1]
            @inbounds xout[1]=(i1-0.5)*is1::Float64
            @inbounds xout[2]=(i2-0.5)*is2::Float64
            transform_point!(c, xout, xin) # store transformed point to xin
            y1=round(Int,xin[1]*is1r+0.5)
            y2=round(Int,xin[2]*is2r+0.5) 
            if y1>=1 && y1<=sizein[1] && y2>=1 && y2<=sizein[2] 
                 @inbounds imgout[i1,i2]=imgin[y1,y2]
            else
                 @inbounds imgout[i1,i2]=default # default value for out-of-range pixels
            end
        end
    end
end 

@fastmath function transform_image_nn!(c::AbstractDeformation,imgout::Image{T,3},
                                 imgin::Image{T,3},opts::Opts) where {T}
    @defaults opts default=zero(T)
    #default=zero(T)
    println("explicit 3D version of transform_image_nn!")
    #@debugprintln("explicit 3D version of transform_image_nn!")
    sizein = size(imgin)
    sizeout = size(imgout)
    xin=zeros(Float64, 3)        # point coordinates to imgin
    xout=zeros(Float64, 3)       # point coordinates to imgout
    is1=float(Images.pixelspacing(imgout)[1])
    is2=float(Images.pixelspacing(imgout)[2])
    is3=float(Images.pixelspacing(imgout)[3])
    #it is important to use proper pixelspacing (images can have diferent pixel size)
    is1r::Float64=1. / float(Images.pixelspacing(imgin)[1])
    is2r::Float64=1. / float(Images.pixelspacing(imgin)[2])
    is3r::Float64=1. / float(Images.pixelspacing(imgin)[3])
    y1::Int=1 ; y2::Int=1 ; y3::Int=1 
    imgoutd=imgout.data
    for i3=1:sizeout[3]
        for i2=1:sizeout[2]
            for i1=1:sizeout[1]
                @inbounds xout[1]=(i1-0.5)*is1::Float64
                @inbounds xout[2]=(i2-0.5)*is2::Float64
                @inbounds xout[3]=(i3-0.5)*is3::Float64
                transform_point!(c, xout, xin) # store transformed point to xin
                y1=round(Int,xin[1]*is1r+0.5)
                y2=round(Int,xin[2]*is2r+0.5) 
                y3=round(Int,xin[3]*is3r+0.5) 
                if y1>=1 && y1<=sizein[1] && y2>=1 && y2<=sizein[2] && y3>=1 && y3<=sizein[3] 
                    @inbounds imgout[i1,i2,i3]=imgin[y1,y2,y3]
                else
                    @inbounds imgout[i1,i2,i3]=default # default value for out-of-range pixels
                end
            end
        end
    end
end 

"""
    transform_image_nn(c, img, outpsize, opts)
Return `img` transformed by `c`. `outpsize` is size of the output image, pixelspacing
of the output could be defined as optional pararameter, it is the same as in input image by default.
Out of bounds pixels are set to `default` value.
"""
function transform_image_nn(c::AbstractDeformation,img::Image{T,N},
                                 outpsize::Tuple,opts::Opts) where {T,N}
    @defaults opts default=zero(T) pixelspacing = Images.pixelspacing(img)
    # outimg=Images.similar(img, outpsize) # output image
    # TODO no longer supported, need to use ImageMetadata or AxisArrays
    # stopped here
    #println("transform_image_nn")
    outimg=segimgtools.similar_with_pixelspacing(img, outpsize)
    #println("transform_image_nn img=$(typeof(img)) outimg=$(typeof(outimg))")
    # outimg["pixelspacing"] = pixelspacing
    transform_image_nn!(c,outimg,img,opts::Opts)
    #println("transform_image_nn img=$(typeof(img)) outimg=$(typeof(outimg))")
    return outimg
end



# # # round num to integer if imgout is an array of integer type, otherwise leave it alone 
# # round_integer(imgout, num) = num
# # round_integer{T<:Number}(imgout::Image{T}, num) = round(T,num)

# # # generic function for linear interpolation
# # # only works on 2^N matrices
# # @ngenerate N T function interpolate_recursive{T,U<:Number,N}(img::Array{T,N}, point::Vector{U})
    # # @nexprs N i->(@debugassert(size(img,i)<=2))
    # # img2 = similar(img) * 1.
    # # #The if clause says: for dimension number N (outer loop)
    # # #set w to point[N] or 1-point[N] depending on i_N for inner loops do nothing
    # # @nloops N i img j->(if j == N if i_j == 2 w = point[N] else w = 1 - point[N] end else nothing end) begin
        # # (@nref N img2 i) = (@nref N img i) * w
    # # end
    # # return interpolate_recursive(squeeze(sum(img2, N), N), point[1:N-1])
# # end

# # #specialization for 1D (stop the recursion)
# # function interpolate_recursive{T,U<:Number}(img::Array{T,1}, point::Vector{U})
    # # if length(img)>1 # needed to handle the trivial case
        # # return img[1] * (1-point[1]) + img[2] * point[1]
    # # else
        # # return img[1]
    # # end
# # end
# # # this generic function transforms an image using linear interpolation
# # # out of bounds pixels are set to default value
# # @ngenerate N Image{T,N} function transform_image_lin!{T,N}(c::AbstractDeformation,imgout::Image{T,N},
                                 # # imgin::Image{T,N},opts::Options)
    # # @defaults opts default=zero(T)

    # # sizein = size(imgin)
    # # xin=zeros(Float64, N)       # point coordinates to imgin
    # # xout=zeros(Float64, N)      # point coordinates to imgout
    # # yf=zeros(Int64, N)          # integer point coordinates to imgin
    # # yc=zeros(Int64, N)          # integer point coordinates to imgin
    # # y=zeros(Float64, N)         # integer point coordinates to imgin
    # # @nloops N i imgout begin    # for all pixels in output image
        # # # convert from pixel to continuous coordinates
        # # @nexprs N j->(xout[j] = (i_j - 0.5) * Images.pixelspacing(imgout)[j])
        # # transform_point!(c, xout, xin) # store transformed point to xin
        # # # convert from continuous image coordinates to pixels
        # # @nexprs N j->(y[j] = xin[j] / Images.pixelspacing(imgin)[j] + 0.5)
        # # @nexprs N j->(yf[j] = floor(Int64, y[j]))
        # # @nexprs N j->(yc[j] = ceil(Int64, y[j]))
        # # #println("xout=$(xout), xin=$(xin), y=$(y), yf=$(yf), yc=$(yc)")
        # # if (@nall N j->(yc[j] <= sizein[j])) && (@nall N j->(yf[j] >= 1))
            # # # there is probably allocation here, avoid
            # # @inbounds (@nref N imgout i) = round_integer(imgout, interpolate_recursive((@nref N imgin j->(yf[j]:yc[j])), y-yf))
            # # #println("   imgin= $(@nref N imgin j->(yf[j]:yc[j])), yc-y=$(yc-y) out=$(@nref N imgout i)")
        # # else
            # # @inbounds (@nref N imgout i) = default
        # # end
    # # end    
    # # return imgout
# # end


UFix = FixedPointNumbers.Normed{T, f} where f where T<:Unsigned 

    
# round num to integer if imgout is an array of integer type,
# clip if it is a fixed precision number, otherwise leave it alone 
@inline round_pixel(imgout,num) = num
@inline round_pixel(imgout::Image{T},num) where {T<:Integer} = round(T,num)
@inline round_pixel(imgout::Image{T},num) where {T<:UFix} = convert(T,clip_zero_one(num))
@inline round_pixel(imgout::Image{ColorTypes.RGB{T}},num::ColorTypes.RGB{T}) where {T<:UFix} =
            ColorTypes.RGB{T}(clip_zero_one(num.r), clip_zero_one(num.g),clip_zero_one(num.b))

function p1(x)
    @debugassert(abs(x)<=1.0)
    return 1. - abs(x)
end

"""
    multsumpix(y, w, x)
Return ``y + w*x``.
There are specialized implementations for fixed precision RGB to perform saturation
arithmetic addition, as the default leads to a `InexactError`.
"""
@inline multsumpix(y,w,x) = y+w*x

@inline multsumpix(y::ColorTypes.Gray{T},w::Real,x::ColorTypes.Gray{T}) where {T<:UFix} =
            ColorTypes.Gray{T}(clip_zero_one(w*x.val+y.val))

@inline multsumpix(y::ColorTypes.RGB{T},w::Real,x::ColorTypes.RGB{T}) where {T<:UFix} =
    ColorTypes.RGB{T}(clip_zero_one(w*x.r+y.r), clip_zero_one(w*x.g+y.g),clip_zero_one(w*x.b+y.b))

"""
    function transform_image_lin!(c, imgout, imgin, opts)
Transform `imgin` by deformation `c`, put the result into `imgout`.
Use linear interpolation. Out of bounds pixels are set to `default` value.
!!! warning
    Only 2D and 3D images supported.
"""
function transform_image_lin! end

# an explicit 2D version
function transform_image_lin!(c::AbstractDeformation,imgout::Image{T,2},
                                 imgin::Image{T,2},opts::Opts) where {T}
# boundary defines how the 0.5 px at the image boundary is handeled
#boundary 0 - cut --- everything closer than 0.5 px is invalid and thus set to default
#boundary 1 - constant --- interpolates with the last value (constant)
#boundary 2 - mirror --- interpolates with mirrored data
#points outside the image are set to default
    @defaults opts default=zero(T) boundary = 0
    bound::Int = boundary
    def2::T = default   #This is to force type T and avoid allocation in @inbounds imgout[i1,i2]=def2
    sizein = size(imgin)
    sizeout = size(imgout)
    xin=zeros(Float64, 2)        # point coordinates to imgin
    xout=zeros(Float64, 2)       # point coordinates to imgout
    is1::Float64=float(Images.pixelspacing(imgout)[1])
    is2::Float64=float(Images.pixelspacing(imgout)[2])
    #it is important to use proper pixelspacing (images can have diferent pixel size)
    is1r::Float64=1. / float(Images.pixelspacing(imgin)[1])
    is2r::Float64=1. / float(Images.pixelspacing(imgin)[2])
    yi1::Int=1 ; yi2::Int=1  # in-loop variables need to be preallocated
    y1::Float64=0. ; y2::Float64=0.  # in-loop variables need to be preallocated
    w1::Float64=0. ;    w2::Float64=0.
    v=zero(T) * 0.  #the 0. multiplication is here to avoid allocation in similar operation inside the loops
    for i2=1:size(imgout,2)
        for i1=1:size(imgout,1)
            @inbounds xout[1]=(i1-0.5)*is1
            @inbounds xout[2]=(i2-0.5)*is2
            transform_point!(c, xout, xin) # store transformed point to xin
            @inbounds y1=xin[1]*is1r+0.5
            @inbounds y2=xin[2]*is2r+0.5
            ladd = 1.
            hadd = 0.
            if bound > 0
                ladd = 0.
                hadd = .5
            end
            if y1>=ladd && y1<=sizein[1]+hadd && y2>=ladd && y2<=sizein[2]+hadd
                yi1=floor(Int, y1) ; yi2=floor(Int,y2)
                v *= 0.     #set v to zero but maintain type T{Float}
                for ii2=yi2:yi2+1
                    w2=p1(ii2-y2)
                    for ii1=yi1:yi1+1
                        w1=w2*p1(ii1-y1)
                        if bound > 0
                            if ii1 < 1
                                ii1 = bound
                            elseif ii1 > sizein[1]
                                ii1 = sizein[1] - bound + 1
                            end
                            if ii2 < 1
                                ii2 = bound
                            elseif ii2 > sizein[2]
                                ii2 = sizein[2] - bound + 1
                            end
                        end
                        if bound > 0 || (ii1 >= 1 && ii1 <= sizein[1] && ii2 >= 1 && ii2 <= sizein[2])
                            # perform saturation addition if needed
                            v=multsumpix(v,w1,imgin[ii1,ii2])
                        end
                    end
                end
                @inbounds imgout[i1,i2]=round_pixel(imgout, v)
            else
                @inbounds imgout[i1,i2]=round_pixel(imgout, def2)
            end
        end
    end
end 

# an explicit 3D version
function transform_image_lin!(c::AbstractDeformation,imgout::Image{T,3},
                                 imgin::Image{T,3},opts::Opts) where {T}
# boundary defines how the 0.5 px at the image boundary is handeled
#boundary 0 - cut --- everything closer than 0.5 px is invalid and thus set to default
#boundary 1 - constant --- interpolates with the last value (constant)
#boundary 2 - mirror --- interpolates with mirrored data
#points outside the image are set to default
    @defaults opts default=zero(T) boundary=0

    bound::Int = boundary
    sizein = size(imgin)
    sizeout = size(imgout)
    xin=zeros(Float64, 3)        # point coordinates to imgin
    xout=zeros(Float64, 3)       # point coordinates to imgout
    pxspout=zeros(Float64, 3)
    pxspin=zeros(Float64, 3)
    v=zero(T) * 0.  #the 0. multiplication is here to avoid allocation in similar operation inside the loops
    for i = 1:3
        pxspout[i] = Images.pixelspacing(imgout)[i]
        pxspin[i] = Images.pixelspacing(imgin)[i]
    end
    for i3=1:sizeout[3]
        for i2=1:sizeout[2]
            for i1=1:sizeout[1]
                #it is important to use proper pixelspacing (images can have diferent pixel size)
                xout[1]=(i1-0.5)*pxspout[1]
                xout[2]=(i2-0.5)*pxspout[2]
                xout[3]=(i3-0.5)*pxspout[3]
                transform_point!(c, xout, xin) # store transformed point to xin
                y1=xin[1]/pxspin[1]+0.5
                y2=xin[2]/pxspin[2]+0.5
                y3=xin[3]/pxspin[3]+0.5
                ladd = 1.
                hadd = 0.
                if bound > 0
                    ladd = 0.
                    hadd = .5
                end
                if y1>=ladd && y1<=sizein[1]+hadd && y2>=ladd && y2<=sizein[2]+hadd && y3>=ladd && y3<=sizein[3]+hadd
                    yi1 = floor(Int, y1) ; yi2 = floor(Int, y2) ; yi3 = floor(Int, y3)
                    v *= 0.     #set v to zero but maintain type T{Float}
                    for ii3=yi3:yi3+1
                        w3=p1(ii3-y3)
                        for ii2=yi2:yi2+1
                            w2=w3*p1(ii2-y2)
                            for ii1=yi1:yi1+1
                                w1=w2*p1(ii1-y1)
                                if bound > 0
                                    if ii1 < 1
                                        ii1 = bound
                                    elseif ii1 > sizein[1]
                                        ii1 = sizein[1] - bound + 1
                                    end
                                    if ii2 < 1
                                        ii2 = bound
                                    elseif ii2 > sizein[2]
                                        ii2 = sizein[2] - bound + 1
                                    end
                                    if ii3 < 1
                                        ii3 = bound
                                    elseif ii3 > sizein[3]
                                        ii3 = sizein[3] - bound + 1
                                    end
                                end
                                if bound > 0 || (ii1 >= 1 && ii1 <= sizein[1] && ii2 >= 1 && ii2 <= sizein[2] && ii3 >= 1 && ii3 <= sizein[3])
                                    v=multsumpix(v,w1,imgin[ii1,ii2,ii3])
                                end
                            end
                        end
                    end
                    # store to the output image, rounding as needed (for integer images)
                    imgout[i1,i2,i3]=round_pixel(imgout,v)
                else
                    imgout[i1,i2,i3]=round_pixel(imgout,default)
                end
            end
        end
    end
end 

"""
    transform_image_lin(c, img, outpsize, opts)
Return `img` transformed by `c`. `outpsize` is size of the output image, pixelspacing
of the output could be defined as optional pararameter, it is the same as in input image by default.
Out of bounds pixels are set to `default` value.
"""
function transform_image_lin(c::AbstractDeformation,img::Image{T,N},
                                 outpsize,opts::Opts) where {T,N}
    #@debug "transform_image_lin pixelspacing img=$(Images.pixelspacing(img))"
    outimg=similar_with_pixelspacing(img,outpsize) # output image
    #@debug "transform_image_lin pixelspacing outimg=$(Images.pixelspacing(outimg))"
    transform_image_lin!(c, outimg, img, opts)
    #@debug "transform_image_lin pixelspacing outimg=$(Images.pixelspacing(outimg))"
    return outimg
end

"""
    resample_to_pixelspacing(imgin, pxspout)
Return image with target pixelspacing created by resampling the input image.
"""
function resample_to_pixelspacing(imgin::Image{T,N}, pxspout) where T where N
    pxspin = Images.pixelspacing(imgin)
    if pxspin == pxspout
        return imgin
    end
    sizein = [size(imgin)...]
    sizeout = ntuple(k->(floor(Int, sizein[k] * pxspin[k] / pxspout[k])), N)
    imgout = with_pixelspacing(Array{T}(sizeout),pxspout)
    # imgout["pixelspacing"] = pxspout
    if N == 2
        def = create_identity(sizein, RigidDeformation2D)
    elseif N == 3
        def = create_identity(sizein, RigidDeformation3D)
    else
        error("Unsuported image dimension")
    end
    transform_image_lin!(def, imgout, imgin, @options boundary = 1)
    return imgout
end

#----------------------AbstractAffineDeformation--------------------------
"""
This Abstract class enables to write functions common to 2D and 3D affine transformations.

5 parameters are expected in affine transformations:
* `isizemm::Vector{Int64}`: image size
* `theta::Vector{Float64}`: parameter vector
* `mintheta::Vector{Float64}`: maximal parameter vector
* `maxtheta::Vector{Float64}`: minimal parameter vector
* `A::Array{Float64, 2}`: transform matrix
The length (structure) of the `theta` is not known.

!!! warning
    The subclassing is meant in the sense of common internal structure, the transformation matrix.
    All affine deformations are linear functions of the coordinates. However, the parameterization
    is different, the rigid transformations in particular are NOT linear functions
    of their parameter vector theta.
"""
abstract type AbstractAffineDeformation <: AbstractDeformation end

"""
    lower_bound(c)
Return lower bound on parameter vector.
"""
function lower_bound end

# Lower bounds for the parameters
@inline lower_bound(c::T) where {T<:AbstractAffineDeformation} = c.mintheta

"""
    upper_bound(c)
Return upper bound on parameter vector.
"""
function upper_bound end

# Upper bounds for the parameters
@inline upper_bound(c::AbstractAffineDeformation) = c.maxtheta

# Set the parameter vector
function set_theta!(c::AbstractAffineDeformation, theta::Vector{Float64})
    c.theta[:] = theta
    c.A = get_matrix(c.isizemm, theta, typeof(c))
end

# Retrieve the parameter vector
@inline get_theta(c::AbstractAffineDeformation) = c.theta

# Copy fields of RigidDeformation structure
function copy!(new::T, c::T) where {T<:AbstractAffineDeformation}
    new.isizemm[:] = Base.copy(c.isizemm)
    new.theta[:] = Base.copy(c.theta)
    new.mintheta[:] = Base.copy(c.mintheta)
    new.maxtheta[:] = Base.copy(c.maxtheta)
    new.A[:] = Base.copy(c.A)
    return nothing
end

# creates composition def of the transformation dst and src y=def(x) ~ y=dst(src(x))
# context and limits are took from dst. Implemented only for rigid transformations
function compose(dst::T, src::T) where {T<:AbstractAffineDeformation}
    def = copy(dst)
    compose!(def, src) # this should be implemented for target deformation class
    return def
end

# returns true if the transformation is identity
function is_identity(def::AbstractAffineDeformation)
    for j = 1:size(def.A, 2)
        for i = 1:size(def.A, 1)
            if (i == j)
                if def.A[i, j] != 1
                    return false
                end
            elseif def.A[i, j] != 0
                return false
            end
        end
    end
    return true
end

# transform one point inp and store the transformed point to outp
function transform_point!(c::AbstractAffineDeformation, inp::Vector{Float64}, outp::Vector{Float64})
    @debugassert(length(inp) == size(c.A, 1))
    @debugassert(length(outp) == size(c.A, 1))

    outp[:] = c.A * [inp; 1]
end

# version calling add_grad_contrib! for compatibility
@inline add_grad_contrib_kpt!(grad::AbstractArray{Float64,1}, dval::Float64, def::AbstractAffineDeformation, kptsf::Vector{V}, kptsg::Vector{U}, kptin::Int) where {U<:keypoints.AbstractKeypoint,V<:keypoints.AbstractKeypoint} =
            add_grad_contrib!(grad, dval, kptsg[kptin].normal, def, kptsf[kptin].pos) #the last argument is not used in affineTransforms

# transform_point_lin!(c,inp,outp) is a variant of transform_point for linear transformations that
# uses the coefficients_iterator interface. It should be suitable for any linear transformation
# This function is mostly for testing coefficients_iterator, it is probably slower than specialized
# implementations
function transform_point_lin!(def::AbstractDeformation, inp::Vector{Float64}, outp::Vector{Float64})
    @debugassert(is_linear(def)) # TODO: make this a "trait" for more efficiency
    transform_initial!(outp,def,inp)
    theta=get_theta(def)
    dim=2
    @debugassert(length(inp)==dim)
    for (j,c) in coefficients_iterator(def,inp)
        #axpy!(c,theta[j*dim+1:(j+1)*dim],outp)
        #axpy!(c,Base.view(theta,j*dim+1:(j+1)*dim),outp)
        #broadcast!(+,outp,outp,c*theta[j*dim+1:(j+1)*dim])
        for i in 1:dim
            outp[i]+=c*theta[i+j*dim]
        end
    end
end

# transform_point_kpt_lin! is a variant of transform_point_kpt! for linear transformations
# uses the coefficients_iterator interface. It should be suitable for any linear transformation
function transform_point_kpt_lin!(def::AbstractDeformation,
            outp::Vector{Float64},kptsf::Vector{V},ind::Int) where {V<:keypoints.AbstractKeypoint}
    @debugassert(is_linear(def)) # TODO: make this a "trait" for more efficiency
    transform_initial!(outp,def,kptsf[ind].pos)
    theta=get_theta(def)
    dim=length(outp)
    @debugassert(length(outp)==dim)
    for (j,c) in coefficients_iterator_keypoint(def,ind,kptsf)
        for i in 1:dim
            outp[i]+=c*theta[i+j*dim]
        end
    end
end

#----------------------AbstractAffineDeformation2D--------------------------
"""
This Abstract class enables to write functions common to 2D affine transformations
"""
abstract type AbstractAffineDeformation2D <: AbstractAffineDeformation end

# transform one point inp and store the transformed point to outp
function transform_point!(c::AbstractAffineDeformation2D, inp::Vector{Float64}, outp::Vector{Float64})
    @debugassert(length(inp) == 2)
    @debugassert(length(outp) == 2)

    #  matrix-vector multiplication unrolled for speed
    outp[1]=c.A[1,1]*inp[1]+c.A[1,2]*inp[2]+c.A[1,3]
    outp[2]=c.A[2,1]*inp[1]+c.A[2,2]*inp[2]+c.A[2,3]
    return nothing
end

    

# transform one vector normal inv and store the transformed vector to outv
# 'inp' is the point where we are
function transform_normal!(c::AbstractAffineDeformation2D, inv::Vector{Float64}, outv::Vector{Float64}, inp::Vector{Float64})
    @debugassert(length(inv) == 2)
    @debugassert(length(outv) == 2)

    #  matrix-vector multiplication unrolled for speed
    outv[1]=c.A[1,1]*inv[1]+c.A[1,2]*inv[2]
    outv[2]=c.A[2,1]*inv[1]+c.A[2,2]*inv[2]
    return nothing
end


#------------------------AffineDeformation2D--------------------------
mutable struct AffineDeformation2D <: AbstractAffineDeformation2D
    isizemm::Vector{Float64}    # image size
    theta::Vector{Float64}      # parameter vector
    mintheta::Vector{Float64}   # parameter vector
    maxtheta::Vector{Float64}   # parameter vector
    A::Array{Float64, 2}        # transform matrix
    
    # The constructor creates the transform and its context given an param vector theta and
    # the image size.
    function AffineDeformation2D(isizemm::Vector{T}, theta::Vector{U}, mintheta::Vector{V}, maxtheta::Vector{W}) where {T<:Number, U<:Number, V<:Number, W<:Number}
        @debugassert(length(isizemm) == 2 && length(theta) == 6 && length(mintheta) == 6
                     && length(maxtheta) == 6)
        new(isizemm, theta, mintheta, maxtheta, get_matrix(isizemm, theta, AffineDeformation2D))
    end
    
    function AffineDeformation2D()
        new(Array{Int}(undef,2), Array{Float64}(undef,6), Array{Float64}(undef,6),
            Array{Float64}(undef,6), Array{Float64}(undef,(2, 3)))
    end

end

# The constructor creates transform and its context with default limits given an image size
AffineDeformation2D(isizemm::Vector{T}, theta::Vector{U}) where {T<:Number, U<:Number} =
    AffineDeformation2D(isizemm, theta, [-10.; -10.; -10.; -10.; -isizemm / 2.], [10.; 10.; 10.; 10.; isizemm / 2.])

# Creates identity transform and its context given an image size
"""
    create_identity(isizemm, type, params)
Returns a identity of given `type`, `params`can contain optional arguments for some deformations.
"""
function create_identity end

@inline create_identity(isizemm::Vector{T}, ::Type{AffineDeformation2D}, params = []) where {T<:Number} =
            AffineDeformation2D(isizemm, [1.; 0.; 0.; 1.; 0.; 0.])

# Creates transform and its context given an image size
@inline create(isizemm::Vector{T}, theta::Vector{U}, ::Type{AffineDeformation2D}) where {T<:Number, U<:Number} = 
            AffineDeformation2D(isizemm, theta)

# Copy the object
function copy(c::AffineDeformation2D)
    new = AffineDeformation2D()
    copy!(new,c)
    return new
end


""" given two images `f` and `g`, binarize them by setting everything not equal to `background_val` to 0 and 1 otherwise. Find a euclidean transformation that matches the foregrounds (anything not `background_val`) best. Choose orientation maximising `criterion_f`

I am not sure this is correct. I started from Martin Dolejsi's test_admm:init_transform
See Umeyama, 1991: Least-squares estimation of Transformation Parameters between two point patterns.

"""
function euclidean_fit(f::Image{T,2},g::Image{T,2},bg_val::T, criterion_f::Function) where T
   binarize_f = x -> x==bg_val ? zero(T) : one(T)
   bf=map(binarize_f,f)
   bg=map(binarize_f,g)
   cogf = segimgtools.object_centroid(bf)
   cogg = segimgtools.object_centroid(bg)
   eigf=segimgtools.object_cov_mat(bf, cogf)
   eigg=segimgtools.object_cov_mat(bg, cogg)
   eigf = eigfact!(eigf)
   eigg = eigfact!(eigg)
   #scale = sum(sqrt.(eigg[:values] ./ eigf[:values])) / 2.
   scale=sqrt( eigg[:values][2]/eigf[:values][2] ) # estimate scale from largest eigenvalues
   vf=eigf[:vectors][:, 2] # principal eigenvectors 
   vg=eigg[:vectors][:, 2]
   H= vf * vg'
   R1= segimgtools.get_rot(H) # get the orthogonal part through SVD
   R2= segimgtools.get_rot(-H) # the other rotation, offset by 180 degrees
   # calculate the two possible solutions for rotation matrix R and translation T 
    # calculate the two possible solutions for rotation matrix R and translation T 
   #R1 = segimgtools.get_rot((-eigf[:vectors][:, 1]) * (-eigg[:vectors][:, 1])' + (eigf[:vectors][:, 1]) * (eigg[:vectors][:, 1])')
   #t1 = (cogg + eigg[:vectors][:, 1] ./ 2.) - scale * R1 * (cogf + eigf[:vectors][:, 1] ./ 2.)
   t1 = cogg-scale*R1*cogf
   t2 = cogg-scale*R2*cogf
    
   #R2 = segimgtools.get_rot((eigf[:vectors][:, 1] ./ 2.) * (-eigg[:vectors][:, 1] ./ 2.)' +
   #                          (-eigf[:vectors][:, 1] ./ 2) * (eigg[:vectors][:, 1] ./ 2.)')
   #t2 = (cogg + eigg[:vectors][:, 1] ./ 2) - scale * R2 * (cogf - eigf[:vectors][:, 1] ./ 2.)
   # find, which ones leads to a lower cost
   def1 = AffineDeformation2D([size(f)...] .* Images.pixelspacing(f), [(scale * R1)[:]; t1])
   def2 = AffineDeformation2D([size(f)...] .* Images.pixelspacing(f), [(scale * R2)[:]; t2])
   gw1 = transform_image_nn(def1, g, size(f), @options default = bg_val)
   gw2 = transform_image_nn(def2, g, size(f), @options default = bg_val)
   #ImageView.imshow(segimgtools.classimage2rgb(bf,1);name="f")
   #ImageView.imshow(segimgtools.classimage2rgb(bg,1);name="g")
   #ImageView.imshow(segimgtools.classimage2rgb(gw1,maximum(g));name="gw1")
   #ImageView.imshow(segimgtools.classimage2rgb(gw2,maximum(g));name="gw2")
   c1=criterion_f(f,gw1)
   c2=criterion_f(f,gw2)
   return c1 > c2 ? def1 : def2
end    

# return 2 by 6 matrix containing the Jacobian matrix, the partial derivatives of the
# output coordinates with respect to the parameter vector.
function get_jacobian(c::AffineDeformation2D, inp::Vector{Float64})
    @debugassert(length(inp)==2)
    #  matrix-vector multiplication unrolled for speed
    dT = Array{Float64}(2, 6)
    @inbounds dT[1, 1] = inp[1]
    @inbounds dT[2, 1] = 0
    @inbounds dT[1, 2] = 0
    @inbounds dT[2, 2] = inp[1]
    @inbounds dT[1, 3] = inp[2]
    @inbounds dT[2, 3] = 0
    @inbounds dT[1, 4] = 0
    @inbounds dT[2, 4] = inp[2]
    @inbounds dT[1, 5] = 1
    @inbounds dT[2, 5] = 0
    @inbounds dT[1, 6] = 0
    @inbounds dT[2, 6] = 1
    return dT
end

# effective implementation of generic function add_grad_contrib!
function add_grad_contrib!(grad::AbstractArray{Float64,1}, dval::Float64, normal::Vector{Float64}, c::AffineDeformation2D, inp::Vector{Float64})
    dval /= norm2(normal)
    grad[1] += dval * inp[1] * normal[1]
    grad[2] += dval * inp[1] * normal[2]
    grad[3] += dval * inp[2] * normal[1]
    grad[4] += dval * inp[2] * normal[2]
    grad[5] += dval * normal[1]
    grad[6] += dval * normal[2]
    return nothing
end

# auxiliary function to create a 2D 2x3 affine transform matrix
@inline get_matrix(isizemm::Vector{T}, theta::Vector{U}, ::Type{AffineDeformation2D}) where {T<:Number, U<:Number} =
            reshape(theta, 2, 3)

@inline is_linear(c::AffineDeformation2D) = true

# coefficients_iterator returns a list of pairs (j,c) representing the coefficients of the linear
# transformation - see transform_point_lin! above
struct AffineDeformation2DIterator
  # this is what we need to remember
  def::AffineDeformation2D  # a reference to the deformation function and the dimensions
  inp::Vector{Float64}
end

function coefficients_iterator(def::AffineDeformation2D,inp::Vector{Float64})
    AffineDeformation2DIterator(def,inp)
end

function Base.iterate(itr::AffineDeformation2DIterator)
    return ((0,itr.inp[1]),1)
end

function Base.iterate(itr::AffineDeformation2DIterator,state)
    if state>2
        return nothing
    end
    r= if state==0
         (0,itr.inp[1])
       elseif state==1
         (1,itr.inp[2])
       elseif state==2
         (2,1.)
       else
         error("Unexpected state=$state in AffineDeformation2DIterator.next.")
       end    
    return (r,state+1)     
end
        
Base.length(itr::AffineDeformation2DIterator) = 3

# # the iterator state is a counter
# Base.start(i::AffineDeformation2DIterator) =  0
# function Base.done(i::AffineDeformation2DIterator,state)
#     state>2
# end


# function Base.next(itr::AffineDeformation2DIterator,state)
#     r= if state==0
#          (0,itr.inp[1])
#        elseif state==1
#          (1,itr.inp[2])
#        elseif state==2
#          (2,1.)
#        else
#          error("Unexpected state=$state in AffineDeformation2DIterator.next.")
#        end    
#     (r,state+1)     
# end

# TODO:  comment out for Julia 0.5
Base.eltype(::Type{AffineDeformation2DIterator}) = (Int,Float64)
#Base.iteratoreltype(::Type{AffineDeformation2DIterator})=Base.HasElType()
Base.IteratorEltype(::Type{AffineDeformation2DIterator})=Base.HasElType()
#Base.iteratorsize(::Type{AffineDeformation2DIterator})=Base.HasLength()
Base.IteratorSize(::Type{AffineDeformation2DIterator})=Base.HasLength()


"""
    coafficients_iterator_keypoint is an auxiliary function that provides an
    iterator for the coefficients of the linear transformation at keypoint 'i' of 'kpts'.
    For some transformations, notably Bsplines, it relies on `set_keypoints!(def,kpts)`
    having been called earlier

    Here is the generic implementation, which will not work for B-splines, where
    coefficients_iterator is not (yet) implemented.
"""
function coefficients_iterator_keypoint(def::deformation.AbstractDeformation,
                                i::Integer,kptsf::Vector{K}) where {K<:AbstractKeypoint}
    #@debugprintln("calling generic coefficients_iterator_keypoint")
    deformation.coefficients_iterator(def,kptsf[i].pos)
end

# end of the coefficients_iterator implementation ---------------------------------------------




#------------------------ScaleDeformation2D--------------------------
"""
`ScaleDeformation2D` represents rotation, shift and isotropical scaling.
The parameter vector is [angle, shiftx shifty, scale] with angle in degrees
and shift in pixels.
"""
mutable struct ScaleDeformation2D <: AbstractAffineDeformation2D
    isizemm::Vector{Float64}    # image size
    theta::Vector{Float64}      # parameter vector
    mintheta::Vector{Float64}   # parameter vector
    maxtheta::Vector{Float64}   # parameter vector
    A::Array{Float64, 2}        # transform matrix
    
    # The constructor creates the transform and its context given an param vector theta and
    # the image size.
    function ScaleDeformation2D(isizemm::Vector{T}, theta::Vector{U}, mintheta::Vector{V}, maxtheta::Vector{W}) where {T<:Number, U<:Number, V<:Number, W<:Number}
        @debugassert(length(isizemm) == 2 && length(theta) == 4 && length(mintheta) == 4
                     && length(maxtheta) == 4)
        new(isizemm, theta, mintheta, maxtheta, get_matrix(isizemm, theta, ScaleDeformation2D))
    end
    
    function ScaleDeformation2D()
        new(Array{Int}(2), Array{Float64}(4), Array{Float64}(4),
            Array{Float64}(4), Array{Float64}(2, 3))
    end

end

# The constructor creates transform and its context with default limits given an image size
ScaleDeformation2D(isizemm::Vector{T}, theta::Vector{U}) where {T<:Number, U<:Number} =
    ScaleDeformation2D(isizemm, theta, [-180; -isizemm / 2; 1/3], [180; isizemm / 2; 3])

# Creates identity transform and its context given an image size
@inline create_identity(isizemm::Vector{T}, ::Type{ScaleDeformation2D}, params = []) where {T<:Number} =
            ScaleDeformation2D(isizemm, [zeros(Float64, 3); 1])

# Creates transform and its context given an image size
@inline create(isizemm::Vector{T}, theta::Vector{U}, ::Type{ScaleDeformation2D}) where {T<:Number, U<:Number} = 
            ScaleDeformation2D(isizemm, theta)

# Copy the object
function copy(c::ScaleDeformation2D)
    new = ScaleDeformation2D()
    copy!(new, c)
    return new
end

# transform one point inp and store the transformed point to outp
function transform_point!(c::ScaleDeformation2D, inp::Vector{Float64}, outp::Vector{Float64})
    @debugassert(length(inp) == 2)
    @debugassert(length(outp) == 2)

    #  matrix-vector multiplication unrolled for speed
    outp[1] = c.theta[4] * (c.A[1,1] * inp[1] + c.A[1,2] * inp[2]) + c.A[1,3]
    outp[2] = c.theta[4] * (c.A[2,1] * inp[1] + c.A[2,2] * inp[2]) + c.A[2,3]
    return nothing
end

# transform one vector normal inv and store the transformed vector to outv
# 'inp' is the point where we are
function transform_normal!(c::ScaleDeformation2D, inv::Vector{Float64}, outv::Vector{Float64}, inp::Vector{Float64})
    @debugassert(length(inv) == 2)
    @debugassert(length(outv) == 2)

    #  matrix-vector multiplication unrolled for speed
    outv[1] = c.theta[4] * (c.A[1,1] * inv[1] + c.A[1,2] * inv[2])
    outv[2] = c.theta[4] * (c.A[2,1] * inv[1] + c.A[2,2] * inv[2])
    return nothing
end

# return 2 by 4 matrix containing the Jacobian matrix, the partial derivatives of the
# output coordinates with respect to the parameter vector. In this case
# [dTdAlpha[x,y]', dTdx[x,y]', dTdy[x,y]']
function get_jacobian(c::ScaleDeformation2D, inp::Vector{Float64})
    @debugassert(length(inp)==2)
    #  matrix-vector multiplication unrolled for speed
    dT = Array{Float64}(2, 4)
    thp = (inp - 0.5 * c.isizemm)
    @inbounds dT[1, 1] = c.theta[4] * ( c.A[2,1] * thp[1] + c.A[2,2] * thp[2]) * degprad # -sin  cos
    @inbounds dT[2, 1] = c.theta[4] * (-c.A[2,2] * thp[1] + c.A[2,1] * thp[2]) * degprad # -cos -sin
    @inbounds dT[1, 2] = 1
    @inbounds dT[2, 2] = 0
    @inbounds dT[1, 3] = 0
    @inbounds dT[2, 3] = 1
    @inbounds dT[1, 4] = c.A[1,1] * thp[1] + c.A[1,2] * thp[2] #  cos sin
    @inbounds dT[2, 4] = c.A[2,1] * thp[1] + c.A[2,2] * thp[2] # -sin cos

    return dT
end

# effective implementation of generic function add_grad_contrib! for abstract deformation
function add_grad_contrib!(grad::AbstractArray{Float64,1}, dval::Float64, normal::Vector{Float64}, c::ScaleDeformation2D, inp::Vector{Float64})
    dval /= norm2(normal)
    @inbounds grad[1] += dval * c.theta[4] * ((c.A[2,1] * (inp[1] - 0.5 * c.isizemm[1]) + c.A[2,2] * (inp[2] - 0.5 * c.isizemm[2])) * normal[1] + (-c.A[2,2] * (inp[1] - 0.5 * c.isizemm[1]) + c.A[2,1] * (inp[2] - 0.5 * c.isizemm[2])) * normal[2]) * degprad
    grad[2] += dval * normal[1]
    grad[3] += dval * normal[2]
    grad[4] += dval * ((c.A[1,1] * (inp[1] - 0.5 * c.isizemm[1]) + c.A[1,2] * (inp[2] - 0.5 * c.isizemm[2])) * normal[1] + (c.A[2,1] * (inp[1] - 0.5 * c.isizemm[1]) + c.A[2,2] * (inp[2] - 0.5 * c.isizemm[2])) * normal[2])
    return nothing
end

# auxiliary function to create a 2D 2x3 rotation + shift + scale matrix
function get_matrix(isizemm::Vector{T}, theta::Vector{U}, ::Type{ScaleDeformation2D}) where {T<:Number, U<:Number}
    f = theta[1] * degprad          # angle in radians
    cf = cos(f); sf=sin(f)
    A = [ cf sf ; -sf cf ]
    c = 0.5 * isizemm            # center coordinates
    t =  (c - theta[4] * A * c) + theta[2:3]
    return hcat(A,t)
end


#------------------------RigidDeformation2D--------------------------
"""
`RigidDeformation2D represents rotation and shift. The parameter vector is
[angle, shiftx, shifty] with angle in degrees and shift in pixels
"""
mutable struct RigidDeformation2D <: AbstractAffineDeformation2D
    isizemm::Vector{Float64}    # image size
    theta::Vector{Float64}      # parameter vector
    mintheta::Vector{Float64}   # parameter vector
    maxtheta::Vector{Float64}   # parameter vector
    A::Array{Float64, 2}        # transform matrix
    
    # The constructor creates the transform and its context given an param vector theta and
    # the image size.
    function RigidDeformation2D(isizemm::Vector{T}, theta::Vector{U}, mintheta::Vector{V}, maxtheta::Vector{W}) where {T<:Number, U<:Number, V<:Number, W<:Number}
        @debugassert(length(isizemm) == 2 && length(theta) == 3 && length(mintheta) == 3
                     && length(maxtheta) == 3)
        new(isizemm, theta, mintheta, maxtheta, get_matrix(isizemm, theta, RigidDeformation2D))
    end
    
    function RigidDeformation2D()
        new(Array{Int}(2), Array{Float64}(3), Array{Float64}(3),
            Array{Float64}(3), Array{Float64}(2, 3))
    end

end

# The constructor creates transform and its context with default limits given an image size
RigidDeformation2D(isizemm::Vector{T}, theta::Vector{U}) where {T<:Number, U<:Number} =
    RigidDeformation2D(isizemm, theta, [-180; -isizemm / 2], [180; isizemm / 2])

# Creates identity transform and its context given an image size
@inline create_identity(isizemm::Vector{T}, ::Type{RigidDeformation2D}, params = []) where {T<:Number} =
            RigidDeformation2D(isizemm, zeros(Float64, 3))

# Creates transform and its context given an image size
@inline create(isizemm::Vector{T}, theta::Vector{U}, ::Type{RigidDeformation2D}) where {T<:Number, U<:Number} = 
            RigidDeformation2D(isizemm, theta)

# Copy the object
function copy(c::RigidDeformation2D)
    new = RigidDeformation2D()
    copy!(new,c)
    return new
end

# creates composition of transformations dst and src y=def(x) ~ y=dst(src(x))
# context and limits are took from dst
function compose!(dst::RigidDeformation2D, src::RigidDeformation2D)
    ntheta = Array{Float64}(length(dst.theta))
    ntheta[1] = src.theta[1] + dst.theta[1]
    ntheta[2:3] = transform_normal(dst, src.theta[2:3], src.theta[2:3]) + dst.theta[2:3]
    set_theta!(dst, ntheta)
    return nothing
end

# transform one point inp and store the transformed point to outp
function transform_point_inv!(c::RigidDeformation2D, inp::Vector{Float64}, outp::Vector{Float64})
    @debugassert(length(inp) == 2)
    @debugassert(length(outp) == 2)
    inp1 = inp - [c.A[1,3],c.A[2,3]]
    f = -c.theta[1] * degprad          # angle in radians
    cf = cos(f); sf=sin(f)
    A = [ cf sf ; -sf cf ]
    #  matrix-vector multiplication unrolled for speed
    outp[1]=A[1,1]*inp1[1]+A[1,2]*inp1[2] #  cos  sin
    outp[2]=A[2,1]*inp1[1]+A[2,2]*inp1[2] # -sin  cos
    return nothing
end

# return 2 by 3 matrix containing the Jacobian matrix, the partial derivatives of the
# output coordinates with respect to the parameter vector. In this case
# [dTdAlpha[x,y]', dTdx[x,y]', dTdy[x,y]']
function get_jacobian(c::RigidDeformation2D, inp::Vector{Float64})
    @debugassert(length(inp)==2)
    #  matrix-vector multiplication unrolled for speed
    dT = Array{Float64}(2, 3)
    thp = inp - 0.5 * c.isizemm
    @inbounds dT[1, 1] = ( c.A[2,1] * thp[1] + c.A[2,2] * thp[2]) * degprad # -sin  cos
    @inbounds dT[2, 1] = (-c.A[2,2] * thp[1] + c.A[2,1] * thp[2]) * degprad # -cos -sin
    @inbounds dT[1, 2] = 1
    @inbounds dT[2, 2] = 0
    @inbounds dT[1, 3] = 0
    @inbounds dT[2, 3] = 1

    return dT
end

# effective implementation of generic function add_grad_contrib! for abstract deformation
function add_grad_contrib!(grad::AbstractArray{Float64,1}, dval::Float64, normal::Vector{Float64}, c::RigidDeformation2D, inp::Vector{Float64})
    dval /= norm2(normal)
    @inbounds grad[1] += dval * ((c.A[2,1] * (inp[1] - 0.5 * c.isizemm[1]) + c.A[2,2] * (inp[2] - 0.5 * c.isizemm[2])) * normal[1] + (-c.A[2,2] * (inp[1] - 0.5 * c.isizemm[1]) + c.A[2,1] * (inp[2] - 0.5 * c.isizemm[2])) * normal[2]) * degprad
    grad[2] += dval * normal[1]
    grad[3] += dval * normal[2]
    return nothing
end

# auxiliary function to create a 2D 2x3 rotation + shift matrix
function get_matrix(isizemm::Vector{T}, theta::Vector{U}, ::Type{RigidDeformation2D}) where {T<:Number, U<:Number}
    f = theta[1] * degprad          # angle in radians
    cf = cos(f); sf=sin(f)
    A = [ cf sf ; -sf cf ]
    c = 0.5 * isizemm            # center coordinates
    t = c - A * c + theta[2:3]
    return hcat(A,t)
end

# options-less invocation
@inline transform_rigid2D(img::Image,outpsize,angle::Float64,shift::Vector{Float64}) =
            transform_rigid2D(img, outpsize, angle, shift, @options)

# transform 2D image rigidly
function transform_rigid2D(img::Image,outpsize,angle::Float64,shift::Vector{Float64},opts::Opts)
    c = RigidDeformation2D([outpsize...] .* Images.pixelspacing(img), vcat(angle,shift))
    return transform_image_nn(c,img,outpsize,opts)
end

#----------------------AbstractAffineDeformation3D--------------------------
"""
This Abstract class enables to write functions common to 3D affine transformations.
"""
abstract type AbstractAffineDeformation3D <: AbstractAffineDeformation end

# transform one point inp and store the transformed point to outp
function transform_point!(c::AbstractAffineDeformation3D, inp::Vector{Float64}, outp::Vector{Float64})
    @debugassert(length(inp) == 3)
    @debugassert(length(outp) == 3)

    #  matrix-vector multiplication unrolled for speed
    outp[1]=c.A[1,1] * inp[1] + c.A[1,2] * inp[2] + c.A[1,3] * inp[3] + c.A[1,4]
    outp[2]=c.A[2,1] * inp[1] + c.A[2,2] * inp[2] + c.A[2,3] * inp[3] + c.A[2,4]
    outp[3]=c.A[3,1] * inp[1] + c.A[3,2] * inp[2] + c.A[3,3] * inp[3] + c.A[3,4]
    return nothing
end

# transform one vector inv and store the transformed vector to outv
function transform_normal!(c::AbstractAffineDeformation3D, inv::Vector{Float64}, outv::Vector{Float64}, inp::Vector{Float64})
    @debugassert(length(inv) == 3)
    @debugassert(length(outv) == 3)

    #  matrix-vector multiplication unrolled for speed
    outv[1]=c.A[1,1] * inv[1] + c.A[1,2] * inv[2] + c.A[1,3] * inv[3]
    outv[2]=c.A[2,1] * inv[1] + c.A[2,2] * inv[2] + c.A[2,3] * inv[3]
    outv[3]=c.A[3,1] * inv[1] + c.A[3,2] * inv[2] + c.A[3,3] * inv[3]
    return nothing
end


#------------------------AffineDeformation3D--------------------------
mutable struct AffineDeformation3D <: AbstractAffineDeformation3D
    isizemm::Vector{Float64}    # image size
    theta::Vector{Float64}      # parameter vector
    mintheta::Vector{Float64}   # parameter vector
    maxtheta::Vector{Float64}   # parameter vector
    A::Array{Float64, 2}        # transform matrix
    
    # The constructor creates the transform and its context given an param vector theta and
    # the image size.
    function AffineDeformation3D(isizemm::Vector{T}, theta::Vector{U}, mintheta::Vector{V}, maxtheta::Vector{W}) where {T<:Number, U<:Number, V<:Number, W<:Number}
        @debugassert(length(isizemm) == 3 && length(theta) == 12 && length(mintheta) == 12
                     && length(maxtheta) == 12)
        new(isizemm, theta, mintheta, maxtheta, get_matrix(isizemm, theta, AffineDeformation3D))
    end
    
    function AffineDeformation3D()
        new(Array(Int, 3), Array(Float64, 12), Array(Float64, 12),
            Array(Float64, 12), Array(Float64, 3, 4))
    end

end

# The constructor creates transform and its context with default limits given an image size
AffineDeformation3D(isizemm::Vector{T}, theta::Vector{U}) where {T<:Number, U<:Number} =
    AffineDeformation3D(isizemm, theta, [-10.; -10.; -10.; -10.; -10.; -10.; -10.; -10.; -10.; -isizemm / 2.], [10.; 10.; 10.; 10.; 10.; 10.; 10.; 10.; 10.; isizemm / 2.])

# Creates identity transform and its context given an image size
@inline create_identity(isizemm::Vector{T}, ::Type{AffineDeformation3D}, params = []) where {T<:Number} =
            AffineDeformation3D(isizemm, [1.; 0.; 0.; 0.; 1.; 0.; 0.; 0.; 1.; 0.; 0.; 0.])

# Creates transform and its context given an image size
@inline create(isizemm::Vector{T}, theta::Vector{U}, ::Type{AffineDeformation3D}) where {T<:Number, U<:Number} = 
            AffineDeformation3D(isizemm, theta)

# Copy the object
function copy(c::AffineDeformation3D)
    new = AffineDeformation3D()
    copy!(new,c)
    return new
end

@inline is_linear(c::AffineDeformation3D) = true


# return 3 by 12 matrix containing the Jacobian matrix, the partial derivatives of the
# output coordinates with respect to the parameter vector.
function get_jacobian(c::AffineDeformation3D, inp::Vector{Float64})
    @debugassert(length(inp)==2)
    #  matrix-vector multiplication unrolled for speed
    dT = Array(Float64, 3, 12)
    @inbounds dT[1, 1] = inp[1]
    @inbounds dT[2, 1] = 0
    @inbounds dT[3, 1] = 0
    @inbounds dT[1, 2] = 0
    @inbounds dT[2, 2] = inp[1]
    @inbounds dT[3, 2] = 0
    @inbounds dT[1, 3] = 0
    @inbounds dT[2, 3] = 0
    @inbounds dT[3, 3] = inp[1]
    @inbounds dT[1, 4] = inp[2]
    @inbounds dT[2, 4] = 0
    @inbounds dT[3, 4] = 0
    @inbounds dT[1, 5] = 0
    @inbounds dT[2, 5] = inp[2]
    @inbounds dT[3, 5] = 0
    @inbounds dT[1, 6] = 0
    @inbounds dT[2, 6] = 0
    @inbounds dT[3, 6] = inp[2]
    @inbounds dT[1, 7] = inp[3]
    @inbounds dT[2, 7] = 0
    @inbounds dT[3, 7] = 0
    @inbounds dT[1, 8] = 0
    @inbounds dT[2, 8] = inp[3]
    @inbounds dT[3, 8] = 0
    @inbounds dT[1, 9] = 0
    @inbounds dT[2, 9] = 0
    @inbounds dT[3, 9] = inp[3]
    @inbounds dT[1,10] = 1
    @inbounds dT[2,10] = 0
    @inbounds dT[3,10] = 0
    @inbounds dT[1,11] = 0
    @inbounds dT[2,11] = 1
    @inbounds dT[3,11] = 0
    @inbounds dT[1,12] = 0
    @inbounds dT[2,12] = 0
    @inbounds dT[3,12] = 1
    return dT
end

# effective implementation of generic function add_grad_contrib!
function add_grad_contrib!(grad::AbstractArray{Float64,1}, dval::Float64, normal::Vector{Float64}, c::AffineDeformation3D, inp::Vector{Float64})
    dval /= norm2(normal)
    grad[1] += dval * inp[1] * normal[1]
    grad[2] += dval * inp[1] * normal[2]
    grad[3] += dval * inp[1] * normal[3]
    grad[4] += dval * inp[2] * normal[1]
    grad[5] += dval * inp[2] * normal[2]
    grad[6] += dval * inp[2] * normal[3]
    grad[7] += dval * inp[3] * normal[1]
    grad[8] += dval * inp[3] * normal[2]
    grad[9] += dval * inp[3] * normal[3]
    grad[10] += dval * normal[1]
    grad[11] += dval * normal[2]
    grad[12] += dval * normal[3]
    return nothing
end

# auxiliary function to create a 3D 3x4 affine transform matrix
@inline get_matrix(isizemm::Vector{T}, theta::Vector{U}, ::Type{AffineDeformation3D}) where {T<:Number, U<:Number} =
            reshape(theta, 3, 4)

#------------------------RigidDeformation3D--------------------------
"""
`RigidDeformation3D` represents rotation and shift. The parameter vector is
[angle1, angle2, angle3, shiftx, shifty, shiftz] with angle in degrees and shift in pixels.
"""
mutable struct RigidDeformation3D <: AbstractAffineDeformation3D
    isizemm::Vector{Float64}    # image size
    theta::Vector{Float64}      # parameter vector
    mintheta::Vector{Float64}   # parameter vector
    maxtheta::Vector{Float64}   # parameter vector
    A::Array{Float64, 2}        # transform matrix
    
    # The constructor creates the transform and its context given an param vector theta and
    # the image size.
    function RigidDeformation3D(isizemm::Vector{T}, theta::Vector{U}, mintheta::Vector{U}, maxtheta::Vector{U}) where {T<:Number, U<:Number}
        @debugassert(length(isizemm) == 3 && length(theta) == 6 && length(mintheta) == 6
                     && length(maxtheta) == 6)
        new(isizemm, theta, mintheta, maxtheta, get_matrix(isizemm, theta, RigidDeformation3D))
    end
    
    function RigidDeformation3D()
        new(Array(Int, 3), Array(Float64, 6), Array(Float64, 6),
            Array(Float64, 6), Array(Float64, 3, 4))
    end

end

# The constructor creates transform and its context with default limits given an image size
@inline RigidDeformation3D(isizemm::Vector{T}, theta::Vector{U}) where {T<:Number, U<:Number} =
    RigidDeformation3D(isizemm, theta, [-180.; -180.; -180.; -isizemm / 2], [180.; 180.; 180.; isizemm / 2])

# Creates identity transform and its context given an image size
@inline create_identity(isizemm::Vector{T}, ::Type{RigidDeformation3D}, params = []) where {T<:Number} =
            RigidDeformation3D(isizemm, zeros(Float64, 6))

# Creates transform and its context given an image size
@inline create(isizemm::Vector{T}, theta::Vector{U}, ::Type{RigidDeformation3D}) where {T<:Number, U<:Number} = 
            RigidDeformation3D(isizemm, theta)
                        
# Copy the object
function copy(c::RigidDeformation3D)
    new = RigidDeformation3D()
    copy!(new, c)
    return new
end

# creates composition of transformations dst and src y=def(x) ~ y=dst(src(x))
# context and limits are took from dst
function compose!(dst::RigidDeformation3D, src::RigidDeformation3D)
    ntheta = Array(Float64, length(dst.theta))
    ntheta[1:3] = src.theta[1:3] + dst.theta[1:3]

    ntheta[4:6] = transform_normal(dst, src.theta[4:6], src.theta[4:6]) + dst.theta[4:6]
    set_theta!(dst, ntheta)
    return nothing
end

# return 3 by 5 matrix containing the Jacobian matrix, the partial derivatives of the
# output coordinates with respect to the parameter vector. In this case
# [dTdPhi[x,y]', dTdTheta[x,y]', dTdx[x,y]', dTdy[x,y]' dTdz[x,y]']
function get_jacobian(c::RigidDeformation3D, inp::Vector{Float64})
    @debugassert(length(inp)==3)
    dT = Array(Float64, 3, 6)
    thp = inp - 0.5 * c.isizemm
    
    k = c.theta[1] * degprad        # angle in radians
    l = c.theta[2] * degprad        # angle in radians
    m = c.theta[3] * degprad        # angle in radians
    ck = cos(k); sk = sin(k)
    cl = cos(l); sl = sin(l)
    cm = cos(m); sm = sin(m)
    dAdk = [1 0 0; 0 cm -sm; 0 sm cm] * [cl 0 sl; 0 1 0; -sl 0 cl] * [-sk ck 0; -ck -sk 0; 0 0 0]
    dAdl = [1 0 0; 0 cm -sm; 0 sm cm] * [-sl 0 cl; 0 0 0; -cl 0 -sl] * [ck sk 0; -sk ck 0; 0 0 1]
    dAdm = [0 0 0; 0 -sm -cm; 0 cm -sm] * [cl 0 sl; 0 1 0; -sl 0 cl] * [ck sk 0; -sk ck 0; 0 0 1]
    
    @inbounds dT[1, 1] = (dAdk[1,1] * thp[1] + dAdk[1,2] * thp[2] + dAdk[1,3] * thp[3])*degprad
    @inbounds dT[2, 1] = (dAdk[2,1] * thp[1] + dAdk[2,2] * thp[2] + dAdk[2,3] * thp[3])*degprad
    @inbounds dT[3, 1] = (dAdk[3,1] * thp[1] + dAdk[3,2] * thp[2] + dAdk[3,3] * thp[3])*degprad
    @inbounds dT[1, 2] = (dAdl[1,1] * thp[1] + dAdl[1,2] * thp[2] + dAdl[1,3] * thp[3])*degprad
    @inbounds dT[2, 2] = (dAdl[2,1] * thp[1] + dAdl[2,2] * thp[2] + dAdl[2,3] * thp[3])*degprad
    @inbounds dT[3, 2] = (dAdl[3,1] * thp[1] + dAdl[3,2] * thp[2] + dAdl[3,3] * thp[3])*degprad
    @inbounds dT[1, 3] = (dAdm[1,1] * thp[1] + dAdm[1,2] * thp[2] + dAdm[1,3] * thp[3])*degprad
    @inbounds dT[2, 3] = (dAdm[2,1] * thp[1] + dAdm[2,2] * thp[2] + dAdm[2,3] * thp[3])*degprad
    @inbounds dT[3, 3] = (dAdm[3,1] * thp[1] + dAdm[3,2] * thp[2] + dAdm[3,3] * thp[3])*degprad
    @inbounds dT[1, 4] = 1; @inbounds dT[2, 4] = 0; @inbounds dT[3, 4] = 0
    @inbounds dT[1, 5] = 0; @inbounds dT[2, 5] = 1; @inbounds dT[3, 5] = 0
    @inbounds dT[1, 6] = 0; @inbounds dT[2, 6] = 0; @inbounds dT[3, 6] = 1
    
    return dT
end

# auxiliary function to create a 2D 2x3 rotation + shift matrix
function get_matrix(isizemm::Vector{T}, theta::Vector{U}, ::Type{RigidDeformation3D}) where {T<:Number, U<:Number}
    k = theta[1] * degprad        # angle in radians
    l = theta[2] * degprad        # angle in radians
    m = theta[3] * degprad        # angle in radians
    
    ck = cos(k); sk = sin(k)
    cl = cos(l); sl = sin(l)
    cm = cos(m); sm = sin(m)

    A = [1 0 0; 0 cm -sm; 0 sm cm] * [cl 0 sl; 0 1 0; -sl 0 cl] * [ck sk 0; -sk ck 0; 0 0 1]
    
    c = 0.5 * isizemm           # center coordinates
    t = c - A * c + theta[4:6]
    return hcat(A,t)
end

# option-less invocation
@inline transform_rigid3D(img::Image{T,3}, outpsize, angle::Vector{Float64}, shift::Vector{Float64}) where {T} =
            transform_rigid3D(img, outpsize, angle, shift, @options)

# transform 3D image rigidly
function transform_rigid3D(img::Image{T,3}, outpsize, angle::Vector{Float64},shift::Vector{Float64},opts::Opts) where {T}
    c = RigidDeformation3D([outpsize...] .* Images.pixelspacing(img), vcat(angle,shift))
    return transform_image_nn(c,img,outpsize,opts)
end

function fit_landmarks(def::RigidDeformation3D,s::Matrix{Float64},t::Matrix{Float64})
    # optimize the deformation parameters to minimize the least squares geometric error
    # betweeen transformed source landmarks and target landmarks
    @assert(size(s,1)==3)
    @assert(size(t,1)==3)
    n=size(s,2)
    @assert(size(t,2)==n)
    count=0
    function crit!(theta,grad) # cost function
        @assert(length(grad)==0) # we do not implement the gradient
        set_theta!(def, theta)
        critval=0.0
        wp=zeros(Float64,3)
        for i=1:n
            transform_point!(def,s[:,i],wp)
            critval+=sum((wp-t[:,i]).^2)
        end
        count+=1
       # println("crit theta=$(theta) crit=$(critval)")
        return critval
    end
    theta0=get_theta(def)
    ndof=length(theta0)
    opt=NLopt.Opt(:LN_BOBYQA,ndof)
    NLopt.min_objective!(opt,crit!)
    NLopt.xtol_abs!(opt,1e-6*ones(Float64,ndof))
    NLopt.maxeval!(opt,1000) # at most "neval" evaluations
    tic()
    (optf,optx,ret)=NLopt.optimize(opt,theta0)
    @debugprintln_with_color(:blue, "fit_landmarks: Optimization finished after $(toq())s and $count iterations, crit=$optf")
    set_theta!(def, optx) # the resulting transformation of this iteration
end

#------------------------BsplineDeformation--------------------------
# The transformation is given by
# f(x) = x + sum_i c_i bspln(x/h-ofs_i)
#

mutable struct BsplineDeformation{N,O} <: AbstractDeformation
    theta::Array{Float64}           # parameter vector (reshaped to [N, n1,n2,...nN])
    mintheta::Array{Float64}        # parameter vector lower limit
    maxtheta::Array{Float64}        # parameter vector upper limit
    h::Vector{Float64}              # multiplicative factor, see equation above  
    ofs::Vector{Float64}            # additive factor, see equation above
    thetainds::Array{Int}           # created by set_keypoints to speed up keypoint transformation
    prodbeta::Array{Float64}        # created by set_keypoints to speed up keypoint transformation

    # Innter constructor computes distance and shift
    function BsplineDeformation{N,O}(isizemm::Vector{Float64}, theta::Array{Float64},
            mintheta::Array{Float64}, maxtheta::Array{Float64}) where {N,O}
        @debugassert(size(theta) == size(mintheta) == size(maxtheta))
        @debugassert(size(maxtheta, 1) == length(isizemm) == N )
        ofs = ones(Float64, size(isizemm)) * fld(O, 2) 
        ths = [size(theta)...]
        @debugassert( all(2*ofs .< ths[2:N+1] .- 1))
        h = isizemm ./ (ths[2:N+1] .- ofs * 2 .- 1)
        new(theta, mintheta, maxtheta, h, ofs, Int[], Float64[])
    end
end

# Constructor computes min and maxtheta
@generated function BsplineDeformation(order::Int, isizemm::Vector{Float64}, theta::Array{Float64,M}) where {M}
quote
    @debugassert( size(theta,1) == $(M-1) == length(isizemm))
    maxtheta=Array{Float64}(undef,size(theta))
    for j=1:$(M-1)
        @nloops $(M-1) i k->(1:size(theta,k+1)) begin
            (@nref $M maxtheta k->(k == 1 ? j : i_{k-1})) = isizemm[j] * 1.5
        end
    end
    BsplineDeformation{$(M-1), order}(isizemm, theta, -maxtheta, maxtheta)
end #quote
end

# Constructor initializes transformation as identity
function BsplineDeformation(order::Int, isizemm::Vector{Float64}, bsplineCount::Vector{Int})
    @debugassert(length(bsplineCount) == length(isizemm))
    theta = zeros(Float64, [length(isizemm); bsplineCount]...)
    BsplineDeformation(order, isizemm, theta)
end

# Set the parameter vector or matrix
function set_theta!(c::BsplineDeformation{N,O}, theta::Array{Float64}) where {N,O}
    @debugassert(length(c.theta) == length(theta))
    for i = 1:length(c.theta)
        c.theta[i] = theta[i]
    end
end

# Retrieve the parameter vector, as a 1D vector
@inline get_theta(c::BsplineDeformation{N,O}) where {N,O} = reshape(c.theta, length(c.theta))

# Retrieve the parameter matrix reference
@inline get_theta_mat(c::BsplineDeformation{N,O}) where {N,O} = c.theta

# Get lower bounds for the parameters
@inline lower_bound(c::BsplineDeformation) = reshape(c.mintheta, length(c.mintheta))

# Get upper bounds for the parameters
@inline upper_bound(c::BsplineDeformation) = reshape(c.maxtheta, length(c.maxtheta))

function copy(c::BsplineDeformation{N,O}) where {N,O}
    newc = BsplineDeformation{N,O}(Base.copy(c.h), Base.copy(c.theta),
            Base.copy(c.mintheta), Base.copy(c.maxtheta))
    copy!(newc, c)
    newc.thetainds = Base.copy(c.thetainds)
    newc.prodbeta = Base.copy(c.prodbeta)
    return newc
end

function copy!(new::T, c::T) where {T<:BsplineDeformation}
    Base.copyto!(new.theta, c.theta)
    Base.copyto!(new.mintheta, c.mintheta)
    Base.copyto!(new.maxtheta, c.maxtheta)
    Base.copyto!(new.h, c.h)
    Base.copyto!(new.ofs, c.ofs)
    new.thetainds = Base.copy(c.thetainds)
    new.prodbeta = Base.copy(c.prodbeta)
    return nothing
end

# Returns true if c is identity
function is_identity(c::BsplineDeformation)
    is_id = true
    for i=1:length(c.theta)
        if c.theta[i] != 0
            is_id = false
            break
        end
    end
    return is_id
end

@inline is_linear(c::BsplineDeformation) = true

function sub2indt(thetas, subs, sind, modif)
    out = 1
    mul = 1
    for i = 1:length(thetas)
        if sind == i
            out += (subs[i] + modif - 1) * mul
        else
            out += (subs[i] - 1) * mul
        end
        mul *= thetas[i]
    end
    return out
end

"""
    get_nhood_nodes!(nhood, def, i)

Return neighbor indicies for node `i` in b-spline node mesh.
"""
function get_nhood_nodes!(nhood::Array{Int,1}, def::BsplineDeformation{N,K}, i::Int) where {N,K}
    thetas = size(def.theta)
    ind = 1
    subs = CartesianIndices(thetas)[i] # ind2sub(thetas, i) 
    for sind = 2:length(subs)
        if subs[sind] - 1 > 0
            #nhood[ind] = sub2indt(thetas, subs, sind, -1)
            ind +=1
        end
        if subs[sind] + 1 <= thetas[sind]
            nhood[ind] = sub2indt(thetas, subs, sind, 1)
            ind +=1
        end
    end
    return ind - 1
end

function get_diff_regularization_matrix_RtR(def::BsplineDeformation{N,K}, mu::Float64) where {N,K}
    thetal = length(get_theta(def))
    RtR = spzeros(thetal, thetal)
    if mu == 0.
        return RtR
    end
    nhood = Array{Int}(2*N)
    mu2 = mu * mu
    for i=1:thetal
        len = get_nhood_nodes!(nhood, def, i)
        RtR[i, i] = 2 * len * mu2
        for ninx = 1:len
            RtR[i,nhood[ninx]] = -2 * mu2
        end
    end
    return RtR
end

"""
    create_identity(isizemm, ::Type{BsplineDeformation}, params)
The `params[1]` is order of b-spline, `params[2:end]` is number of b-splines in each dimension.
"""
@inline create_identity(isizemm::Vector{T}, ::Type{BsplineDeformation}, params = [3, 7, 7]) where {T<:Number} =
            BsplineDeformation(params[1], isizemm, params[2:end])

#transform point from physical cordinates into b-splines matrix coordinates +1 is because of 1 based indexing
@inline x2xt(x::Float64, h::Float64, ofs::Float64) = x / h + ofs + 1

#compute b-spline value
@inline bspln(x::Number, bsplnt::Type{BsplineDeformation{N,K}}) where {N,K} = bsplines.bspln(x, K)
#compute linear b-spline value
@inline bspln(x::Number, bsplnt::Type{BsplineDeformation{N,1}}) where {N} = bsplines.lbspln(x)
#compute cubic b-spline value
@inline bspln(x::Number, bsplnt::Type{BsplineDeformation{N,3}}) where {N} = bsplines.cbspln(x)

#compute b-spline derivative
@inline bderiv(x::Number, bsplnt::Type{BsplineDeformation{N,K}}) where {N,K} = bsplines.bsplnfder(x, K)
#compute linear b-spline derivative
@inline bderiv(x::Number, bsplnt::Type{BsplineDeformation{N,1}}) where {N} = bsplines.lbderiv(x)
#compute cubic b-spline derivative
@inline bderiv(x::Number, bsplnt::Type{BsplineDeformation{N,3}}) where {N} = bsplines.cbderiv(x)
    
# Compute the shift of the point inp using cubic B-splines and store it to outp 
# x0 is added to the shift
@generated function transform_point_relative!(c::BsplineDeformation{N,O}, inp::Vector{Float64}, outp::Vector{Float64}, x0::Vector{Float64}) where {N,O}
quote
    @debugassert(length(inp)==$N)
    @debugassert(length(x0)==$N)
    @debugassert(length(outp)==$N)

    # transform coordinates into bspline matix
    @nexprs $N k->(xt_k = x2xt(inp[k], c.h[k], c.ofs[k]))

    # init outp by x0
    @nexprs $N k->(outp[k] = x0[k])

    @nexprs $N k->(sz_k=size(c.theta, k + 1))
    
    # find out what indices contribute
    @nexprs $N k->(mi_k=bsplines.bsupport_min(xt_k, sz_k, O))
    @nexprs $N k->(ma_k=bsplines.bsupport_max(xt_k, sz_k, O))
    
    @nloops $N i k->(mi_k:ma_k) k->(b_k = bspln(xt_k-i_k, typeof(c))) begin
        st = 1.
        @nexprs $N k->(st *= b_k)
        ct1 = 0
        stride = $N
        @nexprs $N k->(begin ct1 += (i_k - 1) * stride; stride *= sz_k end)
        for j = 1:$N
        #The next line (commented) allocates in each execution I have no idea why. It is not
        #related to ct definition: ct = c.theta because ct = Base.copy(c.theta) also allocates.
            # outp[j] += (@nref $(N+1) ct k->(k == 1 ? j : i_{k-1})) * st
        #this is the solution:
            outp[j] += c.theta[j + ct1] * st
        #this line also allocates:
            # outp[j] += ct[j,i_1,i_2] * st
        end
    end
    return nothing
end
end

@inline transform_point!(c::BsplineDeformation{N,O}, inp::Vector{Float64}, outp::Vector{Float64}) where {N,O} =
            transform_point_relative!(c, inp, outp, inp)

# Transform keypoint i of pos inp using c with the data precomputed by set_keypoints!()
function transform_point_relative_kpt!(c::BsplineDeformation{N,O}, outp::Vector{Float64}, kpts::Vector{U}, kptin::Int, x0::Vector{Float64}) where {N,O,U<:keypoints.AbstractKeypoint}
    #initialize linear indicies (avoid allocation)
    supp = size(c.prodbeta, 1)
    lib = (kptin - 1) * supp
    lit = lib * N
    
    for dim = 1:N                  # TODO: JK: many times could be removed by setting y0=x0
        outp[dim] = x0[dim]
    end
    for i=1:supp
        lib += 1
        for dim = 1:N
            lit += 1
            outp[dim] += c.theta[c.thetainds[lit]]*c.prodbeta[lib]
        end
    end
    return nothing
end

@inline transform_point_kpt!(c::BsplineDeformation{N,O}, outp::Vector{Float64}, kpts::Vector{U}, kptin::Int) where {N,O,U<:keypoints.AbstractKeypoint} =
            transform_point_relative_kpt!(c, outp, kpts, kptin, kpts[kptin].pos)

# Computes the coordinate change of one normal vector 'inv' using b-splines store the resuln into outv and adds n0
@generated function transform_normal_relative!(c::BsplineDeformation{N,O}, inv::Vector{Float64}, outv::Vector{Float64}, inp::Vector{Float64}, n0::Vector{Float64}) where {N,O}
quote
    @debugassert(length(inv)==$N)
    @debugassert(length(outv)==$N)
    @debugassert(length(inp)==$N)

    # transform coordinates into bspline matix
    @nexprs $N k->(xt_k = x2xt(inp[k], c.h[k], c.ofs[k]))

    @nexprs $N k->(sz_k=size(c.theta, k + 1))
    
    # find out what indices contribute
    @nexprs $N k->(mi_k=bsplines.bsupport_min(xt_k, sz_k, O))
    @nexprs $N k->(ma_k=bsplines.bsupport_max(xt_k, sz_k, O))

    #precompute b-spline values and derivations
    betacnt = 1
    @nexprs $N k->(betacnt *= ma_k - mi_k + 1)
    beta = ones(Float64, $N, betacnt)
    for j = 1:$N
        lind = 1
        #go through the support
        @nloops $N i k->(mi_k:ma_k) k->(begin b_k = bspln(xt_k-i_k, typeof(c)); db_k = bderiv(xt_k-i_k, typeof(c)) / c.h[k] end) begin
            @nexprs $N k->(beta[j, lind] *= (k == j ? db_k : b_k))
            lind += 1
        end
    end

    #initialize output vector
    @nexprs $N k->(outv[k] = n0[k])
    
    st = zeros(Float64, $N)
    for j = 1:$N
        lind = 1
        st = fill!(st, 0.)
        #go through the support
        @nloops $N i k->(mi_k:ma_k) begin
            ct = j #linear index to c.theta see tansform_point!() for explanation
            stride = $N
            @nexprs $N k->(begin ct += (i_k - 1) * stride; stride *= sz_k end)
            
            @nexprs $N l->(st[l] += c.theta[ct] * beta[l, lind])
            lind += 1
        end
        outv[j] +=  sum(st .* inv)
    end
    return nothing
end
end

function transform_normal!(c::BsplineDeformation{N,O}, inv::Vector{Float64}, outv::Vector{Float64}, inp::Vector{Float64}) where {N,O}
    transform_normal_relative!(c, inv, outv, inp, inv)
end

# returns the constatnt element phi_0(x) of transform point function.
@inline transform_initial(def::BsplineDeformation{N,O}, v::Vector{Float64}) where {N, O} = v

@inline function transform_initial!(
      y,def::BsplineDeformation{N,O}, v::Vector{Float64}) where {N, O} 
  Base.copyto!(y,v)
end

@inline transform_initial(def::BsplineDeformation{N,O}, kpts::Vector{T}, i::Int) where {N, O, T<:keypoints.AbstractKeypoint} = kpts[i].pos

function transform_initial!(y, def::BsplineDeformation{N,O}, kpts::Vector{T}, i::Int) where {N, O, T<:keypoints.AbstractKeypoint}
    for j = 1:length(kpts[i].pos)
        y[j] = kpts[i].pos[j]
    end
end

# adds contribution of one normal to the gradient
@generated function add_grad_contrib!(grad::AbstractArray{Float64,1}, dval::Float64, inv::Vector{Float64}, c::BsplineDeformation{N,O}, inp::Vector{Float64}) where {N,O}
quote
    @debugassert(length(inp)==$N)

    # transform coordinates into bspline matix
    @nexprs $N k->(xt_k = x2xt(inp[k], c.h[k], c.ofs[k]))
    
    @nexprs $N k->(sz_k=size(c.theta, k + 1))

    # find out what indices contribute
    @nexprs $N k->(mi_k=bsplines.bsupport_min(xt_k, sz_k, O))
    @nexprs $N k->(ma_k=bsplines.bsupport_max(xt_k, sz_k, O))

    dval /= norm2(inv)
    #go through the support
    @nloops $N i k->(mi_k:ma_k) k->(b_k = bspln(xt_k-i_k, typeof(c)))begin
        st = 1.
        @nexprs $N k->(st *= b_k)
        ct = 0 #index to grad see tansform_point!() for explanation
        stride = $N
        @nexprs $N k->(begin ct += (i_k - 1) * stride; stride *= sz_k end)
        for j = 1:$N
            grad[j + ct] += dval * inv[j] * st  #the result is different for dval*inv[j]*st and st*dval*inv[j]
        end
    end
    return nothing
end
end

# adds contribution of one normal to the gradient for precomputed keypoints
function add_grad_contrib_kpt!(grad::AbstractArray{Float64,1}, dval::Float64, c::BsplineDeformation{N,O}, kptsf::Vector{V}, kptsg::Vector{U}, kptin::Int) where {N,O,U<:keypoints.AbstractKeypoint,V<:keypoints.AbstractKeypoint}
    #initialize linear indices (avoid allocation)
    supp = size(c.prodbeta, 1)
    lib = (kptin - 1) * supp
    lit = lib * N
    
    dval /= norm2(kptsg[kptin].normal)
    for i=1:supp
        lib += 1
        for dim = 1:N
            lit += 1
            grad[c.thetainds[lit]] += dval*kptsg[kptin].normal[dim]*c.prodbeta[lib]
        end
    end
    return nothing
end

# precompute b-spline values and coresponding linear index into theta
# c(kpts[i].pos[j]) = sum_s(c.thetainds[s,j,i]*c.prodbeta[s,i])
@generated function set_keypoints!(c::BsplineDeformation{N,O}, kpts::Vector{T}) where {N,O,T<:keypoints.AbstractKeypoint}
quote
    #initialize
    lenkpts = length(kpts)
    cbtl = round(Int, (2 * bsplines.ehsupp(O))^$N)
    # @debugprintln("set_keypoints! for Bsplines called cbtl=$cbtl")
    c.thetainds = ones(Int, $N, cbtl, lenkpts)          #linear index into theta
    c.prodbeta = zeros(Float64, cbtl, lenkpts)          #b-spline values = b_1*b_2*...b_N
    @nexprs $N k->(sz_k=size(c.theta, k + 1))           #size of b-spline coeficient nmatrix
    
    for kptin = 1:lenkpts
        # transform coordinates into bspline matrix
        @nexprs $N k->(xt_k = x2xt(kpts[kptin].pos[k], c.h[k], c.ofs[k]))

        # find out what indices contribute
        @nexprs $N k->(mi_k=bsplines.bsupport_min(xt_k, sz_k, O))
        @nexprs $N k->(ma_k=bsplines.bsupport_max(xt_k, sz_k, O))
        
        #precompute b-spline values
        indb = (kptin-1) * cbtl + 1
        indt = (kptin-1) * $N * cbtl + 1
        #go through the support
        @nloops $N i k->(mi_k:ma_k) k->(begin b_k = bspln(xt_k-i_k, typeof(c)); end) begin
            c.prodbeta[indb] = 1
            @nexprs $N k->(c.prodbeta[indb] *= b_k)  #b-spline values = b_1*b_2*...b_N
            ct1 = 0
            stride = $N
            @nexprs $N k->(begin ct1 += (i_k - 1) * stride; stride *= sz_k end)
            for j = 1:$N
                c.thetainds[indt] = j + ct1           #linear index into theta
                indt += 1
            end
            indb += 1
        end
    end
    return nothing
end
end

# iterator for all coefficients of the linear combination at keypoint 'i'.
# set_keypoints! must have been called - we are reusing and existing work there
# but it might be more clear to redo it.
struct BsplineIterator{N,O}
    def::BsplineDeformation{N,O}
    i::Integer # index into def.prodbeta
    n::Integer # last index into def.prodbeta
end

# TODO:  comment out for Julia 0.5
Base.eltype(::Type{BsplineIterator}) = (Int,Float64)
#Base.iteratoreltype(::Type{BsplineIterator})=Base.HasElType()
Base.IteratorEltype(::Type{BsplineIterator})=Base.HasElType()
#Base.iteratorsize(::Type{BsplineIterator})=Base.HasLength()
Base.IteratorSize(::Type{BsplineIterator})=Base.HasLength()

function Base.iterate(itr::BsplineIterator{N,O}) where {N,O}
    return ((div(itr.def.thetainds[1+(itr.i-1)*N]-1,N),itr.def.prodbeta[itr.i]),itr.i+1)
end

Base.length(itr::BsplineIterator) = itr.n


function Base.iterate(itr::BsplineIterator{N,O},ind::Integer) where {N,O}
    if ind>itr.n
        return nothing
    else    
        return ((div(itr.def.thetainds[1+(ind-1)*N]-1,N),itr.def.prodbeta[ind]),ind+1)
    end
end


# # the iterator state is a counter into def.prodbeta
# Base.start(itr::BsplineIterator) =  itr.i
# function Base.done(itr::BsplineIterator,ind)
#     ind>itr.n
# end

# # return ((j,val),newstate) where (j,val) is suitable for transform_point_lin!
# function Base.next(itr::BsplineIterator{N,O},state) where {N,O}
#    ((div(itr.def.thetainds[1+(state-1)*N]-1,N),itr.def.prodbeta[state]),state+1)
# end

# now a specialized implementation for B-splines. It ignores `kptsf`
function coefficients_iterator_keypoint(def::deformation.BsplineDeformation{N,O},
                                i::Integer,kptsf::Vector{K}) where {K<:AbstractKeypoint,N,O}
    stride=size(def.prodbeta,1)
    @debugassert(stride>0)
    #@debugprintln("calling Bspline coefficients_iterator_keypoint with i=$i stride=$stride $(1+(i-1)*stride) $(i*stride-1)")
    BsplineIterator(def,1+(i-1)*stride,i*stride-1)
end

#----------------------Inverse-----------------------

# the following functions are needed since in Julia 0.6 generated functions cannot contain closures
# and broadcast operators compile to closures

#function reciprocal(x) = 1 ./ x end

to_array(t::Tuple)=[t...]


"""
``set_inverse!(g,f,imgf,imgg;downsample=1)``

Set a linear (e.g.B-spline) deformation `g` on image `imgg` to be as close as
possible (in the least squares sense) to the inverse of an arbitrary deformation
`f` on image `f`, on pixel coordinates of image `imgf`. For `imgf` and `imgg`,
just the dimensions and pixelspacing are used.
By default, we use 'oversample' sample points per spline in every dimension.
'lambda' is a regularization, needed if the initial transformation does not cover some of the spline knots in 'imgg', set it low in order not to bias the 

"""
@fastmath @generated function set_inverse!(g::BsplineDeformation{N,O},f::AbstractDeformation,imgf::Image{T,N},imgg::Image{T2,N};oversample=4,lambda::Float64=1e-9) where {N,O,T,T2}
quote
    # assert(is_linear(g))
    # loop over (a subset of) pixels in `img`
    sizef = size(imgf)
    sizeg = size(imgg)
    @nexprs $N k->(sz_k=size(g.theta, k + 1))
    @nexprs $N k->(stp_k= max(1,round(Int,sizef[k] / sz_k / oversample)) )
    #print("stp: ",stp_1,stp_2)
    @nexprs $N k->(isf_k=float(Images.pixelspacing(imgf)[k]))

    @nexprs $N k->(isgr_k::Float64=1. / float(Images.pixelspacing(imgg)[k]))
    xf=zeros(Float64, $N)       # point coordinates to imgf
    xg=zeros(Float64, $N)       # physical point coordinates to imgg
    xgp=zeros(Float64, $N)      # pixel point coordinates to imgg

    npts=1
    @nexprs $N k->(npts*=length(1:stp_k:sizef[k])) # number of sampling points
    nbspln=div(length(g.theta),$N) # number of B-spline coefficient (vectors)
    A=spzeros(npts,nbspln) # empty system matrix, sparse not supported at the moment
    #println("A=",size(A))
    rhs=zeros(npts,$N)                 # right hand side vector, for all dimensions
    ipts=0   # point index
    @nloops $N i j->(1:stp_j:sizef[j]) begin
            ipts+=1
            # #@inbounda
            @nexprs $N k->(xf[k]=(i_k-0.5)*isf_k::Float64) # pixel to physical coordinates in `imgf`
            transform_point!(f, xf, xg) # store transformed point to xg
            @nexprs $N k->(xgp[k]=xg[k]*isgr_k+0.5) # physical to pixel coordinates
            # ignore points outside the 'imgg'
            if @nall $N k->(1.0 <= xg[k] <= sizeg[k])
                # transform coordinates into bspline matix
                @nexprs $N k->(xt_k = x2xt(xgp[k], g.h[k], g.ofs[k]))
                # find out what indices contribute
                @nexprs $N k->(mi_k=bsplines.bsupport_min(xt_k, sz_k, O))
                @nexprs $N k->(ma_k=bsplines.bsupport_max(xt_k, sz_k, O))
                @nloops $N l k->(mi_k:ma_k) k->(b_k = bspln(xt_k-l_k, BsplineDeformation{N,O})) begin
                    st = 1. # B-spline tensor product
                    @nexprs $N k->(st *= b_k)
                    ct1 = 1 # index to A
                    stride = 1
                    @nexprs $N k->(begin ct1 += (l_k - 1) * stride; stride *= sz_k end)
                    #println("A[$ipts,$ct1]+=$st   sz=($sz_1,$sz_2)")
                    A[ipts,ct1]+=st
                end
                rhs[ipts,:]=xf-xg
            end     
    end

    # find the coefficients
    #Ainv=Base.LinAlg.pinv(A) # pseudoinverse
    #println("rhs=",rhs)
    #println("A=",A," theta=",size(g.theta), "rhs=",rhs)
    theta=zeros($N,nbspln)
    # TODO: test if it is not faster to form the pseudoinverse or factorization explicitely, avoiding
    # to repeat the iterative solution
    @nexprs $N k-> (theta[k,:]=IterativeSolvers.lsqr(A,rhs[:,k],damp=lambda*sizef[k])) #;verbose=true))
    # theta=Ainv*rhs
    #print("theta=",theta)
    # TODO: set directly to avoid allocation
    set_theta!(g,reshape(theta,length(theta))) 
    ## now checking
    # xfp=zeros(Float64, N)       # point coordinates to imgf
    # for i2=1:stp_2:sizef[2]
    #     for i1=1:stp_1:sizef[1]
    #         @inbounds xf[1]=(i1-0.5)*isf_1::Float64 # physical coordinates in `imgf`
    #         @inbounds xf[2]=(i2-0.5)*isf_2::Float64
    #         transform_point!(f, xf, xg) # store transformed point to xg
    #         transform_point!(g,xg,xfp)  # inverse transformation to xfp
    #         err=norm(xfp-xf,2)
    #         println("i=",[i1,i2]," xf=",xf, " xg=",xg, " xfp=",xfp, " err=",err)
    #     end
    # end
end
end

#----------------------HierarchicalDeformation-----------------------
"""
The transformation is given by

``y=c(x) = x + \\sum_j^N(c_j(x) - x)``

where ``c_j(x)`` is any deformation <: AbstractDeformation and ``N`` is number of
`HierarchicalDeformation` levels.

The `actualLevel` defines the level on which get... set... functions are used,
when `actualLevel == 0` `HierarchicalDeformation` is treated as one level transformation and the
parameter vector (as well as the limits mintheta and maxtheta) consists of all the parameters
from all the internal levels.

!!! note
    ``c_j(x) - x`` is computed by `transform_point_relative!` so any deformation used in
    the `HierarchicalDeformation` should implement it.
"""
mutable struct HierarchicalDeformation <: AbstractDeformation
    def::Vector{AbstractDeformation}
    actualLevel::Int
end

function HierarchicalDeformation(def::Vector{T}) where {T<:AbstractDeformation}
    HierarchicalDeformation(def, 0)
end

# sets the level for multi level HierarchicalDeformation
function set_level!(c::HierarchicalDeformation, level::Int)
    @debugassert(level >= 0 && level <= length(c.def))
    c.actualLevel = level
end

#is normal displacement linear function of theta? For actual level or whole transform
function is_linear(c::HierarchicalDeformation)
    defl = length(c.def)
    if c.actualLevel > 0
        return is_linear(c.def[min(c.actualLevel, defl)])
    else
        islin = true
        for i = 1:defl
            islin &= is_linear(c.def[i])
        end
        return islin
    end
end

function get_diff_regularization_matrix_RtR(c::HierarchicalDeformation, mu::Float64)
    defl = length(c.def)
    if c.actualLevel > 0
        y = get_diff_regularization_matrix_RtR(c.def[c.actualLevel], mu)
    else
        error("Not implemented")
    end
    return y
end


# returns the constatnt element phi_0(x) of transform point function.
function transform_initial(c::HierarchicalDeformation, kpts::Vector{T}, i::Int) where {T<:keypoints.AbstractKeypoint}
    y = zeros(size(kpts[i].pos))
    transform_initial!(y, c, kpts, i)
    return y
end

function transform_initial!(y0, c::HierarchicalDeformation, kpts::Vector{T}, i::Int) where {T<:keypoints.AbstractKeypoint}
    defl = length(c.def)
    if c.actualLevel > 0
        transform_initial!(y0, c.def[c.actualLevel], kpts, i)
        #y = Base.copy(y0)
        for lev = 1:defl
            if lev != c.actualLevel
                #transform_point_relative!(c.def[lev], kpts[i].pos, y, y0)
                #transform_point_relative_kpt!(c.def[lev], kpts[i].pos, y0, y0)
                transform_point_relative_kpt!(c.def[lev], y0, kpts,i, y0)
                #Base.copy!(y0, y) # copy y -> y0
            end
        end
    else
        error("Not implemented")
    end
    return y0
end




# coefficients_iterator_keypoint just delegates to the current level deformation
function coefficients_iterator_keypoint(c::HierarchicalDeformation,
                                i::Integer,kptsf::Vector{K}) where {K<:AbstractKeypoint}
    coefficients_iterator_keypoint(c.def[c.actualLevel],i, kptsf)
end


    
# gets the level for multi level HierarchicalDeformation, if actualLevel==0 deformation is treatetd as one-level,
# thus 1 is returned
get_valid_level(c::HierarchicalDeformation) = max(c.actualLevel, 1)

# Lower bounds for the parameters at level 'actualLevel' or for all levels if actualLevel == 0
function lower_bound(c::HierarchicalDeformation, lev::Int = 0)
    defl = length(c.def)
    if c.actualLevel > 0
        return lower_bound(c.def[min(c.actualLevel, defl)])
    else
        linx = 0
        for i = 1:defl
            linx += length(lower_bound(c.def[i]))
        end
        mintheta = Array{Float64}(undef,linx)
        linx = 1
        for i = 1:defl
            minthetal = lower_bound(c.def[i])
            for j = 1:length(maxthetal)
                mintheta[linx] = minthetal[j]
                linx += 1
            end
        end
        return mintheta
    end
end

# Upper bounds for the parameters at level 'actualLevel' or for all levels if actualLevel == 0
function upper_bound(c::HierarchicalDeformation)
    defl = length(c.def)
    if c.actualLevel > 0
        return upper_bound(c.def[min(c.actualLevel, defl)])
    else
        linx = 0
        for i = 1:defl
            linx += length(upper_bound(c.def[i]))
        end
        maxtheta = Array{Float64}(undef,linx)
        linx = 1
        for i = 1:defl
            maxthetal = upper_bound(c.def[i])
            for j = 1:length(maxthetal)
                maxtheta[linx] = maxthetal[j]
                linx += 1
            end
        end
        return maxtheta
    end
end

# Set the parameter vector or matrix at level 'actualLevel' or the parameter vector at all levels if 'actualLevel == 0'
function set_theta!(c::HierarchicalDeformation, theta::Array{Float64})
    defl = length(c.def)
    if c.actualLevel > 0
        set_theta!(c.def[min(c.actualLevel, defl)], theta)
    else
        linx = 1
        for i = 1:defl
            tlen = length(get_theta(c.def[i]))
            set_theta!(c.def[i], theta[linx:linx+tlen-1])
            linx += tlen
        end
    end
end

# Retrieve the parameter vector, as a 1D vector at level 'actualLevel' or at all levels if actualLevel == 0
function get_theta(c::HierarchicalDeformation)
    defl = length(c.def)
    if c.actualLevel > 0
        return get_theta(c.def[min(c.actualLevel, defl)])
    else
        linx = 0
        for i = 1:defl
            linx += length(get_theta(c.def[i]))
        end
        theta = Array{Float64}(linx)
        linx = 1
        for i = 1:defl
            thetal = get_theta(c.def[i])
            for j = 1:length(thetal)
                theta[linx] = thetal[j]
                linx += 1
            end
        end
        return theta
    end
end

# Retrieve the parameter matrix reference at level 'actualLevel'
function get_theta_mat(c::HierarchicalDeformation)
    if c.actualLevel < 1
        error("ActualLevel should be >= 1. Use set_level!(c::HierarchicalDeformation, level::Int)")
    end
    return get_theta_mat(c.def[min(c.actualLevel, length(c.def))])
end

function copy(c::HierarchicalDeformation)
    defn = [copy(def) for def in c.def]
    return HierarchicalDeformation(defn, c.actualLevel)
end

function copy!(new::T, c::T) where {T<:HierarchicalDeformation}
    for i = 1:length(c)
        copy!(new.def[i], c.def[i])
    end
    new.actualLevel = c.actualLevel
    return nothing
end

# Returns true if c is identity
function is_identity(c::HierarchicalDeformation)
    is_id = true
    for def in c.def
        if !is_identity(def)
            is_id = false
            break
        end
    end
    return is_id
end

function transform_point_relative!(c::HierarchicalDeformation, inp::Vector{Float64}, outp::Vector{Float64}, x0::Vector{Float64})
    warn(" transform_point_relative!() not implemented for HierarchicalDeformation. Default version used")
    transform_point_relative!(c, inp, outp, x0)
end

function transform_point!(c::HierarchicalDeformation, inp::Vector{Float64}, outp::Vector{Float64})
    ## outt = zeros(outp)
    ## for def in c.def
    ##     transform_point_relative!(def, inp, outp, outt)
    ##     outt[:] = outp
    ## end
    ## for i = 1: length(outp)
    ##     outp[i] += inp[i]
    ## end
    ndef=length(c.def)
    transform_point!(c.def[ndef],inp,outp)
    for i=ndef-1:-1:1
        transform_point_relative!(c.def[i],inp,outp,outp)
        end
    return nothing
end

function transform_point_kpt!(c::HierarchicalDeformation, outp::Vector{Float64}, kpts::Vector{U}, kptin::Int) where {U<:keypoints.AbstractKeypoint}
    outt = fill(0.,size(outp)) #zeros(outp)
    for def in c.def
        transform_point_relative_kpt!(def, outp, kpts, kptin, outt)
        outt[:] = outp
    end
    for i = 1: length(outp)
        outp[i] += kpts[kptin].pos[i]
    end
    return nothing
end

function transform_normal_relative!(c::HierarchicalDeformation, inv::Vector{Float64}, outv::Vector{Float64}, inp::Vector{Float64}, n0::Vector{Float64})
    warn(" transform_normal_relative!() not implemented for HierarchicalDeformation. Default version used")
    transform_normal_relative!(c, inv, outv, inp, n0)
end

function transform_normal!(c::HierarchicalDeformation, inv::Vector{Float64}, outv::Vector{Float64}, inp::Vector{Float64})
    #outt = zeros(outv)
    outt = fill(0.,size(outv))
    for def in c.def
        transform_normal_relative!(def, inv, outv, inp, outt)
        outt[:] = outv
    end
    for i = 1: length(outv)
        outv[i] += inv[i]
    end
    return nothing
end

# at level 'c.actualLevel' or for all levels if c.actualLevel == 0
function add_grad_contrib!(grad::AbstractArray{Float64,1}, dval::Float64, inv::Vector{Float64}, c::HierarchicalDeformation, inp::Vector{Float64})
    defl = length(c.def)
    if c.actualLevel > 0
        add_grad_contrib!(grad, dval, inv, c.def[min(c.actualLevel, defl)], inp)
    else
        linx = 1
        for i = 1:defl
            thetat = get_theta(c.def[i])
            add_grad_contrib!(slice(grad,linx:linx + length(thetat) - 1), dval, inv, c.def[i], inp)
            linx += length(thetat)
        end
    end
    return nothing
end

function add_grad_contrib_kpt!(grad::AbstractArray{Float64,1}, dval::Float64, c::HierarchicalDeformation, kptsf::Vector{V}, kptsg::Vector{U}, kptin::Int) where {U<:keypoints.AbstractKeypoint,V<:keypoints.AbstractKeypoint}
    defl = length(c.def)
    if c.actualLevel > 0
        add_grad_contrib_kpt!(grad, dval, c.def[min(c.actualLevel, defl)], kptsf, kptsg, kptin)
    else
        linx = 1
        for i = 1:defl
            thetat = get_theta(c.def[i])
            add_grad_contrib_kpt!(slice(grad,linx:linx + length(thetat) - 1), dval, c.def[i], kptsf, kptsg, kptin)
            linx += length(thetat)
        end
    end
    return nothing
end

function set_keypoints!(c::HierarchicalDeformation, kpts::Vector{T}) where {T<:keypoints.AbstractKeypoint}
    for def in c.def
        set_keypoints!(def, kpts)
    end
    return nothing
end
##########################
# testing

function test_jacobian_2D_rigid()
    angle=10. ; shift=[5.;-20.] ; outpsize=[1000,567];
    theta=vcat(angle, shift) ;
    x=[237.;-198.] ; step=0.01
    c=RigidDeformation2D(outpsize,theta)
    @show Janal= get_jacobian(c,x)
    @show Jnum = segimgtools.numJacobian(t-> begin set_theta!(c,t) ; transform_point(c,x) end ,theta,step)
    @show norm(Janal-Jnum)/norm(Jnum)
end

function test_ScaleDeformation2D()
    img2 = Images.load("imgs/simple.jpg")
    def = ScaleDeformation2D([size(img2)...], [45, 0, 0, 2])
    imgout = similar(img2)
    imgout[1,1]=imgout[2,1] # just to force allocation
    @debugtime( transform_image_lin!(def, imgout, img2, @options),"transform lin")
    #ImageView.view(imgout, name="cubic")
end

function test_bspline_normal()
    img2 = Array{RGB{Normed{UInt8,8}}}(100,100)
    bspdist = 3
    def = BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), [bspdist,bspdist])
    theta0 = get_theta_mat(def)
    yy = 2
    xx = 1
    theta0[1,:,:]=[ 0  0  0; 
                    0  5  0;
                    0 -0  0]


    #testing transform normal
    println("testing transform normal")
    delta = 1.
    h=[delta; 0.]
    pt=[51.; 50.]
    poi1 = transform_point(def, pt)
    poi2 = transform_point(def, pt-h)
    println("n = poi2 - poi1 = $poi2 - $poi1 = $(poi2 - poi1)")
    nn = Array{Float64}(2)
    mul = 1
    transform_normal!(def, h*mul, nn, pt)
    println("$mul * n = $nn")
end

function test_bspline_deformation()
    # img2 = Images.load("imgs/simple.jpg")
    # img2 = Image(Array(RGB{UFixed{UInt8,8}},200,320))
    # img2[100,:]=RGB(0x01,0x01,0x01)
    # ImageView.view(img2)
    imgout = similar(img2)
    imgout[1,1]=imgout[2,1] # just to force allocation
    
    println("rotation using cubic b-splines")
    bspdist = 4
    def = BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bspdist,bspdist])
    theta0 = get_theta_mat(def)
    yy = 2
    xx = 1
    theta0[1,:,:]=[0 size(img2,xx) 2*size(img2,xx) 3*size(img2,xx); 
              -size(img2,xx) 0 size(img2,xx) 2*size(img2,xx);
              -2*size(img2,xx) -1*size(img2,xx) 0 size(img2,xx);
              -3*size(img2,xx) -2*size(img2,xx) -size(img2,xx) 0]
    theta0[2,:,:]=[3*size(img2,yy) 2*size(img2,yy) 1*size(img2,yy) 0; 
               2*size(img2,yy) size(img2,yy) 0 -size(img2,yy);
               size(img2,yy) 0 -size(img2,yy) -2*size(img2,yy);
               0 -size(img2,yy) -2*size(img2,yy) -3*size(img2,yy)]

    @debugtime( transform_image_lin!(def, imgout, img2, @options),"transform lin")
    #ImageView.view(imgout, name="cubic")
    
    println("rotation using linear b-splines")
    bspdist = 2
    def = BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), [bspdist,bspdist])
    # theta0::Array{Float64,3} = randn(bspdist,bspdist,2)*50
    theta0 = get_theta_mat(def)
    theta0[1,:,:]=[ 0 size(img2,xx); 
                    -size(img2,xx) 0]
    theta0[2,:,:]=[ size(img2,yy) 0; 
                    0 -size(img2,yy)]

    @debugtime(transform_image_lin!(def, imgout, img2, @options),"transform lin")
    #ImageView.view(imgout, name="linear")

    #testing transform normal
    println("testing transform normal")
    delta=0.01
    h=[delta;0]
    pt=[60.;30.]
    poi1 = transform_point(def, pt)
    poi2 = transform_point(def, pt+h)
    println("n = poi2 - poi1 = $poi2 - $poi1 = $(poi2 - poi1)")
    nn = Array{Float64}(2)
    mul = 100
    transform_normal!(def, h*mul, nn, pt)
    println("$mul * n = $nn")
    
    for i = 1:bspdist
       for j = 1:bspdist
           theta0[i,j,1]=(i-1)*10
           theta0[i,j,2]=0.
       end
    end
    println(get_theta(def))
    pts=[[0; 0.]  [size(img2,1); 0.]  [0.; size(img2,2)]  [size(img2, 1); size(img2, 2)] ]
    for pti in 1:size(pts,2)
        println("transform point $pti")
        ptx=transform_point(def,pts[:,pti])
        println("Deformed $(pts[:,pti]) -> $ptx")
    end
end

function test_hierarchical_deformation()
    img2 = Images.load("imgs/simple.png")
    #ImageView.view(img2)
    imgout = similar(img2)
    imgout[1,1]=imgout[2,1] # just to force allocation
    
    println("rotation using cubic b-splines")
    bspdist = 1
    sizemm = [size(img2)...] .* Images.pixelspacing(img2)
    # def = HierarchicalDeformation([BsplineDeformation(3,sizemm , [bspdist,bspdist]+3);
                                   # BsplineDeformation(3,sizemm , 2*[bspdist,bspdist]+3)])
    def = HierarchicalDeformation([BsplineDeformation(3,sizemm , [bspdist,bspdist]+3);
                                   create_identity(sizemm, RigidDeformation2D)])

    yy = 2
    xx = 1
    #Rotate image
    def.def[1].theta[1,:,:]=[0 size(img2,xx) 2*size(img2,xx) 3*size(img2,xx); 
                                -size(img2,xx) 0 size(img2,xx) 2*size(img2,xx);
                                -2*size(img2,xx) -1*size(img2,xx) 0 size(img2,xx);
                                -3*size(img2,xx) -2*size(img2,xx) -size(img2,xx) 0]
    def.def[1].theta[2,:,:]=[3*size(img2,yy) 2*size(img2,yy) 1*size(img2,yy) 0; 
                                 2*size(img2,yy) size(img2,yy) 0 -size(img2,yy);
                                 size(img2,yy) 0 -size(img2,yy) -2*size(img2,yy);
                                 0 -size(img2,yy) -2*size(img2,yy) -3*size(img2,yy)]

    set_theta!(def.def[2], [90,0,0.])

    # #Disturb image
    # randn!(def.def[2].theta); def.def[2].theta *= 20

    @debugtime( transform_image_lin!(def, imgout, img2, @options),"transform lin")
    #ImageView.view(imgout, name="cubic")
    
    #testing transform normal
    println("testing transform normal")
    delta=0.01
    h=[delta;delta]
    pt=[1.;200.]
    poi1 = transform_point(def, pt)
    poi2 = transform_point(def, pt+h)
    println("n = poi2 - poi1 = $poi2 - $poi1 = $(poi2 - poi1)")
    nn = Array{Float64}(2)
    mul = 100
    transform_normal!(def, h*mul, nn, pt)
    println("$mul * n = $nn")
end

function test_coefficients_iterator()
    def=deformation.create_identity([100;150], deformation.AffineDeformation2D)
    set_theta!(def,[0.9 ; -0.1 ; 0.1 ; 0.8 ; 5. ; 7. ])
    p=[ 45. ; 120. ]
    @time pt1=transform_point(def,p)
    pt2=zeros(2)
    @time transform_point_lin!(def,p,pt2)
    println("test_coefficients_iterator: pt1=",pt1," pt2=",pt2)
end


function test_bspline_inverse()
    ImageView.closeall()
    img2 = Images.load("imgs/simple.png")
    # img2 = Image(Array(RGB{UFixed{UInt8,8}},200,320))
    # img2[100,:]=RGB(0x01,0x01,0x01)
    #ImageView.imshow(img2)
    imgout = similar(img2)
    #imgout[1,1]=imgout[2,1] # just to force allocation
    
    #println("rotation using cubic b-splines")
    bspdist = 4# 
    def = BsplineDeformation(3,float([size(img2)...]) .* Images.pixelspacing(img2), [bspdist,bspdist])
    theta0 = get_theta_mat(def)
    println("theta0=",size(theta0))
    yy = 2
    xx = 1
    #theta0[1,:,:]=0.                
    #theta0[2,:,:]=-5.
    #theta0[1,2:3,3:4]=10.
     theta0[1,:,:]=[0 size(img2,xx) 2*size(img2,xx) 3*size(img2,xx); 
               -size(img2,xx) 0 size(img2,xx) 2*size(img2,xx);
               -2*size(img2,xx) -1*size(img2,xx) 0 size(img2,xx);
               -3*size(img2,xx) -2*size(img2,xx) -size(img2,xx) 0]
     theta0[2,:,:]=[3*size(img2,yy) 2*size(img2,yy) 1*size(img2,yy) 0; 
                2*size(img2,yy) size(img2,yy) 0 -size(img2,yy);
                size(img2,yy) 0 -size(img2,yy) -2*size(img2,yy);
                0 -size(img2,yy) -2*size(img2,yy) -3*size(img2,yy)]

    @debugtime( transform_image_lin!(def, imgout, img2, @options boundary=1),"transform lin")
    ImageView.imshow(imgout, name="warped")
    
    defg = BsplineDeformation(3,float([size(imgout)...]) .* Images.pixelspacing(imgout), [bspdist,bspdist])
    #println("thetag=",size(defg.theta))
    println("Calling set_inverse")
    @time set_inverse!(defg,def,img2,imgout;lambda=0.,oversample=4)
    imginv=similar(imgout)
    transform_image_lin!(defg, imginv, imgout, @options boundary=1)
    ImageView.imshow(imginv, name="inverse")
    ImageView.imshow(overlay(imgout,img2),name="overlay before")
    ImageView.imshow(overlay(imginv,img2),name="overlay after")
end


end # module deformation
