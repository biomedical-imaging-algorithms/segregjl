"""
Functions to extract keypoints from images and visualization.

Jan Kybic, Martin Dolejsi, 2014-2015
"""
module keypoints

import Images
import ImageFiltering
#import ImageView
using ..segimgtools
using Colors
using Base.Cartesian
import Base.isless
import NearestNeighbors
#using MicroLogging
using DataStructures
using LinearAlgebra
using Logging

export AbstractKeypoint, SimpleKeypoint, TwoclassKeypoint, TwoclassKeypoints, SampledKeypoint
export find_keypoints, find_keypoints_and_neighbors, find_keypoints_from_gradients
export subsample_keypoints_equidistant, reduce_keypoint, sample_normal, sample_normal!, resample_normals!
export hmax_to_maxdist_moving, hmax_to_maxdist, hmax_default
export create_image_with_keypoints

include("debugassert.jl")
switchasserts(true)
include("debugprint.jl")
switchprinting(true)

#configure_logging(min_level=:debug)


"""
All keypoint types should be subtype of AbstractKeypoints.
Any keypoind type is expected to contain at least the fields:
* `pos::Vector{Float64}`: position of the keypoint
* `normal::Vector{Float64}`: normal vector

The keypoints to be used on the fixed image should contain also:
* `count::Float64`: number of boundary points
"""
abstract type AbstractKeypoint end

"""
The simplest keypoint possible.
"""
mutable struct SimpleKeypoint <: AbstractKeypoint
    """`pos::Vector{Float64}`: position of the keypoint"""
    pos::Vector{Float64}
    """`normal::Vector{Float64}`: normal vector"""
    normal::Vector{Float64}
end    

function SimpleKeypoint(k::AbstractKeypoint)
    SimpleKeypoint(k.pos, k.normal)
end


"""
Type of keypoint that stores the image values along a normal.
"""
mutable struct SampledKeypoint{T} <: AbstractKeypoint
    pos::Vector{Float64} # position of the keypoint
    normal::Vector{Float64} # normal vector
    count::Float64 # number of boundary points. 
    pixels::Vector{T} # here we store the pixels, see 'sample_normal'
end

"""
Type for keypoints on the boundary of homogenous classes.
"""
mutable struct TwoclassKeypoint <: AbstractKeypoint
    pos::Vector{Float64} # position of the keypoint
    count::Float64 # number of boundary points
    normal::Vector{Float64} # normal vector
    classpos::UInt8 # class in the positive direction of the normal vector
    classneg::UInt8 # class in the negative direction of the normal vector
end

"""
Here we store temporary data about each boundary point.
"""
mutable struct ProtoKeyPoint
    count::Int             # number of boundary points
    size::Float64          # edgelet size (length in 2D, arean in 3D)
    pos::Vector{Float64}   # sum of pixel coordinates x
    pos2::Matrix{Float64}  # sum of x^t x
    c1::Int                # class 1
    x1sum::Vector{Float64} # sum of pixel coordinates of class 1
    c2::Int                # class 2
    x2sum::Vector{Float64} # sum of pixel coordinates of class 2
end

function ProtoKeyPoint(c1,c2,d) # with constructor (a factory function)
    return ProtoKeyPoint(0,0,zeros(Float64,d),zeros(Float64,d,d),
                         c1,zeros(Float64,d),c2,zeros(Float64,d))
end

"""
Here we store temporary data about each boundary point
"""
mutable struct ProtoKeyPointSmall
    parent::Int32           # parent index
    pos::Vector{Int}        # sum of pixel coordinates x
    dist2::Float64          # distance to parent
    coord::Int32            # do not search in this direction
end

"""
Here we store temporary data about each boundary point
"""
mutable struct ProtoKeyPointSmallest
    pos::Vector{Int}        # sum of pixel coordinates x
    coord::Int              # do not search in this direction
end

const TwoclassKeypoints = Vector{TwoclassKeypoint}

"""
    hmax_to_maxdist(hmax, kt)
Return half the number of sampled pixels in the fixed image,
for a keypoint type `kt`, given the maximum allowed displacement`hmax`.
"""
function hmax_to_maxdist(hmax::Integer,kt::Type{TwoclassKeypoint})
    2hmax
    # hmax+1
end

function hmax_to_maxdist(hmax::Integer,kt::Type{SampledKeypoint{T}}) where {T}
    #2hmax
    hmax
end

"""
    hmax_to_maxdist_moving(hmax, kt)
Return half the number of sampled pixels in the moving image,
for a keypoint type `kt`, given the maximum allowed displacement`hmax`.
"""
function hmax_to_maxdist_moving(hmax::Integer,kt::Type{TwoclassKeypoint})
    2hmax
    # hmax+1
end

function hmax_to_maxdist_moving(hmax::Integer,kt::Type{SampledKeypoint{T}}) where {T}
    2hmax
end

"""
    findlast(v)
Find the last nonzero value in an array `v` and return its index or 0 if not found.

Similar to array.jl:findfirst
"""
function findlast(v::AbstractArray{T}) where {T}
    for i= length(v):-1:1
        if v[i] != zero(T)
            return i
        end
    end
    return 0
end

"""
    findlastnot{T}(v, x)
Find the last element in an array `v` that is not equal to `x` and return its index or 0 if not found.
"""
function findlastnot(v::AbstractArray{T},x::T) where {T}
    for i= length(v):-1:1
        if v[i] != x
            return i
        end
    end
    return 0
end

"""
    findfirstnot{T}(v, x)
Find the first element in an array `v` that is not equal to `x` and return its index or 0 if not found.

Similar to array.jl:findfirst
"""
function findfirstnot(v::AbstractArray{T},x::T) where {T}
    for i= 1:length(v)
        if v[i] != x
            return i
        end
    end
    return 0
end

"""
    fill_zero!(v, x)
Find the first and last elements in an array `v` that are not equal to `x` and fill the tails
using their values (first in the beginning, and last at the end).
"""
function fill_zero!(v::AbstractArray{T},x::T) where {T}
    inz = findfirstnot(v, x);
    if inz>0 # we need a non-zero element
        v[1:inz - 1] .= v[inz]
    end
    inz=findlastnot(v,x)
    if inz>0 # we need a non-zero element
        v[inz + 1:end] .= v[inz]
    end
    return nothing
end


""" 
    smallest_eigv(a)

    find the eigenvector corresponding to the smallest eigenvalue
"""
function smallest_eigv(a::Matrix)
    e=eigen(a)
    i=Base.argmin(e.values)
    return e.vectors[:,i]
end

"""
    process_keypoint(keyval, nlength, pos = keyval.pos/keyval.count, center = keyval.pos/keyval.count)
Auxiliary function for `find_keypoints`. It takes a
prototype keypoint (mean coordinates of the points on the boundary
between two superpixels and their squares) and calculates the normal
of desired length and determines the classes with respect to the
normal orientation.
"""
function process_keypoint(keyval::ProtoKeyPoint, nlength, pos, center)
    #m=keyval.pos2/keyval.count-center*center' # covariance matrix
    # normal is the eigenvector of the smallest eigenvalue   
    # WARNING: we assume that the eigenvectors
    # are normalized to unit length
    #lv = eigen(keyval.pos2 / keyval.count - center * center')
    #if lv.values[1] != minimum(lv.values)
    #    println("eigenvalues=",lv.values)
    #end        
    #@debugassert(lv.values[1] == minimum(lv.values))
    #normv=lv.vectors[:, 1]
    normv=smallest_eigv(keyval.pos2 / keyval.count - center * center')
    @debugassert(abs(norm(normv) - 1) < 0.01)
    # we now need to determine the classes
    if dot(keyval.x2sum - keyval.x1sum, normv) > 0.
        cpos = keyval.c2; cneg = keyval.c1
    else
        cpos = keyval.c1; cneg = keyval.c2
    end
    return TwoclassKeypoint(pos, keyval.count, normv * nlength, cpos, cneg)
end

function process_keypoint(keyval::ProtoKeyPoint, nlength)
    return process_keypoint(keyval, nlength, keyval.pos/keyval.count, keyval.pos/keyval.count)
end

"""
    update_covariance!(pos2, c, s)
Auxiliary function for `find_keypoints`. It updates the sum of tensor
products of the point coordinates, used to calculate the covariance
matrix. `c` is the coordinate, `pos2` is the matrix and `s` is there only
to ditinguish betweeen 2D, 3D and ND case.
"""
function update_covariance!(pos2, c, s::Image{T, 2}) where {T}
    @debugassert(2 == length(c))
    pos2[1,1] += c[1] * c[1]
    pos2[2,2] += c[2] * c[2]
    ct = c[1] * c[2]
    pos2[1,2] += ct; pos2[2,1] += ct
    return nothing
end

function update_covariance!(pos2, c, s::Image{T, 3}) where {T}
    @debugassert(3 == length(c))
    pos2[1,1] += c[1] * c[1]
    pos2[2,2] += c[2] * c[2]
    pos2[3,3] += c[3] * c[3]
    ct = c[1] * c[2]
    pos2[1,2] += ct; pos2[2,1] += ct
    ct = c[1] * c[3]
    pos2[1,3] += ct; pos2[3,1] += ct
    ct = c[2] * c[3]
    pos2[2,3] += ct; pos2[3,2] += ct
    return nothing
end

function update_covariance!(pos2, c, s::Image)
    pos2 += c * c'
    return nothing
end

#protoKeyPoint_closure(c1,c2,n) = () -> ProtoKeyPoint(c1,c2,n)

"""
    check_differs!{T,N}(x1, x2, s, l, kpts)
Auxiliary function for `find_keypoints`, checks if two pixels at coordinates `x1`, `x2`
are from different superpixels and from different classes. If they are, the point is added to
the dictionary of prototype keypoints, and an existing keypoint parameters are updated.
"""
@generated function check_differs!(x1, x2, s::Image{T, N}, l, kpts) where {T,N}
quote
    @nexprs $N k->(x1_k = x1[k])
    @nexprs $N k->(x2_k = x2[k])
    if (@nref $N s x1) > (@nref $N s x2)
        st = ((@nref $N s x1), (@nref $N s x2))
    else
        st = ((@nref $N s x2), (@nref $N s x1))
    end
    if st[1] != st[2]                       # two different superpixels
        if l[st[1]] != l[st[2]]             # two different classes, let us update the table
            c = (x1 + x2) / 2. .- 0.5;       # this is a center, a float vector
            c .*= Images.pixelspacing(s)    #convert to physical units (mm)
            # update the value for (s1, s2), creating it if needed
            facearea = 1.
            facearea *= @nexprs $N k->((abs(x1[k] - x2[k])) == 0 ? Images.pixelspacing(s)[k] : 1)
            # the following no longer works in Julia 0.6
            #kpt=get!(()->ProtoKeyPoint(l[st[1]], l[st[2]], $N), kpts, st)
            #kpt= get!(protoKeyPoint_closure(l[st[1]], l[st[2]], $N),kpts,st) 
            if ((kpt=get(kpts,st,nothing))==nothing)
                kpt=ProtoKeyPoint(l[st[1]], l[st[2]], $N)
                kpts[st]=kpt
            end
            kpt.size += facearea
            kpt.count += 1
            kpt.pos += c;
            update_covariance!(kpt.pos2, c, s)
            if (@nref $N s x1) > (@nref $N s x2)
                kpt.x1sum += (x1 .- 0.5) .* Images.pixelspacing(s)
                kpt.x2sum += (x2 .- 0.5) .* Images.pixelspacing(s)
            else
                kpt.x2sum += (x1 .- 0.5) .* Images.pixelspacing(s)
                kpt.x1sum += (x2 .- 0.5) .* Images.pixelspacing(s)
            end
        end
    end
    return nothing
end
end

"""
    add_keypoints!(nhoodKP, kpts, sp2i, ind, tSP, nhSP)
Auxiliary function for `find_nhood_keypoints`. Adds keypoints to n-hood
"""
function add_keypoints!(nhoodKP, kpts, sp2i, ind, tSP, nhSP)
    found = false
    if tSP > nhSP
        sp = (tSP, nhSP)
    else
        sp = (nhSP, tSP)
    end
    kp = get(kpts, sp, nothing)
    if kp != nothing # does the keypoint between superpixels sp[1] and sp[2] exist?
        #@bp
        nhKP = get(sp2i, sp, 0)
        if ind != nhKP   # to avoid self reference
            push!(nhoodKP[ind], nhKP)
            found = true
        end
    end
    return found
end

"""
    find_nhood_keypoints{T}(kpts, nhoodSP)
Goes through the keypoints and return neighbours for every keypoint.
"""
function find_nhood_keypoints(kpts, nhoodSP::Array{Set{T}}) where {T}
    count = length(kpts)
    sp2i=Dict{Tuple{UInt16, UInt16}, Int64}()
    ind = 1
    # assigns numbers to all keypoints and stores them in a dictionary indexed by superpixel numbers
    for keyval in kpts
        get!(sp2i,keyval[1], ind) # Q: why not sp2i[keyval[i]]=ind ? A: because sp2i is Dict, and keyval[1] is tuple
        ind += 1
    end
    nhoodKP = [Set{T}() for i=1:count] # array of sets to contain neighbors for each keypoint
    ind = 1
    for keyval in kpts                      # for all the protokeypoints (or keypoints)
        for spn = 1:length(keyval[1])       # for all superpixels attached to keypoint (2)
            for nhSP in nhoodSP[keyval[1][spn]] # for al the neighbours uf the superpixel keyval[1][spn]
                add_keypoints!(nhoodKP, kpts, sp2i, ind, keyval[1][spn], nhSP)
                # TODO decide what nhood is the usable one
                # if add_keypoints!(nhoodKP, kpts, sp2i, ind, keyval[1][spn], nhSP)
                    # continue
                # end
                # for nhnhSP in nhoodSP[nhSP]
                    # if add_keypoints!(nhoodKP, kpts, sp2i, ind, nhSP, nhnhSP)
                        # break
                    # end
                # end
            end
        end
        ind += 1
    end
    return nhoodKP
end

""" converts a tuple into an array. This is needed since [t...] does not work in generated functions in Julia 0.6 """
to_array(t::Tuple)=[t...]

"""
    find_keypoint_candidates(s, l)
Auxiliary function for `find_keypoints`.
Go through the image and find boundaries between superpixels.
We only check vertical and horizontal boundaries.
The dictionary kpts will accumulate coordinates of boundary points
between superpixels assigned to different classes
"""
@generated function find_keypoint_candidates(s::Image{U, N}, l::Vector{T}) where {T<:Integer, U<:Integer, N}
quote
    imSize = to_array(size(s))
    kpts=Dict{Tuple{Int, Int}, ProtoKeyPoint}()

    # go over all pixels
    base = Array{Int32}(undef,$N)
    nhood = Array{Int32}(undef,$N)
    @nloops $N i d->(1:imSize[d] - 1) begin
        @nexprs $N k->(base[k] = i_k)
        # check all neighbors of base=i obtained by incrementing one of the coordinates
        for j=1:$N
            @nexprs $N k->(nhood[k] = i_k)
            nhood[j] += 1 # the new coordinate is in nhood
            check_differs!(base, nhood, s, l, kpts)
        end
    end
    return kpts
end
end


"""
    find_keypoints(s, l, minboundarysize = 5 ^ (N - 1), nlength = 1)
Given an image segmented to superpixels and with classes assigned to each superpixel,
return an array of Keypoints.
"""
function find_keypoints(s::Image{U, N}, l::Vector{T}, minboundarysize = 5 ^ (N - 1), nlength = 1) where {T<:Integer, U<:Integer, N}
    # 's' is an integer matrix of the superpixel number for each pixel
    # 'l' is a mapping from superpixel numbers to classes
    # optional paramater "minboundarysize" - boundaries consisting of less than "minboundarysize"
    #                                        pixels/voxels are ignored
    # returns an array of Keypoint
    # find keypoint candidates
    kpts = find_keypoint_candidates(s, l)
    # filter out very small boundaries
    filter!(kv -> kv[2].size>=minboundarysize, kpts)
    # construct Keypoint structures - calculate landmark positions and normals
    # the comprehension "for keyval in kpts" gives key,value pairs
    return [ process_keypoint(keyval[2], nlength)::TwoclassKeypoint for keyval in kpts ]::TwoclassKeypoints
end

"""
    find_keypoints_and_neighbors(s, l, nhoodSP, minboundarysize = 5 ^ (N - 1), nlength = 1)
The same as `find_keypoints` but returns also array of sets defining neighbouring keipoints.
`nhoodSP` is calculated by `SLICsuperpixels.getNeighbors`
"""
function find_keypoints_and_neighbors(s::Image{U, N}, l::Vector{T}, nhoodSP, minboundarysize = 5 ^ (N - 1), nlength = 1) where {T<:Integer, U<:Integer, N}
    kpts = find_keypoint_candidates(s, l)
    # @debug "Found $(length(kpts)) keypoint candidates"
    filter!(kv -> kv[2].size >= minboundarysize, kpts)
    kpout = TwoclassKeypoint[ process_keypoint(keyval[2], nlength)::TwoclassKeypoint for keyval in kpts ]
    return (kpout, find_nhood_keypoints(kpts, nhoodSP))
end

const settype = UInt32  #type used for n-hood set
isless(a::ProtoKeyPointSmall, b::ProtoKeyPointSmall) = isless(a.dist2, b.dist2)

# The following is for finding keypoints from a segmentation without superpixels
"""
    recalculate_heap!(vec, npoi, newparent, pxsp)
Maintain the heap structure.
"""
function recalculate_heap!(vec, npoi, newparent, pxsp)
    for tpoi in vec
        ndist2 = sum(((tpoi.pos - 0.5) .* pxsp - npoi.pos) .^ 2)
        if ndist2 < tpoi.dist2
            tpoi.parent = newparent
            tpoi.dist2 = ndist2
        end
    end
    heapify!(vec)
    return nothing
end

"""
    expand_point!(s, candidates, nhoodKP, opened, closed, tpoi, keypointdist)
Search the image and find candidate keypoints.
"""
@generated function expand_point!(s::Image{T,N}, candidates, nhoodKP, opened, closed, tpoi, keypointdist) where {T,N}
#TODO try to use parent = candidates[tpoi.parent] to speed-up
quote
    parent = candidates[tpoi.parent]
    npos = zeros(Int, $N)
    nposIn = zeros(Int, $N)
    ssize = size(s)
    # for all neighbors of tpoi obtained by incrementing or decrementing one
    # of the coordinates (except tpoi.coord)
    #println("expand_point")
    for coord = 1:$N
        if tpoi.coord == coord
            continue
        end
        for step in [-1, 1]
            @nexprs $N k->(npos[k] = tpoi.pos[k])
            npos[coord] += step
            if npos[coord] > 0 && npos[coord] <= ssize[coord]
                #if the point is not expanded
                if (@nref $N closed i->(npos[i]))
                    continue
                end
                # check all neighbors of npos obtained by incrementing or dectementing one
                # of the coordinates
                for coordIn = 1:$N, stepIn in [-1, 1]
                    @nexprs $N k->(nposIn[k] = npos[k])
                    nposIn[coordIn] += stepIn
                    if nposIn[coordIn] > 0 && nposIn[coordIn] <= ssize[coordIn]
                        #if there is an edge
                        if ( @nref $N s i->(npos[i])) != ( @nref $N s i->(round(Int, nposIn[i])))
                            #and if the classes along the edge hold
                            if (( @nref $N s i->(nposIn[i])) == parent.c1 ||
                                ( @nref $N s i->(nposIn[i])) == parent.c2) &&
                               (( @nref $N s i->(npos[i])) == parent.c1 ||
                                ( @nref $N s i->(npos[i])) == parent.c2)
                                dist2 = 0.
                                @nexprs $N i->(dist2 += sqrv(parent.pos[i] - ((npos[i] - 0.5) * Images.pixelspacing(s)[i])) )
                                esize = 1.
                                for j = 1:$N
                                    if coordIn != j
                                        esize *= Images.pixelspacing(s)[j]
                                    end
                                end
                                #if the point is too far, create new keypoint
                                if  dist2 >= keypointdist ^ 2
                                    if ( @nref $N s i->(round(Int, nposIn[i]))) == parent.c1
                                         push!(candidates, ProtoKeyPoint(0, esize, elmult(((npos + nposIn) / 2 - 0.5),Images.pixelspacing(s)), zeros(Float64, $N, $N), ( @nref $N s i->(round(Int, nposIn[i]))), elmult((copy(nposIn) - 0.5),Images.pixelspacing(s)), ( @nref $N s i->(round(Int, npos[i]))), elmult((copy(npos) - 0.5), Images.pixelspacing(s))))
                                    else
                                         push!(candidates, ProtoKeyPoint(0, esize, elmult(((npos + nposIn) / 2 - 0.5),Images.pixelspacing(s)), zeros(Float64, $N, $N), ( @nref $N s i->(round(Int, npos[i]))), elmult((copy(npos) - 0.5),Images.pixelspacing(s)), ( @nref $N s i->(round(Int, nposIn[i]))), elmult((copy(nposIn) - 0.5),Images.pixelspacing(s))))
                                    end
                                    push!(nhoodKP, Set{settype}(tpoi.parent))
                                    push!(nhoodKP[tpoi.parent], length(candidates))
                                    tpoi.parent = length(candidates)
                                    parent = candidates[tpoi.parent]
                                    recalculate_heap!(opened, candidates[end], length(candidates), Images.pixelspacing(s))
                                    dist2 = 0
                                elseif dist2 > 0.       #using the field count a bit differently (filter out single point edges)
                                    parent.count += 1
                                    parent.size += esize
                                end
                                heappush!(opened, ProtoKeyPointSmall(tpoi.parent, copy(npos), dist2, coordIn))
                                heappush!(opened, ProtoKeyPointSmall(tpoi.parent, copy(nposIn), dist2, coordIn))
                                @inbounds ( @nref $N closed i->(npos[i])) = true
                                @inbounds ( @nref $N closed i->(nposIn[i])) = true
                             end
                        end
                    end
                end
            end
        end
    end
    #println("expand_point returned")
    return nothing
end
end


# the following functions are needed since in Julia 0.6 generated functions cannot contain closures
# and broadcast operators compile to closures

""" weighted l2 distance squared """
function wdist2(x,y,w)
   sum( ((x - y) .* w) .^ 2)
end

""" elementwise squaring """
function sqrv(x)
    x .^ 2
end

""" elementwise product """
function elmult(x,y)
    x .* y
end


"""
    find_keypoint_candidates(s, keypointdist, closed)
Find keypoint candidates.
"""
@generated function find_keypoint_candidates(s::Image{T,N}, keypointdist, closed::BitArray{N}) where {T,N}
#candidates - proto keypoints in physical units (mm)
#opened - queue of point coordinates to be evaluated in image units ra(px) and distances in physical units (mm)
quote
    # print("started find_keypoints_candidates")
    candidates = ProtoKeyPoint[]
    opened = ProtoKeyPointSmall[]
    nhoodKP = Set{settype}[]
    ssize = size(s)
    nposi = Array{Int}($N)
    nposx = Array{Int}($N)
    npos = Array{Float64}($N)
    pixelsp=Images.pixelspacing(s)
    @nloops $N x s begin             # for all the pixels in the image
        if !( @nref $N closed x)     # if not visited
            # check all neighbors of x obtained by incrementing or decrementing one of the coordinates
            for coord = 1:$N, step in [-1, 1]
                @nexprs $N k->(nposi[k] = x_k)
                nposi[coord] += step # new coordinate is in nposi
                if nposi[coord] > 0 && nposi[coord] <= ssize[coord]
                    if ( @nref $N s x) != ( @nref $N s i->(nposi[i]))                         #if there is an edge
                         @nexprs $N j->(npos[j] = (x_j + nposi[j]) / 2)
                         @nexprs $N j->(nposx[j] = x_j)   # old coordinate is in nposx
                         esize = 1.
                         for i = 1:$N
                             if coord != i
                                 esize *= Images.pixelspacing(s)[i]
                             end
                         end
                         push!(candidates, ProtoKeyPoint(0, esize, elmult(copy(npos) - 0.5,pixelsp), zeros(Float64, $N, $N), ( @nref $N s x), elmult(copy(nposx) - 0.5,pixelsp), ( @nref $N s i->(round(Int,nposi[i]))), elmult(copy(nposi) - 0.5,pixelsp)))
                         push!(nhoodKP, Set{settype}())
                         # TODO: calculate dist2 using nexprs, is it faster to index in Images.pixelspacing(s)[k] or take this vector..
                         # dist2 = sum(((nposx - npos) .* Images.pixelspacing(s)).^ 2) #distance is in physical units (mm)
                         dist2 = wdist2(nposx,npos,pixelsp) #distance is in physical units (mm)
                         heappush!(opened, ProtoKeyPointSmall(length(candidates), copy(nposx), dist2, coord))
                         heappush!(opened, ProtoKeyPointSmall(length(candidates), copy(nposi), dist2, coord))
                         @inbounds ( @nref $N closed x) = true  #centerpoint is expanded
                         @inbounds ( @nref $N closed i->(nposi[i])) = true
                    end # if 
                end # if nposi
                if ( @nref $N closed x)
                     break
                end
            end # for
           while opened != []
               expand_point!(s, candidates, nhoodKP, opened, closed, heappop!(opened), keypointdist)
           end
        end # if
    end
    candidates1 = ProtoKeyPoint[]
    old2new = zeros(settype, length(candidates))
    for this = 1:length(candidates)             #search the points with size < keypointdist^N / 3 and delete them
        if candidates[this].size > keypointdist ^ ($N-1) / 3
            push!(candidates1, candidates[this])
            old2new[this] = length(candidates1)
        end
    end
    nhoodKP1 = Set{settype}[]
    for this = 1:length(nhoodKP)                #rebuild n-hood
        if old2new[this] != 0
            nnh = Set{settype}()
            for ind in nhoodKP[this]
                if old2new[ind] !=0
                    push!(nnh, old2new[ind])
                end
            end
            push!(nhoodKP1, nnh)
        end
    end
    #print("ending find_keypoints_candidates")
    return (candidates1, nhoodKP1)
    end
end

"""
    find_patches_for_normals!(candidates, s, keypointdist, closed)
For all keypoint candidates, find the class interface patches and covariance matrix of
its coordinates for normal vector computation.
"""
@generated function find_patches_for_normals!(candidates, s::Image{T,N}, keypointdist, closed::BitArray{N}) where {T,N}
    #fills dist2, x1sum, x2sum and count fields for each candidate point
quote
    #println("Starting find_patches_for_normals")
    pxsp = Images.pixelspacing(s)
    patchsize2 = div(keypointdist, 2) ^ 2
    patchsize2 = max((maximum(pxsp) * 2) ^ 2, patchsize2)
    ssize = size(s)
    opened = ProtoKeyPointSmallest[]
    npos = Array{Int}($N)
    nposIn = Array{Int}($N)
    for cand in candidates
         fill!(closed, false )
         @inbounds ( @nref $N closed d->(round.(Int, cand.x1sum[d] / pxsp[d] + 0.5))) = true
         @inbounds ( @nref $N closed d->(round.(Int, cand.x2sum[d] / pxsp[d] + 0.5))) = true
         cand.count = 1
         update_covariance!(cand.pos2, cand.pos, s)
         coord = 1
         for i = 1:$N
             if cand.x1sum[i] != cand.x2sum[i]
                 coord = i
                 break
             end
         end
         push!(opened, ProtoKeyPointSmallest(round.(Int, copy(cand.x1sum) ./ pxsp + 0.5), coord))
         push!(opened, ProtoKeyPointSmallest(round.(Int, copy(cand.x2sum) ./ pxsp + 0.5), coord))
         while opened != []
             tpoi = pop!(opened)
             for coord = 1:$N
                 if tpoi.coord == coord
                     continue
                 end
                 for step in [-1, 1]
                     @nexprs $N k->(npos[k] = tpoi.pos[k])
                     npos[coord] += step
                     if npos[coord] > 0 && npos[coord] <= ssize[coord]
                         #if the point is not expanded
                         if ( @nref $N closed i->(npos[i]))
                             continue
                         end
                         # check all neighbors of npos obtained by incrementing or dectementing one
                         # of the coordinates
                         for coordIn = 1:$N, stepIn in [-1, 1]
                             @nexprs $N k->(nposIn[k] = npos[k])
                             nposIn[coordIn] += stepIn
                             if nposIn[coordIn] > 0 && nposIn[coordIn] <= ssize[coordIn]
                                 #if there is an edge
                                 if ( @nref $N s i->(npos[i])) != ( @nref $N s i->(round(Int,nposIn[i])))
                                     #and if the classes along the edge hold
                                     if (( @nref $N s i->(nposIn[i])) == cand.c1 ||
                                         ( @nref $N s i->(nposIn[i])) == cand.c2) &&
                                        (( @nref $N s i->(npos[i])) == cand.c1 ||
                                         ( @nref $N s i->(npos[i])) == cand.c2)
                                         dist2 = 0.
                                         @nexprs $N i->(dist2 += sqrv(elmult(cand.pos[i] - (npos[i] - 0.5),
                                                                             pxsp[i])))
                                         #if the point isn't too far update patch
                                         if  dist2 <= patchsize2
                                             push!(opened, ProtoKeyPointSmallest(copy(npos), coordIn))
                                             push!(opened, ProtoKeyPointSmallest(copy(nposIn), coordIn))
                                             @inbounds (@nref $N closed d->(npos[d])) = true
                                             @inbounds (@nref $N closed d->(nposIn[d])) = true
                                             # here we replaced broadcast operators by non-broadcast
                                             update_covariance!(cand.pos2, elmult((npos + nposIn) / 2 - 0.5, pxsp),
                                                                s)
                                             cand.count += 1
                                             if ( @nref $N s i->(round(Int,nposIn[i]))) == cand.c1
                                                 @nexprs $N j->(cand.x1sum[j] += (nposIn[j] - 0.5) * pxsp[j])
                                                 @nexprs $N j->(cand.x2sum[j] += (npos[j] - 0.5) * pxsp[j])
                                             else
                                                 @nexprs $N j->(cand.x1sum[j] += (npos[j] - 0.5) * pxsp[j])
                                                 @nexprs $N j->(cand.x2sum[j] += (nposIn[j] - 0.5) * pxsp[j])
                                             end
                                         end # if dist2
                                     end
                                 end # if @nref
                             end # if nposIn
                         end # for
                     end # if
                 end # for step
             end # for coord
        end # while 
    end
    return nothing
end#quote
end

"""
    find_keypoints_and_neighbors(s, keypointdist = 5, nlength = 1)
Given an segmented image and distance between keypoints (no superpixels). Return an array of
`TwoclassKeypoints` and array of sets defining neighbouring keypoints.
"""
function find_keypoints_and_neighbors(s::Image{segimgtools.LabelType, N}, keypointdist::O = 5, nlength = 1) where {N,O<:Number}
    # 's' is an segmented image
    # optional paramater "keypointdist" - keypoints are generated aproximately "keypointdist" apart
    # returns an array of TwoclassKeypoint and nhood information
    @debugassert(keypointdist >= 2)
    #print("started find_keypoints_and_neighbors")
    closed = falses(size(s))
    # find keypoint candidates
    candidates, nhoodKP = find_keypoint_candidates(s, keypointdist, closed)
    #print("started find_patches_for_normals")
    find_patches_for_normals!(candidates, s, keypointdist, closed)
    #print("ended find_patches")
    return (TwoclassKeypoint[ process_keypoint(keyval, nlength, keyval.pos, (keyval.x1sum + keyval.x2sum) / 2 / keyval.count)::TwoclassKeypoint for keyval in candidates ], nhoodKP)
end

"""
    create_image_with_keypoints(l, p, kpts, k, llen = 6, bsize = 3)
Return an image for visualization purposes created from image or pixel labels `l`, superpixel
numbers for each pixel `p`, a list of keypoints `kpts` and number of classes `k`.
If superpixels are not available or should not be displayed, set `p` to `nothing` or ommit it.
"""   
function create_image_with_keypoints(imgi::Image{T,N}, kpts::Vector{K}; llen::Int = 6, bsize::Int = 3,thickness=2, colbox=colorant"black", colline=colorant"red") where {T,K<:AbstractKeypoint,N}
# a variant for non-segmented images
    img = copy(imgi)
    for kpt in kpts # show keypoints as black squares with the normal direction
        # println(kpt)
        normal = kpt.normal ./ Images.pixelspacing(img)
        x0 = round.(Int,kpt.pos ./ Images.pixelspacing(img) .+ 0.5)
        draw_line!(img, x0, normal, -llen, llen, thickness,colline)
        draw_box!(img, x0, bsize,colbox)
        draw_line!(img, x0, normal, -llen, llen, thickness,colline)
    end
    return img
end

# a variant for non-segmented images    
function create_image_with_keypoints(img::Image{T,N}, p, 
                                           kpts::Vector{K}; llen::Int = 6, bsize::Int = 3, thickness=2, colbox=colorant"black", colline=colorant"red") where {T,K<:AbstractKeypoint,N}
    bndcolor=convert(eltype(img), colorant"gray")
    if p!=nothing
        draw_superpixels!(img, p, bndcolor)
    end
    for kpt in kpts # show keypoints as black squares with the normal direction
        # println(kpt)
        normal = kpt.normal ./ Images.pixelspacing(img)
        x0 = round.(Int,kpt.pos ./ Images.pixelspacing(img) .+ 0.5)
        draw_line!(img, x0, normal, -llen, llen, thickness,colline)
        draw_box!(img, x0, bsize,colbox)
        draw_line!(img, x0, normal, -llen, llen, thickness,colline)
    end
    return img
end
    
function create_image_with_keypoints(l::segimgtools.LabelImage{N}, p,
                                     kpts::Vector{K}, k::Int; llen::Int = 6, bsize::Int = 3, thickness=2, col=colorant"gray") where {K<:AbstractKeypoint,N}

    img=classimage2rgb(l,k) # show segmentation

    return create_image_with_keypoints(img,kpts;llen=llen,bsize=bsize,thickness=thickness, col=col)
end

# this specialization seems no longer need as it overlaps with the previous one
#function create_image_with_keypoints{K<:AbstractKeypoint,N}(l::segimgtools.LabelImage{N}, 
#                                     kpts::Vector{K}, k::Int; llen::Int = 6, bsize::Int = 3)
#    return create_image_with_keypoints(l, nothing, kpts, k; llen = llen, bsize = bsize)
#end

#function create_image_with_keypoints{T,K<:AbstractKeypoint,N}(img::Image{T,N}, 
#                                     kpts::Vector{K}; llen::Int = 6, bsize::Int = 3)
#    return create_image_with_keypoints(img, nothing, kpts; llen=llen, bsize=bsize)
#end
    
"""
    reduce_keypoint(k::TwoclassKeypoint, factor::Vector)
Reduce the length of the keypoint normal. The coordinates are in physical units and thus do not change    
"""
function reduce_keypoint(k::TwoclassKeypoint, factor::Vector)
#TODO does using nexprs instead .* speeds-up the function?
    return TwoclassKeypoint(k.pos, k.count, k.normal .* factor, k.classpos, k.classneg)
end

function reduce_keypoint(k::SampledKeypoint, factor::Vector)
    return SampledKeypoint(k.pos, k.normal .* factor,k.count, copy(k.pixels))
end

function reduce_keypoint(k::SimpleKeypoint, factor::Vector)
    return SimpleKeypoint(k.pos, k.normal .* factor)
end
    
"""
    subsample_keypoints_equidistant(kpts, nhood, factor)
Reduces the number of keypoints by two by eliminating every second keypoint by breadth-first search,
normal length is scaled using `factor`
"""    
function subsample_keypoints_equidistant(kpts::Vector{K}, nhood::Array{Set{T}}, factor::Vector) where {T,K<:AbstractKeypoint}
    used = falses(length(kpts))
    kept = falses(length(kpts))
    kptsn = K[]
    kptsi = Int64[]                 #mapping between old and new indicies old=kptsi[new]
    nhoodn = Set{T}[]
    pNewThis = 1
    for pOrigThis = 1:length(kpts)  #for all keypoints
        if !used[pOrigThis]         #if not visited
            push!(kptsn, reduce_keypoint(kpts[pOrigThis], factor)) #take it
            push!(kptsi, pOrigThis)     #store old index
            push!(nhoodn, Set{T}())     #prepare n-hood set
            used[pOrigThis] = true      #mark as visited
            while pNewThis <= length(kptsn)
                # mark nhood of kptsn as used and ->
                for nhoodErase in nhood[kptsi[pNewThis]]
                    #@bp nhoodErase == 12 || nhoodErase == 50 || nhoodErase == 8 || nhoodErase == 70 || nhoodErase == 38 || nhoodErase == 6 || nhoodErase == 75 || nhoodErase == 59 || nhoodErase == 47
                    used[nhoodErase] = true
                end
                # -> and expand its nhood to kptsn
                for nhoodErase in nhood[kptsi[pNewThis]]
                    if !kept[nhoodErase]
                        for nhoodKeep in nhood[nhoodErase]
                            #@bp nhoodKeep == 12 || nhoodKeep == 50 || nhoodKeep == 8 || nhoodKeep == 70 || nhoodKeep == 38 || nhoodKeep == 6 || nhoodKeep == 75 || nhoodKeep == 59 || nhoodKeep == 47
                            if !used[nhoodKeep]
                                used[nhoodKeep] = true
                                push!(kptsn, reduce_keypoint(kpts[nhoodKeep], factor))
                                push!(kptsi, nhoodKeep)
                                push!(nhoodn, Set{T}())
                                push!(nhoodn[pNewThis], length(kptsn))
                                push!(nhoodn[length(kptsn)], pNewThis)
                                kept[nhoodKeep] = true
                            end
                        end
                    end
                end
                pNewThis += 1
            end
        end
    end
    return kptsn, nhoodn
end

"""
    gradient_magnitude(f)
Return the directional derivatives (components of the gradient)
and the gradient magnitude for both color and grayscale images. For
`spatialorder(f)=="xy"`, `grad[1]` is a derivative wrt x, the first
coordinate and `grad[2]` wrt to the second...

!!! warning

    Awailable only for 2D nad 3D.
"""
function gradient_magnitude(f::Image{T,2}) where {T<:ColorTypes.Gray}
    @assert(length(size(f))==2) # only 2D supported for the moment
    s=ImageFiltering.Kernel.sobel()
    grad=[Images.imfilter(f,s[i]) for i in 1:2]
    return grad,Images.magnitude(grad[1],grad[2])
end

function cpsx(dst::Image{T,3}, src::Image{T,3}, temp::Image{T,3}, sf) where {T<:ColorTypes.Gray{Float64}}
    #boundary conditions (reflection) for smoothing
    if sf[1] > 1
        for z=1:sf[3]
            for y=1:sf[2]
                dst[1, y, z] = (src[1, y, z] + src[2, y, z]) / 2
            end
        end
        for z=1:sf[3]
            for y=1:sf[2]
                dst[sf[1], y, z] = (src[sf[1], y, z] + src[sf[1] - 1, y, z]) / 2
            end
        end
    end
    #copy filtered data
    for z=1:sf[3]
        for y=1:sf[2]
            for x=2:sf[1] - 1
                dst[x, y, z] = temp[x - 1, y, z]
            end
        end
    end
end

function cpsy(dst::Image{T,3}, src::Image{T,3}, temp::Image{T,3}, sf) where {T<:ColorTypes.Gray{Float64}}
    #boundary conditions (reflection) for smoothing
    if sf[2] > 1
        for z=1:sf[3]
            for x=1:sf[1]
                dst[x, 1, z] = (src[x, 1, z] + src[x, 2, z]) / 2
            end
        end
        for z=1:sf[3]
            for x=1:sf[1]
                dst[x, sf[2], z] = (src[x, sf[2], z] + src[x, sf[2] - 1, z]) / 2
            end
        end
    end
    #copy filtered data
    for z=1:sf[3]
        for y=2:sf[2] - 1
            for x=1:sf[1]
                dst[x, y, z] = temp[x, y - 1, z]
            end
        end
    end
end

function cpsz(dst::Image{T,3}, src::Image{T,3}, temp::Image{T,3}, sf) where {T<:ColorTypes.Gray{Float64}}
    #boundary conditions (reflection) for smoothing
    if sf[3] > 1
        for y=1:sf[2]
            for x=1:sf[1]
                dst[x, y, 1] = (src[x, y, 1] + src[x, y, 2]) / 2
            end
        end
        for y=1:sf[2]
            for x=1:sf[1]
                dst[x, y, sf[3]] = (src[x, y, sf[3]] + src[x, y, sf[3] - 1]) / 2
            end
        end
    end
    #copy filtered data
    for z=2:sf[3] - 1
        for y=1:sf[2]
            for x=1:sf[1]
                dst[x, y, z] = temp[x, y, z - 1]
            end
        end
    end
end
    
function gradient_magnitude(f::Image{T,3}) where {T<:ColorTypes.Gray}
    smk = [1.; 2.; 1.]     #separable sobel
    grk = [-1.; 0.; 1.]    #separable sobel
    sf = size(f)
    gradtx = Array{ColorTypes.Gray{Float64}}(max(0, sf[1] - 2), sf[2], sf[3])
    #    gradty = Image(Array(ColorTypes.Gray{Float64},sf[1], max(0, sf[2] - 2), sf[3]), Dict{Any,Any}("spatialorder"=>["x", "y", "z"]))
    gradty = Array{ColorTypes.Gray{Float64}}(sf[1], max(0, sf[2] - 2), sf[3])
    gradtz = Array{ColorTypes.Gray{Float64}}(sf[1], sf[2], max(0, sf[3] - 2))
    grad = [zeros(ColorTypes.Gray{Float64}, sf) for i = 1:3]

    #reshape the filter for grad_x
    grk1 = reshape(grk, 3,1,1)
    smk1 = reshape(smk, 1,3,1)
    smk2 = reshape(smk, 1,1,3)
    # separable filtering x
    Images.imfilter!(gradty,f,smk1)         #smooth y
    cpsy(grad[1], f, gradty, sf)            #set grad[1]
    cpsy(grad[3], f, gradty, sf)            #set grad[3]
    Images.imfilter!(gradtz, grad[1], smk2) #smooth z
    cpsz(grad[1], grad[1], gradtz, sf)      #set grad[1]
    Images.imfilter!(gradtx, grad[1], grk1) #div x
    grad[1][1,:,:] = 0.                     #set grad[1]
    grad[1][sf[1],:,:] = 0.
    for z=1:sf[3]
        for y=1:sf[2]
            for x=2:sf[1] - 1
                grad[1][x,y,z] = gradtx[x - 1,y,z]
            end
        end
    end
    
    #reshape the filter for grad_y
    grk1 = reshape(grk, 1,3,1)  
    smk1 = reshape(smk, 3,1,1)
    smk2 = reshape(smk, 1,1,3)
    # separable filtering y
    Images.imfilter!(gradtx,f,smk1)         #smooth x
    cpsx(grad[2], f, gradtx, sf)            #set grad[2]
    Images.imfilter!(gradtz, grad[2], smk2) #smooth z
    cpsz(grad[2], grad[2], gradtz, sf)      #set grad[2]
    Images.imfilter!(gradty, grad[2], grk1) #div y
    grad[2][:,1,:] = 0.                     #set grad[2]
    grad[2][:,sf[2],:] = 0.
    for z=1:sf[3]
        for y=2:sf[2] - 1
            for x=1:sf[1]
                grad[2][x,y,z] = gradty[x,y - 1,z]
            end
        end
    end

    #reshape the filter for grad_z
    grk1 = reshape(grk, 1,1,3)
    smk2 = reshape(smk, 3,1,1)
    # separable filtering z
    Images.imfilter!(gradtx, grad[3], smk2) #smooth x
    cpsx(grad[3], grad[3], gradtx, sf)      #set grad[3]
    Images.imfilter!(gradtz,grad[3],grk1)   #div z
    grad[3][:,:,1] = 0.                     #set grad[3]
    grad[3][:,:,sf[3]] = 0.
    for z=2:sf[3] - 1
        for y=1:sf[2]
            for x=1:sf[1]
                grad[3][x,y,z] = gradtz[x,y,z - 1]
            end
        end
    end
    gradmag = grad[1].^2 + grad[2].^2 + grad[3].^2
    for i = 1: length(gradmag)
        gradmag[i] = sqrt(gradmag[i])
    end
    return grad,gradmag
end

# for color images, we convert them to grayscale first
function gradient_magnitude(f::Image{T,N}) where {T,N}
    gradient_magnitude(convert(Image{Gray{eltype(eltype(T))}},f))
end

const hmax_default=10::Int

"""
    sample_normal{T,N}(f::Image{T,N}, kpt, maxdist, pxsp, defaultval = zero(T))
Given an image `f` and a keypoint `kpt`, sample along the normal at points
`y[i] = def(kpt.pos) + def(kpt.normal) * (i - maxdist - 0.5)`, for `i` = 1 ``\\ldots`` 2 * `maxdist`
and store it into a vector of pixel values of size 2 * `maxdist`
if the points `y[i]` are outside the image, they are replaced by the label
of the closest point inside the image on the normal.
Returns the array  or `nothing` if the keypoint is outside the image,

!!! note

    Compared with `sample_normals`, we have lost the indication of image landmarks.
"""
@generated function sample_normal(f::Image{T,N},kpt::AbstractKeypoint,
                                       maxdist::Integer,pxsp,defaultval=zero(T)) where {T,N}
    quote
        @assert (maxdist>0 && maxdist<200)
        c::Vector{T}=fill(defaultval,2*maxdist)
        #empty::Bool=true
        imgsize=size(f)
        firstvalid=-1 # first valid index
        lastvalid=-1  # last valid index
        for i=1:2 * maxdist
            imd05::Float64 = i - maxdist - 0.5
            @nexprs $N k->(a_k = (kpt.pos[k]+kpt.normal[k]*imd05) / pxsp[k] + 0.5)
            @nexprs $N k->(y_k = round(Int,a_k))
            if (@nall $N k->(y_k >= 1)) && (@nall $N k->(y_k <= imgsize[k]))
                @inbounds c[i]=(@nref $N f y)
                if firstvalid==-1
                    firstvalid=i
                end
                lastvalid=i
            end
        end
        if firstvalid==-1 # no valid pixel positions found
            return nothing # Nullable{Vector{T}}()
        end
        #fill_zero!(c,defaultval)
        # fill the first and last part of the array with the first/last valid value
        for i=1:firstvalid-1
            c[i]=c[firstvalid]
        end
        for i=lastvalid+1:2*maxdist
            c[i]=c[lastvalid]
        end
        
        return c #Nullable(c)
    end
end

"""
    sample_normal!{T,N}(f::Image{T,N}, kpt, maxdist, c, pxsp, defaultval = zero(T))
An in-place version of `sample_normal`, returns `true` if at least part of the normal is inside
the image, sampled values are stored to `c`
"""    
@generated function sample_normal!(f::Image{T,N},kpt::AbstractKeypoint,maxdist::Integer,
                                        c::Vector{T},pxsp,defaultval=zero(T)) where {T,N}
quote
        @debugassert (maxdist>0 && maxdist<200)
        @debugassert(length(c)==2*maxdist)
        empty::Bool=true
        imgsize=size(f)
        for i=1:2 * maxdist
            imd05::Float64 = i - maxdist - 0.5
            @nexprs $N k->(a_k = (kpt.pos[k]+kpt.normal[k]*imd05) / pxsp[k] + 0.5)
            @nexprs $N k->(y_k = round(Int,a_k))
            if (@nall $N k->(y_k >= 1)) && (@nall $N k->(y_k <= imgsize[k]))
                @inbounds c[i]=(@nref $N f y)
                empty=false
            else                 
                @inbounds c[i]=defaultval
            end
        end
        if empty
            return false
        end
        fill_zero!(c,defaultval)
        return true
    end
end
    
### Do not resample pixels in keypoints other than SampledKeypoints.
resample_normals!(f::Image{T},kpts::Array{U,1}) where {T,U<:AbstractKeypoint} = nothing



"""
    resample_normals!(f, kpts)
Resample pixels alog normals in SampledKeypoints, leave other keypoints alone.
"""
function resample_normals!(f::Image{T},kpts::Array{SampledKeypoint{T},1}) where {T}
    @assert(length(kpts)>0)
    maxdist = div(length(kpts[1].pixels), 2)
    pxsp = Images.pixelspacing(f)
    map(kpts) do kpt 
        r=sample_normal(f,kpt,maxdist,pxsp) # returns Nullable
        kpt.pixels[:] = (r!==nothing) ? r : fill(oneunit(T),2*maxdist)  # default value
    end
    return nothing
end

"""
    fill_ellipse(img, c, r, fillval)
Elements inside an ellipse or ellipsoid with center `c` and semi-axis
half-lengths `r` are filled with `fillval`.
"""
@generated function fill_ellipse!(img::AbstractArray{T,N},c,r,fillval) where {T,N}
quote
    n=size(img)
    @nloops $N i d->max(1,min(n[d],ceil(Int,c[d]-r[d]))):max(1,min(n[d],floor(Int,c[d]+r[d]))) begin
        sum2 = 0.
        @nexprs $N j->(sum2 += ((i_j - c[j]) / r[j]) ^ 2)
        if sum2 <= N
            (@nref $N img i)=fillval
        end
    end
end
end

"""
    normalize(x)
Zero lenght aware normalization of a vector.
"""
function normalize(x::Vector{T}) where {T<:Number}
    @debugassert(norm(x) > 10 * eps(T) || norm(x) == 0)
    x/(norm(x)+eps(T))
end

"""
    find_keypoints_from_gradients{T,N}(f::Image{T,N}; spacing = 10, numbe = 100, sigma = ones(Float64, N), nlength = 1., mask = [])
Take a color or grayscale image and return `SimpleKeypoint`s located at high-gradient positions
of the image `f`. Returns `number` keypoints at least `spacing` pixels apart. If this is not
possible, returns less keypoints. `sigma` is the Gaussian filter size. You can suppress keypoint
creation at some locations by setting particular voxels in `mask` to 0. Sizes of `mask` and `f`
should be equal.

The coordinates are in physical units.

!!! note
    If the calculations in the inner loop happen to be too slow, we can make a specialized version for isotropic pixels.
"""
function find_keypoints_from_gradients(f::Image{T,N};spacing=10,number::Int=100,sigma=ones(Float64,N),nlength=1.,mask=[]) where {T,N}
    @assert number>0

    # calculate gradient magnitude
    grad,g=@debugtime( gradient_magnitude(
        ImageFiltering.imfilter(f,ImageFiltering.Kernel.gaussian(sigma))), "Gradient magnitude")
    #const
    marker = zero(eltype(g)) # marker for masked gradients we don't want keypoints at the points of 0 gradient
    if mask != []
        @debugassert( size(g) == size(mask))
        for i = 1:length(g)
            if mask[i] == 0
                g[i] = marker
            end
        end
    end
    # sort indices by decreasing magnitude
    #ind=@debugtime( collect(eachindex(g)), "Indices")
    #@debugtime( sort!(ind,by=i->g[i],rev=true), "Sort")
    ind=@debugtime( sortperm(reshape(g,length(g)),alg=PartialQuickSort(2*number*spacing^(N-1)),rev=true),"Sort")
    numkpts=0 # number of keypoints found so far
    kpts=Array{SimpleKeypoint}(undef,number)
    pspacing = map(float,Images.pixelspacing(f))
    rspacing = spacing ./ pspacing
    # go through indices from the largest gradient and put each
    for i in ind
        # if we already have enough keypoints
        if numkpts >= number break ;    end
        # if this point is too close to another
        if g[i]==marker continue
        end
        if g[i] > 10 * eps(Float64)
            # good keypoint, let's add it
            numkpts+=1

            ppos=Tuple(CartesianIndices(size(g))[i]) # position in pixel units
            pos=([ppos...] .- 0.5) .* pspacing    # position in physical units
        
            norm=normalize(Float64[ grad[k][i] for k=1:N]) * nlength
            kpts[numkpts]=SimpleKeypoint(pos,norm)
            # prevent the neighbors from being added
            fill_ellipse!(g,ppos,rspacing,marker)
        end
    end # for i

    if numkpts<number # shrink array if less keypoints were found
        resize!(kpts,numkpts)
    end
    return kpts
end # function find_keypoints_from_gradients


"""
    create_sampled_keypoints{T}(f::Image{T}, kpts, hmax; spacing = 1.0, defaultval = zero(T))
Take a sequence of `SimpleKeypoint`s and produce a set of `SampledKeypoint`s
The values are sampled up to displacement `maxdist = 2 * hmax`
""" 
function create_sampled_keypoints(f::Image{T},kpts::AbstractVector{U},hmax::Integer;spacing::Float64=1.0,defaultval=zero(T)) where {T,U<:AbstractKeypoint}
    maxdist = hmax_to_maxdist(hmax, SampledKeypoint{T})
    pxsp = Images.pixelspacing(f)
    map(kpts) do kpt 
        r=sample_normal(f,kpt,maxdist,pxsp,defaultval) # can return nothing
        pixels = r===nothing ? fill(defaultval,2*maxdist) : r
        SampledKeypoint(kpt.pos,kpt.normal,spacing,pixels)
    end
end

"""
    keypoint_neighbors(kpt, k = 4)
From a vector of keypoints, calculate their `k` nearest neighbors in the
same form as `find_keypoints_and_neighbors` (Array{Set{Int}}). However, this method
is universal, applicable to any type of keypoints.
"""
function keypoint_neighbors(kpts::AbstractVector{K},k=4) where {K<:AbstractKeypoint}
    # the coordinates need to be copied to an array. This might be avoided by a more flexible interface to KDTree
    nkpts=length(kpts)
    coords=zeros(Float64,length(kpts[1].pos),nkpts)
    for i=1:nkpts
        coords[:,i]=kpts[i].pos
    end
    tree=NearestNeighbors.KDTree(coords) # build a k-D tree with default parameters
    # returns a vector of sets, each containing the indices of the 'k' nearest neighbors
    map(kpts) do kpt
        idxs,dists=NearestNeighbors.knn(tree,kpt.pos,k)
        Set(idxs)
    end
end


""" Given a vector of keypoints, return their norm. Return an error if the norms are too different. """    
function keypoint_normal_norm(kpts::AbstractVector{K}) where {K<:AbstractKeypoint}
  minnorm=Inf
  maxnorm=-Inf  
  for k in kpts
      m=norm(k.normal)
      if m>maxnorm
          maxnorm=m
      end
      if m<minnorm
          minnorm=m
      end
  end
  if maxnorm/(minnorm+eps(minnorm))>2.
      error("keypoint_normal_norm: minnorm=$minnorm and maxnorm=$maxnorm are too different.")
  end
  return maxnorm  
end    
    
# -------- test functions follow ------------------
# function test_find_keypoints_from_gradients()
#     a=Images.load("imgs/lena.png")
#     #a=Images.imread("imgs/simple.png")
#     #Profile.clear()
#     @time kpts=@debugtime(find_keypoints_from_gradients(a;spacing=10,number=500,sigma=[1.;1.]),"find_keypoints")
#     #@profile k=find_keypoints_from_gradients(a;spacing=10,number=100)
#     #Profile.print()
#     img=create_image_with_keypoints(a,kpts,llen=10,bsize=2)
#     ImageView.imshow(img)
# end
    
end # module keypoints
