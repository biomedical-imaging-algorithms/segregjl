"""
  Register segmented images of Drosophila ovaries
"""

module register_ovaries

import Images
import ImageView
import segimgtools
import keypoints
import deformation
import mil

using linprog_register
using Options
#using segimgtools

#using MicroLogging
using Logging

configure_logging(min_level=:debug)
include("debugprint.jl")
switchprinting(true)
include("debugassert.jl")
switchasserts(true)


# a list of possible base directories, for compatibility
const base_dirs =  [ "/mnt/tmp","/datagrid/Medical","/home/kybic/data/Medical","m:/" ]

function find_base_dir()
    for b in base_dirs
        basedir= joinpath(b,"microscopy/drosophila")
        if isdir(basedir)
            @info "basedir=" basedir
            return basedir
        end
    end
    error("Basedir not found.")
end

""" build a map from image filenames as found in 'segmdir' to stages, based on the file 
    all_ovary_image_info_for_prague.txt """
function get_stages(basedir)
    finfo = readdlm(joinpath(basedir, "all_ovary_image_info_for_prague.txt"), '\t', '\n'; skipstart = 1)  #Image metadata
    stages=Dict{String,Int}()
    for i=1:size(finfo,1)
        fn=replace(finfo[i,8],r"\.tif$","") # file name  without .tif extension
        stages[fn]=finfo[i,6] # stage
    end
    return stages
end

""" register a pair of ovary segmentations. 
fsegm is the file name of the reference image. Save the warped image to outimg """
function register_ovary(fsegm,gsegm,fimg,gimg,outimg;display=false,binarize=true)
    # read the two segmentations. We increment the numbers by one, so background is now 1, since for the MIL
    # criterion, 0 means "ignore"
    @info "Starting register_ovary for fsegm=$(fsegm) gsegm=$(gsegm)"
    forig=reinterpret(UInt8,Images.load(fsegm))+UInt8(1)
    gorig=reinterpret(UInt8,Images.load(gsegm))+UInt8(1)
    # let us limit the number of classes to two - foreground and background
    f=copy(forig) ; g=copy(gorig)
    if binarize
        binarize_f = c -> c>1 ? UInt8(2) : 1
        segimgtools.map_class!(f,binarize_f)
        segimgtools.map_class!(g,binarize_f)
    end
    
    k=Int(max(maximum(f),maximum(g))) # number of classes
    korig=Int(max(maximum(forig),maximum(gorig))) # number of classes
    @assert minimum(f)==1
    @assert minimum(g)==1
    hmax=10 # maximum displacement at a keypoint
    if display 
        ImageView.imshow(segimgtools.classimage2rgb(f, k), name = "static image segmentation")
        ImageView.imshow(segimgtools.classimage2rgb(g, k), name = "moving image segmentation")
    end
    keypointdist=10
    kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(f, keypointdist)
    if display
        img1kpts=keypoints.create_image_with_keypoints(segimgtools.classimage2rgb(f, k),kpts, llen=hmax, bsize=2)
        ImageView.imshow(img1kpts, name = "segmentation with keypoints")
    end
    #kptsampled=keypoints.create_sampled_keypoints(f,kptsimple,hmax;spacing=float(keypointdist))

    #isize=to_array(size(img1))
    imgsize=float([size(f)...]) .* Images.pixelspacing(f)
    #def=deformation.create_identity(keypoints.to_array(imgsize), deformation.AffineDeformation2D)
    # find the best initial euclidean transformation
    criterion_f(x,y) = mil.calculateMIL(x,y,korig)
    initialdef=deformation.euclidean_fit(forig,gorig,UInt8(1),criterion_f)
    @elapsed gw=deformation.transform_image_nn(initialdef,g,size(f),@options)
    if display
        ImageView.imshow(deformation.overlay(segimgtools.classimage2rgb(f, k), segimgtools.classimage2rgb(gw, k)), name = "segmentation overlay before")
    end
    def=deformation.HierarchicalDeformation([
       deformation.BsplineDeformation(3,imgsize, [8,8]), 
       deformation.BsplineDeformation(3,imgsize, [4,4]), 
       initialdef
       ])
    critparams=mil.MILParameters(k)        #mutual information
    opts=@options hmax=hmax resample=true fdiffreg=1e-3 lambda=1e-3 maxResample=5 minKP=500 display=false maxsize=250 minsize=64 knot_spacing=8 start_with_last_level=false
    t1=time()
    d=register_fast_linprog_multiscale(f,g,kpts,def,nhoodKP,critparams,opts)
    t2=time()
    println("Time: registration $(t2-t1)")
    @elapsed gw=deformation.transform_image_nn(d,g,size(f),@options)
    println("g: $(typeof(g)) gw: $(typeof(gw))")
    gimg=Images.load(gimg)
    if display
        ImageView.imshow(segimgtools.classimage2rgb(gw,k),name="warped segmentation")
        ImageView.imshow(deformation.overlay(f,gw),name="overlay segmentation after")
        fimg=Images.load(fimg)
        ImageView.imshow(fimg, name = "fimg")
        ImageView.imshow(gimg, name = "gimg")
        ImageView.imshow(deformation.overlay(fimg,gimg),name="overlay before")
        ImageView.imshow(deformation.overlay(f,gw),name="overlay segmentation after")
    end
    warped=@debugtime(deformation.transform_image_lin(d,gimg,size(f),@options),"warping")
    if display
        ImageView.imshow(warped,name="warped")
        ImageView.imshow(deformation.overlay(fimg,warped),name="overlay after")
    end
    @info "Saving results to $outimg"
    Images.save(outimg,warped)
    @info "register_ovary finished"
    return
end

# try to register a selected pair of images
function test_register_ovary()
    ImageView.closeall()
    basedir=find_base_dir()
    fname="insitu39430_1"
    gname="insitu39429_1"
    fsegm=joinpath(basedir,"RESULTS/PIPELINE_ovary_all_images/4_cut-ellipse-segm",fname * ".png")
    gsegm=joinpath(basedir,"RESULTS/PIPELINE_ovary_all_images/4_cut-ellipse-segm",gname * ".png")
    fimg=joinpath(basedir,"RESULTS/PIPELINE_ovary_all_images/4_cut-images",fname * ".png")
    gimg=joinpath(basedir,"RESULTS/PIPELINE_ovary_all_images/4_cut-images",gname * ".png")
    outimg=joinpath(basedir,"RESULTS/PIPELINE_ovary_all_images/6_reg-images",fname * ".png")
    register_ovary(fsegm,gsegm,fimg,gimg,outimg,display=false)
end

    
# register all ovary images from the directories below    
function main()
    ImageView.closeall()
    basedir=find_base_dir()
    segmdir=joinpath(basedir,"RESULTS/PIPELINE_ovary_all_images/4_cut-ellipse-segm")
    imgdir=joinpath(basedir,"RESULTS/PIPELINE_ovary_all_images/4_cut-ellipse-images")
    outsegmdir=joinpath(basedir,"RESULTS/PIPELINE_ovary_all_images/6_reg-segm")
    outimgdir=joinpath(basedir,"RESULTS/PIPELINE_ovary_all_images/6_reg-images")


    # read information about all images and build a dictionary of stages
    stages=get_stages(basedir)

    # reference images for different stages
    refimages=Dict{Integer,String}(2 => "insitu39430_1")
    # read all files from the input directory
    filelist=collect(Base.Iterators.take(readdir(segmdir),10))
    @sync begin
        @parallel for gname in filelist # for all filenames  
        if ismatch(r".*\.png",gname)
            basegname=replace(gname,r"(_[0-9])?\.png$","") # remove suffix (_<number>) and extension
            gname=replace(gname,r"\.png$","") # remove suffix (_<number>) 
            @info "Considering gname=$(gname)"
            if haskey(stages,basegname)
                stage=stages[basegname]
                @info("File $(gname) stage=$(stage)")
                if haskey(refimages,stage)
                    fname=refimages[stage] # f = reference image
                    outimg=joinpath(outimgdir,gname * ".png")
                    if isfile(outimg)
                        @info("Output file $(outimg) exists, skipping.")
                    else
                        # here the registration starts
                        fsegm=joinpath(segmdir,fname * ".png")
                        gsegm=joinpath(segmdir,gname * ".png")
                        fimg=joinpath(imgdir,fname * ".png")
                        gimg=joinpath(imgdir,gname * ".png")
                        outimg=joinpath(outimgdir,gname * ".png")
                        register_ovary(fsegm,gsegm,fimg,gimg,outimg;display=false)
                    end
                else
                    @info("Unknown reference image for stage $(stage), skipping.")
                end    
            else
                @info("Unknown stage for file $(fn), skipping.")
            end
        end
        end # for
    end
    return nothing
end
    
end
