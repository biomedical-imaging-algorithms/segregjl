"""
Regulerization for optimizers defined in [optimizer](@ref) and [admm](@ref).
All the subtypes of `AbstractDeformation` can be used as deformation models, see [deformation](@ref).

Any regularization is defined by its parameters, a subtype of `AbstractRegularizationParams`. For
each regularization method functions
* `regularize!(def, par, grad = [])` used in [optimizer](@ref)
* `make_applyRtR(def, par)` used in [admm](@ref)
should be defined.
"""
module regularization

using ..Options
using ..deformation

include("debugassert.jl")
switchasserts(false)
include("debugprint.jl")
switchprinting(false)

export  AbstractRegularizationParams, NoneRegularizationParams, DiffRegularizationParams,
        SumRegularizationParams
export  regularize!, make_applyRtR

#----------------------AbstractRegularizationParams--------------------------
"""
Common ancestor. Any regularization parameters should be subtypes of `AbstractRegularizationParams`.
It represents any parameters for any regularization.
"""
abstract type AbstractRegularizationParams end

"""
    regularize!(def, par, grad = [])
Return regularization for deformation parameter `deformatin.get_theta(def)` and modify
`grad` if not empty. To be called by [`optimizer.optimize`](@ref).
"""
function regularize! end

"""
    make_applyRtR(def, par)
Return function ``R^TR`` used in [admm](@ref) oprimization.
"""
function make_applyRtR end

#----------------------NoneRegularizationParams------------------------------
"""
No regularization.
"""
mutable struct NoneRegularizationParams<:AbstractRegularizationParams
end

regularize!(c::T, par::NoneRegularizationParams, grad::Vector{Float64} = []) where {T<:deformation.AbstractDeformation} = 0 

# """ return a function computing RtR*theta for regularization purposes"""
function make_applyRtR(def::deformation.AbstractDeformation, par::NoneRegularizationParams)
    function applyRtR(theta)
        return zeros(Float64, size(theta))
    end
    return applyRtR
end


#------------------SumRegularizationParams----------------------------
"""
Regularization

``R=w_i\\sum_j^N\\frac{|\\theta_j|}{N}``,

where ``w`` is weight vector and ``i`` deformation level. Simple deformations
have only one level so ``i = 1``.  See [`deformation.HierarchicalDeformation`](@ref)
for details.
"""
mutable struct SumRegularizationParams<:AbstractRegularizationParams
    weight::Vector{Float64}
end

function regularize!(c::T, par::SumRegularizationParams, grad::Vector{Float64} = []) where {T<:deformation.AbstractDeformation}
    lev = deformation.get_valid_level(c)
    theta = deformation.get_theta(c)
    if grad != []
        grad[:] += par.weight[lev] .* sign(theta)
    end
    return sum(abs(theta)) * par.weight[lev] / length(theta)
end

function make_applyRtR(def::deformation.AbstractDeformation, par::SumRegularizationParams)
    lev = deformation.get_valid_level(def)
    function applyRtR(theta)
        return par.weight[lev] * par.weight[lev] * theta
    end
    return applyRtR
end


#------------------DiffRegularizationParams----------------------------
"""
Regularization penalizing differnce of neghbor parameters.

!!! note
    It doesn't make much sense to use this regularization for methods where the
    spatial neigborhood doesn't exist, e.g. affine. The neihborhood is defined in
    [`deformation.get_diff_regularization_matrix_RtR`](@ref)
!!! warning
    Only ADMM optimization supported yet.
"""
mutable struct DiffRegularizationParams<:AbstractRegularizationParams
    weight::Vector{Float64}
end


function make_applyRtR(def::deformation.AbstractDeformation, par::DiffRegularizationParams)
    lev = deformation.get_valid_level(def)
    RtR = deformation.get_diff_regularization_matrix_RtR(def, par.weight[lev])
    function applyRtR(theta)
        return RtR*theta
    end
    return applyRtR
end
end     #module
        
