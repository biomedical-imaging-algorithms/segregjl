"""
Multidimensional optimization as replaceable plugins.
Every optimization method (optimizer) is defined by the type containing its parameters, it should be
subtype of `AbstractOptimizer`.

For each optimizer the `optimize` function should be implemented. The `optimize` functions
are defined in this module, but there is not a problem to define them outside.

# Implemented optimizers:
* OptimizerMMA for the LD_MMA method from NLopt.
* OptimizerCOBYLA for a derivative-free method COBYLA from NLopt.
* OptimizerGradientDescent for gradient descent optimizer.
* OptimizerGreedyDescent for greedy search through the parameter space.

!!! warning "LBFGS"

    We would like to use LBFGS implemented in `NLopt` but it occassionaly fails for unknown reasons.

!!! note

    The ADMM optimization method implemented in [admm](@ref) nodule doesnt fit into this framework.
"""
module optimizer

using ..Options
import NLopt

include("gradientdescent.jl")
using .gradientdescent

include("debugassert.jl")
switchasserts(false)
include("debugprint.jl")
switchprinting(true)


export AbstractOptimizer, OptimizerMMA, OptimizerGradientDescent, OptimizerGreedyDescent
export optimize


#We shall try to make the interface for the optimizers as uniform as possible.
abstract type AbstractOptimizer end

"""
Defines interface to MMA optimiation in `NLopt` module.
"""
mutable struct OptimizerMMA <:  AbstractOptimizer
    neval::Integer    # maximum number of iterations
    abstol::Float64   # absolute tolerance
    initstep::Float64 # initial step size
end

"""
`OptimizerMMA` constructor with optional parameters.
"""
function OptimizerMMA(;neval=1000,abstol=1e-6,initstep=0.1)
    OptimizerMMA(neval, abstol, initstep)
end

"""
Defines interface to gradient descent optimization in `gradientdescent` module.
"""
mutable struct OptimizerGradientDescent <: AbstractOptimizer
    neval::Integer    # maximum number of iterations
    abstol::Float64   # absolute tolerance
    initstep::Float64 # initial step size
end

"""
`OptimizerGradientDescent` constructor with optional parameters.
"""
function OptimizerGradientDescent(;neval=1000,abstol=1e-6,initstep=0.1)
    OptimizerGradientDescent(neval, abstol, initstep)
end

"""
Defines interface to greedy descent optimization in `gradientdescent` module
"""
mutable struct OptimizerGreedyDescent <: AbstractOptimizer
    neval::Integer    # maximum number of iterations
    abstol::Float64   # absolute tolerance
    initstep::Float64 # initial step size
end

"""
`OptimizerGreedyDescent` constructor with optional parameters.
"""
function OptimizerGreedyDescent(;neval=1000,abstol=1e-6,initstep=0.1)
    OptimizerGreedyDescent(neval, abstol, initstep)
end

"""
Defines interface to COBYLA optimization in `NLopt` module.
"""
mutable struct OptimizerCOBYLA <:  AbstractOptimizer
    neval::Integer    # maximum number of iterations
    abstol::Float64   # absolute tolerance
    initstep::Float64 # initial step size
end

"""
`OptimizerCOBYLA` constructor with optional parameters.
"""
function OptimizerCOBYLA(;neval=1000,abstol=1e-6,initstep=0.1)
    OptimizerCOBYLA(neval, abstol, initstep)
end


"""
    optimize(optpar, f!, r!, x0, lbound = [], ubound = [])
Given an optimizer and a starting value ``x_0``, optimize a criterion ``J(x) = f(x) + r(x)`` composed
of a data ``f(x)`` and a regularization ``r(x)`` parts, 

# Arguments
* `f!::Function`: takes (x, grad) and evaluates the data part of the criterion at `x`.
    If `length(grad) > 0` then the gradient of the data criterion is stored there in-place.
* `r!:Function`: takes (x, grad) and evaluates the regularization for `x`. If `length(grad) > 0`
    then the gradient is modified in-place to be ``\\nabla J``.
* `x0::Vector{Float64}`: starting point ``x_0``.
* `lbound::Vector{Float64} = []`: lower bound to the parameter vector ``x``.
* `ubound::Vector{Float64} = []`: upper bound to the parameter vector ``x``.

!!! note

    The OptimizerGradientDescent and OptimizerGreedyDescent optimizers ignore the bounds.
    It is probably good idea to move `lbound` and `ubound` into `optimizer`
"""
function optimize(optpar::OptimizerMMA, f!::Function, r!::Function, x0::Vector{Float64},
                  lbound::Vector{Float64}=[], ubound::Vector{Float64}=[])
    count=0
    function ff!(x,grad)
        count+=1
        return f!(x,grad) + r!(x,grad)
    end
    ndof = length(x0) # number of degrees of freedom
    opt=NLopt.Opt(:LD_MMA,ndof)
    NLopt.min_objective!(opt,ff!)
    if length(lbound)>0 NLopt.lower_bounds!(opt,lbound) ; end
    if length(ubound)>0 NLopt.upper_bounds!(opt,ubound) ; end
    NLopt.xtol_abs!(opt,optpar.abstol*ones(Float64,ndof)) # absolute tolerance
    NLopt.maxeval!(opt,optpar.neval) # at most "neval" evaluations
    NLopt.initial_step!(opt,optpar.initstep*ones(Float64,ndof)) # initial step
    tic()
    (optf,optx,ret)=NLopt.optimize(opt,x0)
    #@debugprintln_with_color(:green, "Optimization with MMA finished after $(toq())s and $count iterations, crit=$optf")
    return optx
end

function optimize(optpar::OptimizerCOBYLA, f!::Function, r!::Function, x0::Vector{Float64},
                  lbound::Vector{Float64}=[], ubound::Vector{Float64}=[])
    count=0
    function ff!(x,grad)
        @debugassert(length(grad)==0)
        count+=1
        f!(x,grad) + r!(x,grad)
    end
    ndof = length(x0) # number of degrees of freedom
    opt=NLopt.Opt(:LN_COBYLA,ndof)
    NLopt.min_objective!(opt,ff!)
    if length(lbound)>0 NLopt.lower_bounds!(opt,lbound) ; end
    if length(ubound)>0 NLopt.upper_bounds!(opt,ubound) ; end
    NLopt.xtol_abs!(opt,optpar.abstol*ones(Float64,ndof)) # absolute tolerance
    NLopt.maxeval!(opt,optpar.neval) # at most "neval" evaluations
    NLopt.initial_step!(opt,optpar.initstep*ones(Float64,ndof)) # initial step
    tic()
    (optf,optx,ret)=NLopt.optimize(opt,x0)
    @debugprintln_with_color(:green, "Optimization with COBYLA finished after $(toq())s and $count iterations, crit=$optf")
    return optx
end


function optimize(optpar::OptimizerGradientDescent,f!::Function, r!::Function, x0::Vector{Float64},
                  lbound::Vector{Float64}=[], ubound::Vector{Float64}=[])
    tic()
    count=0
    function ff!(x,grad)
        count+=1
        f!(x,grad) + r!(x,grad)
    end

    if length(lbound)>0 || length(ubound)>0
        warn("optimizer.optimize with OptimizerGradientDescent ignores the bounds on parameters.")
    end
    
    optx, optf = gradient_descent(ff!, x0, optpar.initstep, optpar.initstep*1e-6, optpar.neval,
                                  @options normalizeGrad = false minstepMulRepeat = false minstepMul = 1e-3)

    @debugprintln_with_color(:green, "Optimization with gradient descent finished after $(toq())s and $count iterations, crit=$optf")
    return optx
end

function optimize(optpar::OptimizerGreedyDescent,f!::Function, r!::Function, x0::Vector{Float64},
                  lbound::Vector{Float64}=[], ubound::Vector{Float64}=[])
    tic()
    count=0
    function ff!(x,grad)
        count+=1
        f!(x,grad) + r!(x,grad)
    end

    if length(lbound)>0 || length(ubound)>0
        warn("optimizer.optimize with OptimizerGreedyDescent ignores the bounds on parameters.")
    end
    
    optx, optf = gradientdescent.greedy_descent(ff!, x0, optpar.initstep, optpar.initstep*1e-6, optpar.neval, @options)

    @debugprintln_with_color(:green, "Optimization with greedy descent finished after $(toq())s and $count iterations, crit=$optf")
    return optx
end



end #module
