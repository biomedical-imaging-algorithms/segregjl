"""

    Interface for registering binary and probabilistic patterns for 
    binary patterns dictionary learning.

    Jan Kybic, 2018
"""

module register_patterns

import keypoints
import Images
import deformation
import ssd

using linprog_register
using segimgtools
using Options
#using MicroLogging
using Logging

export register_pattern, deform_pattern, inverse_transformation

configure_logging(min_level=:error)



"""
     register_pattern(f,g[,reg])

Register a binary image `f` (0=background, 1=foreground) with a grayscale image `g` (continuous value between 0 and 1) by minimizing a SSD. Returns a deformation `def<:deformation.AbstractDeformation` represented by B-splines. 

`reg` is a regularization parameter to penalize difference between neighboring B-spline coefficients: 0 means no regularization. Try values such as 1e-6 or 1e-3. 

"""
function register_pattern(f::AbstractArray,g::AbstractArray,reg::Float64=1e-9)
    # these parameters might need to be adapted for different image sizes
    hmax=5    # maximum displacement
    spacing=2 # distance between keypoints
    kptn=50   # maximum number of keypoints
    bc=6      # number of B-spline knots in each dimension
    # find keypoints, their neighbors and sample along normals
    kptsimple=keypoints.find_keypoints_from_gradients(f;spacing=spacing,number=kptn,sigma=[1.;1.])
    nhood=keypoints.keypoint_neighbors(kptsimple) 
    kptsampled=keypoints.create_sampled_keypoints(f,kptsimple,hmax;spacing=float(spacing))
    isize=[size(f)...]
    imgsize=float(isize) .* Images.pixelspacing(f) # image size in physical units
    def=deformation.BsplineDeformation(3,imgsize, [bc,bc])
    opts=@options hmax=hmax resample=true fdiffreg=reg lambda=1e-9 maxResample=5 minKP=100 display=false maxsize=1000 minsize=32 knot_spacing=spacing start_with_last_level=false
    critparams=ssd.SSDParameters()
    d=register_fast_linprog_multiscale(f,g,kptsampled,def,nhood,critparams,opts)
    return d
end

"""
        deform_patterns(f,g,def)

Deform a grayscale image `g` by deformation `def` (returned by `register_pattern`)
Only the size of the image  `f` is used - `def` maps coordinates from `f` to `g`.

"""

function deform_patterns(f::AbstractArray,g::AbstractArray,def)
    warped=deformation.transform_image_lin(def,g,size(f),@options)
    #return reinterpret(UInt8,warped) # less universal, used so that Python provides a usable type
    # less efficient but I do not know how to return an 8 bit image to Python
    return warped
end


"""
    inverse_transformation(def,f,g,[bsplineCount,lambda])

Return an inverse (B-spline) transformation to deformation `def` (returned by `register_pattern`) from `f` to `g` 
The images are only needed for their sizes and pixelsizes. The inverse transformation is represented with [8,8] knots unless given otherwise.  The inverse may not be exact. 'lambda' is a regularization of the B-spline coefficient magnitude.
"""

function inverse_transformation(def,f,g,bsplineCount=[8,8],lambda=1e-3)
    imgsize=float([size(g)...]) .* Images.pixelspacing(g) # image size in physical units
    defg=deformation.BsplineDeformation(3,imgsize, bsplineCount) 
    deformation.set_inverse!(defg,def,f,g;lambda=lambda,oversample=8)
    return defg
end

end # module
