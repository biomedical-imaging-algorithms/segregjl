"""
Generic image similarity criterion to be maximized.

Represent criteria in the form ``\\sum_i \\phi(f_i,g_i)``
where ``f_i``, ``g_i`` are pixel values

The concrete type should implement the following methods:
* `criterion_init(f::Image, g::Image, p::CriterionParameters)` return `CriterionState`
* `criterion_eval(s::CriterionState, pixel_f, pixel_g)` return `Float64`

here we just make a skeleton implementation, to have something to redefine.

Jan Kybic, October 2015
"""
module criterion

export CriterionParameters, CriterionState, criterion_init, criterion_eval

abstract type CriterionParameters end
abstract type CriterionState{P<:CriterionParameters} end

using Images

function criterion_init(f::AbstractArray, g::AbstractArray, p::CriterionParameters)
    error("criterion_init should be specialized for $(typeof(f)) $(typeof(g)) $(typeof(p))")
end

function criterion_eval(s::CriterionState,pixel_f,pixel_g)
    error("criterion_eval should be specialized for $(typeof(s)) $(typeof(pixel_f)) $(typeof(pixel_g))")
end


end # modele criterion
