"""
B-splines implementation in Julia

Jan Kybic, January 2014
"""
module bsplines

# uncomment for testing, 
#using PyPlot 
#import Winston
using ..segimgtools

export lbspln, lbderiv, lbdderiv
export cbspln, cbderiv, cbdderiv, convolve, cbsplneval, cbsplnevalam, cbsupportam
export bsupport_min, bsupport_max, bsupport

get_support_half(n::Int) = n / 2. + 0.5

"""
    lbspln(x)
Return the value of a linear B-spline at point `x`.
"""
function lbspln(x)
    x = abs(x) ;
    if x > 1. 
        return 0.
    end
    return -x + 1
end 

"""
    lbderiv(x)
return value of the linear B-spline derivative at point `x`.
"""
function lbderiv(x)
    return -sign(x)
end

"""
    lbdderiv(x)
Return the value of the linear B-spline second derivative at point `x`.
"""
function lbdderiv(x)        
  return 0.
end   

"""
    cbspln(x)
Return the value of a cubic B-spline at point `x`.
"""
function cbspln(x)
    x = abs(x)
    if x > 2. 
        return 0.
    end

    if x > 1. 
        return (2. - x) ^ 3 / 6.
    end

    return 2. / 3. - x * x * (1. - x * 0.5) 
end 

"""
    cbderiv(xi)
Return the value of the cubic B-spline derivative at point `x`.
"""
function cbderiv(xi)
  # 
    x = abs(xi) 
    y = 0.
    if x > 2.
        return y
    elseif x>1.
        y = -0.5 * (2 - x) ^ 2
    else
        y = (1.5 * x - 2) * x
    end
    return -copysign(y, xi)
end

"""
    cbdderiv(x)
Return the value of the cubic B-spline second derivative at point `x`.
"""
function cbdderiv(x)
  x::typeof(x)=abs(x)
  if x>2.
    return zero(x)
  end
  if x>1.
    return 2. - x
  end
  return -2+3x    
end
        
"""
    convolve(f, g)
Convolution of two vectors, direct method.
"""
function convolve(f::Vector{A}, g::Vector{B}) where {A<:Number,B<:Number}
    # TODO: add a type signature allowing any two vectors with real elements
    @assert !(isempty(f))
    @assert !(isempty(g))
    nf=length(f)
    ng=length(g)
    nr=nf+ng-1
    r=zeros(promote_type(A,B),nr)
    for t=1:nr
        minx=max(1,t-ng+1)
        maxx=min(nf,t)
        for x=minx:maxx
            # print("t=",t," x=",x," f[x]=",f[x]," g[t-x+1]=",g[t-x+1],"\n")
            r[t]+=f[x]*g[t-x+1]
        end
    end
    return r
end

"""
    bspln(x, n)
Return the value of a B-spline of order `n` at point `x`.
"""
function bspln(x, n::Integer)
    @assert n>=0
    # n=0 needs to be treated separately
    if n==0
        return abs(x)<0.5 ? 1. : 0.
    end
        
    s=0. 
    for k=0:n+1
        s+=binomial(n+1,k)*(-1)^k*(max(0,x-k+(n+1)/2.)^n)
    end
    return s/factorial(n)
end

"""
    bsplnsampled(n)
Retyrn B-spline of order `n` sampled at integer values, non-zero values only
"""
function bsplnsampled(n::Integer)
    @assert n>=0
    nh=div(n,2)
    #t=linspace(-nh,nh,2*nh+1)
    t=range(-nh,stop=nh,length=2*nh+1)
    return map(x -> bspln(x,n),t)
end

"""
    bsplnfder(x, n)
Return first derivative of B-spline of order `n` at point `x`.
"""
function bsplnfder(x, n::Integer)
    @assert n>=0
    if n==0
        return 0.
    end
    return bspln(x+0.5,n-1)-bspln(x-0.5,n-1)
end

"""
    bsplnder(x, q, n)
Return `q`-th derivative of B-spline of order `n` at point `x`.
"""
function bsplnder(x, q::Integer, n::Integer)
    @assert q>=0
    if q==0
        return bspln(x,n)
    end
    if q==1
        return bsplnfder(x,n)
    end
    # this is a simple but not a very efficient way
    return bsplnder(x+0.5,q-1,n-1)-bsplnder(x-0.5,q-1,n-1)
end        

"""
    numder(f, x, h = 1e-3)
Numerical derivative of function `f` at point `x` using step `h`.
"""
function numder(f, x, h = 1e-3)
    return (f(x+h)-f(x-h))/(2. * h)
end

const G2 = [ 0.5 - sqrt(1/12.) ; 0.5 + sqrt(1/12.) ] 

"""
    gaussq2_unit(f, t0=0.)
Return ``\\int_0^1 f(t) dt`` by Gaussian quadrature with two points, accurate for polynomials
up to degree three when `t0` is given, we integrate over ``t_0\\ldots t_0+1``.
"""
function gaussq2_unit(f, t0=0.)
    return 0.5 * (f(G2[1]+t0)+f(G2[2]+t0))
end

"""
    gaussq2_int(f, l, h)
Return ``\\int_l^h f(t) dt`` (with `l`, `h` integers), assumming that
`f` is an at most cubic polynomial in each subinterval ``i\\ldots i+1``.
"""
function gaussq2_int(f, l, h)
    s=0.
    for t=l:h-1
        s+=gaussq2_unit(f,t)
    end
    return s
end

"""
    gammavalnum(x, q, n)
Evaluate numerically integral of the scalar product of two `q`-th derivatives of B-splines of order `n`
shifted by `t`.

Equation (3) in Arigovindan et al, IEEE TIP 2005.
"""
function gammavalnum(x, q::Integer, n::Integer)
    f = x -> bsplnder(x,q,n)
    g = t -> f(t) * f(x-t)
    return quadgk(g,-(n+1)/2., +(n+1)/2.)[1] # the other return value is the error
#    return gaussq2_int(g,-(n+1)/2., +(n+1)/2.)
end

"""
    gamma23valnum(i, j, l, h)
Evaluate numerically the integral of
``\\int_l^h \\beta^2_3(t-i) \\beta^2_3(t-j) dt``,

where ``\\beta^2_3(x)`` is second derivative of cubic B-spline at point ``x``.
"""
function gamma23valnum(i, j, l, h)
    @assert h>=l
    f = x -> bsplnder(x,2,3)
    g = t -> f(t-i) * f(t-j)
    return gaussq2_int(g,max(l,max(i,j)-2),min(h,min(i,j)+2))
end
    
"""
    gammanum(q, n)
Evaluate numerically at integers the integral of the scalar product of two `q`-th derivatives
of B-splines of order `n` shifted by `t`.

Equation (3) in Arigovindan et al, IEEE TIP 2005.
"""
function gammanum(q::Integer, n::Integer)
    t=linspace(-n,n,2*n+1)
    return map( x -> gammavalnum(x,q,n), t)
end    

"""
    finitedifop(m)
Finite difference operator of order `m`.
"""
function finitedifop(m::Integer)
    if m==0
        return [1]
    elseif m==1
        return [1;-1]
    else
        return convolve(finitedifop(m-1),[1;-1])
    end
end

"""
    gamma(q, n)
Calculate analytically the values at integers of the integral of the scalar
product of two `q`-th derivatives of B-splines of order `n` shifted
by `t`. Equation (3) in Arigovindan et al, IEEE TIP 2005
"""
function gamma(q::Integer, n::Integer)
    
    @assert n>=q>=0
    t=bsplnsampled(2n-2q+1)
    h=finitedifop(2q)
    return convolve(t,h)
end

const gamma23vals = gamma(2,3)

"""
    gamma23val(i, j, l, h)
The same as [`gamma23valnum`](ref) but if possible use gamma (which is faster).
"""
function gamma23val(i::Integer, j::Integer, l::Integer, h::Integer)
    
    if i<=j
        if j+2<=h && i-2>=l
            d=j-i
            return d>3 ? 0. : gamma23vals[4+d]
        else # we are close to the boundary
            return gamma23valnum(i,j,l,h)
        end
    else    
        return gamma23val(j,i,l,h)
    end
end

"""
    cbsplneval(x, c, h = 1, d = 2)
Given a vector of coefficients `c` with ``N`` values, evaluate a cubic B-spline (``\\beta_3``)
approximation at point `x`. `h` is the node spacing, `d` is the shift.
i.e. ``f(x)=\\sum_{i=1}^N c_i \\beta_3(x/h+d-i)``, ``i\\in 1\\ldots N``
for default `h`, `d`, ``f(x)`` is fully described inside the interval ``0\\ldots N-3``
and is zero outside for ``x<-3, x>N``.
"""
function cbsplneval(x::Float64, c::Vector{Float64}, h::Number=1, d::Number=2)
    nc=length(c)
    # find out what indices contribute
    hsupp=2.0 # size of the support
    xt=x/h+d  # transformed version of the parameter
    (imin,imax)=cbsupport(xt,nc)
    s=x
    for i=max(imin,1):imax
        s+=c[i]*cbspln(xt-i)
    end
    return s
end

"""
    getmirrored(c, i)
Given an array `c` with ``N`` elements, get an element `i` value with mirror-on-bounds
boundary condition for ``1<=i<=N``, returns `c[i]`
"""
function getmirrored(c, i)
    nc=length(c)
    if i<1
        return 2*c[1]-getmirrored(c,2-i)
    elseif i>nc
        return 2*c[nc]-getmirrored(c,2nc-i)
    else return c[i]
    end
end

"""
    cbsplnevalam(x, c, h = 1, d = 2)
Given a vector of coefficients `c` with ``N`` elements, evaluate a cubic B-spline
approximation at point `x`. `h` is the node spacing, `d` is the shift
i.e. ``f(x)=\\sum_{i=1}^N c_i \\rextrm{bspln}(x/h+d-i)``
for default `h`, `d`, ``f(x)`` is fully described inside the interval ``0\\ldots N-3``
and is zero outside for ``x<-3, x>N``
use anti-mirror-on-bounds boundary conditions.
"""
function cbsplnevalam(x, c, h = 1, d = 2)
    nc=length(c)
    # find out what indices contribute
    hsupp=2.0 # size of the support
    xt=x/h+d  # transformed version of the parameter
    imin,imax=cbsupportam(xt)
    s=0.
    for i=imin:imax
        bval=cbspln(xt-i)
        s+=getmirrored(c,i)*bval
    end
    return s
end

ehsupp(n::Int) = get_support_half(n) - eps(get_support_half(n))

"""
    bsupport_min(t, n, o)
Given `t`, find ``i_{min}`` such that ``\\beta_o(t-i)=0``
for ``i<i_{min}``.
Clip to ``1<=t<=n``.
"""
function bsupport_min(t,n,o::Int)
    return segimgtools.clip(ceil(Int, t - get_support_half(o) - eps(t)), 1, n)
end

"""
    bsupport_max(t, n, o)
Given `t`, find ``i_{max}`` such that ``\\beta_o(t-i)=0``
for ``i>i_{max}``.
Clip to ``1<=t<=n``.
"""
function bsupport_max(t,n,o::Int)
    tmp = t + get_support_half(o)
    return segimgtools.clip(floor(Int, tmp - eps(tmp)), 1, n)
end

"""
    bsupport(t, n, o)
Given `t`, find ``i_{min}``, ``i_{max}`` such that ``\\beta_o(t-i)=0``
for ``i<i_{min}``, and ``i>i_{max}``.
Clip to ``1<=t<=n``.
"""
@inline function bsupport(t, n, o::Int)
    return bsupport_min(t,n,o), bsupport_max(t,n,o)
end

"""
    bsupportam(t, o)
Given `t`, find ``i_{min}``, ``i_{max}`` such that ``beta_o(t-i)=0``
for ``i<i_{min}``, ``i>i_{max}``.
Version of anti-mirror-on-bounds, no clipping
"""
function bsupportam(t, o::Int)
    return ceil(t-ehsupp(o)),floor(t+ehsupp(o))
end



# ------------------------------------------------------------------

function draw_function(f;lims=3.)
    #warn("This code needs PyPlot")
    #t=linspace(-3.,3.,9)
    #y=[ cbspln(x) for x=t ]
    #println("cbspln: \n")
    ## print ([t y])
    #for i=1:length(t)
    #    @printf("%7.3f %7.3f\n",t[i],y[i])
    #end
    t=linspace(-lims,lims,300)
    y=Float64[ f(x) for x=t ]
    #plot(t,y,label=string(f))
    Winston.plot(t,y)
end    

function test_all()
    #PyPlot.clf()
    draw_function(cbspln)
    draw_function(cbderiv)
    #draw_function(cbdderiv)
    #draw_function(x -> bspln(x,3))
    #draw_function(x -> bspln(x,4))
    #draw_function(x -> bspln(x,5))
    #draw_function(x -> bsplnder(x,1,3))
    #draw_function(x -> bsplnder(x,2,3))
    #draw_function(x -> bsplnder(x,3,3))
end

function test_der()
    PyPlot.figure(1) 
    PyPlot.clf()
    der1= x -> bsplnder(x,1,3)
    draw_function(der1)
    draw_function(x -> numder(xx -> bspln(xx,3),x))
    PyPlot.title("first derivative")
    PyPlot.figure(2) 
    PyPlot.clf()
    der2= x -> bsplnder(x,2,3)
    der2num= x -> numder(der1,x)
    p1=draw_function(der2)
    p2=draw_function(der2num)
    PyPlot.title("second derivative")
    PyPlot.legend([p1,p2],["anal","num"])
    PyPlot.figure(3) 
    PyPlot.clf()
    der3= x -> bsplnder(x,3,3)
    der3num= x -> numder(der2,x)
    draw_function(der3)
    draw_function(der3num)
    PyPlot.title("third derivative")
end

function test_gammavalnum()
    PyPlot.clf()
    # This takes a long time !!!
    draw_function(x-> gammavalnum(x,0,3),lims=4)
end

function test_convolve()
    a=[1; 2;3;2;1]
    b=[1; -1]
    print(convolve(a,b))
end    

function test_gamma(q,n)
    num=gammanum(q,n)
    anal=gamma(q,n)
    err=norm(num-anal)
    print("num=",num," anal=",anal," err=",err)
end

function compgamma23(i,j,l,h)
    g0=abs(i-j)>3 ? 0. : gamma23vals[4+i-j]
    #g1=gamma23valnum(i,j,l,h)
    g1=gamma23val(i,j,l,h)
    err=abs(g0-g1)
    println("i=",i," j=",j," g0=",g0," g1=",g1," err=",err)
end
    
function test_gamma23()
    l=0 ; h=10 ;
    println("These should be the same")
    compgamma23(3,5,l,h)
    compgamma23(4,5,l,h)
    compgamma23(5,5,l,h)
    compgamma23(6,5,l,h)
    compgamma23(7,6,l,h)
    compgamma23(8,7,l,h)
    compgamma23(9,8,l,h)
    println("The rest will not be the same")
    compgamma23(10,9,l,h)
    compgamma23(11,10,l,h)
    compgamma23(12,11,l,h)
end

end # module
