"""
Image similarity criterion--number of equal pixels (EQ).

Represent criteria ``\\sum_i \\phi(f_i,g_i)``,

where ``\\phi(f_i,g_i) = \\begin{cases}\\begin{array}{cl} 1 & \\textrm{for }f_i=g_i \\\\ 0 & \\textrm{otherwise,}\\end{array}\\end{cases}``

``f_i``, ``g_i`` are pixel values.
"""
module eqcrit

using criterion
import segimgtools

export EQParameters, EQState

#-------------- implement the Criterion protocol
"""
There are no parameters for the EQ criterion, just used for dispatching.
"""
struct EQParameters <: CriterionParameters

end

"""
There is no state for the EQ criterion, just used for dispatching.
"""
struct EQState <: CriterionState{EQParameters}

end

"""
    criterion_init(f, g, p)
Initialize the criterion state given two images `p`, `q` and parameters `p`.
"""
function criterion.criterion_init(f::segimgtools.LabelImage, g::segimgtools.LabelImage, p::EQParameters)
    EQState()
end

"""
    criterion_eval(s, pf, pg)
Return criterion value ``\\phi(f_i,g_i)`` for two pixels `pf`, `pg` and criterion state `s`.
"""
function criterion.criterion_eval(s::EQState, pf::segimgtools.LabelType, pg::segimgtools.LabelType)
    convert(Float64, pf == pg)
end
#-------------- end criterion protocol

end #module
