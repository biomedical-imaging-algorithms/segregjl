# Print macros @debugprintln, @debuftime etc work like println(), @time ... except that it can be switched
# on and off by calling switchprinting(true) and switchprinting(false)
#
# Example:
# using debugprint
# # include("debugprint")
# switchprinting(true)
# @debugprintln("Hello world")
# switchprinting(false)
# @debugprintln("Hello world")
#
# Martin Dolejsi , dolejm1@fel.cvut.cz, March 2015

#module debugprint
#
#export switchprinting

function switchprinting(doprint::Bool)
    if doprint
        # define the macros as working
        @eval macro debugtime(ex, msg)
            return quote
                #                print_with_color(:cyan, string($msg, "\n"))
                printstyled(string($msg, "\n");color=:cyan)
                @time $(esc(ex))
            end
        end
        @eval macro debugtimeline(ex, msg)
            return quote
                #print_with_color(:cyan, string($msg, "\n"))
                #@time $(esc(ex))
                local t0=time()
                local rval=$(esc(ex))
                local t1=time()
                #print_with_color(:cyan, string($msg, " took $(t1-t0)s\n"))
                printstyled(string($(esc(msg)), " took ",t1-t0, "s\n");color=:cyan)
                rval # cannot use 'return' here
            end
        end

        @eval macro debugprintln_with_color(color, msg)
            return quote
                #print_with_color($color, string($msg, "\n"))
                printstyled(string($(esc(msg)), "\n");color=$color)
            end
            
        end
        @eval macro debugprintln(msg)
            return quote
                println($msg)
            end
        end
    else
        # define the macros as empty
        @eval macro debugtime(ex, msg)
            return quote
                $(esc(ex))
            end
        end
        @eval macro debugtimeline(ex, msg)
            return quote
                $(esc(ex))
            end
        end
        @eval macro debugprintln_with_color(color, msg)
            return :nothing
        end
        @eval macro debugprintln(msg)
            return :nothing
        end
    end
end

#end
# switchprinting(false) # by default, do nothing
