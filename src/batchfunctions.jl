"""
Functions used in Jirka Borovec automatic evaluation scripts.

You can find the script files calling the functions with example usage in the function description.

Martin Dolejsi, September 2015
"""
module batchfunctions

using Images, FixedPointNumbers
using Clustering
using segimgtools
using finereg
import SLICsuperpixels
import deformation
import mil
using Options
using optimizer
using regularization
import CPUTime

include("debugassert.jl")
switchasserts(false)
include("debugprint.jl")
switchprinting(false)

export AbstractRegParams, FlagshipRegParams
abstract type AbstractRegParams end

"""
Contains all possible parametes for registration of flagship images and its default values
"""
mutable struct FlagshipRegParams<:AbstractRegParams
    spedge::Int
    regular::Int
    k::Int
    hmax::Int
    blab::Bool
    repeatKM::Int
    minimgsize::Int
    minkpts::Int
    kpdist::Int
    largerot::Bool
    spkp::Bool
    hdef::Bool
    resampleFactor::Vector{Int}
    levelProg::Vector{Int}
    def::Vector{Type}
    defPar::Vector{Vector{Int}}
    
    function FlagshipRegParams()
        new(40, 20, 4, 20, false, 30, 128, 500, 50, false, true, false, [0; 1], [1; 0], [deformation.RigidDeformation2D], Vector{Int}[])
    end
end

"""
    function parse_bool(params, i)
Return `Boole` valure by parsing `params[i, 2]`
"""
function parse_bool(params, i)
    if  (params[i, 2] == false) || (params[i, 2] == 0) || (params[i, 2] == "false")
        return false
    elseif (params[i, 2] == true) || (params[i, 2] == 1) || (params[i, 2] == "true")
        return true
    else
        warn("Boole expected, but the value $couldn't be parsed")
    end
end

"""
    parse_def(par, param, defN)
Return vector of deformation types to be used for registration.
"""
function parse_def!(par, param, defN)
    if defN == 1
        par.def = Type[]
        par.defPar = Vector{Int}[]
    end
    stdef = split(param)
    if stdef[1] == "BsplineDeformation"
        push!(par.def, deformation.BsplineDeformation)
        push!(par.defPar, parse.(stdef[2:end]))
    else
        if stdef[1] == "RigidDeformation2D"
            push!(par.def, deformation.RigidDeformation2D)
        elseif stdef[1] == "ScaleDeformation2D"
            push!(par.def, deformation.ScaleDeformation2D)
        elseif stdef[1] == "AffineDeformation2D"
            push!(par.def, deformation.AffineDeformation2D)
        elseif stdef[1] == "RigidDeformation3D"
            push!(par.def, deformation.RigidDeformation3D)
        elseif stdef[1] == "AffineDeformation3D"
            push!(par.def, deformation.AffineDeformation3D)
        end
        push!(par.defPar, Int[])
    end
end

"""
    get_params_from_file(par, paramfile)
Function to load registration parameters from the file. Input parameter is a structure containing
default values. The defaults are changed only if the field is present in the `pramfile`.

The function is used in:
* [`test_register_flagship_script`](@ref)
"""
function get_params_from_file{T<:AbstractRegParams}(par::T, paramfile)
    params = readdlm(paramfile, '=', '\n')
    defN = 1
    for i = 1:size(params, 1)
        if params[i, 1] == "Superpixel edge length"
            par.spedge =  params[i, 2]; continue
        end
        if params[i, 1] == "Regularization"
            par.regular =  params[i, 2]; continue
        end
        if params[i, 1] == "k"
            par.k =  params[i, 2]; continue
        end
        if params[i, 1] == "Maximal shift"
            par.hmax =  params[i, 2]; continue
        end
        if params[i, 1] == "Lab conversion"
            par.blab = parse_bool(params, i); continue
        end
        if params[i, 1] == "k-means repetitions"
            par.repeatKM =  params[i, 2]; continue
        end
        if params[i, 1] == "Minimal image size"
            par.minimgsize =  params[i, 2]; continue
        end
        if params[i, 1] == "Minimal keypoints count"
            par.minkpts =  params[i, 2]; continue
        end
        if params[i, 1] == "Keypoints distance"
            par.kpdist =  params[i, 2]; continue
        end
        if params[i, 1] == "Resolve large rotation"
            par.largerot = parse_bool(params, i)
        end
        if params[i, 1] == "Use superpixels for keypoints"
            par.spkp = parse_bool(params, i); continue
        end
        if params[i, 1] == "Use HierarchicalDeformation"
            par.hdef = parse_bool(params, i); continue
        end
        if params[i, 1] == "Resample factor"
            par.resampleFactor = parse.(split(params[i, 2])); continue
        end
        if params[i, 1] == "Level progresion"
            par.levelProg = parse.(split(params[i, 2])); continue
        end
        if params[i, 1] == "Deformation"
            parse_def!(par, params[i, 2], defN); defN += 1; continue
        end
    end
    return par
end

"""
    test_register_script(moving, static, destination, paramfile, land = [])
Register `static` and `moving` images rigidly and save the result to `destination` folder.
Parameters from `paramfile` are used.

In the `destination` folder are:
* time.txt: duration of the registration
* registered.png: registered image
* landmarks.txt: registered landmarks if `land` is a .csv file with the static image landmarks

This function is called by benchmark.jl.
"""
function test_register_flagship_script(moving, static, destination, paramfile, land = [])
    println("Registering...")
    #default params
    par = FlagshipRegParams()
    par = get_params_from_file(par, paramfile)

    #prepare destination folder
    if !isdir(destination)
        mkdir(destination)
    end
    imgreg = []   # this looks like a dirty trick to make the variable appear outside of the 'for' loop.
    imgbase = []
    timeTot = 0.
    d = []
    for i = 1:2
        timeTot = CPUTime.CPUtime_us()
        #timeTot = time()/
        #read images image
        imgbase = Images.load(static)
        imgreg = Images.load(moving)
        
        # Segment base image
        lb, count = SLICsuperpixels.slicSize(imgbase, par.spedge^2, par.regular, par.blab, UInt16)
        fbase = SLICsuperpixels.getMeans(lb, imgbase, count)
        nhoodSP = SLICsuperpixels.getNeighbors(lb, count)
        lbase = []
        pbase = []
        lcnt = 0
        while length(lbase) == 0 && lcnt < 3
            lbase, pbase = segimgtools.kmeans_segmentation(lb, fbase, par.k, par.repeatKM)
            lcnt += 1
        end
        if length(lbase) == 0
            println("k-Means failed")
            return nothing
        end
        # Segment second image
        lr, count = SLICsuperpixels.slicSize(imgreg, par.spedge^2, par.regular, par.blab, UInt16)
        freg = SLICsuperpixels.getMeans(lr, imgreg, count)
        lreg = []
        preg =[]
        lcnt = 0
        while length(lreg) == 0 && lcnt < 3
            lreg, preg = segimgtools.kmeans_segmentation(lr, freg, par.k, par.repeatKM)
            lcnt += 1
        end
        if length(lreg) == 0
            println("k-Means failed")
            return nothing
        end
        # Init registration
        if par.spkp
            kpts, nhoodKP = finereg.find_keypoints_and_neighbors(lb, lbase, nhoodSP)
        else
            kpts, nhoodKP = finereg.find_keypoints_and_neighbors(pbase, par.kpdist)
        end
        
        crit_params=mil.MILParameters(par.k)
        reg_params = regularization.NoneRegularizationParams()
        opt_params = optimizer.OptimizerMMA(abstol=1e-6, neval=5000, initstep=1.0)
        
        isize=[size(pbase)...]

        # Register
        if par.hdef
            dv = Vector{deformation.AbstractDeformation}(length(par.def))
            for i = 1:length(par.def)
                dv[i] = deformation.create_identity(isize .* Images.pixelspacing(preg), par.def[i], par.defPar[i])
            end
            def = deformation.HierarchicalDeformation(dv)
            opt = @options hmax = par.hmax minKP = par.minkpts largerotation = par.largerot resample = true
            d = finereg.register_fast_multiscaleHB(pbase, preg, kpts, def, nhoodKP, crit_params,
                                            opt_params, reg_params, par.resampleFactor, par.levelProg, opt)
        else
            def=deformation.create_identity(isize .* Images.pixelspacing(preg), par.def[end])   # using the last deformation is for safety reasons, if one switch between hierarchical an conventional transformation.
            opt = @options hmax = par.hmax minsize = par.minimgsize minKP = par.minkpts largerotation = par.largerot
            d = finereg.register_fast_multiscale(pbase, preg, kpts, def, nhoodKP, crit_params,
                                            opt_params, reg_params, opt)
        end

        #timeTot = time() - timeTot
        timeTot = (CPUTime.CPUtime_us() - timeTot)/1e6
    end
    # Evaluate the registration
    println("Creating output...")
    y  = zeros(Float64, 2)
    if land != []
        fstr = open(land, "r")
        landreg = readdlm(fstr, Float64; skipstart = 2)
        close(fstr)
        fstr = open(string(destination, "landmarks.txt"), "w")
        write(fstr, "point\n")
        print_shortest(fstr, size(landreg, 1))
        write(fstr, "\n")
        for poin = 1:size(landreg, 1)
            deformation.transform_point!(d, vec(landreg[poin, :]), y)
            print_shortest(fstr, y[1])
            write(fstr, " ")
            print_shortest(fstr, y[2])
            write(fstr, "\n")
        end
        close(fstr)
    end
    fstr = open(string(destination, "time.txt"), "w")
    print_shortest(fstr, timeTot)
    close(fstr)
    opts = @options default=imgreg[1,1]
    registered = deformation.transform_image_lin(d, imgreg, size(imgbase), opts)
    Images.save(string(destination, "registered.png"), registered)
end


end         #module
