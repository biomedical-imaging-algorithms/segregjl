# Experiments with SoftMax segmentation in Julia
# Martin Dolejsi, Jan Kybic, July 2015

module testsoftmax

using FixedPointNumbers
using SLICsuperpixels
using deformation
using segimgtools
using ImageView
using softmax
using Options
using PyCall
using PyPlot
using Images
using Colors
#using Debug


include("debugassert.jl")
switchasserts(true)
include("debugprint.jl")
switchprinting(true)

function test_basic_learn()
    # tr=[([2661, 2193],              [1.000 -0.773 -0.327 -0.207; 1.000 -1.045 -0.504 -0.385]),
        # ([4546, 2947, 3406],        [1.000 -0.860 -0.984 -0.977; 1.000 -1.006 -1.080 -1.084; 1.000 -0.834 -0.864 -0.858]),
        # ([2916, 2916, 2809, 2861],  [1.000  1.225  1.212  1.189; 1.000  1.222  1.211  1.195; 1.000  1.228  1.217  1.190; 1.000 1.227 1.214 1.192])]
    # tr=[([26., 26.], [1.000 1. 0. 0.]),
        # ([26., 26.], [1.000 0. 1. 0.]),
        # ([26., 26.], [1.000 0. 0. 1.])]
    tr=[([26., 26.], [1 .9 .9 .9]'),
        ([26., 26.], [1 .6 .4 .4]'),
        ([26., 26.], [1 .9 .8 .7]')]
    smc = softmax.SoftMaxClassifier(1, length(tr), size(tr[1][2], 1))
    softmax.train_classifier!(smc, tr)
  
    println(smc)
    for i = 1:length(tr)
        println("class $i,->$(softmax.hard_classify(smc, tr[i][2]))")k
    end
    set_a!(smc,[0 .3 .3 .3; 0 2.5 -1.25 -1.25; 0 .9 .9 -.9])
    for i = 1:length(tr)
        println("class $i,->$(softmax.hard_classify(smc, tr[i][2]))")
    end
    cr, gr = softmax.softmax_crit(smc, tr)
    println(cr, gr)
end

function test_MI_crit()
    img2 = Images.load("imgs/simple.png"); spedge = 5; regular = 20

    angles = 0.:10.:90.
    angle=0.; shift=[0.; 0.];
    J2 = Array(Float64, length(angles))
    J3 = Array(Float64, length(angles))
    count1 = 0
    count2 = 0
    for i = 1:length(angles)
        angle = angles[i]
        println("Evaluating for angle $angle")
        opts = @options default = img2[]
        img1 = deformation.transform_rigid2D(img2, size(img2), angle, shift, opts)
        
        s2l, count2 = SLICsuperpixels.slicSize(img2, spedge^2, regular, false, UInt16)
        s2f = [ones(count2)'; SLICsuperpixels.getMeans(s2l, img2, count2)]
        
        s1l, count1 = SLICsuperpixels.slicSize(img1, spedge^2, regular, false, UInt16)
        s1f = [ones(count1)'; SLICsuperpixels.getMeans(s1l, img1, count1)]
    
        k = 2; 
        order = 1
        classtwo2 = softmax.SoftMaxTwo(order, k, size(s2f, 1))
        softmax.init_softmaxtwo_random!(classtwo2, segimgtools.SuperpixelImage[s1l, s2l], Array{Float64, ndims(s2f)}[s1f, s2f], 3. * spedge)
        overlap = segimgtools.segmentation_overlap_array(s1l, s2l)
        J2[i] = softmax.init_softmaxtwo_random_optimize_repeat!(classtwo2, segimgtools.SuperpixelImage[s1l, s2l], Array{Float64, ndims(s2f)}[s1f, s2f], overlap, 3. * spedge, false, 10, 0.001)
        
        k = 3; 
        order = 1
        classtwo3 = softmax.SoftMaxTwo(order, k, size(s2f, 1))
        softmax.init_softmaxtwo_random!(classtwo3, segimgtools.SuperpixelImage[s1l, s2l], Array{Float64, ndims(s2f)}[s1f, s2f], 3. * spedge)
        overlap = segimgtools.segmentation_overlap_array(s1l, s2l)
        J3[i] = softmax.init_softmaxtwo_random_optimize_repeat!(classtwo3, segimgtools.SuperpixelImage[s1l, s2l], Array{Float64, ndims(s2f)}[s1f, s2f], overlap, 3. * spedge, false, 10, 0.001)
        
        assig = zeros(count1)
        assig = softmax.hard_classify(classtwo3.class[1], s1f)
        p1 = Images.Image(assig[s1l], spatialorder=Images.spatialorder(s1l))
        assig = zeros(count2)
        assig = softmax.hard_classify(classtwo3.class[2], s2f)
        p2 = Images.Image(assig[s2l], spatialorder=Images.spatialorder(s2l))

        ImageView.view(p1/maximum(p1), name = string(angle))
        ImageView.view(p2/maximum(p2), name = string(angle))
    end
    println(J2)
    println(J3)
    println((J3-J2)./J2)
    println((J2./J3).*100)

    figure(1); clf;
    plot(angles, J2, "g")
    plot(angles, J3, "r")
    return nothing
end

function test_random_init()
    # img2t = Images.imread("imgs/simple.png"); k = 3; spedge = 5
    img2t = Images.imread("imgs/Case008_HER2-sm-sm.png"); k = 3; spedge = 5
    # img2t = Images.imread("m:/microscopy/Flagship/images-MultiGeneSignatureMaps_convert/scale-5pc/Case001/case01-1-he.jpg");k = 3; spedge = 30

    angle=5.; shift=[0.; 0.]; regular = 20
    img1t = deformation.transform_rigid2D(img2t, size(img2t), angle, shift)
    # img1t=imread("m:/microscopy/Flagship/images-MultiGeneSignatureMaps_convert/scale-5pc/Case001/case01-2-ki67.jpg");
sz = larger_size(size(img1t), size(img2t))
img1 = extend_to_size(img1t, sz, (img1t[2, 2] + img1t[2, end - 1] + img1t[end - 1, end - 1] + img1t[end - 1, 2]) / 4, false)
img2 = extend_to_size(img2t, sz, (img2t[2, 2] + img2t[2, end - 1] + img2t[end - 1, end - 1] + img2t[end - 1, 2]) / 4, false )

    println("Finding superpixels in the fixed image:")
    s2l, count2 = @time SLICsuperpixels.slicSize(img2, spedge^2, regular, false, UInt16)
    s2f = [ones(count2)'; SLICsuperpixels.getMeans(s2l, img2, count2)]
    println("Finding superpixels in the moving image:")
    s1l, count1 = @time SLICsuperpixels.slicSize(img1, spedge^2, regular, false, UInt16)
    s1f = [ones(count1)'; SLICsuperpixels.getMeans(s1l, img1, count1)]

    order = 1
    classtwo = softmax.SoftMaxTwo(order, k, size(s1f, 1))
    # ####### Profiling
    # print_with_color(:cyan, "Segmentation profiling:\n")
    # Profile.clear()
    # Profile.init(delay = 0.0001)
    # @profile softmax.init_softmaxtwo_random!(classtwo, segimgtools.SuperpixelImage[s1l, s2l], Array{Float64, ndims(s2f)}[s1f, s2f], 3 * spedge)
    # aaa=open("prof.txt","w")
    # Profile.print(aaa,format = :flat, combine = true)
    # close(aaa)
    # Profile.clear()
    # #######
    softmax.init_softmaxtwo_random!(classtwo, segimgtools.SuperpixelImage[s1l, s2l], Array{Float64, ndims(s2f)}[s1f, s2f], 3. * spedge)
    
    assig = zeros(count1)
    assig = softmax.hard_classify(classtwo.class[1], s1f)
    p1 = Images.Image(assig[s1l], spatialorder=Images.spatialorder(s1l))
    assig = zeros(count2)
    assig = softmax.hard_classify(classtwo.class[2], s2f)
    p2 = Images.Image(assig[s2l], spatialorder=Images.spatialorder(s2l))

    ImageView.view(p1/maximum(p1))
    ImageView.view(p2/maximum(p2))
    ImageView.view(img2)
end

function test_optimize_init_rep()
    # img2t = Images.imread("imgs/simple.png"); k = 3; spedge = 5
    # img2t = Images.imread("imgs/Case008_HER2-sm-sm.png"); k = 3; spedge = 5
    img2t = Images.imread("imgs/case03-5-he-small.png"); k = 3; spedge = 10
    # img2t = Images.imread("m:/microscopy/Flagship/images-MultiGeneSignatureMaps_convert/scale-5pc/Case001/case01-1-he.jpg");k = 3; spedge = 30

    angle= 3.; shift = [0.; 0.]; regular = 20
    img1t = deformation.transform_rigid2D(img2t, size(img2t), angle, shift)
    # img1t = Images.imread("m:/microscopy/Flagship/images-MultiGeneSignatureMaps_convert/scale-5pc/Case001/case01-2-ki67.jpg");
sz = larger_size(size(img1t), size(img2t))
img1 = extend_to_size(img1t, sz, (img1t[2, 2] + img1t[2, end - 1] + img1t[end - 1, end - 1] + img1t[end - 1, 2]) / 4, false)
img2 = extend_to_size(img2t, sz, (img2t[2, 2] + img2t[2, end - 1] + img2t[end - 1, end - 1] + img2t[end - 1, 2]) / 4, false )

    println("Finding superpixels in the fixed image:")
    s2l, count2 = @time SLICsuperpixels.slicSize(img2, spedge^2, regular, false, UInt16)
    s2f = SLICsuperpixels.getMeans(s2l, img2, count2)
    s2f = [ones(1, size(s2f, 2)); s2f]
    
    println("Finding superpixels in the moving image:")
    s1l, count1 = @time SLICsuperpixels.slicSize(img1, spedge^2, regular, false, UInt16)
    s1f = SLICsuperpixels.getMeans(s1l, img1, count1)
    s1f = [ones(1, size(s1f, 2)); s1f]
    
    overlap = segimgtools.segmentation_ovrelap_array(s1l, s2l)
    order = 1
    classtwo = softmax.SoftMaxTwo(order, k, size(s1f,1))
    J = softmax.init_softmaxtwo_random_optimize_repeat!(classtwo, segimgtools.SuperpixelImage[s1l, s2l], Array{Float64, ndims(s2f)}[s1f, s2f], overlap, 3. * spedge, false, 10, 0.001)
    println("J0 = $J")
    
    assig = zeros(count1)
    assig = softmax.hard_classify(classtwo.class[1], s1f)
    p1 = Images.Image(assig[s1l], spatialorder=Images.spatialorder(s1l))
    assig = zeros(count2)
    assig = softmax.hard_classify(classtwo.class[2], s2f)
    p2 = Images.Image(assig[s2l], spatialorder=Images.spatialorder(s2l))
    ImageView.view(p1/maximum(p1))
    ImageView.view(p2/maximum(p2))

    # set_a!(classtwo.class[1], [0, 0, 0, 10., 0, 0, 0, 10, 0, 0, 0, 10])
    # set_a!(classtwo.class[2], [0, 0, 0, 10., 0, 0, 0, 10, 0, 0, 0, 10])
    # J = softmax.mutual_inf!([], classtwo, overlap, Array{Float64, ndims(s2f)}[s1f, s2f])
    # println("J1 = $J")
    
    # assig = zeros(count1)
    # assig = softmax.hard_classify(classtwo.class[1], s1f')
    # p1 = Images.Image(assig[s1l], spatialorder=Images.spatialorder(s1l))
    # assig = zeros(count2)
    # assig = softmax.hard_classify(classtwo.class[2], s2f')
    # p2 = Images.Image(assig[s2l], spatialorder=Images.spatialorder(s2l))
    # ImageView.view(p1/maximum(p1))
    # ImageView.view(p2/maximum(p2))
end


end #module testsoftmax
