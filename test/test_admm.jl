"""
Experiments with registration using Alternating Direction Method of Multipliers (ADMM) optimization.
The method itself is implemented in the [admm](@ref) module.
"""
module test_admm

using admm
import Winston
using Options
using Images
using ImageView
import deformation
import keypoints
import segimgtools
import ssd
import mil
import optimizer
import finereg
import regularization
import readMetaimage
import erroreval
using JLD
using SLICsuperpixels
using FixedPointNumbers

include("debugassert.jl")
switchasserts(true)
include("debugprint.jl")
switchprinting(true)


"""
    test_cg()
Test the conjugated gradient solver. [`admm.cg_linear_solve`](@ref)
"""
function test_cg()
    a=[ 10 2 3 ; 2 6 1 ; 3 1 8 ]
    @assert(maximum(abs(a-a'))<1e-6) # symmetric
    @assert(all(x-> x>0.,eig(a)[1])) # positive definite matrix

    function applyA(x)
        a*x
    end

    @show truex=[10.;20.;30.]
    @show b=applyA(truex)
    x=cg_linear_solve(applyA,b,zeros(Float64,3);reltol=1e-6)
    println("x=",x)
    @assert(norm(x-truex,Inf)<1e-3)
end

"""
    test_cg2()
Test the conjugated gradient solver. [`admm.cg_linear_solve`](@ref)
"""
function test_cg2()
    n=100
    ap=randn(n,n)
    a=ap*ap' # now a is symmetric and positive definite
    @assert(maximum(abs(a-a'))<1e-6*maximum(abs(a))) # symmetric
    @assert(all(x-> x>0.,eig(a)[1])) # positive definite matrix

    function applyA(x)
        a*x
    end

    truex=randn(n)
    b=applyA(truex)
    x=cg_linear_solve(applyA,b,zeros(Float64,n);reltol=1e-66,abstol=1e-6)
    res=norm(applyA(x)-b,Inf)
    println("errorx=",norm(x-truex,Inf)," residual=",res)
    @assert(res<1e-3)
end

"""
    test_admm_single()
Test [admm](@ref) by fitting a linear function using least squares to a set of points.
The linear relationship is ``Ax+c=y``, where ``x`` is a 2D vector of parameters,
``A`` is the linear basis and ``c`` is a constant.
"""
function test_admm_single()
    n=100 # number of points
    t=rand(Float64,n)
    function applyA(x,t=t)
        x[1]+x[2]*t
    end

    function applyAt(y)
        [ sum(y); dot(t,y) ]
    end
    
    truex=[10.;3.] # true line parameters
    sigma=0.2 # noise amplitude
    c0=3. 
    z=applyA(truex)+sigma*randn(n)    # measurements corrupted by noise

    function eval_ji(i,y)
        (z[i]+c0-y)^2
    end
    
    function optim_ji(i,w2,w1;opts...)   # keyword arguments not used
        (z[i]+c0-0.5*w1)/(1+w2)
    end

    tw1=Winston.plot(t,z,"k.")
    Base.display(tw1)
    x0=[0.;0.]
    opts=OptimizerADMMS(monitor=true)
    x=admmss_optimize(eval_ji,optim_ji,applyA,applyAt,x0,c0*ones(Float64,n);opts=opts)
    t2=[0.;1.]
    y2=applyA(x,t2)
    yt=applyA(truex,t2)
    Winston.oplot(t2,y2,"g-")     # estimated model
    tw=Winston.oplot(t2,yt,"r--") # true model
    Base.display(tw)
    println("truex=$truex  x=$x  err=$(norm(x-truex,Inf))")
end         

"""
    test_register_fast_affine_admm()
Test affine image registration using [admm](@ref).
"""
function test_register_fast_affine_admm()
    img2=Images.load("imgs/simple.png")
    #angle=15. ; shift=[11.;-5]
    #angle=3. ; shift=[2.;-1.]
    angle=0. ; shift=[3.;0.]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=50,sigma=[1.;1.]),"find simple keypoints")
    hmax=30
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    # img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    #ImageView.view(img1kpts,name="with keypoints")
    #ImageView.view(Images.separate(segimgtools.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    @time crit_state=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    opt_params=admm.OptimizerADMMS(;monitor=true,:maxiter =>1000,errprim=0.1,errdy=0.1,
                                   ji_opts=Dict(:maxiter =>100),
                                   cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>100) )
    reg_params = regularization.NoneRegularizationParams()
    #opt_params=optimizer.OptimizerMMA(abstol=1e-8,neval=10000,initstep=0.1)
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    @debugprintln("Registering")
    @time d=finereg.register_fast(img1,img2,kptsampled,def,crit_state,opt_params,reg_params,@options resample=true hmax=hmax)
    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(segimgtools.overlay(img1,warped)),name="overlay after") 
end    

"""
    test_register_fast_multiscale_admm()
Test affine image registration on multipple scales using [admm](@ref).
"""
function test_register_fast_multiscale_admm()
    # JK: started, not tested
    img2=Images.load("imgs/simple.png")
    angle=15. ; shift=[11.;-5]
    #angle=3. ; shift=[2.;-1.]
    # angle=0. ; shift=[3.;0.]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=50,sigma=[1.;1.]),"find simple keypoints")
    hmax=30
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    #img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    #ImageView.view(img1kpts,name="with keypoints")
    #ImageView.view(Images.separate(segimgtools.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    #@time crit_state=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    crit_params=ssd.SSDParameters()
    #opt_params=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=1.0)
    opt_params=admm.OptimizerADMMS(;monitor=true,:maxiter =>1000,errprim=0.1,errdy=0.1,
                                   ji_opts=Dict(:maxiter =>100),
                                   cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>100) )
    reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_params,reg_params,@options resample=true hmax=hmax minsize=64 minKP=50 largerotation=false)
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(segimgtools.overlay(img1,warped)),name="overlay after") 
end    

"""
    test_register_fast_multiscale_hierarchical_admm()
Test hierarchical registration of image segmentations on multipple scales using [admm](@ref).
"""
function test_register_fast_multiscale_hierarchical_admm()
    img2=Images.load("imgs/case03-5-he-small.png"); k = 3;
    img1=Images.load("imgs/case03-3-psap-small.png")
    spedge=15
    compact = 10
    mossize = 100
    s1l, features, count = SLICsuperpixels.slicSizeMeans(img1, spedge^2, compact, true, UInt16)
    l1, p1 = segimgtools.kmeans_segmentation(s1l, features, k)
    s2l, features, count = SLICsuperpixels.slicSizeMeans(img2, spedge^2, compact, true, UInt16)
    l2, p2 = segimgtools.kmeans_segmentation(s2l, features, k)
    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kpts, nhoods = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")
    
    hmax=20
    #img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    #ImageView.view(img1kpts,name="with keypoints")
    #ImageView.view(Images.separate(segimgtools.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    #@time crit_state=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    crit_params=mil.MILParameters(k)
    #opt_params=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=1.0)
    opt_params=admm.OptimizerADMMS(;monitor=false,:maxiter =>10,errprim=0.1,errdy=0.1,
                                   ji_opts=Dict(:maxiter =>50),
                                   cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>50) )
    # reg_params = regularization.NoneRegularizationParams()
    reg_params = regularization.DiffRegularizationParams([.3,.3])
    isize=[size(img1)...]
    resampleFactor= [0,1,1,1]
    levelProg=      [1,0,0,0]
    bc = 7
    def = deformation.HierarchicalDeformation([deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc, bc]);
                                           deformation.create_identity([size(img2)...] .* Images.pixelspacing(img1), deformation.AffineDeformation2D)])

    @debugprintln("Finding keypoint neighbors")
    opt = @options hmax = hmax minKP = 50 largerotation = false resample = true
    d = @debugtimeline(finereg.register_fast_multiscaleHB(p1, p2, kpts, def, nhoods, crit_params,
                       opt_params, reg_params, resampleFactor, levelProg, opt), "Multiscale registration:")
                       
    warped=deformation.transform_image_nn(d, p2, size(p1), @options)
    ImageView.view(deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(warped, k)), name = "segmentation overlay after registration")
    warpedimg=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options),"transform_image_lin")
    ImageView.view(deformation.overlay(img1,warpedimg), name="overlay after registration")
    ImageView.view(segimgtools.mosaic(img1, warpedimg, mossize), name = "mosaic after registration")
    return nothing
end    

"""
    test_register_fast_multiscale_admm_lena()
Test affine real world image registration on multipple scales using [admm](@ref).
"""
function test_register_fast_multiscale_admm_lena()
    img2=Images.load("imgs/lena.png")
    #angle=15. ; shift=[11.;-5]
    angle=20. ; shift=[5.;-50.]
    #angle=0. ; shift=[5.;0]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    #ImageView.view(img1,name="image 1") 
    #ImageView.view(img2,name="image 2") 
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=100,sigma=[1.;1.]),"find simple keypoints")
    hmax=10
    kptsampled=keypoints.create_sampled_keypoints(img1, kptsimple, hmax; spacing=float(spacing))
    # img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    # ImageView.view(img1kpts,name="with keypoints")
    ImageView.view(Images.separate(deformation.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    crit_params=ssd.SSDParameters()
    #opt_params=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=1.0)
    opt_params=admm.OptimizerADMMS(;monitor=false,maxiter =1000,errprim=0.1,errdy=0.1,rho=1.,
                                   ji_opts=Dict(:maxiter =>200),
                                   cg_opts=Dict(:abstol=>1e-5, :reltol => 0., :maxiter =>200) )
    # reg_params = regularization.NoneRegularizationParams()
    reg_params = regularization.DiffRegularizationParams([.3])
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    # deformation.set_theta!(def,[1,0,0,1,50,50.])
    # @show def.A
    # def=deformation.BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), [2,2])
    # def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [11,11])
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_params,reg_params,@options resample=true hmax=hmax minsize=128 minKP=50 largerotation=false)
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(deformation.overlay(img1,warped)),name="overlay after") 
end

"""
    init_transform(p1, p2, img1, img2)
Return rotation matrix translation vector and scale of 2D transformation.
Transformation is estimated using moments. This is intended as initialization of
`test_register_fast_multiscale_hierarchical_admm_droso_annot`. The rotatation is not unique,
there is also possible to use ``\\phi + \\pi``. The transformation with smaler ssd on RGB images
is returned.
"""
function init_transform(p1, p2, img1, img2)
    cog1 = segimgtools.object_centroid(p1, 1)
    cog2 = segimgtools.object_centroid(p2, 1)
    eig1 = eigfact!(segimgtools.object_cov_mat(p1, cog1, 1))
    eig2 = eigfact!(segimgtools.object_cov_mat(p2, cog2, 1))
    scale = sum(sqrt(eig2[:values] ./ eig1[:values])) / 2

    R = segimgtools.get_rot((-eig1[:vectors][:, 1]) * (-eig2[:vectors][:, 1])' + (eig1[:vectors][:, 1]) * (eig2[:vectors][:, 1])')
    t = (cog2 + eig2[:vectors][:, 1] ./ 2) - scale * R * (cog1 + eig1[:vectors][:, 1] ./ 2)
    
    R1 = segimgtools.get_rot((eig1[:vectors][:, 1] ./ 2) * (-eig2[:vectors][:, 1] ./ 2)' + (-eig1[:vectors][:, 1] ./ 2) * (eig2[:vectors][:, 1] ./ 2)')
    t1 = (cog2 + eig2[:vectors][:, 1] ./ 2) - scale * R1 * (cog1 - eig1[:vectors][:, 1] ./ 2)
    
    i1 = Images.separate(img1)
    i2 = Images.separate(img2)
    i1 = Images.grayim(i1[:,:,1]')
    i2 = Images.grayim(i2[:,:,1]')

    segimgtools.mask_image!(i1, p1, 3, 0)
    segimgtools.mask_image!(i2, p2, 3, 0)

    def = deformation.AffineDeformation2D([size(i1)...] .* Images.pixelspacing(p1), [(scale * R)[:]; t])
    pt1 = deformation.transform_point(def, cog1 + eig1[:vectors][:, 1])
    pt2 = deformation.transform_point(def, cog1 + eig1[:vectors][:, 2])
    println("R transformed points")
    println(pt1-(cog2 + eig2[:vectors][:, 1]))
    println(pt2-(cog2 + eig2[:vectors][:, 2]))
    imgreg1 = deformation.transform_image_lin(def, i2, size(i1), @options default = i1[1])

    def = deformation.AffineDeformation2D([size(i1)...] .* Images.pixelspacing(p1), [(scale * R1)[:]; t1])
    pt1 = deformation.transform_point(def, cog1 - eig1[:vectors][:, 1])
    pt2 = deformation.transform_point(def, cog1 - eig1[:vectors][:, 2])
    println("R1 transformed points")
    println(pt1-(cog2 + eig2[:vectors][:, 1]))
    println(pt2-(cog2 + eig2[:vectors][:, 2]))
    imgreg2 = deformation.transform_image_lin(def, i2, size(i1), @options default = i1[1])
    ssd1 = ssd.ssd_val(i1, imgreg1)
    ssd2 = ssd.ssd_val(i1, imgreg2)
    println("ssd1 = $ssd1")
    println("ssd2 = $ssd2")
    # ImageView.view(imgreg1/maximum(imgreg1))
    # ImageView.view(imgreg2/maximum(imgreg2))

    if ssd1 < ssd2
        return R, t, scale
    end
    return R1, t1, scale
end

"""
    test_register_fast_multiscale_hierarchical_admm_droso_annot()
Register segmented (annotated) drosophila oocytes of selected stage. Registered images
are writen to `savedir`.
"""
function test_register_fast_multiscale_hierarchical_admm_droso_annot()
    basedir = "m:/"
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/"
    end
    savedir = string(basedir, "microscopy/drosophila/RESULTS/ovary_2D_reg/")
    basedir = string(basedir, "microscopy/drosophila/egg_segmentation/")

    KPspacing = 17
    hmax = 20
    iinx1 = 62      #template (static image)
    finfo = readdlm(string(basedir, "ovary_image_info_for_prague.csv"), '\t', '\n'; skipstart = 1)  #Image metadata

    #read 2class segmantation of the static image
    p1 = reinterpret(UInt8, Images.load(string(basedir,"mask_2d_binary/stage2/mask$(finfo[iinx1, 12]).png")))
    #read 4-class segmentation and remove label 4 (set 4->3)
    p14 = reinterpret(UInt8, Images.load(string(basedir,"mask_2d/stage2/mask$(finfo[iinx1, 12]).png")))+1;
    remove4 = px -> px == 4 ? 3 : px
    segimgtools.map_class!(p14, remove4)

    #remove all the componets from p1 except the largest and mask 3-class segmentation in the same way
    p1l = label_components(Images.data(p1))
    cnts = component_lengths(p1l)
    comp = indmax(cnts[2:length(cnts)])
    segimgtools.mask_image!(p14, p1l, comp, 1)
    segimgtools.mask_image!(p1, p1l, comp, 0)
    
    p1 = p14 #register using 3-class segmentation
    # p1 += 1  #register using 2-class segmentation
    
    #read static image
    img1 = Images.load(string(basedir,"ovary_2d/stage2/train$(finfo[iinx1, 12]).tif"))

    isize=[size(p1)...]
    println("Find keypoints")
    kptsimple, nhoodKP = @time keypoints.find_keypoints_and_neighbors(p1, KPspacing, 1)

    acrit=zeros(Float64, size(finfo, 1))
    bcrit=zeros(Float64, size(finfo, 1))
    macrit = 0.
    mbcrit = 0.
    cnt = 0
    revrot = 0  # number of oocytes registered with reverse initialization
    stage = 2
    for iinx2 = 1:size(finfo, 1)
        # skip problematic segmentations
        if (finfo[iinx2, 12] == 20266) || (finfo[iinx2, 12] == 20268) || (finfo[iinx2, 12] == 20330) || (finfo[iinx2, 12] == 20331) || (finfo[iinx2, 12] == 20332) || (finfo[iinx2, 12] == 20333) || (finfo[iinx2, 12] == 20814) || (finfo[iinx2, 12] == 20815) || (finfo[iinx2, 12] == 20816) || (finfo[iinx2, 12] == 21126) || (finfo[iinx2, 12] == 43043)
            continue
        end
        if (finfo[iinx2, 20] == stage) && (iinx2 != iinx1)      #Select stage
            #read 2class segmantation of the moving image
            p2 = reinterpret(UInt8, Images.load(string(basedir,"mask_2d_binary/stage$stage/mask$(finfo[iinx2, 12]).png")))
            #read 4-class segmentation and remove label 4 (set 4->3)
            p24 = reinterpret(UInt8, Images.load(string(basedir,"mask_2d/stage$stage/mask$(finfo[iinx2, 12]).png")))+1
            segimgtools.map_class!(p24, remove4)

            #remove all the componets from p2 except the largest and mask 3-class segmentation in the same way
            p2l = label_components(Images.data(p2))
            cnts = component_lengths(p2l)
            comp = indmax(cnts[2:length(cnts)])
            segimgtools.mask_image!(p24, p2l, comp, 1)
            segimgtools.mask_image!(p2, p2l, comp, 0)

            p2 = p24 #register using 3-class segmentation
            # p2 += 1  #register using 2-class segmentation

            #read moving image
            img2 = Images.load(string(basedir,"ovary_2d/stage2/train$(finfo[iinx2, 12]).tif"))
            jsize=[size(p2)...]
            k = Int(max(maximum(p1), maximum(p2)))

            # anterior and posterior points from the annotation
            pant1 = [finfo[iinx1, 2] - finfo[iinx1, 4], finfo[iinx1, 3] - finfo[iinx1, 5]]
            post1 = [finfo[iinx1, 16] - finfo[iinx1, 4], finfo[iinx1, 17] - finfo[iinx1, 5]]
            pant2 = [finfo[iinx2, 2] - finfo[iinx2, 4], finfo[iinx2, 3] - finfo[iinx2, 5]]
            post2 = [finfo[iinx2, 16] - finfo[iinx2, 4], finfo[iinx2, 17] - finfo[iinx2, 5]]
            
            # transformation (rigid + scale) from the annotation
            mea1 = (pant1 + post1) / 2
            mea2 = (pant2 + post2) / 2
            H = (pant1-mea1)*(pant2-mea2)' + (post1-mea1)*(post2-mea2)'
            u,s,v = svd(H)
            RGT = v*u'
            if det(RGT) < -.5
                v = v * [1 0; 0 -1]
                RGT = v*u'
            end
            scaleGT = sqrt(sum((pant2-post2).^2))/sqrt(sum((pant1-post1).^2))

            tGT = mea2 - scaleGT * RGT * mea1
            println("ScaleGT = $scaleGT")
            println("RGT = $RGT")
            println("tGT = $tGT")
            
            # transformation (rigid + scale) from the moments (2D only)
            R, t, scale = init_transform(p1, p2, img1, img2)
            println("Scale = $scale")
            println("R = $R")
            println("t = $t")
            @debugprintln("Initialize criterion")
            crit_params=mil.MILParameters(k)

            opt_params=admm.OptimizerADMMS(;monitor=false,maxiter =20,errprim=0.1,errdy=0.1,rho=1.,
                                           ji_opts=Dict(:maxiter =>100),
                                           cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>200) )

            reg_params = regularization.DiffRegularizationParams([.3,.1])
            resampleFactor= [1]
            levelProg=      [1]

            def = deformation.HierarchicalDeformation([deformation.BsplineDeformation(3,isize .* Images.pixelspacing(p1), [7,7]);
                                                   deformation.create_identity(isize .* Images.pixelspacing(p1), deformation.AffineDeformation2D)
                                                   ])
            #set initial deformation
            deformation.set_theta!(def.def[2],[(scale * R)[:]; t])
            # deformation.set_theta!(def.def[2],[(scaleGT * RGT)[:]; tGT])

            #check initial transformation based on moments against the trasform based on annotations
            pant1p = deformation.transform_point(def, map(Float64, pant1))
            post1p = deformation.transform_point(def, map(Float64, post1))
            println("Image $iinx2")
            println("$pant1p - $pant2 = $(pant1p-pant2)")
            println("$post1p - $post2 = $(post1p-post2)")
            println(norm(pant1p-pant2))
            println(norm(post1p-post2))
            if (norm(pant1p-pant2) > norm(post2-pant2) / 2) && (norm(post1p-post2) > norm(post2-pant2) / 2)
                revrot += 1 #number of wrongly initialized transforms phiGT = phi + pi
            end

            #show initial deformation
            # preg = deformation.transform_image_nn(def, p2, size(p1), @options default = 0)
            # mos = deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(preg, k))
            # ImageView.view(mos,name="overlay after initiaal trans.")

            #Register images
            @debugprintln("Registering multiscale")
            d = @debugtimeline(finereg.register_fast_multiscaleHB( p1,p2, kptsimple, def, nhoodKP, crit_params,
                               opt_params, reg_params, resampleFactor, levelProg,
                               @options hmax = hmax minKP = 600 largerotation = false resample = true maxResample = 20 equivalentclasses = [1 2 3; 1 2 3; 5000 1000 1000]), "Multiscale registration:")

            #show registered segmentations
            preg = deformation.transform_image_nn(d, p2, size(p1), @options default = 1)
            Images.save(string(savedir,"stage$stage/mask$(finfo[iinx2, 12]).tif"), Images.reinterpret(FixedPointNumbers.Normed{UInt8,8}, preg-1))
            # mos = deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(p2, k))
            # Images.save("c:/Temp/pbef.png", mos)
            # ImageView.view(mos,name="overlay before")
            # mos = deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(preg, k))
            # Images.save("c:/Temp/paft.png", mos)
            # ImageView.view(mos,name="overlay after")

            #transform image
            imgreg = deformation.transform_image_lin(d, img2, size(img1), @options default = img2[1])
            Images.save(string(savedir,"stage$stage/train$(finfo[iinx2, 12]).tif"), imgreg)

            #normalize images for visualization
            img2v = Images.separate(img2)
            img1v = Images.separate(img1)
            imgregv = Images.separate(imgreg)
            minImage1=minimum(minimum(img1v, 2), 1)
            minImage2=minimum(minimum(img2v, 2), 1)
            for i = 1:3
                img1v[:,:,i] = img1v[:,:,i]-minImage1[i]
                img2v[:,:,i] = img2v[:,:,i]-minImage2[i]
                imgregv[:,:,i] = imgregv[:,:,i]-minImage2[i]
            end
            maxImage1=maximum(maximum(img1v, 2), 1)
            maxImage2=maximum(maximum(img2v, 2), 1)
            for i = 1:3
                img1v[:,:,i] = img1v[:,:,i]/maxImage1[i]
                img2v[:,:,i] = img2v[:,:,i]/maxImage2[i]
                imgregv[:,:,i] = imgregv[:,:,i]/maxImage2[i]
            end
            img2v = convert(Images.Image{Images.RGB}, img2v)'
            img1v = convert(Images.Image{Images.RGB}, img1v)'
            imgregv = convert(Images.Image{Images.RGB}, imgregv)'

            # show image overlay
            # mos = deformation.overlay(img1v, img2v)
            # Images.save("c:/Temp/ibef.png", mos)
            # ImageView.view(mos,name="overlay before")
            # mos = deformation.overlay(img1v, imgregv)
            # Images.save("c:/Temp/iaft.png", mos)
            # ImageView.view(mos,name="overlay after")
            
            # compute number of equivalent pixels before and after
            ms = max(isize, jsize)
            beq = 0
            bneq = 0
            for i=1:ms[1], j=1:ms[2]
                if (i<isize[1]) && (j<isize[2]) && (i<jsize[1]) && (j<jsize[2])
                    if p1[i, j] == p2[i, j]
                        beq += 1
                    else
                        bneq += 1
                    end
                else
                    if (i<isize[1]) && (j<isize[2]) && (p1[i, j] == 1)
                        beq += 1
                    else
                        if (i<jsize[1]) && (j<jsize[2]) && (p2[i, j] == 1)
                            beq += 1
                        else
                            bneq += 1
                        end
                    end
                end
            end
            bcrit[iinx2] = beq / (beq + bneq)
            mbcrit += bcrit[iinx2]
            aeq = 0
            aneq = 0
            for i=1:isize[1], j=1:isize[2]
                if p1[i, j] == preg[i, j]
                    aeq += 1
                else
                    aneq += 1
                end
            end
            acrit[iinx2] = aeq / (aeq + aneq)
            macrit += acrit[iinx2]
            cnt += 1
        end
    end
    println("Number of images with initial rotation +180 deg $revrot")
    println("Mean jacckard index: $(macrit/cnt)")
    println("Jacckard for all the pairs:")
    println(acrit)
end

"""
    batch_flagship_seg(sourcelist, outputdir, inputsets; minimgsize, KPspacing, minKPnum,hmax, k, rho, SPsize, SPcompact, lambda, maxsize, reg, maxit)
Register histological slices from the `sourcelist` or its subset and save results in the format
readable by Jirka's batch evaluation. Use ADMM optimizations, mil criterion on segmentations,
hierarchical deformation and DiffRegularization.
!!! note
    All the paths are based on /datagrid/Medical/microscopy/
# Arguments
* `sourcelist::String`: directory path whwre the lists of images are located. Default "lists_regImgSets/mix-small/"
* `outputdir::String`: directory to save all the results. Subdir is created for the parameter settings. Default "TEMPORARY/julia_experimens_ipmi2017/"
* `inputsets::Array{Int, 1}`: indicies of the cases in directory `sourcelist` (sorted alphabetically) to be registered. If empty, all the cases are used. Default [3, 29, 30]
* `minimgsize::Int`: smaller images will not be resampled in multiscale pyramid. Default 64
* `KPspacing::Int`: keypoint spacing. Default 20
* `minKPnum::Int`: minimum keypoint number in multiscale. Default 250
* `hmax::Int`: hmax in register_fast. Default 40
* `k::Int`: number of classe for image segmentation. Default 3
* `rho::Float64`: ADMM regularization. Default 5.
* `SPsize::Int`: number of pixels inside initial sumerpixel. Default 100
* `SPcompact::Int`: superpixel compactness. Default 10
* `reg::Float64`: b-spline regularization. Default 0.5
* `maxit:Int`: maximal number of admm iterations. Default 10
"""
function batch_flagship_seg(sourcelist = "lists_regImgSets/mix-small/",
                            outputdir = "TEMPORARY/julia_experimens_ipmi2017/",
                            inputsets::Array{Int, 1} = [3, 29, 30];
                            minimgsize::Int = 64, KPspacing::Int = 20, minKPnum::Int = 250,
                            hmax::Int = 40, k::Int = 3, rho::Float64 = 5., SPsize::Int = 100,
                            SPcompact::Int = 10, reg::Float64 = .5, maxit::Int = 10)
    basedir = "m:/" 
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/"
    end
    basedir = string(basedir , "microscopy/")
    
    opt = @options hmax = hmax minKP = minKPnum largerotation = false resample = true maxResample = 10
    crit_params=mil.MILParameters(k)
    opt_state=admm.OptimizerADMMS(;monitor=false,maxiter = maxit,errprim = 0.1,errdy = 0.1,rho = rho,
                                           ji_opts=Dict(:maxiter =>100),
                                           cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>200) )
    reg_params = regularization.DiffRegularizationParams([reg; reg; 0])
    y = zeros(Float64, 2)       #temporary storage of landarks

    outdir = string(basedir, outputdir, "s$(length(inputsets))_mil_admm_diff_mis$(minimgsize)_kpsp$(KPspacing)_mkp$(minKPnum)_hm$(hmax)_k$(k)_rho$(rho)_sps$(SPsize)_cmp$(SPcompact)_r$(reg)_mxit$(maxit)/")
    if !isdir(outdir)
        mkpath(outdir, 0o775)
    end

    datasets = readdir(string(basedir, sourcelist))
    sort!(datasets)
    covercsv = string(outdir, "cover.csv")
    fstr = open(covercsv, "w")
    write(fstr, "Base image, Moving image, Base keypoints, Moving keypoints, Registered image, registered landmarks, Time [s]\n")
    close(fstr)
    if inputsets == []
        inputsets = 1:length(datasets)
    end
    for dsind in inputsets
        outdirsuff = string(datasets[dsind][1:end-4], "/")
        outdirthis = string(outdir, outdirsuff)
        if !isdir(outdirthis)
            mkpath(outdirthis, 0o775)
        end
        images = readdlm(string(basedir, sourcelist, datasets[dsind]), ',', '\n')

        for imgbind = 1:2:length(images)-2
            timebase = time()
            #read base landmarks
            fstr = open(string(basedir, images[imgbind+1]), "r")
            landreg = readdlm(fstr, Float64; skipstart = 2)
            close(fstr)
            #read base image
            img1 = Images.load(string(basedir, images[imgbind]))    #base
            s1l, s1f, count, l1, p1 = segimgtools.slic_size_kmeans(img1, SPsize, SPcompact, k, UInt16, true, 30)
            nlen = minimum(Images.pixelspacing(img1))
            kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(p1, KPspacing, nlen)
            kpts = keypoints.create_sampled_keypoints(p1, kpts, hmax; spacing=float(KPspacing))
            
            # img1kpts=keypoints.create_image_with_keypoints(p1,kpts,k;llen=hmax)
            # ImageView.view(img1kpts,name="with keypoints")
            # ImageView.view(img1,name="img1")
            # ImageView.view(segimgtools.classimage2rgb(p1, k),name="img1")

            timebase = time() - timebase
            for imgmind = imgbind+2:2:length(images)
                timemoving = time()
                img2=Images.load(string(basedir, images[imgmind]))  #moving
                s2l, s2f, count, l2, p2 = segimgtools.slic_size_kmeans(img2, SPsize, SPcompact, k, UInt16, true, 30)
                # ImageView.view(deformation.overlay(img1, img2), name = "overlay before")
                isize=[size(img2)...]
                imgsize=isize .* Images.pixelspacing(img2) # image size in physical units
                def = deformation.HierarchicalDeformation([
                   deformation.BsplineDeformation(3,imgsize, [19,19]), 
                   deformation.BsplineDeformation(3,imgsize, [7,7]), 
                   deformation.create_identity(imgsize, deformation.AffineDeformation2D)
                   ])
                @debugprintln("Registering")
                mindim = min(minimum(size(img1)), minimum(size(img2)))
                rspls = 0
                while minimgsize < mindim
                    mindim = mindim/2
                    rspls = rspls + 1
                end
                resampleFactor = ones(Int, rspls + 1)
                resampleFactor[1] = 0
                levelProg = zeros(Int, rspls + 1)
                levelProg[1] = 1
                levelProg[2] = 1
    
                d = finereg.register_fast_multiscaleHB( p1, p2, kpts, def, nhoodKP, crit_params,
                       opt_state, reg_params, resampleFactor, levelProg, opt)

                timemoving = time() - timemoving
                #Landmark transformation
                fstr = open(string(outdirthis, "b$(imgbind)m$(imgmind).txt"), "w")
                write(fstr, "point\n")
                print_shortest(fstr, size(landreg, 1))
                write(fstr, "\n")
                for poin = 1:size(landreg, 1)
                    deformation.transform_point!(d, vec(landreg[poin, :]), y)
                    print_shortest(fstr, y[1])
                    write(fstr, " ")
                    print_shortest(fstr, y[2])
                    write(fstr, "\n")
                end
                close(fstr)

                # warp and save image
                warped=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options), "warping")
                Images.save(string(outdirthis, "b$(imgbind)m$(imgmind).png"), warped)
                # ImageView.view(warped,name="warped")
                # ImageView.view(deformation.overlay(img1,warped),name="overlay after")
                # pw=@debugtime(deformation.transform_image_lin(d, p2, size(p1), @options), "warping")
                # ImageView.view(deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(pw, k)),name="after")
                fstr = open(covercsv, "a")
                write(fstr, "$(images[imgbind]),$(images[imgmind]),$(images[imgbind+1]),$(images[imgmind+1]),$(outdirsuff)b$(imgbind)m$(imgmind).png,$(outdirsuff)b$(imgbind)m$(imgmind).txt,$(timemoving+timebase)\n")
                close(fstr)
            end
        end
    end
end # batch_flagship_seg

"""
    run_batch_flagship_seg()
Create the parameter vectors and run batch_flagship_seg()
"""
function run_batch_flagship_seg()
    hmax = [10, 20, 30]
    spsize = [49, 64, 81]
    for i in hmax
        for j in spsize
            batch_flagship_seg("lists_regImgSets/mix-small/", "TEMPORARY/julia_experimens_ipmi2017/";
                                hmax = i, SPsize = j)
        end
    end
    exit()
end

"""
    test_register_fast_multiscale_affine_3D_RIRE()
Experiments with training sample from RIRE dtaset. Desired deformation is rigid, but admm doesn't
support such deformations, so affine is used.
"""
function test_register_fast_multiscale_affine_3D_RIRE()
    basedir = "m:/RIRE/"
    if !isdir(basedir)
        basedir = "/datagrid/Medical/RIRE/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/RIRE/"
    end
    img2 = readMetaimage.read_data(string(basedir, "training_001/ct/training_001_ct.mhd")); k = 2
    # img2 = readRIRE.read_data(string(basedir, "training_001/mr_PD/training_001_mr_PD.mhd"));  k = 2

    hmax = 20
    KPspacing = 10
    KPcount = 2000
    quantQ = 20
    #angle = [5.; 0.; 0.];
    angle = [-10.11,7.35,5];#-5.7,-1.5,-2.6
    shift = [-16.8; 33.; 5]; regular = 5
    opts = @options default = img2[1, 1, 1]
    # img1 = deformation.transform_rigid3D(img2, size(img2), angle, shift,opts)
    # img1 = readRIRE.read_data(string(basedir, "training_001/mr_T2"));
    img1 = readMetaimage.read_data(string(basedir, "training_001/mr_T1/training_001_mr_T1.mhd"));
    # img1 = readRIRE.read_data(string(basedir, "training_001/mr_T2_rectified"));
    # img1 = readRIRE.read_data(string(basedir, "training_001/mr_PD"));


    # # Finding keypoints
    nlength = max(minimum(Images.pixelspacing(img1)), minimum(Images.pixelspacing(img2)))
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=KPspacing,number=KPcount,sigma=[1.;1.;1.], nlength = nlength),"find simple keypoints")
    
    p2 = segimgtools.quantize(img2, quantQ)
    p1 = segimgtools.quantize(img1, quantQ)

    kptsampled=keypoints.create_sampled_keypoints(p1, kptsimple, hmax; spacing=float(10))
    nhoodKP=keypoints.keypoint_neighbors(kptsampled)
    
    # # Vizualize input
    # img1kpts=keypoints.create_image_with_keypoints(p1, kpts, k;llen= hmax)
    # show_3_normalized_projections_overlay(img1, img2, name = "overlay before registration")
    # show_3_projections_overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(p2, k), name = "segmentation overlay before registration")
    # show_3_projections(img1kpts, name = "static image segmentation with keypoints")
    
    def=deformation.create_identity([size(p2)...] .* Images.pixelspacing(p2), deformation.AffineDeformation3D)

    print_with_color(:cyan, "Multiscale registration:\n")
    crit_params=mil.MILParameters(quantQ + 1)
    opt_params=admm.OptimizerADMMS(;monitor=false,maxiter =20,errprim=0.1,errdy=0.1,rho=1.,
                                   ji_opts=Dict(:maxiter =>100),
                                   cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>200) )
    reg_params = regularization.NoneRegularizationParams()
    d = @debugtimeline(finereg.register_fast_multiscale( p1, p2, kptsampled, def, nhoodKP, crit_params, opt_params, reg_params,
                       @options hmax = hmax minsize = 128 minKP = 800 resample = true largerotation = false), "Multiscale registration:")


    # # measure the error - select the right line vr to imaging modality
    erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_T1.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_T2.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_pd.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, angle, shift)

    r = deformation.get_theta(d)
    @debugprintln_with_color(:green, "Registration result: $r")
    
    i2t=deformation.transform_image_nn(d,img2,size(img2),@options)
    # show_3_normalized_projections_overlay(img1, i2t, name = "overlay after registration")
    # p2t=deformation.transform_image_nn(d,p2,size(p2),@options)
    # show_3_projections_overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(p2t, k), name = "segmentation overlay before registration")
    return nothing
end

"""
    test_register_fast_multiscale_admm_3D(setname = \"01\", par=1.)
Test registration of one POPI case (images 00 and 50. Return registration error
computed from landmarks available for POPI.
"""
function test_register_fast_multiscale_admm_3D(setname = "01", par=1.)
    basedir = "c:/Temp/"

    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/"
    end
    if !isdir(basedir)
        basedir = "m:/"
    end
    img2 = readMetaimage.read_data(string(basedir, "POPI/", setname, "/00.mhd"));
    # img2m=Images.load(string(basedir, "POPI/segmentations/", setname, "/00.tif"))
  
    quantQ = 50
    KPspacing = 5
    KPcount = 6000
    hmax = 10
    angle = [-15.;3.;1.];
    shift = [-5; 10.; 1];
    opts = @options default = img2[1, 1, 1]

    # def = deformation.RigidDeformation3D([size(img2)...] .* Images.pixelspacing(img2), [angle; shift])
    # img1 = deformation.transform_image_lin(def, img2, size(img2), opts)
    img1 = readMetaimage.read_data(string(basedir, "POPI/", setname, "/50.mhd"))
    img1m=Images.load(string(basedir, "POPI/segmentations/", setname, "/50.tif"))

# pxsp1 = [0.976562; 0.976562; 2.0]
# pxsp2 = [0.976562; 0.976562; 2.0]
# pxsp1 = Images.pixelspacing(img1)
# pxsp2 = Images.pixelspacing(img2)
# img1 = Images.grayim(img1[:,1:400,:]);
# img1["spatialorder"] = ["x", "y", "z"]; img1["pixelspacing"] = pxsp1
# img1m = Images.grayim(img1m[:,1:400,:]);
# img1m["spatialorder"] = ["x", "y", "z"]; img1m["pixelspacing"] = pxsp1
# img2 = Images.grayim(img2[:,1:400,:]);
# img2["spatialorder"] = ["x", "y", "z"]; img2["pixelspacing"] = pxsp2

    # kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=KPspacing,number=KPcount,sigma=[1.;1.;1.], nlength = min(minimum(img1["pixelspacing"]), minimum(img2["pixelspacing"])), mask = img1m),"find simple keypoints")
    # save("c:/Temp/keypointsLungsOnly.jld", "kptsimple", kptsimple)
    kptsimple = load("c:/Temp/keypointsLungsOnly.jld", "kptsimple")
    # kptsimple = load("c:/Temp/keypointsMaskOnly.jld", "kptsimple")
    # kptsimple = load("c:/Temp/keypoints.jld", "kptsimple")

    last = 1
    for i = 1:length(kptsimple)
        tpos = round(Int,(kptsimple[i].pos + 2*kptsimple[i].normal)./Images.pixelspacing(img1))
        for j = 1:length(tpos) tpos[j] = segimgtools.clip(tpos[j], 1, size(img1, j)) end
        if img1m[tpos[1],tpos[2],tpos[3]] != 0
            tpos = round(Int,(kptsimple[i].pos - 2*kptsimple[i].normal)./Images.pixelspacing(img1))
            for j = 1:length(tpos) tpos[j] = segimgtools.clip(tpos[j], 1, size(img1, j)) end
            if img1m[tpos[1],tpos[2],tpos[3]] != 0
                kptsimple[last] = kptsimple[i]
                last += 1
            end
        end
    end
    resize!(kptsimple, last - 1)
    
    crit_params=mil.MILParameters(quantQ + 1)
    # crit_params=ssd.SSDParameters()
    
    reg_params = regularization.DiffRegularizationParams([10, 2, .1])
    if typeof(crit_params) == mil.MILParameters
        reg_params = regularization.DiffRegularizationParams([.15,.22,.1])
        img2 = segimgtools.quantize(img2, quantQ)
        img1 = segimgtools.quantize(img1, quantQ)
    end

    kptsampled=keypoints.create_sampled_keypoints(img1, kptsimple, hmax; spacing=float(10))
    # img1kpts=keypoints.create_image_with_keypoints(img1, kptsimple, quantQ;llen= hmax)
       # ImageView.view(img1kpts,name="with keypoints")
    # ImageView.view(deformation.overlay(img1,img2),name="overlay before")

    @debugprintln("Initialize criterion")
    # opt_params=optimizer.OptimizerMMA(abstol=1e-6,neval=2000,initstep=1.0)
    opt_params=admm.OptimizerADMMS(;monitor=false,maxiter =20,errprim=0.1,errdy=0.1,rho=1.,
                                   ji_opts=Dict(:maxiter =>100),
                                   cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>200) )

    isize=[size(img1)...]
    resampleFactor= [0,1,1]
    levelProg=      [1,1,0]
    # def = deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation3D)
    # def = deformation.BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), [2,2,2])
    # def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [11,11,7])
    def = deformation.HierarchicalDeformation([deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [7,7,7]);
                                           deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [5,5,5]);
                                           # deformation.BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), [2,2,2]);
                                           deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation3D)
                                           ])
    # deformation.set_theta!(def.def[2], [0.9982357146992615,0.003511239663151475,0.006231354029248255,-0.006972625382936265,1.0054627186525595,-0.0005102971419026853,0.0070384551972917575,0.0019727926133225296,1.0369054224709873,0.42556001137114685,-2.9344785176171913,-10.022893548224774])
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    # @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_params,reg_params, @options resample=true hmax=hmax minsize=256 minKP=500 largerotation=false)

    d = @debugtimeline(finereg.register_fast_multiscaleHB( img1,img2, kptsampled, def, nhoods, crit_params,
                       opt_params, reg_params, resampleFactor, levelProg,
                       @options hmax = hmax minsize = 256 minKP = 500 largerotation = false resample = true), "Multiscale registration:")

    return erroreval.measure_error_POPI(d, string(basedir, "POPI/", setname, "//50", ".pts"), string(basedir, "POPI/", setname, "//00", ".pts"), string(basedir, "POPI/", setname, "//50", ".mhd"),  string(basedir, "POPI/", setname, "//00", ".mhd"))

    mos = deformation.overlay(Images.grayim((img1 - minimum(img1)) / (maximum(img1) - minimum(img1))), Images.grayim((img2 - minimum(img2)) / (maximum(img2) - minimum(img2))))
    # mos = deformation.overlay(warped, img1)
    ImageView.view(mos,name="overlay before")
    mos["timedim"] = 2
    ImageView.view(mos,name="overlay before")    
    mos["timedim"] = 1
    ImageView.view(mos,name="overlay before")
    
    warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    mos = deformation.overlay(Images.grayim((img1 - minimum(img1)) / (maximum(img1) - minimum(img1))), Images.grayim((warped - minimum(img2)) / (maximum(img2) - minimum(img2))))
    ImageView.view(Images.grayim((warped - minimum(warped)) / (maximum(warped) - minimum(warped))))
    # mos = deformation.overlay(warped, img1)
    ImageView.view(mos,name="overlay after")
    mos["timedim"] = 2
    ImageView.view(mos,name="overlay after")    
    mos["timedim"] = 1
    ImageView.view(mos,name="overlay after")
end

"""
    test_all_POPI()
Run [`test_register_fast_multiscale_admm_3D`](@ref) for all the POPI data with
differnt parammeter, collect the registration accuracy and save it to .csv file.
"""
function test_all_POPI()
    logdir = "c:/Temp/"
    lf = "popiData.csv"
    logfile = string(logdir, lf)
    if !isdir(logdir)
        logfile = lf
    end
    sets = ["01","02","03","04","05","06"];
    
    fstr = open(logfile, "w")
    write(fstr, "units,")
    write(fstr, "mean,")
    write(fstr, "median")
    for i = 1:length(sets)
        write(fstr, ",")
        write(fstr, sets[i])
    end
    write(fstr, "\n")
    close(fstr)
    
    mm = Array(Float64,length(sets))
    px = Array(Float64,length(sets))
    times = Array(Float64,length(sets))
    par = [2 5 10 20 30 40 50 60 80 100]
    for j = 1:length(par)
        for i= 1:length(sets)
            t0 = time_ns()
            mm[i], px[i] = test_register_fast_multiscale_admm_3D(sets[i], par[j])
            times[i] = (time_ns() - t0)/ 1e9
        end
        fstr = open(logfile, "a")
        write(fstr, "par=\n")
        print_shortest(fstr, par[j]); write(fstr, "\n"); write(fstr, "mm,")
        print_shortest(fstr, mean(mm)); write(fstr, ",")
        print_shortest(fstr, median(mm))
        for i = 1:length(sets)
            write(fstr, ",")
            print_shortest(fstr, mm[i])
        end
        write(fstr, "\n"); write(fstr, "px,")
        print_shortest(fstr, mean(px)); write(fstr, ",")
        print_shortest(fstr, median(px))
        for i = 1:length(sets)
            write(fstr, ",")
            print_shortest(fstr, px[i])
        end
        write(fstr, "\n"); write(fstr, "s,")
        print_shortest(fstr, mean(times)); write(fstr, ",")
        print_shortest(fstr, median(times))
        for i = 1:length(sets)
            write(fstr, ",")
            print_shortest(fstr, times[i])
        end
        write(fstr, "\n"); 
        close(fstr)
    end
    return nothing
end

"""
    test_register_fast_multiscale_admm_3D_nirep(fname1 = \"na01\/na01\", fname2 = \"na03\/na03\")
Register two gray  NIREP images. Return registration accuracy based on available segmentations
and ssd, plus registration time.
"""
function test_register_fast_multiscale_admm_3D_nirep(fname1 = "na01/na01", fname2 = "na03/na03")
    basedir = "c:/Temp/NIREP/"

    if !isdir(basedir)
        basedir = "/datagrid/Medical/NIREP/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/NIREP/"
    end
    if !isdir(basedir)
        basedir = "m:/"
    end
    img2 = readMetaimage.read_data(string(basedir, fname1, ".hdr"));
    img1 = readMetaimage.read_data(string(basedir, fname2, ".hdr")); quantQ = 50
    # img2 = reinterpret(UInt8, readMetaimage.read_data(string(basedir, "na01/na01_seg.hdr"))) + 1
    # img1 = reinterpret(UInt8, readMetaimage.read_data(string(basedir, "na02/na02_seg.hdr"))) + 1; quantQ = convert(Int, maximum(img2))-1#50

    KPspacing = 6#8
    KPcount = 1000#1000
    hmax = 50
    angle = [-15.;3.;1.];
    shift = [-5; 10.; 1];
    opts = @options default = img2[1, 1, 1]

    # kptsimple, nhoodKP = @time keypoints.find_keypoints_and_neighbors(img1, KPspacing, 1)
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=KPspacing,number=KPcount,sigma=[1.;1.;1.]),"find simple keypoints")

    # save("c:/Temp/keypointsLungsOnly.jld", "kptsimple", kptsimple)
    # kptsimple = load("c:/Temp/keypointsLungsOnly.jld", "kptsimple")
    # kptsimple = load("c:/Temp/keypointsMaskOnly.jld", "kptsimple")
    # kptsimple = load("c:/Temp/keypoints.jld", "kptsimple")

    # img2 = segimgtools.quantize(img2, quantQ)
    # img1 = segimgtools.quantize(img1, quantQ)

    kptsampled=keypoints.create_sampled_keypoints(img1, kptsimple, hmax; spacing=float(KPspacing))
    # img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    #    ImageView.view(img1kpts,name="with keypoints")
    # ImageView.view(deformation.overlay(img1,img2),name="overlay before")

    @debugprintln("Initialize criterion")
    # crit_params=mil.MILParameters(quantQ + 1)

    crit_params=ssd.SSDParameters()
    # opt_params=optimizer.OptimizerMMA(abstol=1e-6,neval=2000,initstep=1.0)
    opt_params=admm.OptimizerADMMS(;monitor=false,maxiter =10,errprim=0.1,errdy=0.1,rho=1.,
                                   ji_opts=Dict(:maxiter =>100),
                                   cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>200) )
    # reg_params = regularization.NoneRegularizationParams()
    # reg_params = regularization.SumRegularizationParams([1])
    reg_params = regularization.DiffRegularizationParams([.15,.1,.025,.55])
    isize=[size(img1)...]
    resampleFactor= [0,0,1]
    levelProg=      [1,1,1]
    # def = deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation3D)
    # def = deformation.BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), [2,2,2])
    # def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [11,11,7])
    def = deformation.HierarchicalDeformation([deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [11,11,11]);
                                           deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [7,7,7]);
                                           deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [5,5,5]);
                                           deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation3D)
                                           ])
    # deformation.set_theta!(def.def[2], [0.9982357146992615,0.003511239663151475,0.006231354029248255,-0.006972625382936265,1.0054627186525595,-0.0005102971419026853,0.0070384551972917575,0.0019727926133225296,1.0369054224709873,0.42556001137114685,-2.9344785176171913,-10.022893548224774])
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    # @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_params,reg_params, @options resample=true hmax=hmax minsize=256 minKP=500 largerotation=false maxResample = 20)
    regtime = time_ns() / 1e9
    d = @debugtimeline(finereg.register_fast_multiscaleHB( img1,img2, kptsampled, def, nhoods, crit_params,
                       opt_params, reg_params, resampleFactor, levelProg,
                       @options hmax = hmax minsize = 256 minKP = 500 largerotation = false resample = true maxResample = 20), "Multiscale registration:")
    regtime = time_ns() / 1e9 - regtime
    p2 = reinterpret(UInt8, readMetaimage.read_data(string(basedir, fname1, "_seg.hdr")))
    p1 = reinterpret(UInt8, readMetaimage.read_data(string(basedir, fname2, "_seg.hdr")))
    k = max(convert(Int, maximum(p2)), convert(Int, maximum(p1)))
    preg = deformation.transform_image_nn(d, p2, size(p2), @options default = 0)
    imgreg = deformation.transform_image_nn(d, img2, size(img2), @options default = img2[1])
    ssdval = ssd.ssd_val(img1, imgreg)
    
    roaft, roaftbrain= erroreval.measure_error_NIREP(p1, p2, preg)

    # img1kpts=keypoints.create_image_with_keypoints(p1, kptsimple, k;llen= hmax)
    # ImageView.view(img1kpts,name="with keypoints")
    
    p1p2=sum(convert(Array{Int}, (p1.==p2)))
    p1pr=sum(convert(Array{Int}, (p1.==preg)))
    println("Number of equivalent pixels before registration $p1p2")
    println("Number of equivalent pixels after registration $p1pr")
    relerr = p1pr/p1p2
    println("after/before $(relerr)")
    return relerr, p1pr/length(p1), roaftbrain, roaft, ssdval, regtime
    
    p1p2nz=sum(convert(Array{Int}, (p1.!=0) & (p2.!=0) & (p1.==p2)))
    p1prnz=sum(convert(Array{Int}, (p1.!=0) & (preg.!=0) & (p1.==preg)))
    println("Number of equivalent non-zero pixels before registration $p1p2nz")
    println("Number of equivalent non-zero pixels after registration $p1prnz")
    println("after/before $(p1prnz/p1p2nz)")
    p1p2=sum(convert(Array{Int},(p1.!=0) & (p2.!=0) & (p1.!=p2)))
    p1pr=sum(convert(Array{Int},(p1.!=0) & (preg.!=0) & (p1.!=preg)))
    println("Number of non-equivalent non-zero pixels before registration $p1p2")
    println("Number of non-equivalent non-zero pixels after registration $p1pr")
    println("after/before $(p1pr/p1p2)")

    mos = deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(p2, k))
    # mos = deformation.overlay(warped, img1)
    ImageView.view(mos,name="overlay before")
    mos["timedim"] = 2
    ImageView.view(mos,name="overlay before")    
    mos["timedim"] = 1
    ImageView.view(mos,name="overlay before")
    mos = deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(preg, k))
    # mos = deformation.overlay(warped, img1)
    ImageView.view(mos,name="overlay after")
    mos["timedim"] = 2
    ImageView.view(mos,name="overlay after")    
    mos["timedim"] = 1
    ImageView.view(mos,name="overlay after")

    mos = deformation.overlay(Images.grayim((img1 - minimum(img1)) / (maximum(img1) - minimum(img1))), Images.grayim((img2 - minimum(img2)) / (maximum(img2) - minimum(img2))))
    # mos = deformation.overlay(warped, img1)
    ImageView.view(mos,name="overlay before")
    mos["timedim"] = 2
    ImageView.view(mos,name="overlay before")    
    mos["timedim"] = 1
    ImageView.view(mos,name="overlay before")
    
    warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    mos = deformation.overlay(Images.grayim((img1 - minimum(img1)) / (maximum(img1) - minimum(img1))), Images.grayim((warped - minimum(img2)) / (maximum(img2) - minimum(img2))))
    ImageView.view(Images.grayim((warped - minimum(warped)) / (maximum(warped) - minimum(warped))))
    # mos = deformation.overlay(warped, img1)
    ImageView.view(mos,name="overlay after")
    mos["timedim"] = 2
    ImageView.view(mos,name="overlay after")    
    mos["timedim"] = 1
    ImageView.view(mos,name="overlay after")
end

"""
    test_nirep_all()
Run [`test_register_fast_multiscale_admm_3D_nirep`](@ref) for all the NIREP data,
save all the measurements into .csv file
"""
function test_nirep_all()
    romean = 0
    rototmean = 0.
    errmean = 0.
    ssdmean = 0.
    cnt = 0
    regtimemean = 0.
    roaftBrainmean = 0.
    logdir = "c:/Temp/"
    lf = "nirepData.csv"
    logfile = string(logdir, lf)
    if !isdir(logdir)
        logfile = lf
    end
        
    fstr = open(logfile, "w")
    write(fstr, "img1, img2, ssd, err, reg. time, R, R_brain, RO 0-background, 1-L. occipital lobe, 2-R occipital lobe, 3-L. cingulate gyrus, 4-R. cingulate gyrus, 5-L. insula gyrus, 6-R. insula gyrus, 7-L. Temporal pole, 8-R. Temporal pole, 9-L. superior temporal gyrus, 10-R. superior temporal gyrus, 11-L infero temporal region, 12-R. infero temporal region, 13-L. parrahippocampal gyrus, 14-R. parrahippocampal gyrus, 15-L. frontal pole, 16-R. frontal pole, 17-L. superior frontal gyrus, 18-R. superior frontal gyrus, 19-L. middle frontal gyrus, 20-R. middle frontal gyrus, 21-L. inferior gyrus, 22-R. inferior gyrus, 23-L. orbital frontal gyrus, 24-R. orbital frontal gyrus, 25-L. precentral gyrus, 26-R. precentral gyrus, 27-L. superior parietal lobule, 28-R. superior parietal lobule, 29-L. inferior parietal lobule, 30-R. inferior parietal lobule, 31-L. postcentral gyrus, 32-R. postcentral gyrus, 33-unassigned gray matter\n")
    close(fstr)
    for i = 1:16
        for j = (i + 1):16
            if i < 10
                if j < 10
                    relerr, rotot, roaftBrain, roaft, ssdval, regtime = test_register_fast_multiscale_admm_3D_nirep("na0$i/na0$i", "na0$j/na0$j")
                else
                    relerr, rotot, roaftBrain, roaft, ssdval, regtime = test_register_fast_multiscale_admm_3D_nirep("na0$i/na0$i", "na$j/na$j")
                end
            else
                if j < 10
                    relerr, rotot, roaftBrain, roaft, ssdval, regtime = test_register_fast_multiscale_admm_3D_nirep("na$i/na$i", "na0$j/na0$j")
                else
                    relerr, rotot, roaftBrain, roaft, ssdval, regtime = test_register_fast_multiscale_admm_3D_nirep("na$i/na$i","na$j/na$j")
                end
            end
            if i == 1 && j == 1
                romean = copy(roaft)
            else
                romean += roaft
            end
            regtimemean += regtime
            errmean += relerr
            ssdmean += ssdval
            rototmean += rotot
            roaftBrainmean += roaftBrain
            fstr = open(logfile, "a")
            write(fstr,"$i,$j,")
            print_shortest(fstr, ssdval); write(fstr,",")
            print_shortest(fstr, relerr); write(fstr,",")
            print_shortest(fstr, regtime); write(fstr,",")
            print_shortest(fstr, rotot); write(fstr,",")
            print_shortest(fstr, roaftBrain); write(fstr,",")
            for k = 1:length(romean)
                print_shortest(fstr, roaft[k]); write(fstr, ",")
            end
            write(fstr, "\n");
            close(fstr)
            cnt +=1
        end
    end
    fstr = open(logfile, "a")
    write(fstr,"mean, ,")
    print_shortest(fstr, ssdmean / cnt); write(fstr,",")
    print_shortest(fstr, errmean / cnt); write(fstr,",")
    print_shortest(fstr, regtimemean / cnt); write(fstr,",")
    print_shortest(fstr, rototmean / cnt); write(fstr,",")
    print_shortest(fstr, roaftBrainmean / cnt); write(fstr,",")
    for k = 1:length(romean)
        print_shortest(fstr, romean[k] / cnt); write(fstr, ",")
    end
    write(fstr, "\n");
    close(fstr)
    return errmean, romean
end


end # module
