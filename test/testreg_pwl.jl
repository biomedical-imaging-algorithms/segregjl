""" Experiments with registration algorithms using piecewise linear approximation of the criterion

    Jan Kybic, 2016
"""

module testreg_pwl

using Images
using ImageView
import optimizer
import finereg
import deformation
import keypoints
#importall criterion
import ssd
import mil
import segimgtools
import regularization
import testreg_mi: quantize

using Options

include("debugassert.jl")
switchasserts(true)
include("debugprint.jl")
switchprinting(true)

# test piecewise linear approximation
function test_register_ssd_pwl()
    img2=Images.load("imgs/simple.png")
    #angle=15. ; shift=[11.;-5]
    angle=5. ; shift=[5.;-2.]
    #angle=0. ; shift=[5.;0]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    #ImageView.view(img1,name="image 1") 
    #ImageView.view(img2,name="image 2") 
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=50,sigma=[1.;1.]),"find simple keypoints")
    hmax=20
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts,name="with keypoints")
    ImageView.view(Images.separate(deformation.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    @time crit_state=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-8,neval=10000,initstep=0.1)
    #opt_state=optimizer.OptimizerCOBYLA(abstol=1e-8,neval=10000,initstep=1.0)
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    @debugprintln("Registering")
    #Profile.clear()
    reg_params = regularization.NoneRegularizationParams()
    #d= finereg.register_fast_pwl(img1,img2,kptsampled,def,crit_state,opt_state,reg_params;hmax=hmax)
    d= finereg.register_fast(img1,img2,kptsampled,def,crit_state,opt_state,reg_params,
                             @options hmax=hmax resample=true pwl=true)
    #Profile.print(format=:flat,sortedby=:count)
    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(deformation.overlay(img1,warped)),name="overlay after") 
end    

function test_register_multiscale_ssd_pwl()
    img2=Images.load("imgs/simple.png")
    angle=15. ; shift=[11.;-5.]
    #angle=0. ; shift=[5.;0]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    #ImageView.view(img1,name="image 1") 
    #ImageView.view(img2,name="image 2") 
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=20,sigma=[1.;1.]),"find simple keypoints")
    hmax=10
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts,name="with keypoints")
    ImageView.view(Images.separate(deformation.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    crit_params=ssd.SSDParameters()
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=1.0)
    #opt_state=optimizer.OptimizerCOBYLA(abstol=1e-8,neval=10000,initstep=1.0)
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    reg_params = regularization.NoneRegularizationParams()
    #@time d=finereg.register_fast_multiscale_pwl(img1,img2,kptsampled,def,nhoods,crit_params,
    #                                             opt_state,reg_params;
    #                                             hmax=hmax,minsize=64, minKP=50)
    @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,
       opt_state,reg_params,
       @options hmax=hmax minsize=64 minKP=50 pwl=true largerotation=false)
    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(deformation.overlay(img1,warped)),name="overlay after") 
end    

function test_register_multiscale_ssd_pwl_lena()
    img2=Images.load("imgs/lena.png")
    angle=15. ; shift=[11.;-5]
    #angle=5. ; shift=[5.;-2.]
    #angle=0. ; shift=[5.;0]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    #ImageView.view(img1,name="image 1") 
    #ImageView.view(img2,name="image 2") 
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=100,sigma=[1.;1.]),"find simple keypoints")
    hmax=10
    kptsampled=keypoints.create_sampled_keypoints(img1, kptsimple, hmax; spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts,name="with keypoints")
    ImageView.view(Images.separate(deformation.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    crit_params=ssd.SSDParameters()
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=10000,initstep=0.1)
    #opt_state=optimizer.OptimizerCOBYLA(abstol=1e-8,neval=10000,initstep=1.0)
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    #@time d=finereg.register_fast_multiscale_pwl(img1,img2,kptsampled,def,nhoods,crit_params,opt_state;
    #                                             hmax=hmax,minsize=64, minKP=50)
    reg_params = regularization.NoneRegularizationParams()
    @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,
       opt_state,reg_params,
       @options hmax=hmax minsize=64 minKP=50 pwl=true largerotation=false)
    r=deformation.get_theta(d)
    println("Register_fast_multiscal_pwl finished. Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(deformation.overlay(img1,warped)),name="overlay after") 
end    


function test_register_bspln_pwl()
        img2=convert(Images.Image{Images.Gray},Images.load("imgs/case03-5-he-small.png"))
    img1=convert(Images.Image{Images.Gray},Images.load("imgs/case03-3-psap-small.png"))
    #create keypoints at high gradients
    spacing=20
    hmax=20
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=500,sigma=[1.;1.]),"find simple keypoints")
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
    ImageView.view(deformation.overlay(img1/maximum(img1),img2/maximum(img2)),name="overlay before") 
    #ImageView.view(img2,name="img2")
    crit_params=ssd.SSDParameters()
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=1.0)
    bc = 4
    def = deformation.BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), round(Int,[bc,bc]))
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    #@time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,@options resample=true hmax=hmax minsize=64 minKP=50 largerotation=false)
#    @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,@options resample=true hmax=hmax minsize=64 minKP=50 largerotation=false)
    reg_params = regularization.NoneRegularizationParams()
    @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,
       opt_state,reg_params,
       @options hmax=hmax minsize=64 minKP=50 pwl=false largerotation=false)
#    @time d=finereg.register_fast_multiscale_pwl(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,
#                                                 reg_params;hmax=hmax,minsize=64, minKP=50)
    r=deformation.get_theta(d)
    println("Registration finished.")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(deformation.overlay(img1/maximum(img1),warped/maximum(warped)),name="overlay after") 
end


function test_register_hbspln()
        img2=convert(Images.Image{Images.Gray},Images.load("imgs/case03-5-he-small.png"))
    img1=convert(Images.Image{Images.Gray},Images.load("imgs/case03-3-psap-small.png"))
    #create keypoints at high gradients
    spacing=20
    hmax=20
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=200,sigma=[1.;1.]),"find simple keypoints")
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
    ImageView.view(deformation.overlay(img1/maximum(img1),img2/maximum(img2)),name="overlay before") 
    #ImageView.view(img2,name="img2")
    crit_params=ssd.SSDParameters()
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=1.0)
    # hierarchical B-splines
    def2 = deformation.BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), round(Int,[4,4]))
    def1 = deformation.BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), round(Int,[2,2]))
    #def = deformation.HBsplineDeformation([def2;def1])
    def = deformation.HierarchicalDeformation([def2;def1])
    resampleFactor= [0,1,1,1,1]
    levelProg=      [1,0,0,0,0]

    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    #@time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,@options resample=true hmax=hmax minsize=64 minKP=50 largerotation=false)
#    @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,@options resample=true hmax=hmax minsize=64 minKP=50 largerotation=false)
    reg_params = regularization.SumRegularizationParams([1e-6, 1e-9])
    d = @debugtimeline(finereg.register_fast_multiscaleHB( img1, img2, kptsampled, def, nhoods, crit_params,
                       opt_state, reg_params, resampleFactor, levelProg,
                       @options hmax = hmax minKP = 500 largerotation = false resample = true pwl=true), "Multiscale registration:")
#    @time d=finereg.register_fast_multiscale_pwl(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,
#                                                 reg_params;hmax=hmax,minsize=64, minKP=50)
    r=deformation.get_theta(d)
    println("Registration finished.")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(deformation.overlay(img1/maximum(img1),warped/maximum(warped)),name="overlay after") 
end

function test_register_ssd_hb()
    # img2=Images.load("imgs/simpleBW.png")
    # img2=convert(Images.Image{Images.Gray},img2)   # on Unix the previous command gets us an RGB image 
    # img2=Images.load("imgs/case03-5-he-smallBW.png")
    img2=Images.load("imgs/case02-2-ki67.jpg")
    #img2=Images.load("m:/microscopy/Flagship/images-MultiGeneSignatureMaps_convert/scale-5pc/Case002/case02-2-ki67.jpg")
    img2 = convert(Images.Image{Images.Gray}, img2)
    # img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    # img1 = deformation.transform_image_lin(def1,img2,size(img2),@options default = img2[1])
    # img1=Images.load("imgs/case03-3-psap-smallBW.png")
    #img1=Images.load("m:/microscopy/Flagship/images-MultiGeneSignatureMaps_convert/scale-5pc/Case002/case02-1-he.jpg"); 
    img1=Images.load("imgs/case02-1-he.jpg")
    img1 = convert(Images.Image{Images.Gray}, img1)
    #quantQ = 5
    spacing=30#15
    kpnum = 1000
    hmax=30
    
    #create keypoints at high gradients
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kpnum,sigma=[1.;1.]),"find simple keypoints")
    
    #img1=quantize(img1, quantQ)
    #img2=quantize(img2, quantQ)
    #sample quantized image
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    # kptsampled, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(img1, spacing, 1),"find_keypoints_and_neighbors:")
    # kptsimple=finereg.transform_keypoints(kptsampled, Nullable())
    
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")

    ImageView.view(deformation.overlay(img1/maximum(img1),img2/maximum(img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    crit_params=ssd.SSDParameters()
    #crit_params=mil.MILParameters(quantQ)
    opt_state=optimizer.OptimizerMMA(abstol=1e-3,neval=5000,initstep=20.0)
    # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-3,neval=50000,initstep=20.0)
    # reg_params = regularization.NoneRegularizationParams()
    reg_params = regularization.SumRegularizationParams([1e-6, 1e-9])
    # reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    # def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    # def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [4,4])
    aa = [deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [7,7]);
          deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [4,4])]
    def = deformation.HierarchicalDeformation(aa)
#    def = deformation.HBsplineDeformation(aa)
    # def=deformation.create_identity([size(img2)...] .* Images.pixelspacing(img2), deformation.RigidDeformation2D)
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")

    resampleFactor= [0,1,1,1,1]
    levelProg=      [1,0,0,0,0]
    #Profile.clear()
    d = #@profile
     @debugtimeline(finereg.register_fast_multiscaleHB( img1, img2, kptsampled, def, nhoods, crit_params,
                       opt_state, reg_params, resampleFactor, levelProg,
                       @options hmax = hmax minKP = 1000 largerotation = false resample = true pwl=false), "Multiscale registration:")
    #Profile.print(format=:flat,sortedby=:count,combine=true)
    #Profile.print(format=:flat,sortedby=:count)
    r=deformation.get_theta(d)
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)# default = img2[1])
    ImageView.view(warped/maximum(warped),name="warped")
    ImageView.view(deformation.overlay(img1/maximum(img1),warped/maximum(warped)),name="overlay after")
    # println("-------------------------")
    # println(def1.splines[1].theta-d.splines[1].theta)
    # println("-------------------------")
    # println(def1.splines[2].theta-d.splines[2].theta)
end


end # module
