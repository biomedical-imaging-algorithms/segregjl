""" Experiments with SSD registration
    Jan Kybic, 2015
"""

module testreg_ssd

using Images
using ImageView
using Options
using keypoints
import optimizer
import finereg
import deformation
import ssd
import mil
import segimgtools
import SLICsuperpixels
import ImageView.view
import Winston
import ColorTypes
import readMetaimage
import erroreval
import regularization

include("debugassert.jl")
switchasserts(true)
include("debugprint.jl")
switchprinting(true)


function test_register_mil()
    img2=Images.load("imgs/simple.png")
    angle=15. ; shift=[11.;-5]
    #angle=5. ; shift=[5.;-2.]
    #angle=0. ; shift=[5.;0]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    #ImageView.view(img1,name="image 1") 
    #ImageView.view(img2,name="image 2") 
    #spacing=10
    k=3 # number of classes
    hmax=20 # maxdist=40
    s2l, count = SLICsuperpixels.slic(img2, 1000, 30)
    s2f = SLICsuperpixels.getMeans(s2l, img2, count)
    l2,p2=segimgtools.kmeans_segmentation(s2l,s2f,k)
    s1l, count = SLICsuperpixels.slic(img1, 1000, 30)
    s1f = SLICsuperpixels.getMeans(s1l, img1, count)
    l1,p1=segimgtools.kmeans_segmentation(s1l,s1f,k)
    ImageView.view(segimgtools.classimage2rgb(p1,k),name="segmented image 1") 
    ImageView.view(segimgtools.classimage2rgb(p2,k),name="segmented image 2") 
    kpts=keypoints.find_keypoints(s1l,l1)
    img1kpts=keypoints.create_image_with_keypoints(img1,kpts)
    ImageView.view(img1kpts,name="with keypoints")
    ImageView.view(Images.separate(segimgtools.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    @time crit_state=mil.criterion_init(p1,p2,mil.MILParameters(k))
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-8,neval=1000,initstep=0.1)
    reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    @debugprintln("Registering")
    @time d=finereg.register_fast(p1,p2,kpts,def,crit_state,opt_state,reg_params,@options resample=true hmax=hmax)

    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(segimgtools.overlay(img1,warped)),name="overlay after") 
end    



function test_register_ssd()
    img2=Images.load("imgs/simple.png")
    angle=15. ; shift=[11.;-5]
    #angle=5. ; shift=[5.;-2.]
    #angle=0. ; shift=[5.;0]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    #ImageView.view(img1,name="image 1") 
    #ImageView.view(img2,name="image 2") 
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=50,sigma=[1.;1.]),"find simple keypoints")
    hmax=90
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts,name="with keypoints")
    ImageView.view(Images.separate(segimgtools.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    @time crit_state=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-8,neval=10000,initstep=0.1)
    #opt_state=optimizer.OptimizerCOBYLA(abstol=1e-8,neval=10000,initstep=1.0)
    reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    @debugprintln("Registering")
    @time d=finereg.register_fast(img1,img2,kptsampled,def,crit_state,opt_state,reg_params,@options resample=true hmax=hmax)

    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(segimgtools.overlay(img1,warped)),name="overlay after") 
end    





function test_register_multiscale_ssd()
    img2=Images.load("imgs/simple.png")
    angle=15. ; shift=[11.;-5.]
    #angle=0. ; shift=[5.;0]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    #ImageView.view(img1,name="image 1") 
    #ImageView.view(img2,name="image 2") 
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=20,sigma=[1.;1.]),"find simple keypoints")
    hmax=10
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts,name="with keypoints")
    ImageView.view(Images.separate(segimgtools.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    crit_params=ssd.SSDParameters()
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=1.0)
    #opt_state=optimizer.OptimizerCOBYLA(abstol=1e-8,neval=10000,initstep=1.0)
    reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,reg_params,@options resample=true hmax=hmax minsize=64 minKP=50 largerotation=false)

    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(segimgtools.overlay(img1,warped)),name="overlay after") 
end    



function test_register_multiscale_ssd_lena()
    img2=Images.load("imgs/lena.png")
    angle=15. ; shift=[11.;-5]
    #angle=5. ; shift=[5.;-2.]
    #angle=0. ; shift=[5.;0]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    #ImageView.view(img1,name="image 1") 
    #ImageView.view(img2,name="image 2") 
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=100,sigma=[1.;1.]),"find simple keypoints")
    hmax=10
    kptsampled=keypoints.create_sampled_keypoints(img1, kptsimple, hmax; spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts,name="with keypoints")
    ImageView.view(Images.separate(segimgtools.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    crit_params=ssd.SSDParameters()
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=10000,initstep=0.1)
    #opt_state=optimizer.OptimizerCOBYLA(abstol=1e-8,neval=10000,initstep=1.0)
    reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    @time d=finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,reg_params,@options resample=true hmax=hmax minsize=64 minKP=50 largerotation=false)

    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(segimgtools.overlay(img1,warped)),name="overlay after") 
end



function test_register_multiscale_ssd_3D()
    basedir = "m:/RIRE/"
    if !isdir(basedir)
        basedir = "/datagrid/Medical/RIRE/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/RIRE/"
    end
    if !isdir(basedir)
        basedir = "c:/Temp/RIRE/"
    end
    img2 = readMetaimage.read_data(string(basedir, "training_001/ct/training_001_ct.mhd"));
    # img2 = readMetaimage.read_data(string(basedir, "training_001/mr_PD/training_001_mr_PD.mhd"));
    # img2 = Images.load("c:/Temp/ph3d.tif"); img2["spatialorder"] = ["x" "y" "z"]; img2["pixelspacing"] = [1; 1; 1]

    KPspacing = 11
    hmax = 20
    angle = [-15.;3.;1.];
    shift = [-5; 0.; 0];
    opts = @options default = img2[1, 1, 1]
    def = deformation.RigidDeformation3D([size(img2)...] .* Images.pixelspacing(img2), [angle; shift])
    # # img1 = Images.grayim(Array(eltype(img2),(div([size(img2)...],2)...)))
    # # img1["spatialorder"] = ["x", "y", "z"]
    # # img1["pixelspacing"] = Images.pixelspacing(img2).*2
    # # deformation.transform_image_lin!(def, img1, img2, opts)
    # img1 = deformation.transform_image_lin(def, img2, size(img2), opts)
    # img1 = deformation.transform_rigid3D(img2, size(img2), angle, shift, opts)
    # img1 = readMetaimage.read_data(string(basedir, "training_001/mr_T2"));
    img1 = readMetaimage.read_data(string(basedir, "training_001/mr_T1/training_001_mr_T1.mhd"));
    # img1 = readMetaimage.read_data(string(basedir, "training_001/mr_T2_rectified"));
    # img1 = readMetaimage.read_data(string(basedir, "training_001/mr_PD"));

    kptsimple = @debugtime(keypoints.find_keypoints_from_gradients(img1; spacing = KPspacing, number = 500, sigma = [1.;1.;1.]), "find simple keypoints")

    kptsampled=keypoints.create_sampled_keypoints(img1, kptsimple, hmax; spacing = float(KPspacing))
    
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")

    ImageView.view(segimgtools.overlay((img1 - minimum(img1)) / (maximum(img1) - minimum(img1)), (img2 - minimum(img2)) / (maximum(img2) - minimum(img2))), name="overlay before")
    # ImageView.view(segimgtools.overlay(img1, img2), name="overlay before")

    @debugprintln("Initialize criterion")
    crit_params = ssd.SSDParameters()
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=10000,initstep=0.1)
    #opt_state=optimizer.OptimizerCOBYLA(abstol=1e-8,neval=10000,initstep=1.0)
    reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation3D)
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    @time d=finereg.register_fast_multiscale(img1, img2, kptsampled, def, nhoods, crit_params, opt_state, reg_params, @options resample=true hmax=hmax minsize=128 minKP=150 largerotation=false)

    ############
    # # measure the error
    # # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_T1.csv"))
    # # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_T2.csv"))
    # # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_pd.csv"))
    erroreval.measure_error_RIRE(img1, img2, d, [], angle, shift)
    
    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    warped = similar(img1)
    @time deformation.transform_image_lin!(d, warped, img2, @options)
    mos = segimgtools.overlay(Images.grayim((warped - minimum(warped)) / (maximum(warped) - minimum(warped))), Images.grayim((img1 - minimum(img1)) / (maximum(img1) - minimum(img1))))
    # mos = segimgtools.overlay(warped, img1)
    ImageView.view(mos)
    mos["timedim"] = 2
    ImageView.view(mos)    
    mos["timedim"] = 1
    ImageView.view(mos)
end

function test_register_lena()
    img2=Images.load("imgs/lena.png")
    angle=15. ; shift=[11.;-5]
    #angle=15. ; shift=[5.;-2.]
    #angle=0. ; shift=[3.;0]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    ImageView.view(img1,name="image 1") 
    ImageView.view(img2,name="image 2") 
    spacing=20
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=30,sigma=[1.;1.]),"find simple keypoints")
    hmax=30
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts,name="image 1 + keypoints") 

    ImageView.view(img1kpts,name="with keypoints")
    ImageView.view(Images.separate(segimgtools.overlay(img1,img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    @time crit_state=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-8,neval=10000,initstep=1.)
    #opt_state=optimizer.OptimizerCOBYLA(abstol=1e-8,neval=10000,initstep=1.0)
    reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    @debugprintln("Registering")
    @time d=finereg.register_fast(img1,img2,kptsampled,def,crit_state,opt_state,reg_params,@options resample=true hmax=hmax)

    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(Images.separate(segimgtools.overlay(img1,warped)),name="overlay after") 
end

# test if the SSD criterio evaluates shifts right
function test_ssd_shifts()
    # test how the criterion varies with shift
    #img2=Images.imread("imgs/simple.png")
    img2=load("imgs/simple.png")::Images.Image
    #img2=load("imgs/lena.png")::Images.Image
    #img2=convert(Images.Image{Images.Gray},img2) # Images.sc(img2)?
    angle=0. ; shift=[8.;0.]
    defaultval=zero(typeof(img2[1]))
    truedef=deformation.RigidDeformation2D([size(img2)...],[angle;shift])
    img1=deformation.transform_image_lin(truedef,img2,size(img2),@options default = img2[1])
    #img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=100,sigma=[1.;1.]),"find simple keypoints")
    hmax=10
    kpts=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts,name="with keypoints")
    critstate=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    refdef=deformation.RigidDeformation2D([size(img1)...],[0.;0.;0.])
    kptsg=finereg.transform_keypoints(kpts,Nullable(refdef))
    hmax=10
    contributions=finereg.precalculate_contributions(img2,kpts,kptsg,critstate,hmax)
    function eval_shift(h)
        y=finereg.eval_contributions([0.;h;0.],refdef,kpts,kptsg,contributions,Float64[])
        @printf("h=%6f    y=%6f\n",h,y)
        return y
    end
    t=collect(range(-10,0.5,50))
    ys=map(eval_shift,t)
    tw=Winston.Table(4,1)
    #p = Winston.FramedPlot(title="shift SSD",
    #        xlabel="shift",
    #        ylabel="criterion" #,yrange=(0.,0.15)
    #                       )
    #Winston.add(p, Winston.Curve(t,ys,yaxis=(0.,0.5)))
    p1=Winston.plot(t,ys,title="shift SSD",xlabel="shift",ylabel="criterion",yrange=(0.0,0.5))
#            ylabel="criterion",yrange=(0.05,0.1))
    #Base.display(p1)
    @show kpts[1].pos
    @show kpts[1].normal
    @show kptsg[1].pos
    @show kptsg[1].normal
    maxdist=2*hmax
    p2=Winston.plot(map(ColorTypes.green,kpts[1].pixels),title="kpts")
    v=get(sample_normal(img2,kptsg[1],2*hmax,defaultval=defaultval) )
    p3=Winston.plot(map(ColorTypes.green,v),title="kptsg")
    p3=Winston.oplot(range(-3,1,2*maxdist),map(ColorTypes.green,kpts[1].pixels),"g-")
    p4=Winston.plot(contributions[1],title="Dv")
    tw[1,1]=p1
    tw[2,1]=p2
    tw[3,1]=p3
    tw[4,1]=p4
    Base.display(tw)
end


# test if the SSD criterio evaluates rotation right
function test_ssd_rotation()
    # test how the criterion varies with shift
    img2=load("imgs/simple.png")
    angle=8. ; shift=[0.;0.]
    truedef=deformation.RigidDeformation2D([size(img2)...],[angle;shift])
    img1=deformation.transform_image_nn(truedef,img2,size(img2),@options default = img2[1])
    ImageView.view(Images.separate(segimgtools.overlay(img1,img2)),name="overlay before") 
    #img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=50,sigma=[1.;1.]),"find simple keypoints")
    hmax=20
    kpts=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    #img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    critstate=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    refdef=deformation.RigidDeformation2D([size(img1)...],[0.;0.;0.])
    kptsg=finereg.transform_keypoints(kpts,Nullable(refdef))
    contributions=finereg.precalculate_contributions(img2,kpts,kptsg,critstate,hmax)
    function eval_shift(h)
        y=finereg.eval_contributions([h;0.;0.],refdef,kpts,kptsg,contributions,Float64[])
        @printf("h=%6f    y=%6f\n",h,y)
        return y
    end
    t=collect(range(-10,0.5,60))
    ys=map(eval_shift,t)
    #Winston.plot(t,ys)
    p = Winston.FramedPlot(title="shift SSD",
            xlabel="angle",
            ylabel="criterion",yrange=(0.,0.15))
    Winston.add(p, Winston.Curve(t,ys))

end


end
