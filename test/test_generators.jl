# Testing the speed of various generator solutions

# first, reimplement range


# iterators
immutable MyRange
    bound::Int    # iterate up to (but not including) the bound
end

Base.start(r::MyRange) = 0 # start from zero
Base.next(r::MyRange, state) = (state*state,state+1)
Base.done(r::MyRange, state) = state >= r.bound
Base.eltype(::Type{MyRange}) = Int
Base.length(r::MyRange) = r.bound
Base.iteratorsize(::Type{MyRange}) = Base.HasLength() # this is the default, anyway
Base.iteratoreltype(::Type{MyRange}) = Base.HasElType() # this is the default, anyway

const n=10000000

function time_iterator(n)
   sum=0::Int
   for i in MyRange(n)
       sum+=i
   end
   return sum 
end

# coroutines
function range_producer(n)
  for i in range(0,n)
      produce(i*i)
  end
end  
    
function time_coroutines(n)
   sum=0::Int
   iter=@task range_producer(n)
   for i in iter
       sum+=i
   end
   return sum 
end

# double iterators
immutable MyDoubleRange
    bound::Int    # iterate up to (but not including) the bound
end

Base.start(r::MyDoubleRange) = 0 # start from zero
Base.next(r::MyDoubleRange, state) = ((state,state*state),state+1)
Base.done(r::MyDoubleRange, state) = state >= r.bound
Base.eltype(::Type{MyDoubleRange}) = (Int,Int)
Base.length(r::MyDoubleRange) = r.bound

const n=100000

function time_double_iterator(n)
   sum=0::Int
   for (i,j) in MyDoubleRange(n)
       sum+=j
   end
   return sum 
end


print("Iterator:")
@time z=time_iterator(n)
@show z
@time z=time_iterator(n)
#print("Coroutines:")
#@time z=time_coroutines(n)
#@show z
#@time z=time_coroutines(n)
print("Double Iterator:")
@time z=time_double_iterator(n)
@show z
@time z=time_double_iterator(n)
