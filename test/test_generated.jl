module test_generated

using Base.Cartesian

@generated function mysum(A::Array{T,N}) where {T,N}
    quote
        s = zero(T)
        @nloops $N i A begin
            s += @nref $N A i
        end
        q=Set[]
        return (s,q)
    end
end

function main()
    a=ones(3,4)
    println(mysum(a))
    b=ones(2,3,4)
    println(mysum(b))
end

end
