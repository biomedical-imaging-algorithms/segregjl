"""
 Experiments with registering the histology and ultrasound images  of the carotid artery

Jan Kybic
"""
module test_carotid
import Images
using Images.ImageCore
import ImageView
import JSON

using segregjl
import .segimgtools
import .deformation
import .coarsereg
using .Options
import .linprog_register

using Debugger
using Logging

include("../src/debugprint.jl")
switchprinting(true)
disable_logging(Logging.BelowMinLevel)


function register_carotid()
    prefix="/home/kybic/work/karotida/regsegm"
    ImageView.closeall()
    img1=Images.load(joinpath(prefix,"0887.png")) # ultrasound
    img2=Images.load(joinpath(prefix,"3641.png")) # histologie
    # convert to integers
    img1=rawview(real(img1)) 
    img2=rawview(real(img2))
    #k1=maximum(img1) ; k2=maximum(img2)
    #ImageView.imshow(img1)
    #ImageView.imshow(segimgtools.classimage2rgb(img1, k1), name = "img1")
    #ImageView.imshow(segimgtools.classimage2rgb(img2, k2), name = "img2")
    # convert to binary segmentations                 
    s1 = UInt8.(img1 .== 1) .+ UInt8(1) 
    s2 = UInt8.((img2 .!= 0) .& (img2 .!= 8)) .+ UInt8(1)
    ImageView.imshow(s1, name = "s1")
    ImageView.imshow(s2, name = "s2")
    # show overlay
    segovlbefore=deformation.overlay(segimgtools.classimage2rgb(s1, 3),segimgtools.classimage2rgb(s2 , 4))
    ImageView.imshow(segovlbefore,name="overlay before")
    theta,val=coarsereg.register_rigid_global(s1,s2,2,niter=100)
    #theta,val=coarsereg.register_rigid(s1,s2,2)
    g=coarsereg.apply_transform(s2,theta,size(s1))
    # show overlay
    segovlafter=deformation.overlay(segimgtools.classimage2rgb(s1, 3),segimgtools.classimage2rgb(g , 4))
    ImageView.imshow(segovlafter,name="overlay after")
    # now use linear programming to refine by registering 
    spacing=10 # distance between keypoints
    hmax=10 # maximum displacement at one level
    k=2 # number of classes
    @bp
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(s1;spacing=spacing,number=250,sigma=[1.;1.]),"find simple keypoints")
    s1kpts=keypoints.create_image_with_keypoints(segimgtools.classimage2rgb(s1, k),kptsimple, llen=hmax, bsize=3)
    ImageView.imshow(s1kpts, name = "segmentation with keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")

    kptsampled=keypoints.create_sampled_keypoints(s1,kptsimple,hmax;spacing=float(spacing))
    isize=[size(s1)...]
    imgsize=float(isize) .* Images.pixelspacing(s1)
  def = deformation.HierarchicalDeformation([
    #    deformation.BsplineDeformation(3,imgsize, [32,32]), 
    #    deformation.BsplineDeformation(3,imgsize, [16,16]), 
        deformation.BsplineDeformation(3,imgsize, [4,4]), 
        deformation.create_identity(imgsize, deformation.AffineDeformation2D)
        ])

    critparams=mil.MILParameters(k)        #mutual information
    opts=@options hmax=hmax resample=true fdiffreg=1e-3 lambda=1e-3 maxResample=5 minKP=500 display=false maxsize=250 minsize=100 knot_spacing=8 start_with_last_level=false
    #critparams=ssd.SSDParameters()
    t1=time()
    #@time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d=linprog_register.register_fast_linprog_multiscale(s1,g,kptsampled,def,nhood,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()
    println("Time: registration $(t2-t1)")
    s2w=@debugtime(deformation.transform_image_nn(d,g,size(img1),@options),"warping segmentation")
    ImageView.imshow(deformation.overlay(segimgtools.classimage2rgb(s1, 3),segimgtools.classimage2rgb(s2w , 4)),name="overlay segmentation after")
end 
 

""" Register all pairs of images from the JSON file by M.Hekrdla"""
function process_all()
    jsonfn="/home/kybic/work/students/hekrdla/ateroskleroza/hekrdla/tasks/registration/json/registration_pairs.json"
    histsegprefix="/mnt/cesnet_FBMI/SANDBOX/hekrdla/data/histology_segmentation/datasets/hist92_size512_classes02_stainVG/labels"
    ussegprefix="/mnt/cesnet_FBMI/SANDBOX/hekrdla/data/ultrasound_segmentation/datasets/ultrasound869_size544_classes02_cutCross/labels"
    usimgprefix="/mnt/cesnet_FBMI/SANDBOX/hekrdla/data/ultrasound_segmentation/datasets/ultrasound869_size544_classes02_cutCross/images"
    outprefix="/mnt/cesnet_FBMI/SANDBOX/kybic/linprog_registration_output"
    d=JSON.parsefile(jsonfn)
    for l in d
      histname,usname=l
      histsegfn=joinpath(histsegprefix,histname * ".png")
      ussegfn=joinpath(ussegprefix, usname * ".png")
      usimgfn=joinpath(usimgprefix, usname * ".png")  
        
      @info "process_all: Starting to register $histname $usname"
      do_register(histname * "-" * usname,histsegfn,ussegfn,usimgfn,outprefix)  
     end    

end


function do_register(ident,histsegfn,ussegfn,usimgfn,outprefix)
    img1=rawview(real(Images.load(histsegfn))) # load histology
    img2=rawview(real(Images.load(ussegfn)))   # load ultrasound
    img2i=Images.load(usimgfn)   # load ultrasound

    # convert to binary segmentations                 
    s1 = UInt8.(img1 .== 1) .+ UInt8(1) 
    s2 = UInt8.((img2 .!= 0) .& (img2 .!= 8)) .+ UInt8(1)
    segovlbefore=deformation.overlay(segimgtools.classimage2rgb(s1, 3),segimgtools.classimage2rgb(s2 , 4))
    Images.save(joinpath(outprefix,"segovlbefore" * ident * ".png"),segovlbefore)
    theta,val=coarsereg.register_rigid_global(s1,s2,2,niter=100) # coarse segmentation
    g=coarsereg.apply_transform(s2,theta,size(s1))
    segovlafter=deformation.overlay(segimgtools.classimage2rgb(s1, 3),segimgtools.classimage2rgb(g , 4))
    Images.save(joinpath(outprefix,"segovlrigid" * ident * ".png"),segovlafter)
    img2ig=coarsereg.apply_transform(img2i,theta,size(s1))
    Images.save(joinpath(outprefix,"warpedusrigid" * ident * ".png"),img2ig)
    # now use linear programming to refine by registering 
    spacing=10 # distance between keypoints
    hmax=10 # maximum displacement at one level
    k=2 # number of classes
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(s1;spacing=spacing,number=250,sigma=[1.;1.]),"find simple keypoints")
    s1kpts=keypoints.create_image_with_keypoints(segimgtools.classimage2rgb(s1, k),kptsimple, llen=hmax, bsize=3)
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")

    kptsampled=keypoints.create_sampled_keypoints(s1,kptsimple,hmax;spacing=float(spacing))
    isize=[size(s1)...]
    imgsize=float(isize) .* Images.pixelspacing(s1)
  def = deformation.HierarchicalDeformation([
        deformation.BsplineDeformation(3,imgsize, [4,4]), 
        deformation.create_identity(imgsize, deformation.AffineDeformation2D)
        ])

    critparams=mil.MILParameters(k)        #mutual information
    opts=@options hmax=hmax resample=true fdiffreg=1e-3 lambda=1e-3 maxResample=5 minKP=500 display=false maxsize=250 minsize=100 knot_spacing=8 start_with_last_level=false
    #critparams=ssd.SSDParameters()
    t1=time()
    #@time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d=linprog_register.register_fast_linprog_multiscale(s1,g,kptsampled,def,nhood,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()
    println("Time: registration $(t2-t1)")
    s2w=@debugtime(deformation.transform_image_nn(d,g,size(img1),@options),"warping segmentation")
    Images.save(joinpath(outprefix,"warpedusseg" * ident * ".png"),s2w)                
    segovlafter=deformation.overlay(segimgtools.classimage2rgb(s1, 3),segimgtools.classimage2rgb(s2w , 4))
    Images.save(joinpath(outprefix,"segovlafter" * ident * ".png"),segovlafter)

    warped=@debugtime(deformation.transform_image_lin(d,img2ig,size(img1),@options),"warping image") 
    Images.save(joinpath(outprefix,"warpedusimg" * ident * ".png"),warped)                

end


end # module
