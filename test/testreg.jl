"""
Experiments with segmentation/registration.

Testing of:
* Keypoint approximation of mutual information criterion for 2D and 3D.
* `NLopt` optimization.
* Superpixels.
* K-means segmentation
* Image transformation.
* Registration of different images with various deformations
* Segmentation + registration.
* Performance on Drosophila imaginal disks and Flagship.
    
Jan Kybic, Martin Dolejsi, 2014 - 2015
"""
module testreg

using Options
using criterion
using PyCall
pygui(:qt) # to avoid a crash when a PyPlot window is selected
using PyPlot
using Images, FixedPointNumbers
using ImageView
using Clustering
using Colors
using gaussian
using segimgtools
import coarsereg
using softmax
using finereg
using keypoints
import SLICsuperpixels
using deformation
import mil
import ssd
import eqcrit
import register
import readRIRE
import readMetaimage
import optimizer
import regularization
using erroreval
using Winston

include("debugassert.jl")
switchasserts(false)
include("debugprint.jl")
switchprinting(false)

include("prototypesdefinition.jl")

"""
    vertdivimg()
Return simple RGB test image 3 vertical stripes
"""
function vertdivimg()
    i=zeros(UInt8,50,100,3)
    i[:,1:33,1]=254; i[:,34:66,2]=255; i[:,67:end,3]=255
    ii=convert(Image{RGB}, colorim(i))
    return ii
end

"""
    horzdivimg()
Return simple RGB test image 3 horizontal stripes
"""
function horzdivimg()
    i=zeros(UInt8,100,50,3)
    i[1:33,:,1]=254; i[34:66,:,2]=255; i[67:end,:,3]=255
    ii=convert(Image{RGB}, colorim(i))
    return ii
end

"""
    circdivimg()
Return simple RGB test image 3 concentric circles
"""
function circdivimg()
    # create a simple test image
    im=zeros(UInt8,51,101,3)
    for j = 1:101
        for i = 1:51
            if ((i - 26) ^ 2 + (j - 51) ^ 2) < 12 ^ 2
                im[i, j, 1] = 255
            elseif ((i - 26) ^ 2 + (j - 51) ^ 2) < 25 ^ 2
                im[i, j, 2] = 255
            else
                im[i, j, 3] = 254
            end
        end
    end
    ii=convert(Image{RGB}, colorim(im))
    return ii
end

"""
    test_superpixels()
Find superpixels for input image and segment the superpixel image using k-means. Show the segmentation.
"""
function test_superpixels()
    img1 = load("imgs/case03-5-he-small.png")
    # img1 = load("imgs/simple.png")
    ImageView.view(img1)
    s1l, count = SLICsuperpixels.slic(img1, 128, 20, false, UInt16)
    s1f = SLICsuperpixels.getMeans(s1l, img1, count)

    ImageView.view(s1l / maximum(s1l))
    k = 4;
    _, p1=segimgtools.kmeans_segmentation(s1l, s1f, k)
    ImageView.view(classimage2rgb(p1, k))
    return nothing
end

"""
    test_kmeans()
Find superpixels for input image and segment the superpixel image using k-means. Show the superpixel
image and the segmentation.
"""
function test_kmeans()
    #img1=load("imgs/simple.png")
    img1=load("imgs/case03-5-he-small.png")
    #img1=load("imgs/Rat_Kidney_HE_Section02-0.png")
    slabels, sfeatures, count = SLICsuperpixels.slicMeans(img1, 2000, 30)
    # permutedims needed, since slabels is row-major
    z=permutedims(cat(3,sfeatures[1,:][slabels],sfeatures[2,:][slabels],sfeatures[3,:][slabels]),[2,1,3])
    ImageView.view(z)
    k=5 # number of classes
    a,p=kmeans_segmentation(slabels,sfeatures,k)
    ImageView.view(classimage2rgb(p,k))
    return nothing
end

"""
    get_medical_dir()
Return path into /datagrid/Medical/
"""
function get_medical_dir()
    basedir = "m:/"
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/"
    end
    if !isdir(basedir)
        basedir = "c:/Temp/"
    end
    return basedir
end

"""
    get_flagship_dir()
Return path into /datagrid/Medical/microscopy/Flagship/
"""
function get_flagship_dir()
    return string(get_medical_dir(), "microscopy/Flagship/")
end

function test_prototype_segmentation()
    img2=Images.load(string( get_flagship_dir(), "images-flagship_convert/scale-5pc/Case001/case001_Cytokeratin.jpg"));
    prototypes = cytokeratin; # see prototypes definition.jl for other dye prototypes
    spedge = 15

    s2l, s2f, count = @debugtime( SLICsuperpixels.slicSizeMeans(img2, spedge^2, 15, true, UInt16), "Creating superpixels:")   #segment using superpixels and k_means

    k = size(prototypes, 2)
    l2p,p2p=segimgtools.prototype_segmentation(s2l, s2f, prototypes)
    l2k,p2k=segimgtools.kmeans_segmentation(s2l, s2f, k)
    l2s,p2s=segimgtools.kmeans_segmentation(s2l, s2f, prototypes)

    ImageView.view(img2, name= "Image")
    ImageView.view(classimage2rgb(p2p, k), name = "Prototype seg.")
    ImageView.view(classimage2rgb(p2k, k), name = "k-means seg.")
    ImageView.view(classimage2rgb(p2s, k), name = "k-means seg. initialized by prototypes");
    return nothing
end

function test_MIL()
    # test mutual information criterion
    img2i=load("imgs/simple.png")
    k=3 # number of classes
    s2l, s2f, count, _, p2 = segimgtools.slic_kmeans(img2i, 1000, 15, k)   #segment using superpixels and k_means
    angle=10. ; shift=[0.;0.]
    p1=deformation.transform_rigid2D(p2,size(p2),angle,shift)
    
    ImageView.view(classimage2rgb(p1,k), name = "image p1")
    ImageView.view(classimage2rgb(p2,k), name = "image p2")
    
    ts=linspace(-180,180,37)
    ms=zeros(size(ts))
    mbest=-Inf
    p2tbest=nothing
    abest=0.
    for i in 1:length(ts)
        a=ts[i] ; s=[0. ; 0.] 
        p2t=deformation.transform_rigid2D(p2,size(p1),a,s)
        m=mil.calculateMIL(p1,p2t,k)
        if m>mbest 
            mbest=m
            abest=a
            p2tbest=p2t
        end
        ms[i]=m
        println("angle=$a mil=$m")
    end
    tw = Winston.plot(ts,ms,"bx-")
    Base.display(tw)
    Winston.xlabel("angle") ; Winston.ylabel("MIL")
    println("best angle = $abest mil=$mbest")
    ImageView.view(classimage2rgb(p2tbest,k), name = "p2tbest")
end

function time_deformation()
    #img2i=load("imgs/simple.png")
    img2i=load("imgs/case03-5-he-small.png")
    # img2i["pixelspacing"]=[1,2]
    
    angle=5. ; shift=[3.,0]
    @show size(img2i)
    # c = deformation.RigidDeformation2D([size(img2i)...] .* Images.pixelspacing(img2i), [angle; shift])
    bc = 5
    c = deformation.BsplineDeformation(3,[size(img2i)...] .* Images.pixelspacing(img2i), [bc,bc])
    imgout = similar(img2i)
    imgout[1,1]=imgout[2,1] # just to force allocation
    # imgout["pixelspacing"] = [2; 2]

    # # Chose linear or nearest nhood interpolation
    # deformation.transform_image_nn!(c, imgout, img2i, @options)                                 # compile funcrtion
    # @debugtime(deformation.transform_image_nn!(c, imgout, img2i, @options),"Transform")         # measure time
    deformation.transform_image_lin!(c, imgout, img2i, @options)                                # compile funcrtion
    @debugtime(deformation.transform_image_lin!(c, imgout, img2i, @options),"Transform")        # measure time
    
    #Profile.clear()
    #@profile deformation.transform_image_lin!(c, imgout, img2i, @options)
    #@profile deformation.transform_image_nn!(c, imgout, img2i, @options)
    #Profile.print()

    ImageView.view(img2i, name = "original")
    ImageView.view(imgout, name = "warped")
    return nothing
end

function test_register_rigid()
    # test affine segmentation registration
    img2i=load("imgs/simple.png")
    k=3 # number of classes
    s2l, s2f, count, _, p2 = segimgtools.slic_kmeans(img2i, 1000, 15, k)   #segment using superpixels and k_means
    #angle=10 ; shift=[20;30]
    #angle=40 ; shift=[0;0]
    angle=10. ; shift=[10.;5.]
    p1 = deformation.transform_rigid2D(p2, size(p2), angle, shift)
    
    # ImageView.view(classimage2rgb(p1,k), name = "segmentation p1")
    # ImageView.view(classimage2rgb(p2,k), name = "segmentation p2")
    ImageView.view(overlay(classimage2rgb(p1,k),classimage2rgb(p2,k)), name = "overlay before")

    d=register.register_rigid_2D_MIL(p1,p2,k,@options)
    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    println("Registration result: $r")
    warped=deformation.transform_image_nn(d,p2,size(p1),@options)
    ImageView.view(overlay(classimage2rgb(p1,k),classimage2rgb(warped,k)), name = "overlay after")
    return nothing
end

function test_register_rigid_multiscale()
    synthetic = false       #use synthetic deformation/real transformation
    # img2i=load("imgs/simple.png")
    img2i=load("imgs/case03-3-psap-small.png")
    k=3 # number of classes

    #angle=10 ; shift=[20;30]
    #angle=40 ; shift=[0;0]
    angle=10. ; shift=[10.;5.]
    if synthetic
        img1i=deformation.transform_rigid2D(img2i,size(img2i),angle,shift)
    else
        img1i=load("imgs/case03-5-he-small.png")
    end

    s1l, s1f, count, _, p1 = segimgtools.slic_kmeans(img1i, 1000, 15, k)   #segment using superpixels and k_means
    s2l, s2f, count, _, p2 = segimgtools.slic_kmeans(img2i, 1000, 15, k)   #segment using superpixels and k_means

    # ImageView.view(classimage2rgb(p1,k), name = "segmentation p1")
    # ImageView.view(classimage2rgb(p2,k), name = "segmentation p2")
    ImageView.view(overlay(classimage2rgb(p1,k),classimage2rgb(p2,k)), name = "overlay before")

    d=register.register_rigid_2D_MIL_multiscale(p1,p2,k,@options)
    #@time d=register.register_rigid_2D_MIL(p1,p2,k,@options)
    r=deformation.get_theta(d)
    if synthetic
        println("Registration result: $r true values: $angle $shift")
    else
        println("Registration result: $r")
    end
    warped=deformation.transform_image_nn(d,p2,size(p1),@options)
    ImageView.view(overlay(classimage2rgb(p1,k),classimage2rgb(warped,k)), name = "overlay after")
    return nothing
end

function test_find_keypoints()
    # img1=load("imgs/case03-5-he-small.png")
    img1=load("imgs/simple.png")
    # img1["pixelspacing"] = [1,2]
    k=4
    s1l, s1f, count, l1, p1 = segimgtools.slic_kmeans(img1, 1000, 30, k)   #segment using superpixels and k_means
    nhoodSP = SLICsuperpixels.getNeighbors(s1l, count)
    # ImageView.view(classimage2rgb(p1,k), name = "segmentation p1")

    # find keypoints without having superpixels
    kpts_, nhood_ = keypoints.find_keypoints_and_neighbors(p1,10)   #first run to compile everything
    kpts_, nhood_ = @debugtime( keypoints.find_keypoints_and_neighbors(p1,10), "Keypoints without superpixels")   #second run to measure time correctly
    println("  number of keypoints=", length(kpts_))
    p1kpts=keypoints.create_image_with_keypoints(p1,s1l,kpts_,k;llen=9)
    ImageView.view(p1kpts, name = "Keypoints without superpixels")

    # find keypoint with superpixels
    kpts_k, nhood_k = keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP)  #first run to compile everything
    kpts_k, nhood_k = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP), "Keypoints with superpixels")  #second run to measure time correctly
    println("  number of keypoints=", length(kpts_k))
    p1kpts=keypoints.create_image_with_keypoints(p1,s1l,kpts_k,k;llen=9)
    ImageView.view(p1kpts, name = "Keypoints with superpixels")

    # # show how the subsampling of the keypoints works
    # kpts = kpts_; nhood = nhood_; name="Keypoints without superpixels"
    # # kpts = kpts_k; nhood = nhood_k; name="Keypoints with superpixels"
    # kpts, nhood = keypoints.subsample_keypoints_equidistant(kpts, nhood, ones(2))
    # println("number of keypoints=", length(kpts))
    # p2kpt=keypoints.create_image_with_keypoints(p1,s1l,kpts,k;llen=9)
    # ImageView.view(p2kpt, name = string(name, " reduced by 2"))
    # kpts, nhood = keypoints.subsample_keypoints_equidistant(kpts, nhood, ones(2))
    # println("number of keypoints=", length(kpts))
    # p2kpt=keypoints.create_image_with_keypoints(p1,s1l,kpts,k;llen=9)
    # ImageView.view(p2kpt, name = string(name, " reduced by 4"))
    return nothing
end

function test_eval_sampled_MIL_criterion_core(p1, p2, def, kpts, hmax, k, theta, ts, thetai)
    crit_params=mil.MILParameters(k)
    reg_params = regularization.NoneRegularizationParams()
    kptsg=finereg.transform_keypoints(kpts,Nullable(def))
    critstate = criterion_init(p1,p2,crit_params)

    println("Precalculating MIL contributions")
    contributions=finereg.precalculate_contributions(p2, kpts, kptsg, critstate, hmax)

    println("Evaluating for different angles")
    ms=zeros(size(ts))  # results of the fast version
    mss=zeros(size(ts)) # results of the slow version
    mbest=-Inf
    thetabest=zeros(size(theta))
    npix=length(p1)
    g=[]
    thetat = copy(theta)
    for i in 1:length(ts)
        thetat[thetai] = ts[i]
        deformation.set_theta!(def,thetat)
        m = -finereg.eval_contributions(thetat,def,kpts,kptsg,contributions,Float64[])
        p2t=deformation.transform_image_nn(def,p2,size(p2),@options)
        mss[i]=mil.calculateMIL(p1,p2t,k)
        
        if m>mbest 
            mbest=m
            thetabest=copy(thetat)
        end
        ms[i]=m
    end
    #plot MIL approximation in blue
    tw = Winston.plot(ts,ms,"bx-")
    Winston.xlabel("angle or shift") ; Winston.ylabel("MIL approx")
    Base.display(tw)
    #plot reference MIL in green
    Winston.figure()
    tw = Winston.plot(ts,mss,"gx-")
    Winston.xlabel("angle or shift") ; Winston.ylabel("MIL ref")
    Base.display(tw)
    #plot approximation and reference into one figure
    Winston.figure()
    tw = Winston.plot(ts,ms-maximum(ms),"bx-",ts,mss-maximum(mss),"gx-")
    Winston.xlabel("angle or shift") ; Winston.ylabel("MIL")
    Base.display(tw)
    
    println("best mil=$mbest")
    println("best $thetabest")
    println("true $theta")
    return thetabest
end

function test_eval_sampled_MIL_criterion()
    img2 = Images.load("imgs/simple.png"); k= 3; numsp = 1000; regular = 20
    # img2=load("imgs/case03-5-he-small.png"); k = 3; numsp = 2000; regular = 20

    hmax = 40
    angle=5. ; shift=[3.; -10.];
    theta = [angle; shift];

    s2l, s2f, count, _, p2 = segimgtools.slic_kmeans(img2, numsp, regular, k, UInt16)   #segment using superpixels and k_means

    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift)
    s1l, s1f, count, l1, p1 = segimgtools.slic_kmeans(img1, numsp, regular, k, UInt16)   #segment using superpixels and k_means
    
    println("Finding keypoints")
    kpts=keypoints.find_keypoints(s1l,l1)

    ImageView.view(classimage2rgb(p1, k), name = "image p1")
    ImageView.view(classimage2rgb(p2, k), name = "image p2")
    ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(p2, k)), name = "overlay before") 

    isize=[size(img2)...]
    def=deformation.create_identity(isize.*Images.pixelspacing(img1), deformation.RigidDeformation2D)

    # compute the criterion in all the points of ts
    ts=linspace(0,10,100)
    thetabest = test_eval_sampled_MIL_criterion_core(p1, p2, def, kpts, hmax, k, theta, ts, 1)

    deformation.set_theta!(def,thetabest)
    p2t=deformation.transform_image_nn(def,p2,size(p2),@options)
    ImageView.view(overlay(classimage2rgb(p1,k),classimage2rgb(p2t,k)), name = "overlay after")
    return nothing
end

function test_register_fast()
    img2=Images.load("imgs/simple.png")
    # img2=Images.load("imgs/case03-5-he-small.png")
    k=3 # number of classes
    # angle=3. ; shift=[2.;0.]
    angle=15. ; shift=[11.;-5]
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    s1l, s1f, count, l1, p1 = segimgtools.slic_kmeans(img1, 1000, 30, k)   #segment using superpixels and k_means
    s2l, s2f, count, l2, p2 = segimgtools.slic_kmeans(img2, 1000, 30, k)   #segment using superpixels and k_means

    #vizualize input
    ImageView.view(classimage2rgb(p1, k), name="segmentation p1")
    ImageView.view(classimage2rgb(p2, k), name="segmentation p2")
    kpts=keypoints.find_keypoints(s1l,l1)
    img1kpts=keypoints.create_image_with_keypoints(p1, s1l, kpts, k)
    ImageView.view(img1kpts, name="Static image segmentation with keypoints")
    ImageView.view(overlay(classimage2rgb(p1,k), classimage2rgb(p2,k)), name = "overlay before") 
    
    isize=[size(p1)...]
    # # Select transformation
    def=deformation.create_identity(isize .* Images.pixelspacing(p1), deformation.ScaleDeformation2D)
    # def=deformation.create_identity(isize .* Images.pixelspacing(p1), deformation.AffineDeformation2D)

    crit_state=mil.criterion_init(p1,p2,mil.MILParameters(k))
    
    # # Select optimizer
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=0.1)
    #opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=0.1)
    
    # # Select regularization
    reg_params = regularization.NoneRegularizationParams()
    # reg_params = regularization.SumRegularizationParams([.3])
    
    d =  @debugtimeline(finereg.register_fast(p1,p2,kpts,def,crit_state,opt_state,reg_params,@options resample=true hmax=35), "Regisrer_fast")
    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    warped=deformation.transform_image_nn(d,p2,size(p1),@options)
    ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(warped, k)), name = "overlay after")
    return nothing
end

function test_register_fast_multiscale()
    synthetic = false       #use synthetic deformation/real transformation
    # img2=Images.load("imgs/simple.png"); k = 3; spedge = 15
    img2=Images.load("imgs/case03-5-he-small.png"); k = 3; spedge = 25

    mossize = 100
    hmax=10
    compact = 10
    angle=10. ; shift=[3.;5.]
    # angle=5. ; shift=[10.;20.]
    opts = @options default=img2[1,1]
    if synthetic
        img1=deformation.transform_rigid2D(img2,size(img2),angle,shift,opts)
    else
        img1=Images.load("imgs/case03-3-psap-small.png");
    end

    s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^2, compact, k,  UInt16), "Superpixels and k-means moving images")   #segment using superpixels and k_means
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k,  UInt16), "Superpixels and k-means static images")   #segment using superpixels and k_means

    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kpts, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")

    # Vizualize input
    img1kpts=keypoints.create_image_with_keypoints(p1, s1l, kpts, k; llen=9, bsize = 3)
    ImageView.view(img1, name = "static image")
    ImageView.view(img2, name = "moving image before registration")
    ImageView.view(overlay(img1, img2), name = "overlay before registration")
    ImageView.view(segimgtools.mosaic(img1, img2, mossize), name = "mosaic before registration")
    ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(p2, k)), name = "segmentation overlay before registration")
    ImageView.view(img1kpts, name = "static image segmentation with keypoints")

    isize=[size(p1)...]
    # # Select transformation
    def=deformation.create_identity(isize .* Images.pixelspacing(p1), deformation.RigidDeformation2D)
    # def=deformation.create_identity(isize .* Images.pixelspacing(p1), deformation.ScaleDeformation2D)
    # def=deformation.create_identity(isize .* Images.pixelspacing(p1), deformation.AffineDeformation2D)

    # # Select criterion
    crit_params=mil.MILParameters(k)        #mutial information
    # crit_params=eqcrit.EQParameters()       #segmentation overlap
    
    # # Select optimizer
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=0.1)
    # opt_state=optimizer.OptimizerGradientDescent(abstol=1e-6,neval=1000,initstep=3)
    # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-6,neval=1000,initstep=0.1)

    # # Select regularization
    reg_params = regularization.NoneRegularizationParams()
    # reg_params = regularization.SumRegularizationParams([.3])

    opt = @options hmax = hmax minsize = 256 minKP = length(kpts) largerotation = false resample = true 
    d = @debugtimeline( finereg.register_fast_multiscale( p1, p2, kpts, def, nhoodKP, crit_params,
                       opt_state, reg_params, opt), "Multiscale registration")

    r=deformation.get_theta(d)
    if synthetic
        @debugprintln_with_color(:green, "Registration result: $r true values: $angle $shift")
    else
        @debugprintln_with_color(:green, "Registration result: $r")
    end
    
    # # Vizualize output
    warped=deformation.transform_image_nn(d, p2, size(p1), @options)
    ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(warped, k)), name = "segmentation overlay after registration")
    warpedimg=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options),"transform_image_lin")
    ImageView.view(overlay(img1,warpedimg), name="overlay after registration")
    ImageView.view(segimgtools.mosaic(img1, warpedimg, mossize), name = "mosaic after registration")
    return nothing
end

function test_register_fast_multiscale_bsplines()
    synthetic = false       #use synthetic deformation/real transformation
    # img2=Images.load("imgs/simple.png"); k = 3; spedge = 15
    img2=Images.load("imgs/case03-5-he-small.png"); k = 3; spedge = 25
    
    mossize = 100
    compact = 20 ; hmax=20
    angle=10. ; shift=[3.;5.]
    opts = @options default=img2[1,1]
    if synthetic
        img1=deformation.transform_rigid2D(img2,size(img2),angle,shift,opts)
    else
        img1=Images.load("imgs/case03-3-psap-small.png")
    end

    s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^2, compact, k,  UInt16, false), "Superpixels and k-means moving images")   #segment using superpixels and k_means
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k,  UInt16), "Superpixels and k-means static images")   #segment using superpixels and k_means

    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kpts, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")

    # # Vizualize input
    # img1kpts=keypoints.create_image_with_keypoints(p1, s1l, kpts, k; llen=9, bsize=3)
    # ImageView.view(img1, name = "static image")
    # ImageView.view(img2, name = "moving image before registration")
    # ImageView.view(overlay(img1, img2), name = "overlay before registration")
    # ImageView.view(segimgtools.mosaic(img1, img2, mossize), name = "mosaic before registration")
    # ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(p2, k)), name = "segmentation overlay before registration")
    # ImageView.view(img1kpts, name = "static image segmentation with keypoints")
    
    isize=[size(p1)...]
    bc = 5
    def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc,bc])

    crit_params=mil.MILParameters(k)
    # crit_params=eqcrit.EQParameters()
    
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=0.1)
    # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-12,neval=50000,initstep=1.)
    
    reg_params = regularization.NoneRegularizationParams()
    # reg_params = regularization.SumRegularizationParams([.01])
    
    opt = @options hmax = hmax minsize = 255 minKP = length(kpts) largerotation = false resample = true
    d = @debugtimeline(finereg.register_fast_multiscale( p1, p2, kpts, def, nhoodKP, crit_params,
                       opt_state, reg_params, opt), "Multiscale registration:")

    r=deformation.get_theta(d)
    @debugprintln_with_color(:green, "Registration result: $r")

    # # Vizualize output
    Profile.clear()
    Profile.init(delay = 0.0001)
    warped = @profile deformation.transform_image_nn(d, p2, size(p1), @options)
    aaa=open("c:/Temp/prof.txt","w")
    Profile.print(aaa,format = :tree, combine = true, sortedby = :count)
    close(aaa)
    Profile.clear()
    
    ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(warped, k)), name = "segmentation overlay after registration")
    warpedimg=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options),"transform_image_lin")
    ImageView.view(overlay(img1,warpedimg), name="overlay after registration")
    ImageView.view(segimgtools.mosaic(img1, warpedimg, mossize), name = "mosaic after registration")
    return nothing
end

function test_register_fast_multiscale_hierarchical()
   synthetic = false       #use synthetic deformation/real transformation
    # img2=Images.load("imgs/simple.png"); k = 3; spedge = 15
    img2=Images.load("imgs/case03-5-he-small.png"); k = 3; spedge = 15
    
    mossize = 100
    compact = 10 ; hmax=10
    angle=10. ; shift=[3.;5.]
    opts = @options default=img2[1,1]
    if synthetic
        img1=deformation.transform_rigid2D(img2,size(img2),angle,shift,opts)
    else
        img1=Images.load("imgs/case03-3-psap-small.png")
    end

    s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^2, compact, k,  UInt16), "Superpixels and k-means moving images")   #segment using superpixels and k_means
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k,  UInt16), "Superpixels and k-means static images")   #segment using superpixels and k_means

    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kpts, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")

    # # Vizualize input
    # img1kpts=keypoints.create_image_with_keypoints(p1, s1l, kpts, k; llen = 9, bsize = 3)
    # ImageView.view(img1, name = "static image")
    # ImageView.view(img2, name = "moving image before registration")
    # ImageView.view(overlay(img1, img2), name = "overlay before registration")
    # ImageView.view(segimgtools.mosaic(img1, img2, mossize), name = "mosaic before registration")
    # ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(p2, k)), name = "segmentation overlay before registration")
    # ImageView.view(img1kpts, name = "static image segmentation with keypoints")

    isize=[size(p1)...]
    resampleFactor= [0,1,1,1]
    levelProg=      [1,0,0,0]
    bc = 7
    def = deformation.HierarchicalDeformation([deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc, bc]);
                                           deformation.create_identity([size(img2)...] .* Images.pixelspacing(p1), deformation.AffineDeformation2D)])

    crit_params=mil.MILParameters(k)
    # crit_params=eqcrit.EQParameters()

    opt_state = optimizer.OptimizerMMA(abstol=1e-6,neval=5000,initstep=1.0)
    # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-5,neval=20000,initstep=5.)

    # reg_params = regularization.SumRegularizationParams([1e-14, 0])
    reg_params = regularization.NoneRegularizationParams()

    opt = @options hmax = hmax minKP = 200 largerotation = false resample = true
    d = @debugtimeline(finereg.register_fast_multiscaleHB( p1, p2, kpts, def, nhoodKP, crit_params,
                       opt_state, reg_params, resampleFactor, levelProg, opt), "Multiscale registration:")

    # # Vizualize output
    warped=deformation.transform_image_nn(d, p2, size(p1), @options)
    ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(warped, k)), name = "segmentation overlay after registration")
    warpedimg=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options),"transform_image_lin")
    ImageView.view(overlay(img1,warpedimg), name="overlay after registration")
    ImageView.view(segimgtools.mosaic(img1, warpedimg, mossize), name = "mosaic after registration")
    return nothing
end

function test_register_fast_multiscale_hierarchicalI()
    synthetic = false       #use synthetic deformation/real transformation
    nn = ["03" "17" "38" "99"]
    # nn = ["99"]
    savestr = "c:/Temp/"
    if !isdir(savestr)
        savestr = ""
    end
    for i = 1: length(nn)
    num = nn[i]
    basedir = string(get_medical_dir(), "microscopy/HeHerSlices/")
    img2=Images.load(string(basedir, "small1to42_norm/", num, "_HE_small.jpg")); k = 3; spedge = 15
    
    mossize = 100
    compact = 10 ; hmax=10
    angle=10. ; shift=[3.;5.]
    opts = @options default=img2[1,1]
    if synthetic
        img1=deformation.transform_rigid2D(img2,size(img2),angle,shift,opts)
    else
        img1=Images.load(string(basedir, "small1to42_norm/", num, "_Her2_small.jpg"))
    end

    s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^2, compact, k,  UInt16), "Superpixels and k-means moving images")   #segment using superpixels and k_means
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k,  UInt16), "Superpixels and k-means static images")   #segment using superpixels and k_means
    
    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kpts, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")
    # kpts, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(p1, spedge),"find_keypoints_and_neighbors:")

    # # Save Images
    Images.save(string(savestr, num, "_Obef.jpg"), overlay(img1, img2))
    Images.save(string(savestr, num, "_sObef.jpg"), overlay(classimage2rgb(p1, k), classimage2rgb(p2, k)))
    
    isize=[size(p1)...]
    resampleFactor= [0,1,1,1]
    levelProg=      [1,0,0,0]
    bc = 7
    def = deformation.HierarchicalDeformation([deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc, bc]);
                                           deformation.create_identity([size(img2)...] .* Images.pixelspacing(p1), deformation.RigidDeformation2D)])

    # deformation.set_theta!(def.def[2], [0; 50; 200.])
    crit_params=mil.MILParameters(k)
    # crit_params=eqcrit.EQParameters()

    opt_state = optimizer.OptimizerMMA(abstol=1e-6,neval=5000,initstep=1.0)
    # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-5,neval=20000,initstep=5.)

    # reg_params = regularization.SumRegularizationParams([1e-14, 0])
    reg_params = regularization.NoneRegularizationParams()

    opt = @options hmax = hmax minKP = 200 largerotation = false resample = true
    d = @debugtimeline(finereg.register_fast_multiscaleHB( p1, p2, kpts, def, nhoodKP, crit_params,
                       opt_state, reg_params, resampleFactor, levelProg, opt), "Multiscale registration:")

    # # Vizualize output
    warped=deformation.transform_image_nn(d, p2, size(p1), @options)
    # ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(warped, k)), name = "segmentation overlay after registration")
    warpedimg=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options),"transform_image_lin")
    # ImageView.view(overlay(img1,warpedimg), name="overlay after registration")
    # ImageView.view(segimgtools.mosaic(img1, warpedimg, mossize), name = "mosaic after registration")
    # # Save Images
    Images.save(string(savestr, num, "_Oaft.jpg"), overlay(img1, warpedimg))
    Images.save(string(savestr, num, "_sOaft.jpg"), overlay(classimage2rgb(p1, k), classimage2rgb(warped, k)))
    Images.save(string(savestr, num, "_Maft.jpg"), segimgtools.mosaic(img1, warpedimg, mossize))
    end
    return nothing
end

function test_register_fast_multiscale_hierarchicalF()
    synthetic = false       #use synthetic deformation/real transformation
    # basedir = string(get_medical_dir(), "microscopy/Flagship/images-MultiGeneSignatureMaps_convert/scale-5pc/Case006/")
    # imgbaset=Images.load(string(basedir, "case06-2-ki67.jpg"))
    # imgregt=Images.load(string(basedir, "case06-3-psap.jpg"))
    basedir = string(get_medical_dir(), "microscopy/Flagship/images-flagship_convert/scale-5pc/Case008/")
    imgbaset=Images.load(string(basedir, "case008_HE.jpg"))
    imgregt=Images.load(string(basedir, "case008_negative.jpg"))
    
    sz = larger_size(size(imgbaset), size(imgregt))
    img1 = imgbaset#extend_to_size(imgbaset, sz, (imgbaset[2, 2] + imgbaset[2, end - 1] + imgbaset[end - 1, end - 1] + imgbaset[end - 1, 2]) / 4, false)
    img2 = imgregt#extend_to_size(imgregt, sz, (imgregt[2, 2] + imgregt[2, end - 1] + imgregt[end - 1, end - 1] + imgregt[end - 1, 2]) / 4, false )
    
    spedge = 40
    compact = 10
    k = 4
    hmax = 20
    blab = false
    repeatKM = 30
    minimgsize = 128
    minkpts = 500
    kpdist = spedge
    largerot = false
    spkp = true
    
    mossize = 100

    s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^2, compact, k,  UInt16, true, 30), "Superpixels and k-means moving images")   #segment using superpixels and k_means
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k,  UInt16, true, 30), "Superpixels and k-means static images")   #segment using superpixels and k_means

    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    if spkp
        kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP)
    else
        kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(p1, kpdist)
    end
    # kpts=keypoints.create_sampled_keypoints(p1, kpts, hmax; spacing = float(kpdist))
    isize=[size(p2)...]
    bc = 7
    # def = deformation.HierarchicalDeformation([deformation.BsplineDeformation(3, isize .* Images.pixelspacing(p2), [bc, bc]);
                               # deformation.create_identity(isize .* Images.pixelspacing(p2), deformation.RigidDeformation2D)])
    def=deformation.create_identity(isize .* Images.pixelspacing(p2), deformation.RigidDeformation2D)
                
    
    
    # # Vizualize input
    # img1kpts=keypoints.create_image_with_keypoints(p1, s1l, kpts, k; llen = 9, bsize = 3)
    # ImageView.view(img1, name = "static image")
    # ImageView.view(img2, name = "moving image before registration")
    # ImageView.view(overlay(img1, img2), name = "overlay before registration")
    # ImageView.view(segimgtools.mosaic(img1, img2, mossize), name = "mosaic before registration")
    # ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(p2, k)), name = "segmentation overlay before registration")
    # ImageView.view(img1kpts, name = "static image segmentation with keypoints")


    crit_params=mil.MILParameters(k)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=2000,initstep=1.0)
    # opt_state=admm.OptimizerADMMS(;monitor=false,:maxiter =>10,errprim=0.1,errdy=0.1,
                       # ji_opts=Dict(:maxiter =>50),
                       # cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>50) )
    reg_params = regularization.NoneRegularizationParams()
    mindim = min(minimum(size(p1)), minimum(size(p2)))
    rspls = 0

    while minimgsize < mindim
        mindim = mindim/2
        rspls = rspls + 1
    end
    resampleFactor = ones(Int, rspls + 1)
    resampleFactor[1] = 0
    levelProg = zeros(Int, rspls + 1)
    levelProg[1] = 1

    d = finereg.register_fast_multiscale(p1, p2, kpts, def, nhoodKP, crit_params, opt_state, reg_params,
           @options hmax = hmax minsize = minimgsize minKP = minkpts resample = true largerotation = largerot)
    # opt = @options hmax = hmax minKP = minkpts largerotation = largerot resample = true
    # d = finereg.register_fast_multiscaleHB( p1, p2, kpts, def, nhoodKP, crit_params,
           # opt_state, reg_params, resampleFactor, levelProg, opt)
           
    # # Vizualize output
    warped=deformation.transform_image_nn(d, p2, size(p1), @options)
    ImageView.view(overlay(classimage2rgb(p1, k), classimage2rgb(warped, k)), name = "segmentation overlay after registration")
    warpedimg=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options),"transform_image_lin")
    ImageView.view(overlay(img1,warpedimg), name="overlay after registration")
    ImageView.view(segimgtools.mosaic(img1, warpedimg, mossize), name = "mosaic after registration")
    return nothing
end


function test_segment_register_fast_multiscale()
    synthetic = false       #use synthetic deformation/real transformation
    # img2t=Images.load("imgs/simple.png"); k = 3; spedge = 15
    img2t=Images.load("imgs/case03-5-he-small.png"); k = 3; spedge = 15
    
    mossize = 100
    compact = 20 ; hmax=10
    angle=10. ; shift=[3.;5.]
    regular = 10
    opts = @options default=img2t[1,1]
    if synthetic
        img1t=deformation.transform_rigid2D(img2t,size(img2t),angle,shift,opts)
    else
        img1t=Images.load("imgs/case03-3-psap-small.png")
    end

    # # Vizualize input
    # ImageView.view(img1, name = "static image")
    # ImageView.view(img2, name = "moving image before registration")
    # ImageView.view(overlay(img1, img2), name = "overlay before registration")
    # ImageView.view(segimgtools.mosaic(img1, img2, mossize), name = "mosaic before registration")
    
    sz = larger_size(size(img1t), size(img2t))
    img1 = extend_to_size(img1t, sz, (img1t[2, 2] + img1t[2, end - 1] + img1t[end - 1, end - 1] + img1t[end - 1, 2]) / 4, false )
    img2 = extend_to_size(img2t, sz, (img2t[2, 2] + img2t[2, end - 1] + img2t[end - 1, end - 1] + img2t[end - 1, 2]) / 4, false )
    
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    # def=deformation.BsplineDeformation(3,[size(img1)...] .* Images.pixelspacing(img1), [5,5])

    crit_params=mil.MILParameters(k)

    opt_state=optimizer.OptimizerMMA(abstol = 1e-6, neval = 500, initstep = 1.0)
    # opt_state=optimizer.OptimizerGradientDescent()

    reg_params = regularization.NoneRegularizationParams()
    # reg_params = regularization.SumRegularizationParams([.001])

    opts1 = @options order = 1 minsize = 256 regular = regular minvol = [] resample = true ninit = 20 hmax = 30
    d = @debugtimeline( finereg.segment_register_fast_multiscale(img2, img1, def,
                        crit_params, opt_state, reg_params, spedge, opts1), "Multiscale seg-reg:")

    # # Vizualize output
    warpedimg=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options),"transform_image_lin")
    ImageView.view(overlay(img1,warpedimg), name="overlay after registration")
    ImageView.view(segimgtools.mosaic(img1, warpedimg, mossize), name = "mosaic after registration")
    return nothing
end

function normalize_gray_image(img::Images.Image)
    return Images.grayim((img - minimum(img)) / (maximum(img) - minimum(img)))
end

function overlay_normalized_gray_images(img1::Images.Image, img2::Images.Image)
    return overlay(normalize_gray_image(img1), normalize_gray_image(img2))
end

function show_3_projections(img; name="3Doverlay")
    # shows two grayscale 3D images in 3 orthogonal views
    a = timedim(img)
    img["timedim"] = 3
    ImageView.view(img, name = string(name, " x-y cut"))
    img["timedim"] = 2
    ImageView.view(img, name = string(name, " x-z cut"))
    img["timedim"] = 1 
    ImageView.view(img, name = string(name, " y-z cut"))
    img["timedim"] = a
    return nothing
end

function show_3_normalized_projections(img::Images.Image; name="3Doverlay")
    return show_3_projections(normalize_gray_image(img), name = name)
end

function show_3_normalized_projections_overlay(img1::Images.Image, img2::Images.Image; name="3Doverlay")
    return show_3_projections(overlay_normalized_gray_images(img1, img2), name = name)
end

function show_3_projections_overlay(img1::Images.Image, img2::Images.Image; name="3Doverlay")
    return show_3_projections(overlay(img1, img2), name = name)
end

function test_eval_sampled_MIL_criterion_3D()
    basedir = string(get_medical_dir(), "RIRE/")
    img2 = readRIRE.read_data(string(basedir, "training_001/ct")); spedge = 10
    # img2 = readRIRE.read_data(string(basedir, "training_001/mr_PD")); spedge = 8
    hmax = 20
    
    angle = [5.; 0.; 0.];
    shift = [0.; 0.; 0.];
    theta = [angle; shift]
    regular = 5
    opts = @options default = img2[1, 1, 1]
    img1 = deformation.transform_rigid3D(img2, size(img2), angle, shift, opts)

    # # Segmentation using threshold
    k=2;
    p2 = reinterpret(UInt8,Images.grayim(convert(Array{UInt8},img2 .> -300)))+1 #CT segmentation
    p2["pixelspacing"] =  Images.pixelspacing(img2)
    p2["spatialorder"] =  Images.spatialorder(img2)
    p1 = reinterpret(UInt8,Images.grayim(convert(Array{UInt8},img1 .> -300)))+1  #CT segmentation
    p1["pixelspacing"] =  Images.pixelspacing(img1)
    p1["spatialorder"] =  Images.spatialorder(img1)

    # # Segmentanion using k-means
    # k = 3
    # s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^3, regular, k, UInt16), "Superpixels and k-means moving images")   #segment using superpixels and k_means
    # s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^3, regular, k, UInt16), "Superpixels and k-means static images")   #segment using superpixels and k_means

    nlength = max(minimum(Images.pixelspacing(p1)), minimum(Images.pixelspacing(p2)))
    kpts, nhoodKP =  @debugtime(keypoints.find_keypoints_and_neighbors(p1, spedge, nlength), "Finding keypoints")

    # # Vizualize input
    # img1kpts=keypoints.create_image_with_keypoints(p1, kpts, k;llen=hmax)
    show_3_normalized_projections_overlay(img1, img2, name = "overlay before registration")
    # show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(p2, k), name = "segmentation overlay before registration")
    # show_3_projections(img1kpts, name = "static image segmentation with keypoints")

    isize=[size(img2)...]
    def=deformation.create_identity(isize.*Images.pixelspacing(img1), deformation.RigidDeformation3D)

    # compute the criterion in all the points of ts
    ts=linspace(2,8,50)
    thetabest = test_eval_sampled_MIL_criterion_core(p1, p2, def, kpts, hmax, k, theta, ts, 1)

    deformation.set_theta!(def,thetabest)
    i2t=deformation.transform_image_nn(def,img2,size(img2),@options)
    show_3_normalized_projections_overlay(img1, i2t, name = "overlay after registration")
    p2t=deformation.transform_image_nn(def,p2,size(p2),@options)
    show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(p2t, k), name = "segmentation overlay before registration")
    return nothing
end

function test_register_fast_multiscale_rigid_3D()
    basedir = string(get_medical_dir(), "RIRE/")
    img2 = readMetaimage.read_data(string(basedir, "training_001/ct/training_001_ct.mhd")); k = 2
    # img2 = readRIRE.read_data(string(basedir, "training_001/mr_PD/training_001_mr_PD.mhd"));  k = 2

    spedge = 20
    slicenum = 10
    kmeansrep = 20
    hmax = 20
    #angle = [5.; 0.; 0.];
    angle = [-10.11,7.35,5];#-5.7,-1.5,-2.6
    shift = [-16.8; 33.; 5]; regular = 5
    opts = @options default = img2[1, 1, 1]
    # img1 = deformation.transform_rigid3D(img2, size(img2), angle, shift,opts)
    # img1 = readRIRE.read_data(string(basedir, "training_001/mr_T2"));
    img1 = readMetaimage.read_data(string(basedir, "training_001/mr_T1/training_001_mr_T1.mhd"));
    # img1 = readRIRE.read_data(string(basedir, "training_001/mr_T2_rectified"));
    # img1 = readRIRE.read_data(string(basedir, "training_001/mr_PD"));

    # # Segmentanion using k-means
    # s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^3, regular, k, UInt16), "Superpixels and k-means moving images")   #segment using superpixels and k_means
    # s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^3, regular, k, UInt16), "Superpixels and k-means static images")   #segment using superpixels and k_means

    # # Segmentation using threshold - select the right line vr to imaging modality
    p2 = reinterpret(UInt8,Images.grayim(convert(Array{UInt8},img2 .> -300)))+1 #CT segmentation
    # p2 = reinterpret(UInt8,Images.grayim(convert(Array{UInt8},img2 .> 150)))+1  #PD segmentation
    p2["pixelspacing"] =  Images.pixelspacing(img2)
    p2["spatialorder"] =  Images.spatialorder(img2)
    # p1 = reinterpret(UInt8,Images.grayim(convert(Array{UInt8},img1 .> -300)))+1 #CT segmentation
    # p1 = reinterpret(UInt8,Images.grayim(convert(Array{UInt8},img1 .> 150)))+1  #PD segmentation
    # p1 = reinterpret(UInt8,Images.grayim(convert(Array{UInt8},img1 .> 70)))+1  #T2 segmentation
    p1 = reinterpret(UInt8,Images.grayim(convert(Array{UInt8},img1 .> 150)))+1  #T1 segmentation
    p1["pixelspacing"] =  Images.pixelspacing(img1)
    p1["spatialorder"] =  Images.spatialorder(img1)

    # # Finding keypoints
    nlength = max(minimum(Images.pixelspacing(p1)), minimum(Images.pixelspacing(p2)))
    # kpts, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP, 25, nlength), "Keypoints from superpixels")
    kpts, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(p1, 10, nlength), "Keypoints without superpixels")
    
    # # Vizualize input
    # img1kpts=keypoints.create_image_with_keypoints(p1, kpts, k;llen=hmax)
    # show_3_normalized_projections_overlay(img1, img2, name = "overlay before registration")
    # show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(p2, k), name = "segmentation overlay before registration")
    # show_3_projections(img1kpts, name = "static image segmentation with keypoints")
    
    def=deformation.create_identity([size(p2)...] .* Images.pixelspacing(p2), deformation.RigidDeformation3D)

    print_with_color(:cyan, "Multiscale registration:\n")
    crit_params=mil.MILParameters(k)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=2000,initstep=1.0)
    reg_params = regularization.NoneRegularizationParams()
    d = @debugtimeline(finereg.register_fast_multiscale( p1, p2, kpts, def, nhoodKP, crit_params, opt_state, reg_params,
                       @options hmax = hmax minsize = 256 minKP = 800 resample = true largerotation = false), "Multiscale registration:")


    # # measure the error - select the right line vr to imaging modality
    erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_T1.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_T2.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_pd.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, angle, shift)

    r = deformation.get_theta(d)
    @debugprintln_with_color(:green, "Registration result: $r")
    
    i2t=deformation.transform_image_nn(d,img2,size(img2),@options)
    show_3_normalized_projections_overlay(img1, i2t, name = "overlay after registration")
    p2t=deformation.transform_image_nn(d,p2,size(p2),@options)
    show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(p2t, k), name = "segmentation overlay before registration")
    return nothing
end

function draw_annotations_t(lp, pxsp, color, imgc, imgd)
    l3 = Array(Float64,2,1)
    for i = 1:size(lp, 1)
        l3[:, 1] = lp[i, 1:2] ./ pxsp[1:2] + .5
        ap = ImageView.AnnotationPoints(copy(l3); t = round(Int, lp[i, 3] / pxsp[3] + .5), color = color)
        idx = ImageView.annotate!(imgc, imgd, ap)
    end
    return nothing
end

function show_3_projections_with_keypoints(mos::Images.Image, lp1::Array, lp2::Array, pxsp1, pxsp2; name="3Doverlay")
    a = timedim(mos)
    mos["timedim"] = 3
    imgc, imgd = ImageView.view(mos, name = name)
    draw_annotations_t(lp1, pxsp1, RGB(0, 1, 0), imgc, imgd)
    draw_annotations_t(lp2, pxsp2, RGB(1, 0, 0), imgc, imgd)

    mos["timedim"] = 2
    imgc, imgd = ImageView.view(mos, name = name)
    lpt = lp1[:, [1, 3, 2]]
    pxspt = pxsp1[[1, 3, 2]]
    draw_annotations_t(lpt, pxspt, RGB(0, 1, 0), imgc, imgd)
    lpt = lp2[:, [1, 3, 2]]
    pxspt = pxsp2[[1, 3, 2]]
    draw_annotations_t(lpt, pxspt, RGB(1, 0, 0), imgc, imgd)

    mos["timedim"] = 1
    imgc, imgd = ImageView.view(mos, name = name)
    lpt = lp1[:, [2, 3, 1]]
    pxspt = pxsp1[[2, 3, 1]]
    draw_annotations_t(lpt, pxspt, RGB(0, 1, 0), imgc, imgd)
    lpt = lp1[:, [2, 3, 1]]
    pxspt = pxsp2[[2, 3, 1]]
    draw_annotations_t(lpt, pxspt, RGB(1, 0, 0), imgc, imgd)
    mos["timedim"] = a
end

function test_register_fast_multiscale_bsplines_3D()
    basedir = string(get_medical_dir(), "POPI/")
    filename2 = "01/00"
    img2 = readMetaimage.read_data(string(basedir, filename2, ".mhd")); k = 4

    maxdist = 10
    spedge = 20
    kpdist = 10
    angle = [-10.11,7.35,5];#-5.7,-1.5,-2.6
    shift = [-16.8; 13.; 5];
    regular = 0
    quantQ=k
    kmeansrep = 10
    opts = @options default = img2[1]
    filename1 = "01/50"
    # img1 = deformation.transform_rigid3D(img2, size(img2), angle, shift,opts)
    img1 = readMetaimage.read_data(string(basedir, filename1, ".mhd"))
    
    # # Segment moving image using linear quantization
    pxsp1 = Images.pixelspacing(img2)
    pxsp2 = Images.pixelspacing(img2)
    img1 = Images.grayim(img1[:,1:400,:]); img1["spatialorder"] = ["x", "y", "z"]; img1["pixelspacing"] = pxsp1
    img2 = Images.grayim(img2[:,1:400,:]); img2["spatialorder"] = ["x", "y", "z"]; img2["pixelspacing"] = pxsp2
    p2 = segimgtools.quantize(img2, quantQ)
    p1 = segimgtools.quantize(img1, quantQ)

    # # Segmentanion using k-means
    # s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^3, regular, k, UInt16), "Superpixels and k-means moving images")   #segment using superpixels and k_means
    # s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^3, regular, k, UInt16), "Superpixels and k-means static images")   #segment using superpixels and k_means

    print_with_color(:cyan, "Finding keypoints:\n")
    nlength = max(minimum(Images.pixelspacing(p1)), minimum(Images.pixelspacing(p2)))
    kpts, nhoodKP = @time keypoints.find_keypoints_and_neighbors(p1, kpdist, nlength)
    kpts=keypoints.create_sampled_keypoints(p1, kpts, maxdist; spacing = float(kpdist))
    
    # # Vizualize input
    # img1kpts=keypoints.create_image_with_keypoints(p1, kpts, k;llen=hmax)
    # show_3_normalized_projections_overlay(img1, img2, name = "overlay before registration")
    # show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(p2, k), name = "segmentation overlay before registration")
    # show_3_projections(img1kpts, name = "static image segmentation with keypoints")
    
    # def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [27,27,19])
    def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [11,11,7])
    # def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [4,4,4])
    
    print_with_color(:cyan, "Multiscale registration:\n")
    crit_params=mil.MILParameters(k)
    opt_state=optimizer.OptimizerMMA(abstol=1e-2,neval=10000,initstep=2.0)
    reg_params = regularization.NoneRegularizationParams()
    opts = @options hmax = maxdist minsize = 1024 minKP = 8000 resample = true largerotation = false pwl = false
    d = @debugtimeline(finereg.register_fast_multiscale( p1, p2, kpts, def, nhoodKP, crit_params, opt_state, reg_params,
                       opts), "Multiscale registration:")

    # Measure and print out registration error
    erroreval.measure_error_POPI(d, string(basedir, filename1, ".pts"), string(basedir, filename2, ".pts"), string(basedir, filename1, ".mhd"), string(basedir, filename2, ".mhd"))

    # Read landmerks
    lp1, _ = erroreval.read_landmarks_POPI(string(basedir, filename1, ".pts"), string(basedir, filename1, ".mhd"))
    lp2, _ = erroreval.read_landmarks_POPI(string(basedir, filename2, ".pts"), string(basedir, filename2, ".mhd"))

    # Show images and landmarks before registration
    mos = overlay_normalized_gray_images(img1, img2)
    show_3_projections_with_keypoints(mos, lp1, lp2, pxsp1, pxsp2, name = "overlay before registration")

    # Show images after registration
    preg = deformation.transform_image_lin(d, img2, size(img2), @options default = img2[1])
    show_3_normalized_projections_overlay(img1, preg, name = "overlay after registration")

    # #Visualization segmentations
    # preg = deformation.transform_image_nn(d,p2,size(p2),@options default = p2[1])
    # show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(preg, k), name = "segmentation overlay after registration")
end

function test_register_fast_multiscale_bsplines_3D_NIREP()
    basedir = string(get_medical_dir(), "NIREP/")
    img2 = readMetaimage.read_data(string(basedir, "na01/na01.hdr")); k = 10

    hmax = 10
    img1 = readMetaimage.read_data(string(basedir, "na02/na02.hdr"));
  
    ############## Load segmentations for NIREP
    p2 = reinterpret(UInt8, readMetaimage.read_data(string(basedir, "na01/na01_seg.hdr"))) + 1; k = convert(Int, maximum(p2))
    p1 = reinterpret(UInt8, readMetaimage.read_data(string(basedir, "na02/na02_seg.hdr"))) + 1

    spacing = 8
    print_with_color(:cyan, "Finding keypoints:\n")
    nlength = max(minimum(Images.pixelspacing(p1)), minimum(Images.pixelspacing(p2)))
    kpts, nhoodKP = @time keypoints.find_keypoints_and_neighbors(p1, spacing, nlength)
    kptsampled=keypoints.create_sampled_keypoints(p1, kpts, hmax; spacing=float(spacing))
    
    # # Vizualize input
    # img1kpts=keypoints.create_image_with_keypoints(p1, kpts, k;llen=hmax)
    show_3_normalized_projections_overlay(img1, img2, name = "overlay before registration")
    show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(p2, k), name = "segmentation overlay before registration")
    # show_3_projections(img1kpts, name = "static image segmentation with keypoints")

    def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [11,19,11])

    print_with_color(:cyan, "Multiscale registration:\n")
    crit_params=mil.MILParameters(k)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=2000,initstep=1.0)
    # reg_params = regularization.NoneRegularizationParams()
    reg_params = regularization.SumRegularizationParams([.002])
    opts = @options hmax = hmax minsize = 128 minKP = 7000 resample = true largerotation = false
    d = @debugtimeline(finereg.register_fast_multiscale( p1, p2, kptsampled, def, nhoodKP, crit_params, opt_state, reg_params,
                       opts), "Multiscale registration:")

    # # Visualization splines
    theta = copy(deformation.get_theta_mat(d))
    println([minimum(theta), maximum(theta)])
    theta -= minimum(theta)
    theta /= maximum(theta)

    # ImageView.view(Images.Image(squeeze(theta[1,:,:,:],1), Dict{Any,Any}("spatialorder"=>["x", "y", "z"])))
    # ImageView.view(Images.Image(squeeze(theta[2,:,:,:],1), Dict{Any,Any}("spatialorder"=>["x", "y", "z"])))
    # ImageView.view(Images.Image(squeeze(theta[3,:,:,:],1), Dict{Any,Any}("spatialorder"=>["x", "y", "z"])))
    
    preg = deformation.transform_image_lin(d, img2, size(img2), @options default = img2[1])
    show_3_normalized_projections_overlay(img1, preg, name = "overlay after registration")
    preg = deformation.transform_image_nn(d, p2, size(p2), @options default = p2[1])
    show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(preg, k), name = "segmentation overlay after registration")
    println("Number of equivalent pixels before registration $(sum(convert(Array{Int},(p1.==p2))))")
    println("Number of equivalent pixels after registration $(sum(convert(Array{Int},(p1.==preg))))")
    println("after/before $(sum(convert(Array{Int},(p1.==preg)))/sum(convert(Array{Int},(p1.==p2))))")
end

function test_register_fast_multiscale_hierarchical_3D()
    basedir = string(get_medical_dir(), "POPI/")
    filename2 = "01/00"
    img2 = readMetaimage.read_data(string(basedir, filename2, ".mhd")); k = 4

    hmax = 10
    spedge = 20
    kpdist = 10
    angle = [-10.11,7.35,5];
    shift = [-16.8; 13.; 5];
    regular = 0
    quantQ=k
    kmeansrep = 10
    opts = @options default = img2[1]
    filename1 = "01/50"
    # img1 = deformation.transform_rigid3D(img2, size(img2), angle, shift,opts)
    img1 = readMetaimage.read_data(string(basedir, filename1, ".mhd"))
    
    # # Segment moving image using linear quantization
    pxsp1 = Images.pixelspacing(img2)
    pxsp2 = Images.pixelspacing(img2)
    img1 = Images.grayim(img1[:,1:400,:]); img1["spatialorder"] = ["x", "y", "z"]; img1["pixelspacing"] = pxsp1
    img2 = Images.grayim(img2[:,1:400,:]); img2["spatialorder"] = ["x", "y", "z"]; img2["pixelspacing"] = pxsp2
    p2 = segimgtools.quantize(img2, quantQ)
    p1 = segimgtools.quantize(img1, quantQ)

    # # Segmentanion using k-means
    # s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^3, regular, k, UInt16), "Superpixels and k-means moving images")   #segment using superpixels and k_means
    # s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^3, regular, k, UInt16), "Superpixels and k-means static images")   #segment using superpixels and k_means

    print_with_color(:cyan, "Finding keypoints:\n")
    nlength = max(minimum(Images.pixelspacing(p1)), minimum(Images.pixelspacing(p2)))
    kpts, nhoodKP = @time keypoints.find_keypoints_and_neighbors(p1, kpdist, nlength)
    kpts=keypoints.create_sampled_keypoints(p1, kpts, hmax; spacing = float(kpdist))
    
    # # Vizualize input
    # img1kpts=keypoints.create_image_with_keypoints(p1, kpts, k;llen= hmax)
    # show_3_normalized_projections_overlay(img1, img2, name = "overlay before registration")
    # show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(p2, k), name = "segmentation overlay before registration")
    # show_3_projections(img1kpts, name = "static image segmentation with keypoints")
    
    resampleFactor= [0,0,1]
    levelProg=      [1,1,0]
    def = deformation.HierarchicalDeformation([#deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [131,131,67])
                                           deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [35,35,19]);
                                           deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [11,11,7]);
                                           deformation.create_identity([size(img2)...] .* Images.pixelspacing(img2), deformation.AffineDeformation3D)])

    print_with_color(:cyan, "Multiscale registration:\n")
    crit_params=mil.MILParameters(k)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=1.0)
    # reg_params = regularization.SumRegularizationParams([1e-10,0.])
    reg_params = regularization.NoneRegularizationParams()
    opts = @options hmax = hmax minKP = 8000 resample = true largerotation = false
    d = @debugtimeline(finereg.register_fast_multiscaleHB( p1, p2, kpts, def, nhoodKP, crit_params, opt_state, reg_params, resampleFactor, levelProg,
                       opts), "Multiscale registration:")

    # Measure and print out registration error
    erroreval.measure_error_POPI(d, string(basedir, filename1, ".pts"), string(basedir, filename2, ".pts"), string(basedir, filename1, ".mhd"), string(basedir, filename2, ".mhd"))
    #Visualization splines
    for i = 1:length(d.def)
        if typeof(d.def[i])<:deformation.BsplineDeformation
            theta = copy(d.def[i].theta)
            println("level $i, theta in range $([minimum(theta), maximum(theta)])")
            theta -= minimum(theta)
            theta /= maximum(theta)

            ImageView.view(Images.Image(squeeze(theta[1,:,:,:],1), Dict{Any,Any}("spatialorder"=>["x", "y", "z"])))
            ImageView.view(Images.Image(squeeze(theta[2,:,:,:],1), Dict{Any,Any}("spatialorder"=>["x", "y", "z"])))
            ImageView.view(Images.Image(squeeze(theta[3,:,:,:],1), Dict{Any,Any}("spatialorder"=>["x", "y", "z"])))
        end
    end
    
    # Read landmerks
    lp1, _ = erroreval.read_landmarks_POPI(img1, string(basedir, filename1, ".pts"), string(basedir, filename1, ".mhd"))
    lp2, _ = erroreval.read_landmarks_POPI(img2, string(basedir, filename2, ".pts"), string(basedir, filename2, ".mhd"))

    # Show images and landmarks before registration
    mos = overlay_normalized_gray_images(img1, img2)
    show_3_projections_with_keypoints(mos, lp1, lp2, pxsp1, pxsp2, name = "overlay before registration")

    # Show images after registration
    preg = deformation.transform_image_lin(d, img2, size(img2), @options default = img2[1])
    show_3_normalized_projections_overlay(img1, preg, name = "overlay after registration")

    # #Visualization segmentations
    # preg = deformation.transform_image_nn(d,p2,size(p2),@options default = p2[1])
    # show_3_projections_overlay(classimage2rgb(p1, k), classimage2rgb(preg, k), name = "segmentation overlay after registration")
end

function test_segment_register_fast_multiscale_rigid_3D(ninit::Int = 10)
    basedir = string(get_medical_dir(), "RIRE/")
    img2 = readRIRE.read_data(string(basedir, "training_001/ct")); k = 2
    # img2 = readRIRE.read_data(string(basedir, "training_001/mr_PD"));  k = 2

    hmax = 20
    spedge = 10
    angle = [-10.11,7.35,5];#-5.7,-1.5,-2.6
    shift = [-16.8; 13.; 5];
    regular = 5
    opts = @options default = img2[1]
    # img1 = deformation.transform_rigid3D(img2, size(img2), angle, shift,opts)
    # img1 = readRIRE.read_data(string(basedir, "training_001/mr_T2"));
    img1 = readRIRE.read_data(string(basedir, "training_001/mr_T1"));
    # img1 = readRIRE.read_data(string(basedir, "training_001/mr_T2_rectified"));
    # img1 = readRIRE.read_data(string(basedir, "training_001/mr_PD"));

    show_3_normalized_projections_overlay(img1, img2, name = "overlay before registration")
    
    isize=[size(img2)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img2), deformation.RigidDeformation3D)
    
    crit_params=mil.MILParameters(k)
    opt_state=optimizer.OptimizerMMA(abstol = 1e-12, neval = 1000, initstep = 1.0)
    reg_params = regularization.NoneRegularizationParams()
    opts1 = @options resample = true hmax = hmax neval = 500 regular = regular order = 1 minvol = 0 ninit = ninit
    d = @debugtimeline( finereg.segment_register_fast_multiscale(img2, img1, def,
                        crit_params, opt_state, reg_params, spedge, opts1), "Multiscale seg-reg:")

    ############
    # # measure the error
    erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_T1.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_T2.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "training_001/transformations.csv/ct_pd.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, angle, shift)

    r=deformation.get_theta(d)
    @debugprintln_with_color(:green, "Registration result: $r")
    #Visualization images
    preg = deformation.transform_image_lin(d, img2, size(img2), @options default = img2[1])
    show_3_normalized_projections_overlay(img1, preg, name = "overlay after registration")
end

function test_segment_register_fast_multiscale_bsplines_3D(ninit::Int = 10)
    basedir = string(get_medical_dir(), "POPI/")
    filename2 = "01/00"
    img2 = readMetaimage.read_data(string(basedir, filename2, ".mhd")); k = 4

    hmax = 20
    spedge = 30
    angle = [-10.11,7.35,5];#-5.7,-1.5,-2.6
    shift = [-16.8; 13.; 5];
    regular = 5
    opts = @options default = img2[1]
    filename1 = "01/50"
    # img1 = deformation.transform_rigid3D(img2, size(img2), angle, shift,opts)
    img1 = readMetaimage.read_data(string(basedir, filename1, ".mhd"))
    
    # show_3_normalized_projections_overlay(img1, img2, name = "overlay before registration")

    isize=[size(img2)...]
    def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [11,11,7])
    
    crit_params=mil.MILParameters(k)
    opt_state=optimizer.OptimizerMMA(abstol = 1e-6, neval = 1500, initstep = 1.0)
    reg_params = regularization.NoneRegularizationParams()
    opts1 = @options resample = true hmax = hmax regular = regular order = 2 minvol = 10 ninit = ninit
    d = @debugtimeline( finereg.segment_register_fast_multiscale(img2, img1, def,
                        crit_params, opt_state, reg_params, spedge, opts1), "Multiscale seg-reg:")

    # # measure the error
    erroreval.measure_error_POPI(d, string(basedir, filename1, ".pts"), string(basedir, filename2, ".pts"), string(basedir, filename1, ".mhd"), string(basedir, filename2, ".mhd"))

    #Visualization images
    preg = deformation.transform_image_lin(d, img2, size(img2), @options default = img2[1])
    show_3_normalized_projections_overlay(img1, preg, name = "overlay after registration")
end

function test_ninit()
    ninit = [5; 10; 20; 30; 50; 100]
    nrep = 30
    errmm = ones(Float64,length(ninit), nrep)
    errpx = ones(Float64,length(ninit), nrep)
    for i = 1:length(ninit)
        for j = 1:nrep
            errmm[i, j], errpx[i, j] = test_segment_register_fast_multiscale_rigid_3D(ninit[i])
        end
        figure(1);clf();
        PyPlot.plot(ninit, minimum(errmm,2), "xg-")
        PyPlot.plot(ninit, maximum(errmm,2), "xr-")
        PyPlot.plot(ninit, mean(errmm,2), "xb-")
        PyPlot.plot(ninit, median(errmm,2), "xk-")
        title("mm")
        figure(2);clf();
        PyPlot.plot(ninit, minimum(errpx,2), "xg-")
        PyPlot.plot(ninit, maximum(errpx,2), "xr-")
        PyPlot.plot(ninit, mean(errpx,2), "xb-")
        PyPlot.plot(ninit, median(errpx,2), "xk-")
        title("px")
    end
end

"""
    test_flagship()
Go through all the Flagship data at 5% scale and register all the relevant pairs. Write results to the
csv file. This function is used to debug and test code for batchfunctions.test_register_script as well
as for making visualizations.
"""
function test_flagship()
    spedge = 40
    regular = 10# 20
    k = 4
    maxd = 20
    blab = true# false
    repeatKM = 30
    minimgsize = 128
    minkpts = 500
    kpdist = spedge
    largerot = false#true
    spkp = true
    tmeanmin = 70
    tmeanstep = 5
    if !isdir("res/")
        mkdir("res/")
    end
    logdir = "c:/Temp/"
    logfile = string(logdir, "output_SK.csv")
    if !isdir(logdir)
        logfile = "output_SK.csv"
    end
    
    basedir = string(get_medical_dir(), "microscopy/")
    listdir = string(basedir, "lists_regImgSets/flagship-scale-5pc/")
    listfiles = readdir(listdir)
    sort!(listfiles)
    # Init log file
    fstr = open(logfile, "w")
    write(fstr, "Base file, Reg. file, mean dist. before [px], mean dist. after [px], std dist.after [px], median dist. after [px], ")
    for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
        write(fstr, "mean$(rtm)% dist. after [px], ")
    end
    write(fstr, "min dist. before [px], min dist. after [px], max dist. before [px], max dist. after [px], total time [s], load+pad time [s],SLIC time [s], K-means time ($(repeatKM)x) [s], KP time [s], register time [s]\n")
    close(fstr)
    # Init output
    tottime = 0.
    tottimeLOA = 0.
    tottimeSP = 0.
    tottimeSEG = 0.
    tottimeREG = 0.
    tottimeKDW = 0.
    totmin0 = 1.11e5
    totmax0 = zero(Float64)
    totlen0 = zero(Float64)
    totTcnt = zero(Float64)
    totdistv = Float64[]
    totRdistv = Float64[]
    # Iterate all the possible file combinations
    for casenum = 1:length(listfiles)
    # @bp
        fstr = open(string(listdir, listfiles[casenum]), "r")
        files = readcsv(fstr)
        close(fstr)
        if (files[1][1:4] == "CIMA")
            continue
        end
        # Init output
        casetime = 0.
        casetimeLOA = 0.
        casetimeSP = 0.
        casetimeSEG = 0.
        casetimeREG = 0.
        casetimeKDW = 0.
        casemin0 = 1.11e5
        casemax0 = zero(Float64)
        caselen0 = zero(Float64)
        caseTcnt = zero(Float64)
        casedistv = Float64[]
        caseRdistv = Float64[]
        for filnum = 1:2:length(files)
        # @bp
            # gc()
            TR1 = time_ns() / 1e9
            # Read base image and anotations
            println(string("Opening base: ", files[filnum]))
            imgbaset = Images.load(string(basedir, files[filnum]))
            fstr = open(string(basedir, files[filnum + 1]), "r")
            landbase = readdlm(fstr, Float64; skipstart = 2)
            close(fstr)
            TR1 = time_ns() / 1e9 - TR1
            # For all the rest of the set
            for filnum2 = filnum + 2:2:length(files)
            # @bp
                # gc()
                # gc_disable()
                T0 = time_ns() / 1e9 - TR1
                println(string("   Opening second: ", files[filnum2]))
                imgregt = Images.load(string(basedir, files[filnum2]))
                fstr = open(string(basedir, files[filnum2 + 1]), "r")
                landreg = readdlm(fstr, Float64; skipstart = 2)
                close(fstr)
                # Find common size and pad
                sz = larger_size(size(imgbaset), size(imgregt))
                imgbase = imgbaset#extend_to_size(imgbaset, sz, (imgbaset[2, 2] + imgbaset[2, end - 1] + imgbaset[end - 1, end - 1] + imgbaset[end - 1, 2]) / 4, false)
                imgreg = imgregt#extend_to_size(imgregt, sz, (imgregt[2, 2] + imgregt[2, end - 1] + imgregt[end - 1, end - 1] + imgregt[end - 1, 2]) / 4, false )
                
                tuptimeLOA = time_ns() / 1e9 - T0
                T0 = time_ns() / 1e9
                # Segment base image
                lb, count = SLICsuperpixels.slicSize(imgbase, spedge^2, regular, blab, UInt16)
                fbase = SLICsuperpixels.getMeans(lb, imgbase, count)
                nhoodSP = SLICsuperpixels.getNeighbors(lb, count)
                tuptimeSP = time_ns() / 1e9 - T0
                lbase = []
                pbase = []
                lcnt = 0
                while length(lbase) == 0 && lcnt < 3
                    lbase, pbase = segimgtools.kmeans_segmentation(lb, fbase, k, repeatKM)
                    lcnt += 1
                end
                # rnd = MersenneTwister(3)
                # while length(lbase) == 0 && lcnt < 3
                    # init = rand(1:size(fbase, 1), k)
                    # lbase, pbase = segimgtools.kmeans_segmentation(lb, fbase, init)
                    # lcnt += 1
                # end
                if length(lbase) == 0
                    fstr = open(logfile, "a")
                    write(fstr, string(files[filnum], ", ", files[filnum2], ",  k-Means failed\n"))
                    close(fstr)
                    continue
                end
                tuptimeSEG = time_ns() / 1e9 - T0 - tuptimeSP
                # Segment second image
                lr, count = SLICsuperpixels.slicSize(imgreg, spedge^2, regular, blab, UInt16)
                freg = SLICsuperpixels.getMeans(lr, imgreg, count)
                tuptimeSP = time_ns() / 1e9 - T0 - tuptimeSEG
                lreg = []
                preg =[]
                lcnt = 0
                while length(lreg) == 0 && lcnt < 3
                    lreg, preg = segimgtools.kmeans_segmentation(lr, freg, k, repeatKM)
                    lcnt += 1
                end
                # rnd = MersenneTwister(3)
                # while length(lreg) == 0 && lcnt < 3
                    # init = rand(1:size(freg, 1), k)
                    # lreg, preg = segimgtools.kmeans_segmentation(lr, freg, init)
                    # lcnt += 1
                # end
                if length(lreg) == 0
                    fstr = open(logfile, "a")
                    write(fstr, string(files[filnum], ", ", files[filnum2], ",  k-Means failed\n"))
                    close(fstr)
                    continue
                end
                regstart = time_ns() / 1e9 - T0
                tuptimeSEG = regstart - tuptimeSP
                # Init registration
                nlen = 1#min(minimum(Images.pixelspacing(imgbase)), minimum(Images.pixelspacing(imgreg)))/2
                if spkp
                    kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(lb, lbase, nhoodSP, 5, nlen)
                else
                    kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(pbase, kpdist, nlen)
                end
                kpts=keypoints.create_sampled_keypoints(pbase, kpts, maxd; spacing = float(kpdist))
                isize=[size(preg)...]
                bc = 7
                def = deformation.HierarchicalDeformation([deformation.BsplineDeformation(3, isize .* Images.pixelspacing(preg), [bc, bc]);
                                           deformation.create_identity(isize .* Images.pixelspacing(preg), deformation.RigidDeformation2D)])
                # def=deformation.create_identity(isize .* Images.pixelspacing(preg), deformation.RigidDeformation2D)
                tuptimeKDW = time_ns() / 1e9 - T0 - regstart
                # Register
                
                crit_params=mil.MILParameters(k)
                opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=2000,initstep=1.0)
                # opt_state=admm.OptimizerADMMS(;monitor=false,:maxiter =>10,errprim=0.1,errdy=0.1,
                                   # ji_opts=Dict(:maxiter =>50),
                                   # cg_opts=Dict(:abstol=>1e-2, :reltol => 0., :maxiter =>50) )
                reg_params = regularization.NoneRegularizationParams()
                mindim = min(minimum(size(pbase)), minimum(size(preg)))
                rspls = 0

                while minimgsize < mindim
                    mindim = mindim/2
                    rspls = rspls + 1
                end
                resampleFactor = ones(Int, rspls + 1)
                resampleFactor[1] = 0
                levelProg = zeros(Int, rspls + 1)
                levelProg[1] = 1

                # d = finereg.register_fast_multiscale(pbase, preg, kpts, def, nhoodKP, crit_params, opt_state, reg_params,
                       # @options hmax = maxd minsize = minimgsize minKP = minkpts resample = true largerotation = largerot)
                opt = @options hmax = maxd minKP = minkpts largerotation = largerot resample = true
                d = finereg.register_fast_multiscaleHB( pbase, preg, kpts, def, nhoodKP, crit_params,
                       opt_state, reg_params, resampleFactor, levelProg, opt)
                tuptimeREG = time_ns() / 1e9 - T0 - regstart - tuptimeKDW

                tuptime = time_ns() / 1e9 - T0
                # gc_enable()
                # gc()
                # Evaluate the registration
                y  = zeros(Float64, 2)
                # Init output
                tupmin0 = 1.11e5
                tupmax0 = zero(Float64)
                tuplen0 = zero(Float64)
                setdistv = Array(Float64, size(landreg, 1))
                for poin = 1:size(landreg, 1)
                    deformation.transform_point!(d, vec(landbase[poin, :]), y)
                    setdistv[poin] = sqrt(sum((vec(landreg[poin, :]) - y).^2))
                    len0 = sqrt(sum((vec(landreg[poin, :]) - vec(landbase[poin, :])).^2))
                    tupmin0 = min(tupmin0, len0)
                    tupmax0 = max(tupmax0, len0)
                    tuplen0 += len0
                end
                sort!(setdistv)
                casetime += tuptime
                casetimeLOA += tuptimeLOA
                casetimeSP += tuptimeSP
                casetimeSEG += tuptimeSEG
                casetimeKDW += tuptimeKDW
                casetimeREG += tuptimeREG
                casemin0 = min(casemin0, tupmin0)
                casemax0 = max(casemax0, tupmax0)
                caselen0 += tuplen0
                caseTcnt += 1
                casedistv = [casedistv; setdistv]
                if (0.5 * tuplen0 > sum(setdistv))
                    caseRdistv = [caseRdistv; setdistv]
                end
                # Write image pair result into log file
                fstr = open(logfile, "a")
                write(fstr, string(files[filnum], ", ", files[filnum2], ", "))
                print_shortest(fstr, tuplen0 / size(landreg, 1))
                write(fstr, ", ")
                print_shortest(fstr, mean(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, std(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, median!(setdistv))
                for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
                    write(fstr, ", ")
                    print_shortest(fstr, mean(setdistv[1:round(Int, length(setdistv) * rtm / 100)]))
                end
                write(fstr, ", ")
                print_shortest(fstr, tupmin0)
                write(fstr, ", ")
                print_shortest(fstr, minimum(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, tupmax0)
                write(fstr, ", ")
                print_shortest(fstr, maximum(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, tuptime)
                write(fstr, ", ")
                print_shortest(fstr, tuptimeLOA)
                write(fstr, ", ")
                print_shortest(fstr, tuptimeSP)
                write(fstr, ", ")
                print_shortest(fstr, tuptimeSEG)
                write(fstr, ", ")
                print_shortest(fstr, tuptimeKDW)
                write(fstr, ", ")
                print_shortest(fstr, tuptimeREG)
                write(fstr, "\n")
                close(fstr)
                r=deformation.get_theta(d)
                @debugprintln_with_color(:green, "Registration result: $r, len: $(tuplen0/size(landreg, 1)) => $(sum(setdistv)/size(landreg, 1))")
                # # # Plot results for visual evaluation
                # #keypoints
                # figure(1); clf();
                # ikpts = keypoints.create_image_with_keypoints(pbase, lb, kpts, k;llen=15,bsize=7)
                # imshow(separate(ikpts)); #title("keypoints")
                # @bp
                # #images only + contour+landmarks
                # figure(1) ; clf()
                # imshow(separate(imgbase)); #title("static")
                # axis([0, size(imgbase, 1), 0, size(imgbase, 2)])
                # axis("off")
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_img_flagship_static.pdf")
                # contour(make_segmentation_binary(pbase'), k, colors = "black", linewidths = .5)
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_img_flagship_static_cont.pdf")
                # figure(1) ; clf()
                # imshow(separate(imgbase)); #title("static")
                # axis([0, size(imgbase, 1), 0, size(imgbase, 2)])
                # axis("off")
                # plot(landbase[:,1],landbase[:,2],markeredgewidth=1.5,marker="+",color = "green",linestyle="",markersize=10)
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_img_flagship_static_land.pdf")
                # figure(1) ; clf()
                # imshow(separate(imgreg)); #title("moving")
                # axis([0, size(imgreg, 1), 0, size(imgreg, 2)])
                # axis("off")
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_img_flagship_moving.pdf")
                # contour(make_segmentation_binary(preg'), k, colors = "black", linewidths = .5)
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_img_flagship_moving_cont.pdf")
                # figure(1) ; clf()
                # imshow(separate(imgreg)); #title("moving")
                # axis([0, size(imgreg, 1), 0, size(imgreg, 2)])
                # axis("off")
                # plot(landreg[:,1],landreg[:,2],markeredgewidth=1.5,marker="+",color = "red",linestyle="",markersize=10)
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_img_flagship_moving_land.pdf")

                # #segmentations only
                # figure(1) ; clf()
                # imshow(pbase'); #title("static")
                # axis([0, size(imgbase, 1), 0, size(imgbase, 2)])
                # axis("off")
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_seg_flagship_static.pdf")
                # figure(1) ; clf()
                # imshow(preg'); #title("moving")
                # axis([0, size(imgbase, 1), 0, size(imgbase, 2)])
                # axis("off")
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_seg_flagship_moving.pdf")

                # #overlays
                # warped=deformation.transform_image_nn(d, preg, size(preg), @options)
                # figure(1) ; clf()
                # imshow(Images.separate(overlay(classimage2rgb(pbase,k),classimage2rgb(preg,k)))); #title("before registration")
                # axis([0, size(imgbase, 1), 0, size(imgbase, 2)])
                # axis("off")
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_seg_flagship_orig.pdf")

                # figure(2) ; clf() 
                # imshow(Images.separate(overlay(classimage2rgb(pbase,k),classimage2rgb(warped,k)))); #title("after registration")
                # axis([0, size(imgbase, 1), 0, size(imgbase, 2)])
                # axis("off")
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_seg_flagship_reg.pdf")

                # figure(3) ; clf()
                # imshow(Images.separate(overlay(imgbase,imgreg))) ; #title("before registration")
                # for i = 1:size(landbase, 1)
                    # arrow(landreg[i,1],landreg[i,2],landbase[i,1]-landreg[i,1],landbase[i,2]-landreg[i,2])
                # end
                # plot(landreg[:,1],landreg[:,2],markeredgewidth=1.5,marker="+",color = "red",linestyle="",markersize=10)
                # plot(landbase[:,1],landbase[:,2],markeredgewidth=1.5,marker="+",color = "green",linestyle="",markersize=10)
                # axis([0, size(imgbase, 1), 0, size(imgbase, 2)])
                # axis("off")
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_img_flagship_orig.pdf")

                # landtrans = similar(landbase)
                # dinv = deformation.copy(d)
                # for poin = 1:size(landreg, 1)
                    # deformation.transform_point_inv!(dinv, vec(landreg[poin, :]), y)
                    # landtrans[poin, :] = y
                # end
                # figure(4) ; clf()
                # warpedimg=deformation.transform_image_nn(d, imgreg, size(imgreg), @options)
                # imshow(Images.separate(overlay(imgbase,warpedimg))) ; #title("after registration")
                # for i = 1:size(landtrans, 1)
                    # arrow(landbase[i,1],landbase[i,2],landtrans[i,1]-landbase[i,1],landtrans[i,2]-landbase[i,2])
                # end
                # plot(landbase[:,1],landbase[:,2],markeredgewidth=1.5,marker="+",color = "red",linestyle="",markersize=10)
                # plot(landtrans[:,1],landtrans[:,2],markeredgewidth=1.5,marker="+",color = "green",linestyle="",markersize=10)
                # axis([0, size(imgbase, 1), 0, size(imgbase, 2)])
                # axis("off")
                # savefig("res/c$(casenum)_f$(filnum)-$(filnum2)_img_flagship_reg.pdf")
                # return
                # @bp
            end
        end
        sort!(casedistv)
        tottime += casetime
        tottimeLOA += casetimeLOA
        tottimeSP += casetimeSP
        tottimeSEG += casetimeSEG
        tottimeKDW += casetimeKDW
        tottimeREG += casetimeREG
        totmin0 = min(totmin0, casemin0)
        totmax0 = max(totmax0, casemax0)
        totlen0 += caselen0
        totTcnt += caseTcnt
        totdistv = [totdistv; casedistv]
        totRdistv = [totRdistv; caseRdistv]
        fstr = open(logfile, "a")
        write(fstr, listfiles[casenum])
        write(fstr, ", , ")
        print_shortest(fstr, caselen0 / length(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, mean(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, std(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, median!(casedistv))
        for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
            write(fstr, ", ")
            print_shortest(fstr, mean(casedistv[1:round(Int, length(casedistv) * rtm / 100)]))
        end
        write(fstr, ", ")
        print_shortest(fstr, casemin0)
        write(fstr, ", ")
        print_shortest(fstr, minimum(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, casemax0)
        write(fstr, ", ")
        print_shortest(fstr, maximum(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, casetime / caseTcnt)
        write(fstr, ", ")
        print_shortest(fstr, casetimeLOA / caseTcnt)
        write(fstr, ", ")
        print_shortest(fstr, casetimeSP / caseTcnt)
        write(fstr, ", ")
        print_shortest(fstr, casetimeSEG / caseTcnt)
        write(fstr, ", ")
        print_shortest(fstr, casetimeKDW / caseTcnt)
        write(fstr, ", ")
        print_shortest(fstr, casetimeREG / caseTcnt)
        write(fstr, "\nRobustness = ")
        print_shortest(fstr, length(caseRdistv) / length(casedistv))
        write(fstr, ", , , ")
        print_shortest(fstr, mean(caseRdistv))
        write(fstr, ", ")
        if isempty(caseRdistv)
            write(fstr, "N/A")
            write(fstr, ", ")
            write(fstr, "N/A")
        else
            print_shortest(fstr, std(caseRdistv))
            write(fstr, ", ")
            print_shortest(fstr, median!(caseRdistv))
        end
        write(fstr, "\n\n")
        close(fstr)
    end
    sort!(totdistv)
    fstr = open(logfile, "a")
    write(fstr, "total, , ")
    print_shortest(fstr, totlen0 / length(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, mean(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, std(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, median!(totdistv))
    for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
        write(fstr, ", ")
        print_shortest(fstr, mean(totdistv[1:round(Int, length(totdistv) * rtm / 100)]))
    end
    write(fstr, ", ")
    print_shortest(fstr, totmin0)
    write(fstr, ", ")
    print_shortest(fstr, minimum(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, totmax0)
    write(fstr, ", ")
    print_shortest(fstr, maximum(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, tottime / totTcnt)
    write(fstr, ", ")
    print_shortest(fstr, tottimeLOA / totTcnt)
    write(fstr, ", ")
    print_shortest(fstr, tottimeSP / totTcnt)
    write(fstr, ", ")
    print_shortest(fstr, tottimeSEG / totTcnt)
    write(fstr, ", ")
    print_shortest(fstr, tottimeKDW / totTcnt)
    write(fstr, ", ")
    print_shortest(fstr, tottimeREG / totTcnt)
    write(fstr, "\nRobustness = ")
    print_shortest(fstr, length(totRdistv) / length(totdistv))
    write(fstr, ", , , ")
    print_shortest(fstr, mean(totRdistv))
    write(fstr, ", ")
    print_shortest(fstr, std(totRdistv))
    write(fstr, ", ")
    print_shortest(fstr, median!(totRdistv))
    write(fstr, "\n")
    close(fstr)
    # gc_enable()
    # gc()
end

"""
    batch_flagship_seg(sourcelist, outputdir, inputsets; minimgsize, KPspacing, minKPnum,hmax, k, SPsize, SPcompact, lambda, maxsize, reg)
Register histological slices from the `sourcelist` or its subset and save results in the format
readable by Jirka's batch evaluation. Use NLopt optimizations, mil criterion on segmentations,
hierarchical deformation and SumRegularization.
!!! note
    All the paths are based on /datagrid/Medical/microscopy/
# Arguments
* `sourcelist::String`: directory path whwre the lists of images are located. Default "lists_regImgSets/mix-small/"
* `outputdir::String`: directory to save all the results. Subdir is created for the parameter settings. Default "TEMPORARY/julia_experimens_ipmi2017/"
* `inputsets::Array{Int, 1}`: indicies of the cases in directory `sourcelist` (sorted alphabetically) to be registered. If empty, all the cases are used. Default [3, 29, 30]
* `minimgsize::Int`: smaller images will not be resampled in multiscale pyramid. Default 64
* `KPspacing::Int`: keypoint spacing. Default 30
* `minKPnum::Int`: minimum keypoint number in multiscale. Default 250
* `hmax::Int`: hmax in register_fast. Default 20
* `k::Int`: number of classe for image segmentation. Default 3
* `SPsize::Int`: number of pixels inside initial sumerpixel. Default 49
* `SPcompact::Int`: superpixel compactness. Default 10
* `reg::Float64`: b-spline regularization. Default 1e-4
"""
function batch_flagship_seg(sourcelist = "lists_regImgSets/mix-small/",
                            outputdir = "TEMPORARY/julia_experimens_ipmi2017/",
                            inputsets::Array{Int, 1} = [3, 29, 30];
                            minimgsize::Int = 64, KPspacing::Int = 30, minKPnum::Int = 250,
                            hmax::Int = 20, k::Int = 3, SPsize::Int = 49,
                            SPcompact::Int = 10, reg::Float64 = 1e-4)
    basedir = "m:/" 
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/"
    end
    basedir = string(basedir , "microscopy/")
    
    opt = @options hmax = hmax minKP = minKPnum largerotation = false resample = true maxResample = 10
    crit_params=mil.MILParameters(k)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=2000,initstep=1.0)
    reg_params = regularization.SumRegularizationParams([reg; reg; 0])
    y = zeros(Float64, 2)       #temporary storage of landarks

    outdir = string(basedir,"TEMPORARY/julia_experimens_ipmi2017/s3_mil_mma_sum_mis$(minimgsize)_kpsp$(KPspacing)_mkp$(minKPnum)_hm$(hmax)_k$(k)_sps$(SPsize)_cmp$(SPcompact)_r$(reg)/")
    if !isdir(outdir)
        mkpath(outdir, 0o775)
    end

    datasets = readdir(string(basedir, sourcelist))
    sort!(datasets)
    covercsv = string(outdir, "cover.csv")
    fstr = open(covercsv, "w")
    write(fstr, "Base image, Moving image, Base keypoints, Moving keypoints, Registered image, registered landmarks, Time [s]\n")
    close(fstr)
    if inputsets == []
        inputsets = 1:length(datasets)
    end
    for dsind in inputsets
        outdirsuff = string(datasets[dsind][1:end-4], "/")
        outdirthis = string(outdir, outdirsuff)
        if !isdir(outdirthis)
            mkpath(outdirthis, 0o775)
        end
        images = readdlm(string(basedir, sourcelist, datasets[dsind]), ',', '\n')

        for imgbind = 1:2:length(images)-2
            timebase = time()
            #read base landmarks
            fstr = open(string(basedir, images[imgbind+1]), "r")
            landreg = readdlm(fstr, Float64; skipstart = 2)
            close(fstr)
            #read base image
            img1 = Images.load(string(basedir, images[imgbind]))    #base
            s1l, s1f, count, l1, p1 = segimgtools.slic_size_kmeans(img1, SPsize, SPcompact, k, UInt16, true, 30)
            nlen = minimum(Images.pixelspacing(img1))
            kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(p1, KPspacing, nlen)
            kpts = keypoints.create_sampled_keypoints(p1, kpts, hmax; spacing=float(KPspacing))
            
            # img1kpts=keypoints.create_image_with_keypoints(copy(p1),kpts,k;llen=hmax)
            # ImageView.view(img1kpts,name="with keypoints")
            # ImageView.view(img1,name="img1")
            # ImageView.view(classimage2rgb(p1, k),name="img1")

            timebase = time() - timebase
            for imgmind = imgbind+2:2:length(images)
                timemoving = time()
                img2=Images.load(string(basedir, images[imgmind]))  #moving
                s2l, s2f, count, l2, p2 = segimgtools.slic_size_kmeans(img2, SPsize, SPcompact, k, UInt16, true, 30)
                # ImageView.view(deformation.overlay(img1, img2), name = "overlay before")
                isize=[size(img2)...]
                imgsize=isize .* Images.pixelspacing(img2) # image size in physical units
                def = deformation.HierarchicalDeformation([
                   deformation.BsplineDeformation(3,imgsize, [19,19]), 
                   deformation.BsplineDeformation(3,imgsize, [7,7]), 
                   deformation.create_identity(imgsize, deformation.AffineDeformation2D)
                   ])
                @debugprintln("Registering")
                mindim = min(minimum(size(img1)), minimum(size(img2)))
                rspls = 0
                while minimgsize < mindim
                    mindim = mindim/2
                    rspls = rspls + 1
                end
                resampleFactor = ones(Int, rspls + 1)
                resampleFactor[1] = 0
                levelProg = zeros(Int, rspls + 1)
                levelProg[1] = 1
                levelProg[2] = 1
    
                d = finereg.register_fast_multiscaleHB( p1, p2, kpts, def, nhoodKP, crit_params,
                       opt_state, reg_params, resampleFactor, levelProg, opt)

                timemoving = time() - timemoving
                #Landmark transformation
                fstr = open(string(outdirthis, "b$(imgbind)m$(imgmind).txt"), "w")
                write(fstr, "point\n")
                print_shortest(fstr, size(landreg, 1))
                write(fstr, "\n")
                for poin = 1:size(landreg, 1)
                    deformation.transform_point!(d, vec(landreg[poin, :]), y)
                    print_shortest(fstr, y[1])
                    write(fstr, " ")
                    print_shortest(fstr, y[2])
                    write(fstr, "\n")
                end
                close(fstr)

                # warm and save image
                warped=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options), "warping")
                Images.save(string(outdirthis, "b$(imgbind)m$(imgmind).png"), warped)
                # ImageView.view(warped,name="warped")
                # ImageView.view(deformation.overlay(img1,warped),name="overlay after")
                # pw=@debugtime(deformation.transform_image_lin(d, p2, size(p1), @options), "warping")
                # ImageView.view(deformation.overlay(classimage2rgb(p1, k), classimage2rgb(pw, k)),name="after")
                fstr = open(covercsv, "a")
                write(fstr, "$(images[imgbind]),$(images[imgmind]),$(images[imgbind+1]),$(images[imgmind+1]),$(outdirsuff)b$(imgbind)m$(imgmind).png,$(outdirsuff)b$(imgbind)m$(imgmind).txt,$(timemoving+timebase)\n")
                close(fstr)
            end
        end
    end
end # batch_flagship_seg

"""
    run_batch_flagship_seg()
Create the parameter vectors and run batch_flagship_seg()
"""
function run_batch_flagship_seg()
    hmax = [10, 20, 30]
    spsize = [49, 64, 81]
    for i in hmax
        for j in spsize
            batch_flagship_seg("lists_regImgSets/mix-small/", "TEMPORARY/julia_experimens_ipmi2017/";
                                hmax = i, SPsize = j)
        end
    end
    exit()
end

"""
    test_drosophila_disks()
Segment and register drosophila imaginal disks of selected stage and gene. Vizualize results.
"""
function test_drosophila_disks()
    stage = 1
    gene = "CG10230"
    spedge = 30#15
    regular = 20
    k = 2
    const dros = [(249. +254+248+253)/4 (254. +254+253+253)/4 (254. +254+255+254)/4; # 1-pozadi
                  (213. +227+214+229)/4 (225. +235+232+242)/4 (239. +245+238+248)/4; # 2-popredi
                 ]' / 255
    maxd = 20
    blab = false
    repeatKM = 5
    minimgsize = 64
    minkpts = 1000
    largerot = false
    contwidth = .7
    if !isdir("res/")
        mkdir("res/")
    end
    basedir = string(get_medical_dir(), "microscopy/drosophila/")
    listfile = string(basedir, "all_disc_image_info_for_prague.txt")
    fstr = open(string(listfile), "r")
    fileinfo = readdlm(fstr, '\t')
    close(fstr)
    first = true
    second = true
    imgbaset = []
    imgmeanreg = []
    imgmeanorig = []
    segmeanreg = []
    segmeanorig = []
    filcnt = 0
    for filinx = 1:size(fileinfo, 1)
        if fileinfo[filinx, 6] == stage && fileinfo[filinx, 3] == gene
            filcnt += 1
            println(fileinfo[filinx, 8])
            if first
                imgbaset = imread(string(basedir, "disc_images/"*fileinfo[filinx, 8]))
                figure(1) ; clf()
                imshow(separate(imgbaset)); #title("static image")
                axis("off")
                savestring = string("res/stage$(stage)_$(gene)_img_disc_$filcnt.pdf")
                savefig(savestring)
                imgmeanreg = Images.Image(zeros(RGB{Float64}, size(imgbaset)))
                imgmeanorig = Images.Image(zeros(RGB{Float64}, size(imgbaset)))
                imgmeanreg += imgbaset
                imgmeanorig += imgbaset
                first = false
                continue
            end
            imgregt = imread(string(basedir, "disc_images/"*fileinfo[filinx, 8]))
            figure(1) ; clf()
            imshow(separate(imgregt)); #title("movimg image")
            axis("off")
            savestring = string("res/stage$(stage)_$(gene)_img_disc_$filcnt.pdf")
            savefig(savestring)
            # Find common size and pad
            sz = larger_size(size(imgbaset), size(imgregt))
            imgbase = extend_to_size(imgbaset, sz, (imgbaset[2, 2] + imgbaset[2, end - 1] + imgbaset[end - 1, end - 1] + imgbaset[end - 1, 2]) / 4)
            imgreg = extend_to_size(imgregt, sz, (imgregt[2, 2] + imgregt[2, end - 1] + imgregt[end - 1, end - 1] + imgregt[end - 1, 2]) / 4)

            # Segment base image
            lb, count = SLICsuperpixels.slicSize(imgbase, spedge^2, regular, blab, UInt16)
            fbase = SLICsuperpixels.getMeans(lb, imgbase, count)
            nhoodSP = SLICsuperpixels.getNeighbors(lb, count)
            lbase = []
            pbase = []
            lcnt = 0
            # while length(lbase) == 0 && lcnt < 3
                # lbase, pbase = segimgtools.kmeans_segmentation(lb, fbase, k, repeatKM)
                # lcnt += 1
            # end
            lbase, pbase = segimgtools.prototype_segmentation(lb, fbase, dros)
            # Segment second image
            lr, count = SLICsuperpixels.slicSize(imgreg, spedge^2, regular, blab, UInt16)
            freg = SLICsuperpixels.getMeans(lr, imgreg, count)
            lreg = []
            preg =[]
            lcnt = 0
            # while length(lreg) == 0 && lcnt < 3
                # lreg, preg = segimgtools.kmeans_segmentation(lr, freg, k, repeatKM)
                # lcnt += 1
            # end
            lreg, preg = segimgtools.prototype_segmentation(lr, freg, dros)
            figure(3) ; clf()
            imshow(preg'); #title("movimg segmentation")
            axis("off")
            jet()
            savefig("res/stage$(stage)_$(gene)_seg_disc_$filcnt.pdf")
            gray()
            savefig("res/stage$(stage)_$(gene)_seg_disc_$(filcnt)_bw.pdf")
            if second
                figure(4) ; clf()
                imshow(pbase'); #title("static segmentation");
                axis("off")
                jet()
                savefig("res/stage$(stage)_$(gene)_seg_disc_1.pdf")
                gray()
                savefig("res/stage$(stage)_$(gene)_seg_disc_1_bw.pdf")
                
                figure(4) ; clf()
                imshow(separate(imgbaset)); #title("static")
                axis("off")
                contour(pbase', k, colors = "black", linewidths = contwidth)
                savefig("res/stage$(stage)_$(gene)_img_disc_1_cont.pdf")
            end
            figure(5) ; clf()
            imshow(separate(imgregt)); #title("moving")
            axis("off")
            contour(preg', k, colors = "black", linewidths = contwidth)
            savefig("res/stage$(stage)_$(gene)_img_disc_$(filcnt)_cont.pdf")

            # Init registration
            kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(lb, lbase, nhoodSP)
            # ikpts=keypoints.create_image_with_keypoints(pbase, lb, kpts, k;llen= 15, bsize = 7)
            # figure(3) ; clf()
            # imshow(separate(ikpts)); #title("movimg segmentation")
            # axis("off")
            # @bp
            # savestring = string("res/stage$(stage)_$(gene)_kpts_disc_mov.pdf")
            # savefig(savestring)
            isize=[size(preg)...]
            def=deformation.create_identity(isize .* Images.pixelspacing(preg), deformation.RigidDeformation2D)
            w = @debugtime( mil.MIL_weights(pbase, preg, k), "Computing MIL weights:")
            # Register
            d = finereg.register_fast_multiscale(pbase, preg, kpts, k, def, nhoodKP, w, @options hmax = maxd minsize = minimgsize minKP = minkpts largerotation = largerot resample = true)
            warpedimg=deformation.transform_image_nn(d, imgreg, size(imgreg), @options)
            imgmeanreg += warpedimg
            imgmeanorig += imgreg
            if second
                segmeanreg = pbase
                segmeanorig = pbase
                second = false
            end
            warpedseg=deformation.transform_image_nn(d, preg, size(preg), @options default = 1)
            segmeanreg += warpedseg
            segmeanorig += preg
            figure(1) ; clf()
            imshow(Images.separate(imgmeanorig)' / filcnt); #title("original")
            axis("off")
            figure(2) ; clf()
            imshow(Images.separate(imgmeanreg)' / filcnt); #title("registered")
            axis("off")
        end
    end
    println(filcnt)
    imgmeanorig = separate(imgmeanorig) / filcnt
    imgmeanorig = imgmeanorig - minimum(imgmeanorig)
    imgmeanorig = imgmeanorig / maximum(imgmeanorig)
    figure(1) ; clf()
    imshow(imgmeanorig'); #title("original")
    axis("off")
    savefig("res/stage$(stage)_$(gene)_img_disc_orig.pdf")
    contour(segmeanorig', colors = "black", linewidths = contwidth)
    savefig("res/stage$(stage)_$(gene)_img_disc_orig_cont.pdf")
    
    imgmeanreg = separate(imgmeanreg) / filcnt
    imgmeanreg = imgmeanreg - minimum(imgmeanreg)
    imgmeanreg = imgmeanreg / maximum(imgmeanreg)
    figure(2) ; clf()
    imshow(imgmeanreg'); #title("registered")
    axis("off")
    savefig("res/stage$(stage)_$(gene)_img_disc_reg.pdf")
    contour(segmeanreg', colors = "black", linewidths = contwidth)
    savefig("res/stage$(stage)_$(gene)_img_disc_reg_cont.pdf")
    
    segmeanorig = separate(segmeanorig) / filcnt
    figure(3) ; clf()
    imshow(segmeanorig'); #title("original")
    axis("off")
    jet()
    savefig("res/stage$(stage)_$(gene)_seg_disc_orig.pdf")
    gray()
    savefig("res/stage$(stage)_$(gene)_seg_disc_orig_bw.pdf")
    segmeanreg = separate(segmeanreg) / filcnt
    figure(4) ; clf()
    imshow(segmeanreg'); #title("registered")
    axis("off")
    jet()
    savefig("res/stage$(stage)_$(gene)_seg_disc_reg.pdf")
    gray()
    savefig("res/stage$(stage)_$(gene)_seg_disc_reg_bw.pdf")
end

"""
    test_drosophila_disks_segmented()
Register imaginal disk segmentations (segmented by Jirka Borovec), save registered segmentations
and RGB images plus average 2-class segmentation.
"""
function test_drosophila_disks_segmented()
    hmax = [40 50 50 50]
    eqcls = (Array(Int, 0, 0), Array(Int, 0, 0), Array(Int, 0, 0), [1 2; 1 2; 1000 1000])
    minKP = 200
    kpdist = 16
    overlapfile = "overlap.csv"
    for stage = 1:4
        basedir = "m:/microscopy/drosophila/RESULTS/type_$(stage)_segm/"
        # basedir = "m:/microscopy/drosophila/real_segmentations/segmentation_2class_stage_$stage/"
        # basedir = "c:/Temp/droso/segM/"
        if !isdir(basedir)
            # basedir = "/datagrid/Medical/microscopy/drosophila/real_segmentations/segmentation_2class_stage_$stage/"
            basedir = "/datagrid/Medical/microscopy/drosophila/RESULTS/type_$(stage)_segm/"
        end
        rgbdir = "m:/microscopy/drosophila/disc_types/type_$stage/"
        # rgbdir = "c:/Temp/droso/orig/"
        if !isdir(rgbdir)
            rgbdir = "/datagrid/Medical/microscopy/drosophila/disc_types/type_$stage/"
        end
        savedir = "/datagrid/Medical/microscopy/drosophila/TEMPORARY/type_$(stage)_segm_reg/"
        # savedir = "/datagrid/Medical/microscopy/drosophila/real_segmentations/segmentation_registered_2class_stage_$stage/"
        # savedir = "m:/microscopy/drosophila/real_segmentations/segmentation_registered_2class_stage_$stage/"
        savedir = "c:/Temp/droso/register/"
        if !isdir(savedir)
            mkdir(savedir)
        end
        savedirRGB = "/datagrid/Medical/microscopy/drosophila/TEMPORARY/type_$(stage)_RGB_reg/"
        # savedirRGB = "/datagrid/Medical/microscopy/drosophila/real_segmentations/original_registered_stage_$stage/"
        # savedirRGB = "m:/microscopy/drosophila/real_segmentations/original_registered_stage_$stage/"
        savedirRGB = "c:/Temp/droso/registerRGB/"
        if !isdir(savedirRGB)
            mkdir(savedirRGB)
        end
        fstr = open(string(savedir, overlapfile), "w")
        write(fstr, "Images, Different pixels\n")
        close(fstr)
        # listfiles = sort(readdir(basedir))
        fstr = open(string(basedir, "segm_good.csv"), "r")
        listfiles = readcsv(fstr; header = true)[1]
        # listfiles = ["insitu113811.png"]
        close(fstr)
        first = false
        # imgbase = Images.Image([1])
        # kpts = []
        # nhoodKP = []
        imgbase = Images.load(string("c:/Temp/droso/templateST$stage.png"))
        # imgbase = convert(Images.Image{Images.Gray}, Images.load(string("/datagrid/Medical/microscopy/drosophila/registration_templates/templateST$stage.png")))
        imgbase = Images.Image(reinterpret(UInt8,data(imgbase)), Dict{Any,Any}("spatialorder"=>spatialorder(imgbase), "pixelspacing"=>pixelspacing(imgbase)))
        cnt = 0
        meanbase = [0; 0]
        # convert from 3 to 2-class
        for y = 1:size(imgbase, 2)
            for x = 1:size(imgbase, 1)
                if imgbase[x, y] > 0 
                    imgbase[x, y] = 1
                    meanbase[1] += x
                    meanbase[2] += y
                    cnt += 1
                end
            end
        end
        meanbase ./= cnt
        # average segmentation
        imgavg = Images.Image(zeros(Float64, size(imgbase)), Dict{Any,Any}("spatialorder"=>spatialorder(imgbase), "pixelspacing"=>pixelspacing(imgbase)))
        imgbase += 1
        first = true
        kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(imgbase, kpdist)
        img1kpts=keypoints.create_image_with_keypoints(imgbase,kpts;llen=hmax[stage])
        ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
        # ImageView.view((imgbase - minimum(imgbase)) /(maximum(imgbase) - minimum(imgbase)))
        # filesinds = [126]
        for filinx = 1:size(listfiles, 1)
            if listfiles[filinx][end-2:end] != "png" || listfiles[filinx][1:4] =="visu"
                continue
            end
            # @bp
            println(string("Registering: ", listfiles[filinx]))
            imgreg2 = convert(Images.Image{Images.Gray},Images.load(string(basedir, listfiles[filinx])))
            imgreg = Images.Image(reinterpret(UInt8,copy(data(imgreg2))), Dict{Any,Any}("spatialorder"=>spatialorder(imgreg2), "pixelspacing"=>pixelspacing(imgreg2)))
            # convert from 3 to 2-class
            meanreg = [0; 0]
            cnt = 0
            for y = 1:size(imgreg, 2)
                for x = 1:size(imgreg, 1)
                    if imgreg[x, y] > 0 
                        imgreg[x, y] = 1
                        meanreg[1] += x
                        meanreg[2] += y
                        cnt += 1
                    end
                end
            end
            meanreg ./= cnt
            imgreg += 1
            # ImageView.view((imgreg - minimum(imgreg)) /(maximum(imgreg) - minimum(imgreg)))
            # @bp

            # Init registration
            isize=[size(imgreg)...]
            pxsp=Images.pixelspacing(imgreg)
            # [#deformation.BsplineDeformation(3, isize .* pxsp, [67,67]);
            # deformation.BsplineDeformation(3, isize .* pxsp, [19,19]);
            # deformation.BsplineDeformation(3, isize .* pxsp, [7,7]);
            # deformation.create_identity(isize .* pxsp, deformation.ScaleDeformation2D)]
            if stage == 1 || stage == 4
                aa = [ deformation.BsplineDeformation(3, isize .* pxsp, [19,19]);
                       deformation.BsplineDeformation(3, isize .* pxsp, [7,7]);
                       deformation.create_identity(isize .* pxsp, deformation.ScaleDeformation2D)];
                deformation.set_theta!(aa[3], [0; meanreg - meanbase; 1])
                if any(deformation.lower_bound(aa[3])[2:3] .>  meanreg-meanbase) || any(deformation.upper_bound(aa[3])[2:3] .< meanreg-meanbase)
                    continue
                end
            elseif stage == 2 || stage == 3
                aa = [ deformation.BsplineDeformation(3, isize .* pxsp, [19,19]);
                       deformation.BsplineDeformation(3, isize .* pxsp, [7,7]);
                       deformation.create_identity(isize .* pxsp, deformation.AffineDeformation2D)];
                deformation.set_theta!(aa[3], [1; 0; 0; 1; meanreg-meanbase])
                if any(deformation.lower_bound(aa[3])[5:6] .>  meanreg-meanbase) || any(deformation.upper_bound(aa[3])[5:6] .< meanreg-meanbase)
                    continue
                end
            end
            def = deformation.HierarchicalDeformation(aa)

            crit_params=mil.MILParameters(2)
            opt_state=optimizer.OptimizerMMA(abstol=1e-3,neval=5000,initstep=2.0)
            # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-3,neval=50000,initstep=20.0)
            reg_params = regularization.NoneRegularizationParams()
            # reg_params = regularization.SumRegularizationParams([1e-5, 0, 0])

            resampleFactor= [1,0,1,0,1]
            levelProg=      [0,1,0,1,0]
            
            # Register
            d = @debugtimeline(finereg.register_fast_multiscaleHB( imgbase, imgreg, kpts, def, nhoodKP, crit_params,
                               opt_state, reg_params, resampleFactor, levelProg,
                               @options hmax = hmax[stage] minKP = minKP largerotation = false resample = true equivalentclasses = eqcls[stage]), "Multiscale registration:")

            # update average segmentation
            warpedimg=deformation.transform_image_nn(d, imgreg, size(imgbase), @options)
            imgavg += warpedimg
            # ImageView.view((warpedimg - minimum(warpedimg)) /(maximum(warpedimg) - minimum(warpedimg)))
            
            # save transformed RGB image
            rgbreg = Images.load(string(rgbdir, listfiles[filinx]))
            warpedimg=deformation.transform_image_nn(d, rgbreg, size(imgbase), @options)
            # ImageView.view(warpedimg)
            Images.save(string(savedirRGB, listfiles[filinx]), warpedimg)
            
            # # save transformed 3-class segmentation
            warpedimg=deformation.transform_image_nn(d, imgreg2, size(imgbase), @options)
            # ImageView.view(warpedimg)
            Images.save(string(savedir, listfiles[filinx]), warpedimg)
            difference = 0
            for px = 1:length(warpedimg)
                if (warpedimg[px] == 0 && imgbase[px] == 2) || (warpedimg[px] > 0 && imgbase[px] == 1)
                    difference += 1
                end
            end
            fstr = open(string(savedir, overlapfile), "a")
            write(fstr, string(listfiles[filinx], ", ", difference, "\n"))
            close(fstr)
        end
        #save average 2-class segmentation
        Images.save(string(savedir, "mean.png"), Images.grayim(reinterpret(FixedPointNumbers.Normed{UInt8,8}, round(UInt8,data(imgavg/maximum(imgavg)*255)))))
        # ImageView.view(imgavg/maximum(imgavg))
    end
end


function test_rire_transform()
    basedir = string(get_medical_dir(), "RIRE/")
    # read landmarks
    land = readdlm(string(basedir, "training_001/transformations.csv/ct_pd.csv"), ',', '\n'; skipstart = 1)
    # the first parameter is the image size in mm
    d=deformation.create_identity([334.,334.,119.], deformation.RigidDeformation3D)
    deformation.fit_landmarks(d,land[:,2:4]',land[:,5:7]')
    #d.A= [  0.9954 -0.0945 -0.0168 2.7764;#forward found from landmarks
    #        0.0950 0.9949 0.0330 41.8252;
    #        0.0136 -0.0345 0.9993 25.9995]
    #d.A = [ 0.9954    0.0950    0.0136   -7.0899;#inverse found from landmarks
    # -0.0945    0.9949   -0.0345  -40.4540;
    # -0.0168    0.0330    0.9993  -27.3172]

    println("Geometric error for the true deformation")
    for i = 1:size(land,1)
        # coordinates in the CT image
        lct=vec(land[i,2:4])
        lpd=vec(land[i,5:7])
        lw=zeros(size(lct)) # transformed point
        deformation.transform_point!(d,lct,lw)
        dl=sqrt(sum(((lw-lpd)).^2))
        println("lnd $i: $(lct) -> $(lw) != $(lpd). $(sqrt(sum(((lct-lpd)).^2))) -> $(dl)")
    end
    # @show d.A

    # read the two images
    @debugprintln("Reading RIRE images.")
    img2 = readRIRE.read_data(string(basedir, "training_001/ct"));
    img1 = readRIRE.read_data(string(basedir, "training_001/mr_PD"));
    @debugprintln("Deforming PD image.")
    img1w=Images.similar(img2)
    deformation.transform_image_nn!(d, img1w, img1, @options)
    @debugprintln("Visualizing registration results.")
    # normalize and convert to gray scale images
    img2g=convert(Images.Image{Images.Gray},Images.sc(img2))
    img1wg=convert(Images.Image{Images.Gray},Images.sc(img1w))
    ov = segimgtools.overlay(img2g,img1wg)
    ImageView.view(Images.sc(img1wg))
    ImageView.view(ov)
    ImageView.view(ov,xy=["x","z","y"])
    ImageView.view(ov,xy=["y","z","x"])
end



end #module 
