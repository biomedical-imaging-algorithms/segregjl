"""
    Experiments with images from Jiri Borovec for
    deformable binary patterns


    Jan Kybic, 2018
"""

module test_binarypatterns

import Images
import ImageView
using Options

using linprog_register
import ssd
import keypoints
import deformation
import segimgtools
import optimizer
import regularization
import finereg
import SLICsuperpixels
import mil
import gaussianMI
#using MicroLogging
using Logging
using Colors
import Winston
import register_patterns

configure_logging(min_level=:debug)

include("debugprint.jl")
switchprinting(true)
include("debugassert.jl")
switchasserts(true)


function register_patterns_seg()
    ImageView.closeall()
    img1=Images.load("imgs/reference.png")
    #img1=Gray.(Images.load("imgs/fuzzyInput.png")) # for some reason, ended up as AGray
    rotangle=0.
    shift=[3.;0.]
    c = deformation.RigidDeformation2D([size(img1)...] .* Images.pixelspacing(img1), vcat(rotangle,shift))
    #img2=deformation.transform_image_lin(c,img1,size(img1),@options default = img1[1])
    img2=Gray.(Images.load("imgs/fuzzyInput.png")) # for some reason, ended up as AGray
    spacing=2 ; k = 2; spedge = 3 ; compact=1.0
    t1=time()
    s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^2, compact, k  ), "Superpixels and k-means moving image")   #segment using superpixels and k_means
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k  ), "Superpixels and k-means static image")   #segment using superpixels and k_means
    println("count=",count)
    hmax=5 ;
    p1color=segimgtools.classimage2rgb(p1, k)
    ImageView.imshow(p1color, name = "static image segmentation")
    p2color=segimgtools.classimage2rgb(p2, k)
    ImageView.imshow(p2color, name = "moving image segmentation")
    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kptsimple, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")
    img1kpts=keypoints.create_image_with_keypoints(RGB.(img1),kptsimple,llen=hmax,bsize=1,thickness=1,colline=colorant"red",colbox=colorant"white")
    ImageView.imshow(img1kpts,name="with keypoints")
    imgovlbefore=deformation.overlay(img1,img2)
    ImageView.imshow(imgovlbefore,name="overlay before")
    segovlbefore=deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(p2, k))
    ImageView.imshow(segovlbefore, name = "segmentation overlay before")
    img1segkpts=keypoints.create_image_with_keypoints(segimgtools.classimage2rgb(p1, k),kptsimple, llen=hmax,bsize=3,thickness=2,colline=colorant"white",colbox=colorant"black")
    ImageView.imshow(img1segkpts, name = "segmentation with keypoints")
    kptsampled=@debugtime( keypoints.create_sampled_keypoints(p1,kptsimple,hmax;spacing=float(spacing)), "create_sampled_keypoints")
    isize=[size(img1)...]
    imgsize=float(isize) .* Images.pixelspacing(img1) # image size in physical units
    critparams=mil.MILParameters(k)        #mutual information
    opts=@options hmax=hmax resample=true fdiffreg=1e-9 lambda=1e-9 maxResample=5 minKP=500 display=false maxsize=1000 minsize=128 knot_spacing=8 start_with_last_level=false
    bc=8
    def=deformation.BsplineDeformation(3,imgsize, [bc,bc])
    d=register_fast_linprog_multiscale(p1,p2,kptsampled,def,nhoodKP,critparams,opts)
    #d=linprog_register.register_fast_linprog(img1,img2,kptsampled,def,critstate,opts)
    warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    p2w=@debugtime(deformation.transform_image_nn(d,p2,size(img1),@options),"warping segmentation")
    t2=time()
    ImageView.imshow(warped,name="warped")
    imgovlafter=deformation.overlay(img1,warped)
    ImageView.imshow(imgovlafter,name="overlay after")
    warpedseg=segimgtools.classimage2rgb(p2w,k)
    segovlafter=deformation.overlay(p1,p2w)
    ImageView.imshow(segovlafter,name="segmentationoverlay after")

    println("Time: registration + warping $(t2-t1)")
end


function register_patterns1()
    # uses SampledKeypoints
    ImageView.closeall()
    img1=Images.load("imgs/reference.png")
    #img1=Gray.(Images.load("imgs/fuzzyInput.png")) # for some reason, ended up as AGray
    img2=Gray.(Images.load("imgs/fuzzyInput.png")) # for some reason, ended up as AGray
    rotangle=4. ; shift=[3.;-2.]
    #shift=[1.;-3]
    c = deformation.RigidDeformation2D([size(img2)...] .* Images.pixelspacing(img2), vcat(rotangle,shift))
    img2=deformation.transform_image_lin(c,img2,size(img2),@options default = img2[1])


    ImageView.imshow(img1,name="image 1")
    ImageView.imshow(img2,name="image 2")
    hmax=5
    spacing=2 ; kptn=50
    t1=time()
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kptn,
                                                                     sigma=[1.;1.]),"find simple keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")
    kptsampled=@debugtime( keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing)), "create_sampled_keypoints")
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple,llen=hmax,bsize=1,thickness=1,colline=colorant"red",colbox=colorant"black")
    ImageView.imshow(img1kpts,name="img1 with keypoints")
    imgovlbefore=deformation.overlay(img1,img2)
    ImageView.imshow(imgovlbefore,name="overlay before")
    isize=[size(img1)...]
    imgsize=float(isize) .* Images.pixelspacing(img1) # image size in physical units
    bc=6
    def=deformation.BsplineDeformation(3,imgsize, [bc,bc])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    opts=@options hmax=hmax resample=true fdiffreg=1e-9 lambda=1e-9 maxResample=5 minKP=100 display=false maxsize=1000 minsize=32 knot_spacing=spacing start_with_last_level=false
    critparams=ssd.SSDParameters()
    #critstate=ssd.criterion_init(img1,img2,critparams)
    d=register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts)
    #d=linprog_register.register_fast_linprog(img1,img2,kptsampled,def,critstate,opts)
    warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    ImageView.imshow(warped,name="warped")
    imgovlafter=deformation.overlay(img1,warped)
    t2=time()
    ImageView.imshow(imgovlafter,name="overlay after")
    ImageView.imshow(float(warped.>0.5)-img1,name="dif.seg.after")
    println("Time: registration + warping $(t2-t1)")
end

    
function show_shifts()
    # show the dependence of the criterion on shifts
    ImageView.closeall()
    img1=Images.load("imgs/reference.png")
    img2=Gray.(Images.load("imgs/fuzzyInput.png")) # for some reason, ended up as AGray
    hmax=5
    spacing=6 ; kptn=50
    t1=time()
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kptn,
                                                                     sigma=[1.;1.]),"find simple keypoints")
    kpts=@debugtime( keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing)), "create_sampled_keypoints")
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kpts,llen=hmax,bsize=1,thickness=1,colline=colorant"red",colbox=colorant"black")
    ImageView.imshow(img1kpts,name="img1 with keypoints")
    critstate=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    refdef=deformation.RigidDeformation2D([size(img1)...],[0.;0.;0.])
    kptsg=finereg.transform_keypoints(kpts,Nullable(refdef))
    defaultval=img2[1]   #zero(typeof(img2[1]))
    contributions=finereg.precalculate_contributions(img2,kpts,kptsg,critstate,hmax)
    function eval_shift(h)
        y=finereg.eval_contributions([0.;h;0.],refdef,kpts,kptsg,contributions,Float64[])
        # @printf("h=%6f    y=%6f\n",h,y)
        return y
    end
    t=collect(range(-10,0.5,41))
    ys=map(eval_shift,t)
    tw=Winston.Table(4,1)
    #p = Winston.FramedPlot(title="shift SSD",
    #        xlabel="shift",
    #        ylabel="criterion" #,yrange=(0.,0.15)
    #                       )
    #Winston.add(p, Winston.Curve(t,ys,yaxis=(0.,0.5)))
    p1=Winston.plot(t,ys,title="shift SSD",xlabel="shift",ylabel="criterion",yrange=(0.0,0.5))
    #@show kpts[1].pos
    @show kpts[1]
    #@show kpts[1].normal
    #@show kptsg[1].pos
    #@show kptsg[1].normal
    maxdist=hmax
    tv=0.5-maxdist:maxdist-0.5
    p2=Winston.plot(tv,real(kpts[1].pixels),title="kpts","o-")
    pxsp=Images.pixelspacing(img2)
    v=get(keypoints.sample_normal(img2,kptsg[1],hmax,pxsp,defaultval) )
    p3=Winston.plot(tv,real(v),title="kptsg")
    p3=Winston.oplot(tv,real(kpts[1].pixels),"g-")
    p4=Winston.plot(-hmax:hmax,contributions[1],title="Dv")
    tw[1,1]=p1
    tw[2,1]=p2
    tw[3,1]=p3
    tw[4,1]=p4
    Base.display(tw)
end

function register_patterns2()
    # like register_patterns1 but using module register_patterns
    ImageView.closeall()
    img1=Images.load("imgs/reference.png")
    #img1=Gray.(Images.load("imgs/fuzzyInput.png")) # for some reason, ended up as AGray
    img2=Gray.(Images.load("imgs/fuzzyInput.png")) # for some reason, ended up as AGray
    rotangle=4. ; shift=[3.;-2.]
    #shift=[1.;-3]
    fdiffreg=1e-9
    c = deformation.RigidDeformation2D([size(img2)...] .* Images.pixelspacing(img2), vcat(rotangle,shift))
    img2=deformation.transform_image_lin(c,img2,size(img2),@options default = img2[1])
    ImageView.imshow(img1,name="image 1")
    ImageView.imshow(img2,name="image 2")
    def=register_patterns.register_pattern(img1,img2,fdiffreg)
    warped=register_patterns.deform_patterns(img1,img2,def)
    imgovlafter=deformation.overlay(img1,warped)
    ImageView.imshow(imgovlafter,name="overlay after")
    invdef=register_patterns.inverse_transformation(def,warped,img2,[8,8],1e-3)
    invwarped=register_patterns.deform_patterns(img2,img1,invdef)
    ImageView.imshow(invwarped,name="inverse warped")
    imgovlafterinv=deformation.overlay(invwarped,img2)
    ImageView.imshow(imgovlafterinv,name="overlay after inv")
    return 
end


end
