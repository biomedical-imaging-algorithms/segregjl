"""
Scripts for experiments with linprog_register

Jan Kybic, 2016-2018
"""
module test_linprog_register

#import ImageMagick # must be first
import Images
import ImageView
using Options

using linprog_register
import ssd
import keypoints
import deformation
import segimgtools
import optimizer
import regularization
import finereg
import SLICsuperpixels
import mil
import gaussianMI
#using MicroLogging
using Logging
using Colors
import ImageFiltering
#using Profile

#configure_logging(min_level=:debug)


include("debugprint.jl")
switchprinting(true)
include("debugassert.jl")
switchasserts(true)


# test piecewise linear approximation
 function test_register_fast_linprog()
    img2=Images.load("imgs/simple.png")
    @debugprintln("Image loaded") 
    rotangle=15. ; shift=[11.;-5]
    #rotangle=5. ; shift=[5.;-2.]
    #rotangle=0. ; shift=[5.;-2]
    img1=deformation.transform_rigid2D(img2,size(img2),rotangle,shift, @options default = img2[1])
    @debugprintln("Image transformed") 
    #ImageView.view(img1,name="image 1")
    #ImageView.view(img2,name="image 2")
    #spacing=10
     spacing=10
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=50,sigma=[1.;1.]),"find simple keypoints")
    #kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=5,sigma=[1.;1.]),"find simple keypoints")
    hmax=20
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.imshow(img1kpts,name="with keypoints")
    ImageView.imshow(deformation.overlay(img1,img2),name="overlay before")
    @debugprintln("Initialize criterion")
    @time crit_state=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    @debugprintln("Registering")
    opts=@options hmax=hmax resample=true
    d=register_fast_linprog(img1,img2,kptsampled,def,crit_state,opts)
    #Profile.clear()
    #Profile.print(format=:flat,sortedby=:count)
    r=deformation.get_theta(d)
    println("Registration result: $r true (rigid) values: $rotangle $shift")
    warped=@debugtime(deformation.transform_image_nn(d,img2,size(img1),@options),"warping")
    ImageView.imshow(deformation.overlay(img1,warped),name="overlay after")
    return 
end

function test_register_lena_linprog()
    img2=Images.load("imgs/lena.png")
    #rotangle=15. ; shift=[11.;-5]
    rotangle=5. ; shift=[5.;-2.]
    #rotangle=0. ; shift=[5.;0]
    spacing=10
    img1=deformation.transform_rigid2D(img2,size(img2),rotangle,shift, @options default = img2[1])
    t0=time()
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=100,sigma=[1.;1.]),"find simple keypoints")
    #kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=5,sigma=[1.;1.]),"find simple keypoints")
    hmax=20
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.imshow(img1kpts,name="with keypoints")
    ImageView.imshow(deformation.overlay(img1,img2),name="overlay before")
    @debugprintln("Initialize criterion")
    @time crit_state=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    @debugprintln("Registering")
    opts=@options hmax=hmax resample=true lambda=1e-3
    t2=time()
    d=register_fast_linprog(img1,img2,kptsampled,def,crit_state,opts)
    #Profile.clear()
    #Profile.print(format=:flat,sortedby=:count)
    r=deformation.get_theta(d)
    t1=time()
    println("Registration times: $(t1-t2) $(t2-t0)")
    warped=@debugtime(deformation.transform_image_nn(d,img2,size(img1),@options),"warping")
    ImageView.imshow(deformation.overlay(img1,warped),name="overlay after")
    return
end # test_register_lena_linprog

function test_bspline_iterator()
     img1=Images.load("imgs/lena.png")
     kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=10,number=100,sigma=[1.;1.]),"find simple keypoints")
     bc=4 # number of B-splines
     def = deformation.BsplineDeformation(3,[size(img1)...] .* map(float,Images.pixelspacing(img1)), [bc,bc])
     theta0=deformation.get_theta(def)
     theta0[15]=100.
     theta0[16]=-50.
     i=50 # which keypoint
     println("Keypoint $i: ",kptsimple[i].pos)
     deformation.set_theta!(def,theta0)
     deformation.set_keypoints!(def,kptsimple)
     outp=deformation.transform_point(def,kptsimple[i].pos)
     println("Transformed to: ",outp)
     deformation.transform_point_kpt!(def,outp,kptsimple,i)
     println("Transform_point_kpt!: ",outp)
     deformation.transform_point_kpt_lin!(def,outp,kptsimple,i)
     println("Transform_point_kpt_lin!: ",outp)
end


function test_register_bsplines_lena_linprog()
    ImageView.closeall()
    img2=Images.load("imgs/lena.png")
    #rotangle=15. ; shift=[11.;-5]
    rotangle=5. ; shift=[5.;-2.]
    #rotangle=0. ; shift=[5.;0]
    spacing=10
    img1=deformation.transform_rigid2D(img2,size(img2),rotangle,shift, @options default = img2[1])
    #img1=finereg.reduce_image(img1,[4,4])
    #img2=finereg.reduce_image(img2,[4,4])
    t0=time()
    #kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=100,sigma=[1.;1.]),"find simple keypoints")
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=250,sigma=[1.;1.]),"find simple keypoints")
    #kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=5,sigma=[1.;1.]),"find simple keypoints")
    hmax=40
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    #img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    #ImageView.imshow(img1kpts,name="with keypoints")
    ImageView.imshow(deformation.overlay(img1,img2),name="overlay before")
    @debugprintln("Initialize criterion")
    @time crit_state=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    isize=[size(img1)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    bc=8
    def=deformation.BsplineDeformation(3,float([size(img2)...] .* Images.pixelspacing(img2)), [bc,bc])
    @debugprintln("Registering")
    opts=@options hmax=hmax resample=true lambda=1e-3 maxResample=20
    #opts=@options hmax=hmax resample=true lambda=nothing maxResample=20
    #opts=@options hmax=hmax resample=true lambda=0.1 maxResample=20
    #opts=@options hmax=hmax resample=true lambda=nothing fdiffreg=1e-6 maxResample=20
    #opts=@options hmax=hmax resample=true lambda=nothing fdiffreg=1e-6 maxResample=20
    t1=time()
    d=register_fast_linprog(img1,img2,kptsampled,def,crit_state,opts)
    #Profile.clear()
    #Profile.print(format=:flat,sortedby=:count)
    r=deformation.get_theta(d)
    t2=time()
    println("Registration time: preprocessing: $(t1-t0) Time: registration $(t2-t1)")
    warped=@debugtime(deformation.transform_image_nn(d,img2,size(img1),@options),"warping")
    #ImageView.imshow(warped,name="warped")
    ImageView.imshow(deformation.overlay(img1,warped),name="overlay after")
    return
end # test_register_bsplines_lena_linprog


function test_register_bsplines_lena_linprog_multiscale()
    ImageView.closeall()
    img2=Images.load("imgs/lena.png")
    #rotangle=15. ; shift=[11.;-5]
    rotangle=5. ; shift=[5.;-2.]
    #rotangle=0. ; shift=[5.;0]
    spacing=10
    img1=deformation.transform_rigid2D(img2,size(img2),rotangle,shift, @options default = img2[1])
    t0=time()
    #kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=100,sigma=[1.;1.]),"find simple keypoints")
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=500,sigma=[1.;1.]),"find simple keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")

    #kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=5,sigma=[1.;1.]),"find simple keypoints")
    hmax=8
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple)
    ImageView.imshow(img1kpts,name="with keypoints")
    ImageView.imshow(deformation.overlay(img1,img2),name="overlay before")
    isize=[size(img1)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    bc=8
    def=deformation.BsplineDeformation(3,float([size(img2)...] .* Images.pixelspacing(img2)), [bc,bc])
    @debugprintln("Registering")
    #opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=20 minsize=64 minKP=1000 display=true maxsize=64
    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=20 minsize=64 minKP=1000 display=false maxsize=2048
    #opts=@options hmax=hmax resample=true lambda=nothing maxResample=20
    #opts=@options hmax=hmax resample=true lambda=0.1 maxResample=20
    #opts=@options hmax=hmax resample=true lambda=nothing fdiffreg=1e-6 maxResample=20
    critparams=ssd.SSDParameters()
    t1=time()
    d=register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts)
    #Profile.clear()
    #Profile.print(format=:flat,sortedby=:count)
    r=deformation.get_theta(d)
    t2=time()
    println("Time preprocessing: $(t1-t0) Time: registration $(t2-t1)")
    warped=@debugtime(deformation.transform_image_nn(d,img2,size(img1),@options),"warping")
    #ImageView.imshow(warped,name="warped")
    ImageView.imshow(deformation.overlay(img1,warped),name="overlay after")
    return 
end # test_register_bsplines_lena_linprog_multiscale

function test_register_bsplines_lena_linprog_multiscale_hierarchical()
    ImageView.closeall()
    img2=Images.load("imgs/lena.png")
    rotangle=15. ; shift=[11.;-5]
    #rotangle=5. ; shift=[5.;-2.]
    #rotangle=0. ; shift=[5.;0]
    spacing=10
    img1=deformation.transform_rigid2D(img2,size(img2),rotangle,shift, @options default = img2[1])
    t0=time()
    #kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=100,sigma=[1.;1.]),"find simple keypoints")
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=250,sigma=[1.;1.]),"find simple keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")

    #kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=5,sigma=[1.;1.]),"find simple keypoints")
    hmax=4
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple)
    ImageView.imshow(img1kpts,name="with keypoints")
    ImageView.imshow(deformation.overlay(img1,img2),name="overlay before")
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #bc=8
    #def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc,bc])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    imgsize=float([size(img2)...]) .* Images.pixelspacing(img2) # image size in physical units
    #def=deformation.create_identity(imgsize, deformation.AffineDeformation2D)
    def = deformation.HierarchicalDeformation([
       deformation.BsplineDeformation(3,imgsize, [8,8]), 
       deformation.BsplineDeformation(3,imgsize, [4,4]), 
       deformation.create_identity(imgsize, deformation.AffineDeformation2D)
       ])
    #def = deformation.HierarchicalDeformation([deformation.create_identity(imgsize, deformation.AffineDeformation2D)])
    @debugprintln("Registering")
    #opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=20 minsize=64 minKP=1000 display=true maxsize=64
#    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=20 minsize=64 minKP=1000 display=false maxsize=2048 knot_spacing=8
    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=20 minKP=1000 display=false maxsize=512 minsize=64 knot_spacing=8

    #opts=@options hmax=hmax resample=true lambda=nothing maxResample=20
    #opts=@options hmax=hmax resample=true lambda=0.1 maxResample=20
    #opts=@options hmax=hmax resample=true lambda=nothing fdiffreg=1e-6 maxResample=20
    critparams=ssd.SSDParameters()
    t1=time()
    @time crit_state=ssd.criterion_init(img1,img2,critparams)
    #d=register_fast_linprog(img1,img2,kptsampled,def,crit_state,opts)
    #d=register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts)
    #Profile.clear()
    d=linprog_register.register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    #Profile.print(format=:flat)
    #r=deformation.get_theta(d)
    t2=time()
    println("Time preprocessing: $(t1-t0) Time: registration $(t2-t1)")
    #warped=@debugtime(deformation.transform_image_nn(d,img2,size(img1),@options),"warping")
    warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    #ImageView.imshow(warped,name="warped")
    ImageView.imshow(deformation.overlay(img1,warped),name="overlay after")
    return
end # test_register_bsplines_lena_linprog_multiscale_hierarchical


function print_keypoints(kptsf,def;n=5)
    kptsg=finereg.transform_keypoints(kptsf,Nullable(def))
    for i = 1 : n
        println(kptsf[i].pos," -> ", kptsg[i].pos)
    end
end
    
# test hierarchical deformation
function test_hierarchical()
    img1=Images.load("imgs/lena.png")
    spacing=10 ; hmax=4
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=250,sigma=[1.;1.]),"find simple keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    isize=[size(img1)...]
    imgsize=isize .* Images.pixelspacing(img1) # image size in physical units
    def = deformation.HierarchicalDeformation([
       deformation.BsplineDeformation(3,imgsize, [4,4]), 
       deformation.create_identity(imgsize, deformation.AffineDeformation2D)
       ])
#    def = deformation.HierarchicalDeformation([
#       deformation.BsplineDeformation(3,imgsize, [4,4]), 
#       deformation.create_identity(imgsize, deformation.AffineDeformation2D)
#       ])
    println("Initial transformation")
    print_keypoints(kptsimple,def,n=5)
    deformation.set_level!(def,2)
    println("Initial transformation level set to 2")
    print_keypoints(kptsimple,def,n=5)
    theta=deformation.get_theta(def)
    println("theta=",theta)
    theta[6]=5.
    deformation.set_theta!(def,theta)
    println("theta set at level 2")
    print_keypoints(kptsimple,def,n=5)
    deformation.set_level!(def,1)
    println("set level 1")
    print_keypoints(kptsimple,def,n=5)
    theta=deformation.get_theta(def)
    println("theta=",theta)
    theta[1]=3.
    deformation.set_theta!(def,theta)
    println("theta set at level 1")
    print_keypoints(kptsimple,def,n=5)
end    
# multiscale


function register_histology()
    ImageView.closeall()
    # register histology images
    img1=Images.load("imgs/case03-3-psap-small.png")   # about 800x600
    img2=Images.load("imgs/case03-5-he-small.png");
    ImageView.imshow(img1, name = "static image")
    ImageView.imshow(img2, name = "moving image")
    spacing=10 ; kptn=500
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kptn,
                                                                     sigma=[1.;1.]),"find simple keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")
    hmax=8
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple)
    ImageView.imshow(img1kpts,name="with keypoints")
    ImageView.imshow(deformation.overlay(img1,img2),name="overlay before")
    isize=[size(img2)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #bc=8
    #def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc,bc])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    imgsize=float(isize) .* Images.pixelspacing(img2) # image size in physical units
    def = deformation.HierarchicalDeformation([
       deformation.BsplineDeformation(3,imgsize, [32,32]), 
       deformation.BsplineDeformation(3,imgsize, [16,16]), 
       deformation.BsplineDeformation(3,imgsize, [8,8]), 
       deformation.create_identity(imgsize, deformation.AffineDeformation2D)
       ])
    #def = deformation.HierarchicalDeformation([
    #   deformation.BsplineDeformation(3,imgsize, [8,8]), 
    #   deformation.create_identity(imgsize, deformation.AffineDeformation2D)
    #   ])
#    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=5 minKP=500 display=false maxsize=16384 minsize=128 knot_spacing=8 start_with_last_level=false
    opts=@options hmax=hmax resample=true fdiffreg=nothing lambda=1e-3 maxResample=5 minKP=500 display=false maxsize=50000 minsize=128 knot_spacing=8 start_with_last_level=false
    critparams=ssd.SSDParameters()
    t1=time()
    @time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d=register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()
    println("Time: registration $(t2-t1)")
    warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    ImageView.imshow(warped,name="warped")
    ImageView.imshow(deformation.overlay(img1,warped),name="overlay after")
    return
end # register_histology



function register_histology2()
    # use SSD to register histology images
    #@warn "This function one does not work well, images too noisy"
    ImageView.closeall()
    # register histology images
    img1=Images.load("/home/kybic/data/Medical/microscopy/Flagship/images-flagship_convert/scale-5pc/public-Case001_Rat_Kidney/Rat_Kidney_Section01_Podocin.jpg")   # about 800x600
    img2=Images.load("/home/kybic/data/Medical/microscopy/Flagship/images-flagship_convert/scale-5pc/public-Case001_Rat_Kidney/Rat_Kidney_Section02_HE.jpg");
    margin=200
    bgval=img1[1,1]
    sz1=Images.size(img1)
    img1=segimgtools.extend_to_size(img1,(sz1[1]+margin,sz1[2]+margin),bgval)
    sz2=Images.size(img2)
    img2=segimgtools.extend_to_size(img2,(sz2[1]+margin,sz2[2]+margin),bgval)
    img1=finereg.reduce_image_lin(img1,[2,2])
    img2=finereg.reduce_image_lin(img2,[2,2])
    #ImageView.imshow(img1, name = "static image")
    #ImageView.imshow(img2, name = "moving image")
    spacing=10 ; kptn=500
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kptn,
                                                                     sigma=[1.;1.]),"find simple keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")
    hmax=10
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple)
    ImageView.imshow(img1kpts,name="with keypoints")
    #Images.save("res/Case001gimg1kpts.jpg",img1kpts)
    #ImageView.imshow(img1kpts,name="with keypoints")
    #ImageView.imshow(deformation.overlay(img1,img2),name="overlay before")
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #bc=8
    #def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc,bc])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    imgsize=float([size(img1)...]) .* Images.pixelspacing(img1) # image size in physical units
    ## def = deformation.HierarchicalDeformation([
    ##    #deformation.BsplineDeformation(3,imgsize, [32,32]), 
    ##    #deformation.BsplineDeformation(3,imgsize, [16,16]), 
    ##    #deformation.BsplineDeformation(3,imgsize, [8,8]), 
    ##    deformation.create_identity(imgsize, deformation.AffineDeformation2D)
    ##    ])
    def = deformation.HierarchicalDeformation([
       deformation.BsplineDeformation(3,imgsize, [8,8]), 
       deformation.create_identity(imgsize, deformation.AffineDeformation2D)
       ])
    opts=@options hmax=hmax resample=true fdiffreg=1e-5 lambda=1e-6 maxResample=2 minKP=500 display=false maxsize=16384 minsize=64 knot_spacing=8 start_with_last_level=false
#    opts=@options hmax=hmax resample=true fdiffreg=nothing lambda=1e-8 maxResample=5 minKP=500 display=true maxsize=250 minsize=64 knot_spacing=8 start_with_last_level=false
    #critparams=ssd.SSDParameters()
    critparams=gaussianMI.GaussianMIParameters()
    t1=time()
#    @time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d= register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts) 
    #d=register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count, mincount=10)
    #Profile.print(mincount=10)
    t2=time()
    println("Registration took $(t2-t1)s")
    warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options default=bgval),"warping")
    #ImageView.imshow(warped,name="warped")
    ImageView.imshow(deformation.overlay(img1,warped),name="overlay after")
    return
end # register_histology2


function register_histology3()
    ImageView.closeall()
    # register histology images using segmentations
    img1=Images.load("/home/kybic/data/Medical/microscopy/Flagship/images-flagship_convert/scale-5pc/public-Case001_Rat_Kidney/Rat_Kidney_Section01_Podocin.jpg")   # about 800x600
    img2=Images.load("/home/kybic/data/Medical/microscopy/Flagship/images-flagship_convert/scale-5pc/public-Case001_Rat_Kidney/Rat_Kidney_Section02_HE.jpg");
    ImageView.imshow(img1, name = "static image")
    ImageView.imshow(img2, name = "moving image")
    Images.save("res/Case001img1.jpg",img1)
    Images.save("res/Case001img2.jpg",img2)

    hmax=10
    spacing=10 ; kptn=500 ; k = 5; spedge = 25 ; compact=10
    s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^2, compact, k  ), "Superpixels and k-means moving image")   #segment using superpixels and k_means
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k  ), "Superpixels and k-means static image")   #segment using superpixels and k_means

    #print("img1=",p1, "img2=",p2)
    
    ImageView.imshow(segimgtools.classimage2rgb(p1, k), name = "static image segmentation")
    ImageView.imshow(segimgtools.classimage2rgb(p2, k), name = "moving image segmentation")
    Images.save("res/Case001img2seg.png",segimgtools.classimage2rgb(p2, k))
    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kptsimple, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple)
    ImageView.imshow(img1kpts,name="with keypoints")
    Images.save("res/Case001img1kpts.jpg",img1kpts)

    ImageView.imshow(deformation.overlay(img1,img2),name="overlay before")
    Images.save("res/Case001imgovlbefore.jpg",deformation.overlay(img1,img2))

    ImageView.imshow(deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(p2, k)), name = "segmentation overlay before")
    img1kpts=keypoints.create_image_with_keypoints(segimgtools.classimage2rgb(p1, k),kptsimple, llen=hmax, bsize=3)
    ImageView.imshow(img1kpts, name = "segmentation with keypoints")
    Images.save("res/Case001img1segkpts.jpg",img1kpts)

    kptsampled=keypoints.create_sampled_keypoints(p1,kptsimple,hmax;spacing=float(spacing))
    isize=[size(img1)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #bc=8
    #def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc,bc])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    imgsize=float(isize) .* Images.pixelspacing(img1) # image size in physical units
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    def = deformation.HierarchicalDeformation([
    #    deformation.BsplineDeformation(3,imgsize, [32,32]), 
    #    deformation.BsplineDeformation(3,imgsize, [16,16]), 
        deformation.BsplineDeformation(3,imgsize, [8,8]), 
        deformation.create_identity(imgsize, deformation.AffineDeformation2D)
        ])

    critparams=mil.MILParameters(k)        #mutual information


    #    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=5 minKP=500 display=false maxsize=16384 minsize=128 knot_spacing=8 start_with_last_level=false
    #opts=@options hmax=hmax resample=true fdiffreg=1e-4 lambda=1e-8 maxResample=5 minKP=500 display=false maxsize=250 minsize=128 knot_spacing=8 start_with_last_level=false
    opts=@options hmax=hmax resample=true fdiffreg=1e-3 lambda=1e-3 maxResample=5 minKP=500 display=false maxsize=250 minsize=128 knot_spacing=8 start_with_last_level=false
    #critparams=ssd.SSDParameters()
    t1=time()
    #@time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d=register_fast_linprog_multiscale(p1,p2,kptsampled,def,nhoodKP,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()
    println("Time: registration $(t2-t1)")
    warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    p2w=@debugtime(deformation.transform_image_nn(d,p2,size(img1),@options),"warping segmentation")
    ImageView.imshow(warped,name="warped")
    ImageView.imshow(deformation.overlay(img1,warped),name="overlay after")
    Images.save("res/Case001imgovlafter.jpg",deformation.overlay(img1,warped))
    ImageView.imshow(segimgtools.classimage2rgb(p2w,k),name="warped segmentation")
    ImageView.imshow(deformation.overlay(p1,p2w),name="overlay segmentation after")
    return
end # register_histology3



####################################################################################################

#function normalize_gray_image(img::Images.Image)
#    return ((img - minimum(img)) / (maximum(img) - minimum(img)))
#end


function test_batch()
    basedir = "m:/" 
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/"
    end
    basedir = string(basedir , "microscopy/")
    
    # outdir = "c:/Temp/resbatch/"
    outdir = string(basedir,"TEMPORARY/julia_experimens_ipmi2017/exp_4/")
    if !isdir(outdir)
        mkpath(outdir)
    end
    KPspacing = 20
    KPnum = 500
    hmax = 40
    opts=@options hmax=hmax resample=true fdiffreg=nothing lambda=1e-6 maxResample=10 minKP=1000 display=true maxsize=300 minsize=128 knot_spacing=8 # defaultval=1.0
    critparams=ssd.SSDParameters()
    y = zeros(Float64, 2)       #temporary storage of landarks
    
    datasets = readdir(string(basedir, "lists_regImgSets/mix-small/"))
    sort!(datasets)
    covercsv = string(outdir, "cover.csv")
    fstr = open(covercsv, "w")
    write(fstr, "Base image, Moving image, Base keypoints, Moving keypoints, Registered image, registered landmarks, Time [s]\n")
    close(fstr)
    for dsind = 29#[3 29 30]     #1:length(datasets)  #
        outdirsuff = string(datasets[dsind][1:end-4], "/")
        outdirthis = string(outdir, outdirsuff)
        if !isdir(outdirthis)
            mkpath(outdirthis)
        end
        images = readdlm(string(basedir, "lists_regImgSets/mix-small/", datasets[dsind]), ',', '\n')

        for imgbind = 1:2:length(images)-2
            timebase = time()
            #read base landmarks
            fstr = open(string(basedir, images[imgbind+1]), "r")
            landreg = readdlm(fstr, Float64; skipstart = 2)
            close(fstr)
            #read base image
            img1rgb = Images.load(string(basedir, images[imgbind]))    #base
            margin=100
            bgval=img1rgb[1,1]
            sz1=Images.size(img1rgb)
            img1rgb=segimgtools.extend_to_size(img1rgb,(sz1[1]+margin,sz1[2]+margin),bgval)
            #img1 = normalize_gray_image(convert(Images.Image{Images.Gray},img1rgb))
            img1 = convert(Images.Image{Images.Gray},img1rgb)
            #img1.data=img1.data-(mean(img1)-0.5))
            ImageView.imshow(img1, name = "static image")
            #ImageView.imshow(img1rgb, name = "img1")
            kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1; spacing = KPspacing, number=KPnum,sigma=[5.;5.]),"find simple keypoints")
            nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")
            kptsampled=keypoints.create_sampled_keypoints(img1, kptsimple, hmax; spacing=float(KPspacing))
            img1kpts=keypoints.create_image_with_keypoints(copy(img1rgb),kptsimple)
            ImageView.imshow(img1kpts,name="with keypoints")
            timebase = time() - timebase
            for imgmind = imgbind+2:2:length(images)
                timemoving = time()
                img2rgb=Images.load(string(basedir, images[imgmind]))  #moving
                bgval=img2rgb[1,1]
                sz2=Images.size(img2rgb)
                img2rgb=segimgtools.extend_to_size(img2rgb,(sz2[1]+margin,sz2[2]+margin),bgval)
                #img2 = normalize_gray_image(convert(Images.Image{Images.Gray},img2rgb))
                img2 = convert(Images.Image{Images.Gray},img2rgb)
                #img2.data=img2.data-(mean(img2)-0.5))
                ImageView.imshow(img2, name = "img2")
                ImageView.imshow(deformation.overlay(img1rgb, img2rgb), name = "overlay before")
                isize=[size(img2)...]
                imgsize=isize .* Images.pixelspacing(img2) # image size in physical units
                ## def = deformation.HierarchicalDeformation([
                ##    deformation.BsplineDeformation(3,imgsize, [8,8]), 
                ##    deformation.BsplineDeformation(3,imgsize, [4,4]), 
                ##    deformation.create_identity(imgsize, deformation.AffineDeformation2D)
                ##    ])
                @debugprintln("Registering")
                def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [4,4])
                crit_state=ssd.criterion_init(img1, img2, critparams)
                #d=register_fast_linprog(img1,img2,kptsampled,def,crit_state,opts)
                opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=0.1)
                reg_params = regularization.NoneRegularizationParams()
                d=finereg.register_fast(img1,img2,kptsampled,def,crit_state,opt_state,reg_params,opts)
                #d=linprog_register.register_fast_linprog_multiscale(img1, img2, kptsampled, def, nhood, critparams, opts)

                timemoving = time() - timemoving
                #Landmark transformation
                fstr = open(string(outdirthis, "b$(imgbind)m$(imgmind).txt"), "w")
                write(fstr, "point\n")
                print_shortest(fstr, size(landreg, 1))
                write(fstr, "\n")
                for poin = 1:size(landreg, 1)
                    deformation.transform_point!(d, vec(landreg[poin, :]), y)
                    print_shortest(fstr, y[1])
                    write(fstr, " ")
                    print_shortest(fstr, y[2])
                    write(fstr, "\n")
                end
                close(fstr)

                # warp and save image
                warped=@debugtime(deformation.transform_image_lin(d, img2rgb, size(img1rgb), @options), "warping")
                Images.save(string(outdirthis, "b$(imgbind)m$(imgmind).png"), warped)
                ImageView.imshow(warped,name="warped")
                ImageView.imshow(deformation.overlay(img1rgb,warped),name="overlay after")
                fstr = open(covercsv, "a")
                write(fstr, "$(images[imgbind]), $(images[imgmind]), $(images[imgbind+1]), $(images[imgmind+1]), $(outdirsuff)b$(imgbind)m$(imgmind).png, $(outdirsuff)b$(imgbind)m$(imgmind).txt, $(timemoving+timebase)\n")
                close(fstr)
            end
        end
    end
end # test_batch

"""
    batch_flagship_seg(sourcelist, outputdir, inputsets; minimgsize, KPspacing, minKPnum,hmax, k, SPsize, SPcompact, lambda, maxsize, maxit)
Register histological slices from the set "lists_regImgSets//mix-small" or its subset and save results
in the for readable by Jirka's batch evaluation. Use linprog optimizations mil criterion on segmentations,
and hierarchical deformation.
!!! note
    All the paths are based on /datagrid/Medical/microscopy/
# Arguments
* `sourcelist::String`: directory path whwre the lists of images are located. Default "lists_regImgSets/mix-small/"
* `outputdir::String`: directory to save all the results. Subdir is created for the parameter settings. Default "TEMPORARY/julia_experimens_ipmi2017/"
* `inputsets::Array{Int, 1}`: indicies of the cases in directory `sourcelist` (sorted alphabetically) to be registered. If empty, all the cases are used. Default [3, 29, 30]
* `minimgsize::Int`: smaller images will not be resampled in multiscale pyramid. Default 64
* `KPspacing::Int`: keypoint spacing. Default 30
* `minKPnum::Int`: minimum keypoint number in multiscale. Default 500
* `hmax::Int`: hmax in register_fast. Default 20
* `k::Int`: number of classe for image segmentation. Default 3
* `SPsize::Int`: number of pixels inside initial sumerpixel. Default 100
* `SPcompact::Int`: superpixel compactness. Default 10
* `lambda::Float64`: regulerization. Default 1e-2
* `maxsize::Int`: for images larger than `maxsize` optimization is not performed in multiscale. Default 256
* `maxit:Int`: maximal number of resamplig steps. Default 5
"""
function batch_flagship_seg(sourcelist = "lists_regImgSets/mix-small/",
                            outputdir = "TEMPORARY/julia_experimens_ipmi2017/",
                            inputsets::Array{Int, 1} = [3, 29, 30];
                            minimgsize::Int = 64, KPspacing::Int = 30, minKPnum::Int = 500,
                            hmax::Int = 20, k::Int = 3, SPsize::Int = 100, SPcompact::Int = 10,
                            lambda::Float64 = 1e-2, maxsize::Int = 256, maxit::Int = 5)
    basedir = "m:/" 
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/"
    end
    basedir = string(basedir , "microscopy/")

    opt = @options hmax=hmax resample=true fdiffreg=nothing lambda=lambda maxResample=maxit minKP=minKPnum display=false maxsize=maxsize minsize=minimgsize knot_spacing=8 start_with_last_level=false
    crit_params=mil.MILParameters(k)
    y = zeros(Float64, 2)       #temporary storage of landarks

    outdir = string(basedir, outputdir, "s$(length(inputsets))_mil_lpr_mis$(minimgsize)_kpsp$(KPspacing)_mkp$(minKPnum)_hm$(hmax)_k$(k)_sps$(SPsize)_cmp$(SPcompact)_lmbd$(lambda)_mas$(maxsize)_maxit$(maxit)/")
    if !isdir(outdir)
        mkpath(outdir, 0o775)
    end

    datasets = readdir(string(basedir, sourcelist))
    sort!(datasets)
    covercsv = string(outdir, "cover.csv")
    fstr = open(covercsv, "w")
    write(fstr, "Base image, Moving image, Base keypoints, Moving keypoints, Registered image, registered landmarks, Time [s]\n")
    close(fstr)
    if inputsets == []
        inputsets = 1:length(datasets)
    end
    for dsind in inputsets
        outdirsuff = string(datasets[dsind][1:end-4], "/")
        outdirthis = string(outdir, outdirsuff)
        if !isdir(outdirthis)
            mkpath(outdirthis, 0o775)
        end
        images = readdlm(string(basedir, sourcelist, datasets[dsind]), ',', '\n')

        for imgbind = 1:2:length(images)-2
            timebase = time()
            #read base landmarks
            fstr = open(string(basedir, images[imgbind+1]), "r")
            landreg = readdlm(fstr, Float64; skipstart = 2)
            close(fstr)
            #read base image
            img1 = Images.load(string(basedir, images[imgbind]))    #base
            s1l, s1f, count, l1, p1 = segimgtools.slic_size_kmeans(img1, SPsize, SPcompact, k, UInt16, true, 30)
            nlen = minimum(Images.pixelspacing(img1))
            kpts, nhoodKP = keypoints.find_keypoints_and_neighbors(p1, KPspacing, nlen)
            kpts = keypoints.create_sampled_keypoints(p1, kpts, hmax; spacing=float(KPspacing))
            
            # img1kpts=keypoints.create_image_with_keypoints(copy(p1),kpts,k;llen=hmax)
            # ImageView.imshow(img1kpts,name="with keypoints")
            # ImageView.imshow(segimgtools.classimage2rgb(p1, k),name="img1")

            timebase = time() - timebase
            for imgmind = imgbind+2:2:length(images)
                timemoving = time()
                img2=Images.load(string(basedir, images[imgmind]))  #moving
                s2l, s2f, count, l2, p2 = segimgtools.slic_size_kmeans(img2, SPsize, SPcompact, k, UInt16, true, 30)
                # ImageView.imshow(deformation.overlay(img1, img2), name = "overlay before")
                isize=[size(img2)...]
                imgsize=isize .* Images.pixelspacing(img2) # image size in physical units
                def = deformation.HierarchicalDeformation([
                   deformation.BsplineDeformation(3,imgsize, [19,19]), 
                   deformation.BsplineDeformation(3,imgsize, [7,7]), 
                   deformation.create_identity(imgsize, deformation.AffineDeformation2D)
                   ])
                @debugprintln("Registering")
                d=register_fast_linprog_multiscale(p1, p2, kpts, def, nhoodKP, crit_params, opt)

                timemoving = time() - timemoving
                #Landmark transformation
                fstr = open(string(outdirthis, "b$(imgbind)m$(imgmind).txt"), "w")
                write(fstr, "point\n")
                print_shortest(fstr, size(landreg, 1))
                write(fstr, "\n")
                for poin = 1:size(landreg, 1)
                    deformation.transform_point!(d, vec(landreg[poin, :]), y)
                    print_shortest(fstr, y[1])
                    write(fstr, " ")
                    print_shortest(fstr, y[2])
                    write(fstr, "\n")
                end
                close(fstr)

                # warm and save image
                warped=@debugtime(deformation.transform_image_lin(d, img2, size(img1), @options), "warping")
                Images.save(string(outdirthis, "b$(imgbind)m$(imgmind).png"), warped)
                # ImageView.imshow(warped,name="warped")
                # ImageView.imshow(deformation.overlay(img1,warped),name="overlay after")
                # pw=@debugtime(deformation.transform_image_lin(d, p2, size(p1), @options), "warping")
                # ImageView.imshow(deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(pw, k)),name="after")
                fstr = open(covercsv, "a")
                write(fstr, "$(images[imgbind]),$(images[imgmind]),$(images[imgbind+1]),$(images[imgmind+1]),$(outdirsuff)b$(imgbind)m$(imgmind).png,$(outdirsuff)b$(imgbind)m$(imgmind).txt,$(timemoving+timebase)\n")
                close(fstr)
            end
        end
    end
end # batch_flagship_seg

function run_batch_flagship_seg()
    hmax = [10, 20, 30]
    spsize = [49, 64, 81]
    for i in hmax
        for j in spsize
            batch_flagship_seg("lists_regImgSets/mix-small/", "TEMPORARY/julia_experimens_ipmi2017/";
                                hmax = i, SPsize = j)
        end
    end
    exit()
end


function show_histology_kpts()
    # use SSD to register histology images
    #@warn "This function one does not work well, images too noisy"
    ImageView.closeall()
    # register histology images
     img1=Images.load("/home/kybic/data/Medical/microscopy/Flagship/images-flagship_convert/scale-5pc/public-Case001_Rat_Kidney/Rat_Kidney_Section01_Podocin.jpg")   # about 800x600
#     img1=Images.load("/home/kybic/data/Medical/microscopy/Flagship/images-flagship_convert/scale-10pc/public-Case001_Rat_Kidney/Rat_Kidney_Section01_Podocin.jpg")   # about 800x600
#    img2=Images.load("/home/kybic/data/Medical/microscopy/Flagship/images-flagship_convert/scale-5pc/public-Case001_Rat_Kidney/Rat_Kidney_Section02_HE.jpg");
    margin=200
    bgval=img1[1,1]
    sz1=Images.size(img1)
    #img1=segimgtools.extend_to_size(img1,(sz1[1]+margin,sz1[2]+margin),bgval)
    #sz2=Images.size(img2)
    #img2=segimgtools.extend_to_size(img2,(sz2[1]+margin,sz2[2]+margin),bgval)
    #img1=finereg.reduce_image_lin(img1,[2,2])
    #img2=finereg.reduce_image_lin(img2,[2,2])
    #ImageView.imshow(img1, name = "static image")
    #ImageView.imshow(img2, name = "moving image")
    spacing=10 ; 
    hmax=10
    k = 6; spedge = 25 ; compact=10
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k  ), "Superpixels and k-means static image")   #segment using superpixels and k_means
    ImageView.imshow(segimgtools.classimage2rgb(p1, k), name = "static image segmentation")
    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kptsimple, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")
    print("segmentation keypoints: ",length(kptsimple))
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple,llen=hmax,bsize=4,thickness=2,colline=colorant"red",colbox=colorant"white")
    ImageView.imshow(img1kpts,name="with keypoints")
    Images.save("res/Case001bigimg1kpts.jpg",img1kpts)
    img1segkpts=keypoints.create_image_with_keypoints(segimgtools.classimage2rgb(p1, k), p1, kptsimple, llen=hmax,bsize=1,thickness=2,colline=colorant"black",colbox=colorant"black")
    ImageView.imshow(img1segkpts, name = "segmentation with keypoints")
    Images.save("res/Case001img1segkpts.png",img1segkpts)
    kptn=length(kptsimple)
    kptsimpleg=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kptn,
                                                                     sigma=[1.;1.]),"find simple keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimpleg),"Finding keypoint neighbors")
    hmax=10
    #kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kptsg=keypoints.create_image_with_keypoints(copy(img1),kptsimpleg,llen=hmax,bsize=4,thickness=2,colline=colorant"red",colbox=colorant"white")
    print("gradient keypoints: ",length(kptsimpleg))
    ImageView.imshow(img1kptsg,name="with gradient keypoints")
    Images.save("res/Case001bigimg1kptsgrad.png",img1kptsg)
end


function show_gradient()
    ImageView.closeall()
    # register histology images
     img1=Images.load("/home/kybic/data/Medical/microscopy/Flagship/images-flagship_convert/scale-5pc/public-Case001_Rat_Kidney/Rat_Kidney_Section01_Podocin.jpg")   # about 800x600
    sigma=[1.;1.] 
    grad,g=@debugtime( keypoints.gradient_magnitude(
       ImageFiltering.imfilter(img1,ImageFiltering.Kernel.gaussian(sigma))), "Gradient magnitude")
    h=Images.adjust_gamma(Images.complement.(g),5.0)
    ImageView.imshow(h,name="gradient")
    Images.save("res/Case001gradient.png",h)
end
    

end # test_linprog_register

