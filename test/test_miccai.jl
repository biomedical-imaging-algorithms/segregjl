"""
 Experiments for the MICCAI2017 paper

Jan Kybic
"""
module test_miccai

import Images
import ImageView

using segregjl

# we need to include local files now
# include("Options.jl")
using .Options

# using MicroLogging
# using Logging
using Colors
using DelimitedFiles
using Statistics

# include("criterion.jl")
# using .criterion
# include("ssd.jl")
# include("SLICsuperpixels.jl")
# include("segimgtools.jl")
# include("bsplines.jl")
# include("keypoints.jl")
# include("deformation.jl")
# include("optimizer.jl")
# include("regularization.jl")
# include("mil.jl")
# include("gaussianMI.jl")
# include("finereg.jl")
# include("linprog_register.jl")

using .linprog_register
#import ssd
import .keypoints
import .deformation
import .segimgtools
#import optimizer
#import regularization
#import finereg
#import SLICsuperpixels
import .mil
import .gaussianMI

#using Profile

#configure_logging(min_level=:debug)


include("../src/debugprint.jl")
switchprinting(true)
include("../src/debugassert.jl")
switchasserts(true)

# register MRI images from Brainweb
function register_mri()
    ImageView.closeall()
    img1=Images.load("imgs/3919transverse1_64.gif")
    img2=Images.load("imgs/4072transverse1_64.gif")
    # synthetic rigid transformation, linear interpolation
    rotangle=15. ; #shift=[11.;-5]
    shift=[1.;-1]
    c = deformation.RigidDeformation2D([size(img2)...] .* Images.pixelspacing(img2), vcat(rotangle,shift))
    img2=deformation.transform_image_lin(c,img2,size(img2),@options default = img2[1])
    #img2=deformation.transform_rigid2D(img2,size(img2),rotangle,shift, @options default = img2[1])
    ImageView.imshow(img1,name="image 1")
    ImageView.imshow(img2,name="image 2")
    Images.save("res/mriimg1ext.jpg",img1)
    Images.save("res/mriimg2ext.jpg",img2)
    hmax=10
    spacing=10 ; k = 3; spedge = 10 ; compact=1.0
    s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^2, compact, k  ), "Superpixels and k-means moving image")   #segment using superpixels and k_means
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k  ), "Superpixels and k-means static image")   #segment using superpixels and k_means
    p1color=segimgtools.classimage2rgb(p1, k)
    ImageView.imshow(p1color, name = "static image segmentation")
    Images.save("res/mriimg1seg.png",p1color)
    p2color=segimgtools.classimage2rgb(p2, k)
    ImageView.imshow(p2color, name = "moving image segmentation")
    Images.save("res/mriimg2seg.png",p1color)
    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kptsimple, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")
    print("count=",count," kptsimple=",length(kptsimple))
    img1kpts=keypoints.create_image_with_keypoints(RGB.(img1),kptsimple,llen=hmax,bsize=1,thickness=1,colline=colorant"red",colbox=colorant"white")
    Images.save("res/mriimg1kpts.png",img1kpts)
    ImageView.imshow(img1kpts,name="with keypoints")
    imgovlbefore=deformation.overlay(img1,img2)
    Images.save("res/mriimgovlbefore.jpg",imgovlbefore)
    ImageView.imshow(imgovlbefore,name="overlay before")
    segovlbefore=deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(p2, k))
    ImageView.imshow(segovlbefore, name = "segmentation overlay before")
    Images.save("res/mriimgovlbefore.jpg",imgovlbefore)
    img1segkpts=keypoints.create_image_with_keypoints(segimgtools.classimage2rgb(p1, k),kptsimple, llen=hmax,bsize=3,thickness=2,colline=colorant"white",colbox=colorant"black")
    ImageView.imshow(img1segkpts, name = "segmentation with keypoints")
    Images.save("res/mriimg1segkpts.png",p1color)
    kptsampled=@debugtime( keypoints.create_sampled_keypoints(p1,kptsimple,hmax;spacing=float(spacing)), "create_sampled_keypoints")
    isize=[size(img1)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #bc=8
    #def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc,bc])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    imgsize=float(isize) .* Images.pixelspacing(img1) # image size in physical units
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #def = deformation.HierarchicalDeformation([
    #    deformation.BsplineDeformation(3,imgsize, [32,32]), 
    #    deformation.BsplineDeformation(3,imgsize, [16,16]), 
    #    deformation.BsplineDeformation(3,imgsize, [4,4]), 
    #    deformation.create_identity(imgsize, deformation.AffineDeformation2D)
    #    ])
   
    critparams=mil.MILParameters(k)        #mutual information
    #    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=5 minKP=500 display=false maxsize=16384 minsize=128 knot_spacing=8 start_with_last_level=false
    #opts=@options hmax=hmax resample=true fdiffreg=1e-4 lambda=1e-8 maxResample=5 minKP=500 display=false maxsize=250 minsize=128 knot_spacing=8 start_with_last_level=false
    opts=@options hmax=hmax resample=true fdiffreg=1e-3 lambda=1e-3 maxResample=5 minKP=500 display=false maxsize=1000 minsize=64 knot_spacing=8 start_with_last_level=false
    #critparams=ssd.SSDParameters()
    t1=time()
    #@time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d=register_fast_linprog_multiscale(p1,p2,kptsampled,def,nhoodKP,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()
    println("Time: registration $(t2-t1)")
    warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    p2w=@debugtime(deformation.transform_image_nn(d,p2,size(img1),@options),"warping segmentation")
    ImageView.imshow(warped,name="warped")
    Images.save("res/mriwarped.jpg",warped)
    imgovlafter=deformation.overlay(img1,warped)
    ImageView.imshow(imgovlafter,name="overlay after")
    Images.save("res/mriimgovlafter.jpg",warped)
    warpedseg=segimgtools.classimage2rgb(p2w,k)
    ImageView.imshow(warpedseg,name="warped segmentation")
    Images.save("res/mriwarpedsegr.jpg",warped)
    segovlafter=deformation.overlay(p1,p2w)
    ImageView.imshow(segovlafter,name="overlay segmentation after")
    Images.save("res/mrisegovlafter.jpg",segovlafter)
    return
end # register MRI


# register MRI without segmentation
function register_mri2()
    ImageView.closeall()
    img1=Images.load("imgs/3919transverse1_64.gif")
    img2=Images.load("imgs/3919transverse1_64.gif")
    #img2=Images.load("imgs/4072transverse1_64.gif")
    # synthetic rigid transformation, linear interpolation
    rotangle=5. ; shift=[5.;-5]
    #rotangle=0. ; shift=[1.;-1]
    c = deformation.RigidDeformation2D([size(img2)...] .* Images.pixelspacing(img2), vcat(rotangle,shift))
    img2=deformation.transform_image_lin(c,img2,size(img2),@options default = img2[1])
    #img2=deformation.transform_rigid2D(img2,size(img2),rotangle,shift, @options default = img2[1])
    ImageView.imshow(img1,name="image 1")
    ImageView.imshow(img2,name="image 2")
    #Images.save("res/mri2img1ext.jpg",img1)
    #Images.save("res/mri2img2ext.jpg",img2)
    hmax=5
    spacing=10 ; kptn=100
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kptn,
                                                                     sigma=[1.;1.]),"find simple keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")
    kptsampled=@debugtime( keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing)), "create_sampled_keypoints")
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple,llen=hmax,bsize=3,thickness=2,colline=colorant"red",colbox=colorant"black")
    Images.save("res/mri2img1kpts.png",img1kpts)
    ImageView.imshow(img1kpts,name="with keypoints")
    imgovlbefore=deformation.overlay(img1,img2)
    #Images.save("res/mri2imgovlbefore.jpg",imgovlbefore)
    ImageView.imshow(imgovlbefore,name="overlay before")
    isize=[size(img1)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #bc=8
    #def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc,bc])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    imgsize=float(isize) .* Images.pixelspacing(img1) # image size in physical units
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #def = deformation.HierarchicalDeformation([
    #    deformation.BsplineDeformation(3,imgsize, [32,32]), 
    #    deformation.BsplineDeformation(3,imgsize, [16,16]), 
    #    deformation.BsplineDeformation(3,imgsize, [4,4]), 
    #    deformation.create_identity(imgsize, deformation.AffineDeformation2D)
    #    ])
    critparams=gaussianMI.GaussianMIParameters()

    #    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=5 minKP=500 display=false maxsize=16384 minsize=128 knot_spacing=8 start_with_last_level=false
    #opts=@options hmax=hmax resample=true fdiffreg=1e-4 lambda=1e-8 maxResample=5 minKP=500 display=false maxsize=250 minsize=128 knot_spacing=8 start_with_last_level=false
#    opts=@options hmax=hmax resample=true fdiffreg=1e-3 lambda=1e-3 maxResample=5 minKP=100 display=true maxsize=1000 minsize=64 knot_spacing=8 start_with_last_level=false
    opts=@options hmax=hmax resample=true maxResample=5 minKP=100 display=false maxsize=1000 minsize=100 knot_spacing=8 start_with_last_level=false
    #critparams=ssd.SSDParameters()
    t1=time()
    #@time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d=register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()
    println("Time: registration $(t2-t1)")
    warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    ImageView.imshow(warped,name="warped")
    #Images.save("res/mri2warped.jpg",warped)
    imgovlafter=deformation.overlay(img1,warped)
    ImageView.imshow(imgovlafter,name="overlay after")
    #Images.save("res/mri2imgovlafter.jpg",warped)
    return
end # register MRI

function readlandmarks(lndfn)
    fstr=open(lndfn,"r")
    landreg = readdlm(fstr, Float64; skipstart = 2)
    close(fstr)
    return landreg
end

function transform_landmarks(d,lnd)
    y=fill(0.::Float64,size(lnd,2))
    lndw=zero(lnd)
    for i=1:size(lnd,1)
        deformation.transform_point!(d, [lnd[i,2],lnd[i,1]], y)
        lndw[i,1]=y[2]
        lndw[i,2]=y[1]
    end
    return lndw
end
    
function landdist(lnd1,lnd2)
    return sum(sqrt.(sum((lnd1-lnd2).^2,dims=2)))/size(lnd1,1)
end

function landdistmed(lnd1,lnd2)
    return median(sqrt.(sum((lnd1-lnd2).^2,dims=2)))
end

# register big histology images using a segmentation
function register_histology4()
   ImageView.closeall()
   img1=Images.load("/home/kybic/data/Medical/microscopy/flagship/images-flagship_convert/scale-10pc/Case012/Case012_HE.jpg")
   img2=Images.load("/home/kybic/data/Medical/microscopy/flagship/images-flagship_convert/scale-10pc/Case012/Case012_PR.jpg")
   land1=readlandmarks("/home/kybic/data/Medical/microscopy/Flagship/landmarks-flagship/scale-10pc/Case012_HE.txt")
   land2=readlandmarks("/home/kybic/data/Medical/microscopy/Flagship/landmarks-flagship/scale-10pc/Case012_PR.txt")
   print("Landmark mean distance before: ",landdist(land1,land2))
   print("Landmark median distance before: ",landdistmed(land1,land2))
    margin=200
    bgval=img1[1,1]
    bgval2=img2[1,1]
    sz1=Images.size(img1)
    img1=segimgtools.extend_to_size(img1,(sz1[1]+margin,sz1[2]+margin),bgval)
    sz2=Images.size(img2)
    img2=segimgtools.extend_to_size(img2,(sz2[1]+margin,sz2[2]+margin),bgval2)
    Images.save("res/Case012img1ext.jpg",img1)
    Images.save("res/Case012img2ext.jpg",img2)


    #ImageView.imshow(img1, name = "static image")
    #ImageView.imshow(img2, name = "moving image")
    hmax=10
    spacing=20 ; k = 5; spedge = 70 ; compact=5
    s2l, s2f, count, l2, p2 = @debugtime( segimgtools.slic_size_kmeans(img2, spedge^2, compact, k  ), "Superpixels and k-means moving image")   #segment using superpixels and k_means
    s1l, s1f, count, l1, p1 = @debugtime( segimgtools.slic_size_kmeans(img1, spedge^2, compact, k  ), "Superpixels and k-means static image")   #segment using superpixels and k_means

    print(" k=",k, " maximum=",maximum(p1)," minimum=",minimum(p2))
    p1color=segimgtools.classimage2rgb(p1, k)
    #ImageView.imshow(p1color, name = "static image segmentation")
    Images.save("res/Case012img1seg.png",p1color)
    p2color=segimgtools.classimage2rgb(p2, k)
    #ImageView.imshow(p2color, name = "moving image segmentation")
    Images.save("res/Case012img2seg.png",p2color)
    nhoodSP = @debugtime( SLICsuperpixels.getNeighbors(s1l, count), "SLIC getNeighbors:")
    kptsimple, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(s1l, l1, nhoodSP),"find_keypoints_and_neighbors:")
    print("count=",count," kptsimple=",length(kptsimple))
    img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple,llen=5*hmax,bsize=10,thickness=8,colline=colorant"red",colbox=colorant"black")
    Images.save("res/Case012img1kpts.png",img1kpts)
    #ImageView.imshow(img1kpts,name="with keypoints")
    imgovlbefore=deformation.overlay(img1,img2)
    Images.save("res/Case012imgovlbefore.jpg",imgovlbefore)
    #ImageView.imshow(imgovlbefore,name="overlay before")
    segovlbefore=deformation.overlay(segimgtools.classimage2rgb(p1, k), segimgtools.classimage2rgb(p2, k))
    #ImageView.imshow(segovlbefore, name = "segmentation overlay before")
    Images.save("res/Case012segovlbefore.png",segovlbefore)
    img1segkpts=keypoints.create_image_with_keypoints(segimgtools.classimage2rgb(p1, k),kptsimple, llen=5*hmax,bsize=20,thickness=16,colline=colorant"black",colbox=colorant"black")
    #ImageView.imshow(img1segkpts, name = "segmentation with keypoints")
    Images.save("res/Case012img1segkpts.png",img1segkpts)
    kptsampled=@debugtime( keypoints.create_sampled_keypoints(p1,kptsimple,hmax;spacing=float(spacing)), "create_sampled_keypoints")
    isize=[size(img1)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #bc=8
    #def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc,bc])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    imgsize=float(isize) .* Images.pixelspacing(img1) # image size in physical units
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D
    #def= deformation.BsplineDeformation(3,imgsize, [8,8])

    def = deformation.HierarchicalDeformation([
    #    deformation.BsplineDeformation(3,imgsize, [32,32]), 
    #    deformation.BsplineDeformation(3,imgsize, [16,16]), 
        deformation.BsplineDeformation(3,imgsize, [8,8]), 
        deformation.create_identity(imgsize, deformation.AffineDeformation2D)
        ])

    critparams=mil.MILParameters(k)        #mutual information


    #    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=5 minKP=500 display=false maxsize=16384 minsize=128 knot_spacing=8 start_with_last_level=false
    #opts=@options hmax=hmax resample=true fdiffreg=1e-4 lambda=1e-8 maxResample=5 minKP=500 display=false maxsize=250 minsize=128 knot_spacing=8 start_with_last_level=false
    opts=@options hmax=hmax resample=true fdiffreg=1e-3 lambda=1e-3 maxResample=5 minKP=500 display=false maxsize=1000 minsize=128 knot_spacing=8 start_with_last_level=false
    #critparams=ssd.SSDParameters()
    t1=time()
    #@time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d=register_fast_linprog_multiscale(p1,p2,kptsampled,def,nhoodKP,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()
    println("Time: registration $(t2-t1)")
    warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    p2w=@debugtime(deformation.transform_image_nn(d,p2,size(img1),@options),"warping segmentation")
    #ImageView.imshow(warped,name="warped")
    Images.save("res/Case012warped.jpg",warped)
    imgovlafter=deformation.overlay(img1,warped)
    #ImageView.imshow(imgovlafter,name="overlay after")
    Images.save("res/Case012imgovlafter.jpg",imgovlafter)
    warpedseg=segimgtools.classimage2rgb(p2w,k)
    #ImageView.imshow(warpedseg,name="warped segmentation")
    Images.save("res/Case012warpedsegr.jpg",warped)
    segovlafter=deformation.overlay(p1,p2w)
    #ImageView.imshow(segovlafter,name="overlay segmentation after")
    Images.save("res/Case012segovlafter.jpg",segovlafter)
    lndw=transform_landmarks(d,land1)
    print("Landmark mean distance before: ",landdist(land1,land2))
    print("Landmark mean distance after: ",landdist(lndw,land2))
    print("Landmark median distance before: ",landdistmed(land1,land2))
    print("Landmark median distance after: ",landdistmed(lndw,land2))
    return
end # register_histology4


# register big histology images without segmentation, using gradient keypoints
function register_histology5()
   ImageView.closeall()
   img1=Images.load("/home/kybic/data/Medical/microscopy/flagship/images-flagship_convert/scale-10pc/Case012/Case012_HE.jpg")
   img2=Images.load("/home/kybic/data/Medical/microscopy/flagship/images-flagship_convert/scale-10pc/Case012/Case012_PR.jpg")
   land1=readlandmarks("/home/kybic/data/Medical/microscopy/Flagship/landmarks-flagship/scale-10pc/Case012_HE.txt")
   land2=readlandmarks("/home/kybic/data/Medical/microscopy/Flagship/landmarks-flagship/scale-10pc/Case012_PR.txt")
   print("Landmark distance before: ",landdist(land1,land2))
    margin=200
    bgval=img1[1,1]
    bgval2=img2[1,1]
    sz1=Images.size(img1)
    img1=segimgtools.extend_to_size(img1,(sz1[1]+margin,sz1[2]+margin),bgval)
    sz2=Images.size(img2)
    img2=segimgtools.extend_to_size(img2,(sz2[1]+margin,sz2[2]+margin),bgval2)
    Images.save("res/Case012gimg1ext.jpg",img1)
    Images.save("res/Case012gimg2ext.jpg",img2)


    #ImageView.imshow(img1, name = "static image")
    #ImageView.imshow(img2, name = "moving image")
    hmax=10
    spacing=50 ; kptn=1600 ;
    kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kptn,
                                                                     sigma=[2.;2.]),"find simple keypoints")
    nhood=@debugtimeline(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")
    kptsampled=@debugtime( keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing)), "create_sampled_keypoints")
    println("nkpts=",length(kptsimple))
    #img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple,llen=5*hmax,bsize=20,thickness=16,colline=colorant"black",colbox=colorant"black")
    #Images.save("res/Case012gimg1kpts.png",img1kpts)
    #ImageView.imshow(img1kpts,name="with keypoints")
    #imgovlbefore=deformation.overlay(img1,img2)
    #Images.save("res/Case012gimgovlbefore.jpg",imgovlbefore)
    #ImageView.imshow(imgovlbefore,name="overlay before")
    isize=[size(img1)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #bc=8
    #def=deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [bc,bc])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    imgsize=float(isize) .* Images.pixelspacing(img1) # image size in physical units
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    def = deformation.HierarchicalDeformation([
    #    deformation.BsplineDeformation(3,imgsize, [32,32]), 
    #    deformation.BsplineDeformation(3,imgsize, [16,16]), 
        deformation.BsplineDeformation(3,imgsize, [8,8]), 
        deformation.create_identity(imgsize, deformation.AffineDeformation2D)
        ])
    critparams=gaussianMI.GaussianMIParameters()

    #    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=5 minKP=500 display=false maxsize=16384 minsize=128 knot_spacing=8 start_with_last_level=false
    #opts=@options hmax=hmax resample=true fdiffreg=1e-4 lambda=1e-8 maxResample=5 minKP=500 display=false maxsize=250 minsize=128 knot_spacing=8 start_with_last_level=false
    opts=@options hmax=hmax resample=true fdiffreg=1e-4 lambda=1e-4 maxResample=5 minKP=2000 display=false maxsize=1000 minsize=128 knot_spacing=8 start_with_last_level=false
    #critparams=ssd.SSDParameters()
    t1=time()
    #@time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d=register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()
    println("Time: registration $(t2-t1)")
    #warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    #ImageView.imshow(warped,name="warped")
    #Images.save("res/Case012gwarped.jpg",warped)
    #imgovlafter=deformation.overlay(img1,warped)
    #ImageView.imshow(imgovlafter,name="overlay after")
    #Images.save("res/Case012gimgovlafter.jpg",imgovlafter)
    lndw=transform_landmarks(d,land1)
    print("Landmark distance before: ",landdist(land1,land2))
    print("Landmark distance after: ",landdist(lndw,land2))
    return
end # register_histology5


# register big histology images without segmentation, using gradient keypoints
# evaluate the effect of the number of keypoints
function register_histology_pts()
   ImageView.closeall()
   img1=Images.load("/home/kybic/data/Medical/microscopy/flagship/images-flagship_convert/scale-10pc/Case012/Case012_HE.jpg")
   img2=Images.load("/home/kybic/data/Medical/microscopy/flagship/images-flagship_convert/scale-10pc/Case012/Case012_PR.jpg")
   land1=readlandmarks("/home/kybic/data/Medical/microscopy/Flagship/landmarks-flagship/scale-10pc/Case012_HE.txt")
   land2=readlandmarks("/home/kybic/data/Medical/microscopy/Flagship/landmarks-flagship/scale-10pc/Case012_PR.txt")
   print("Landmark distance before: ",landdist(land1,land2))
    margin=200
    bgval=img1[1,1]
    bgval2=img2[1,1]
    sz1=Images.size(img1)
    img1=segimgtools.extend_to_size(img1,(sz1[1]+margin,sz1[2]+margin),bgval)
    sz2=Images.size(img2)
    img2=segimgtools.extend_to_size(img2,(sz2[1]+margin,sz2[2]+margin),bgval2)

    hmax=10
    spacing=50 ;
    #kptns=[250,500,750,1000,1250,1500,1750,2000] ;
    kptns=[1000] ;
    timearr=fill(0.,size(kptns))
    errarr=fill(0.,size(kptns))
    for i=1:length(kptns)
        kptn=kptns[i]
        #kptsimple=@debugtimeline(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kptn,sigma=[2.;2.]),"find simple keypoints")
        kptsimple=keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kptn,sigma=[2.;2.])
        #nhood=@debugtime(keypoints.keypoint_neighbors(kptsimple),"Finding keypoint neighbors")
        nhood=keypoints.keypoint_neighbors(kptsimple)
        #kptsampled=@debugtime( keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing)), "create_sampled_keypoints")
        kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
        #img1kpts=keypoints.create_image_with_keypoints(copy(img1),kptsimple,llen=5*hmax,bsize=20,thickness=16,colline=colorant"black",colbox=colorant"black")
    #Images.save("res/Case012gimg1kpts.png",img1kpts)
    #ImageView.imshow(img1kpts,name="with keypoints")
    #imgovlbefore=deformation.overlay(img1,img2)
    #Images.save("res/Case012gimgovlbefore.jpg",imgovlbefore)
    #ImageView.imshow(imgovlbefore,name="overlay before")
    isize=[size(img1)...]
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #bc=8
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    imgsize=float(isize) .* Images.pixelspacing(img1) # image size in physical units
    def=deformation.BsplineDeformation(3,imgsize,[8,8])
    #def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.AffineDeformation2D)
    #def = deformation.HierarchicalDeformation([
    #    deformation.BsplineDeformation(3,imgsize, [32,32]), 
    #    deformation.BsplineDeformation(3,imgsize, [16,16]), 
    #    deformation.BsplineDeformation(3,imgsize, [8,8]), 
    #    deformation.create_identity(imgsize, deformation.AffineDeformation2D)
    #    ])
    critparams=gaussianMI.GaussianMIParameters()

    #    opts=@options hmax=hmax resample=true fdiffreg=1e-6 lambda=1e-6 maxResample=5 minKP=500 display=false maxsize=16384 minsize=128 knot_spacing=8 start_with_last_level=false
    #opts=@options hmax=hmax resample=true fdiffreg=1e-4 lambda=1e-8 maxResample=5 minKP=500 display=false maxsize=250 minsize=128 knot_spacing=8 start_with_last_level=false
    opts=@options hmax=hmax resample=true fdiffreg=1e-3 lambda=1e-3 maxResample=5 minKP=500 display=false maxsize=1000 minsize=64 knot_spacing=8 start_with_last_level=false
    #critparams=ssd.SSDParameters()
    t1=time()
    #@time crit_state=ssd.criterion_init(img1,img2,critparams)
    #Profile.clear()
    d=register_fast_linprog_multiscale(img1,img2,kptsampled,def,nhood,critparams,opts)
    #Profile.print(format=:flat,sortedby=:count)
    t2=time()-t1
    timearr[i]=t2
    println("Time: registration $(t2)")
    #warped=@debugtime(deformation.transform_image_lin(d,img2,size(img1),@options),"warping")
    #ImageView.imshow(warped,name="warped")
    #Images.save("res/Case012gwarped.jpg",warped)
    #imgovlafter=deformation.overlay(img1,warped)
    #ImageView.imshow(imgovlafter,name="overlay after")
    #Images.save("res/Case012gimgovlafter.jpg",imgovlafter)
        lndw=transform_landmarks(d,land1)
        lndd=landdist(lndw,land2)
        #@show errarr lndd
        errarr[i]=lndd
        print("Landmark distance before: ",landdist(land1,land2))
        print("Landmark distance after: ",lndd)
    end
    print("kptns:",kptns)
    print("timearr:",timearr)
    print("errarr:",errarr)
    return
end # register_histology5


using Winston

function draw_miccai_speed_time_graph()
    ns=[200,400,600,800,1000,1200,1400,1600,1800,2000]
    es=[69.55,31.32,27.28,23.18,19.32,18.38,18.71,20.46,22.59,21.17]
    ts=[17.25,18.56,19.76,19.62,20.31,21.07,21.39,22.59,24.48,23.35]
    tw = Winston.plot(ns,ts,"ro-")
    Winston.ylim(0,30)
    Base.display(tw)
    Winston.xlabel("num. points") ; Winston.ylabel("time [s]")
    savefig("res/timevsnumpts.pdf",height=200,width=400)
    tw = Winston.plot(ns,es,"bo-")
    Winston.ylim(0,75)
    Base.display(tw)
    Winston.xlabel("num. points") ; Winston.ylabel("error [px]")
    savefig("res/errvsnumpts.pdf",height=200,width=400)
    
 end


end # module test_miccai

