# A short script to test linear programming
module test_linprog

import MathProgBase
import GLPKMathProgInterface
import Gurobi
using JuMP
# solve x= arg min sum_i | x-z_i |

#z=[ 10. ; 7. ; 3. ; 2. ; 4. ; 5. ]
function test_lp()
z=randn(1000)
tic()
n=length(z)
a=spzeros(Float64,2n,n+1)
c=[ 0 ; ones(Float64,n) ]
b=zeros(Float64,2n)

for i=1:n
    a[2i-1,1]=-1. ;     a[2i,1]=1.  
    a[2i-1,1+i]=1. ;    a[2i,1+i]=1.
    b[2i-1]=-z[i]
    b[2i]=z[i]
end

#@show full(a)


println("GLPK")

solver=GLPKMathProgInterface.GLPKSolverLP(method=:Simplex, presolve=false)
#solver=GLPKMathProgInterface.GLPKSolverLP(method=:InteriorPoint, presolve=false)
@time lpsol=MathProgBase.linprog(c,a,'>',b,-Inf,Inf,solver)
toc()
#@show lpsol
#@show lpsol.status
println("obj=",lpsol.objval," m=",lpsol.sol[1])

println("Gurobi simplex")
solver=Gurobi.GurobiSolver(Method=1,OutputFlag=0) #simplex
@time lpsol=MathProgBase.linprog(c,a,'>',b,-Inf,Inf,solver)
#@show lpsol
#@show lpsol.status
println("obj=",lpsol.objval," m=",lpsol.sol[1])

println("Gurobi barrier")
solver=Gurobi.GurobiSolver(Method=2,OutputFlag=0) 
@time lpsol=MathProgBase.linprog(c,a,'>',b,-Inf,Inf,solver)
#@show lpsol
#@show lpsol.status
println("obj=",lpsol.objval," m=",lpsol.sol[1])
end

function test_jump()
    #solver=GLPKMathProgInterface.GLPKSolverLP(method=:Simplex, presolve=false)
    solver=Gurobi.GurobiSolver(Method=2,OutputFlag=0) 
    n=1000
    z=randn(n)
    tic()
    m=Model(solver=solver)
    @variable(m,mean)
    @variable(m,d[1:n])
    for i=1:n
        @constraint(m,z[i]-mean<=d[i])
        @constraint(m,mean-z[i]<=d[i])
    end
    @objective(m,:Min,sum{d[i],i=1:n})
    solve(m)
    toc()
    println("obj=",sum(getvalue(d))," m=",getvalue(mean))

end
end
