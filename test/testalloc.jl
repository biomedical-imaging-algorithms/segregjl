module testalloc

const n=10000
const m=10000

@fastmath function sins(x::Float64)
    a=Array(Float64,n)
    @simd for i=1:n
        @inbounds a[i]=sin(x+i*0.1)
    end
    return a
end

@fastmath function sins!{T}(x::Float64,a::Array{T})
    @simd for i=1:n
        @inbounds a[i]=sin(x+i*0.1)
    end
end

@fastmath function test1()
    s=0.
    for j=1:m
        s+=sum(sins(0.1*j))
    end
    return s
end

@fastmath function test2()
    s=0.
    @simd for j=1:m
        s+=sum([sin(0.1*j+i*0.1) for i=1:n])
    end
    return s
end

@fastmath function test3()
    s=0.
    a=Array(Float64,n)
    for j=1:m
        sins!(0.1*j,a)
        s+=sum(a)
    end
    return s
end

@fastmath function test4()
    s=0.
    a=Array(Float64,n)
    for j=1:m
        sins!(0.1*j,a)
        @simd for i=1:n
            @inbounds s+=a[i]
        end
    end
    return s
end

function test5()
    s=0.
    a=Array(Any,n)
    for j=1:m
        sins!(0.1*j,a)
        @fastmath s+=sum(a)
    end
    return s
end

function main()
    #no preallocation
    println("test1=",test1())
    @time test1()
    
    #no preallocation with comprehensions
    println("test2=",test2())
    @time test2()
    
    #preallocated
    println("test3=",test3())
    @time test3()
    
    #sum() replaced with for
    println("test4=",test4())
    @time test4()
    
    #Array(Any)
    println("test5=",test5())
    @time test5()
end

end
