module testdef

using Devectorize

function transform_point(A::Matrix{Float64}, inp::Vector{Float64})
 outp=A*[inp ; 1]
end

function transform_point!(A::Matrix{Float64}, inp::Vector{Float64}, outp::Vector{Float64})
    outp = A * [inp; 1]
    return nothing
end

function transform_point3!(A::Matrix{Float64}, inp::Vector{Float64}, outp::Vector{Float64})
    outp=A[:,1:end-1]*inp+A[:,end]
    return nothing
end


function transform_point3(A::Matrix{Float64}, inp::Vector{Float64})
 outp=A[:,1:end-1]*inp+A[:,end]
end

function transform_point4(A::Matrix{Float64}, inp::Vector{Float64})
 outp=A[:,1:end-1]*inp
 outp+=A[:,end]
end


function transform_point2(A::Matrix{Float64}, inp::Vector{Float64})
    #  matrix-vector multiplication unrolled for speed
    outp=zeros(Float64,2) ;
    outp[1]=A[1,1]*inp[1]+A[1,2]*inp[2]+A[1,3] #  cos  sin
    outp[2]=A[2,1]*inp[1]+A[2,2]*inp[2]+A[2,3] # -sin  cos
    return outp
end


function transform_point2!(A::Matrix{Float64}, inp::Vector{Float64}, outp::Vector{Float64})
    #  matrix-vector multiplication unrolled for speed
    outp[1]=A[1,1]*inp[1]+A[1,2]*inp[2]+A[1,3] #  cos  sin
    outp[2]=A[2,1]*inp[1]+A[2,2]*inp[2]+A[2,3] # -sin  cos
    return nothing
end



function test_transform()
    A=[1.  0. 3. ; 0.  1. 4. ];
    inp=[5. ;6.] ;
    outp=zeros(Float64,2) ;
    n=1000000 ;
    tic() ;
    for i=1:n
        outp=transform_point(A,inp)
    end
    toc() ;
    tic() ;
    for i=1:n
        outp=transform_point2(A,inp)
    end
    toc() ;
    tic() ;
    for i=1:n
        outp=transform_point3(A,inp)
    end
    toc() ;
    tic() ;
    for i=1:n
        outp=transform_point4(A,inp)
    end
    toc() ;
    tic() ;
    for i=1:n
        transform_point!(A,inp,outp)
    end
    toc() ;
    tic() ;
    for i=1:n
        transform_point2!(A,inp,outp)
    end
    toc() ;
    tic() ;
    for i=1:n
        transform_point3!(A,inp,outp)
    end
    toc() ;
    @time outp=transform_point(A,inp)
    @time outp=transform_point2(A,inp)
    @time outp=transform_point3(A,inp)
    @time outp=transform_point4(A,inp)
    @time transform_point!(A,inp,outp)
    @time transform_point2!(A,inp,outp)
    @time transform_point3!(A,inp,outp)
end


function test_transform_big()
    A=ones(1000,1001);
    inp=ones(1000) 
    outp=zeros(Float64,1000) ;
    n=100 ;
    tic() ;
    for i=1:n
        outp=transform_point(A,inp)
    end
    toc() ;
    tic() ;
    for i=1:n
        outp=transform_point3(A,inp)
    end
    toc() ;
    tic() ;
    for i=1:n
        outp=transform_point4(A,inp)
    end
    toc() ;
    tic() ;
    for i=1:n
        transform_point!(A,inp,outp)
    end
    toc() ;
    tic() ;
    for i=1:n
        transform_point3!(A,inp,outp)
    end
    toc() ;
    @time outp=transform_point(A,inp)
    @time outp=transform_point3(A,inp)
    @time outp=transform_point4(A,inp)
    @time transform_point!(A,inp,outp)
    @time transform_point3!(A,inp,outp)
end


end
