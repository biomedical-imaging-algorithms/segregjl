""" Experiments with MI registration
    Martin Dolejsi, 2015
"""

module testreg_mi

using Images
using ImageView
using Options
using keypoints
import optimizer
import finereg
import deformation
using criterion
import ssd
import mil
import segimgtools
import SLICsuperpixels
import ImageView.view
import Winston
import ColorTypes
import readMetaimage
import erroreval
using Iterators
using regularization

include("debugassert.jl")
switchasserts(true)
include("debugprint.jl")
switchprinting(true)

function test_mi()
    # img2=Images.load("imgs/simpleBW.png")
    img2=Images.load("imgs/case03-5-he-small.png")
    img2=convert(Images.Image{Images.Gray},img2)
    # angle=15. ; shift=[11.;-5.]
    angle=3. ; shift=[0.;0.]

    opts = @options default = img2[1]
    def = deformation.RigidDeformation2D([size(img2)...] .* Images.pixelspacing(img2), [angle; shift])
    img1 = deformation.transform_image_lin(def, img2, size(img2), opts)
    # img1=Images.load("imgs/case03-3-psap-smallBW.png")
    
    quantQ = 20
    hmax=20
    spacing=20
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=200,sigma=[1.;1.]),"find simple keypoints")

    img1=segimgtools.quantize(img1, quantQ)
    img2=segimgtools.quantize(img2, quantQ)
    
    h = mil.joint_histogram(img1, img2, quantQ)
    # ImageView.view(img1,name="img1")
    # ImageView.view(img2,name="img2")
    ImageView.view(h,name="joint histogram")
    kpts=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    # ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
    
    crit_params=mil.MILParameters(quantQ)
    critstate::CriterionState{typeof(crit_params)}=criterion_init(img1, img2, crit_params)
    
    contributions=finereg.precalculate_contributions(img2, kpts, kptsimple, critstate, hmax)
    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    a = collect(-2:.25:4)
    crit = Array(Float64, length(a))
    for i =1:length(a)
        theta = [a[i],0.,0.]
        crit[i] = finereg.eval_contributions(theta, def, kpts, kpts, contributions, Float64[])
        println("$(a[i]): $(crit[i])")
    end
    Winston.figure()
    p=Winston.plot(a,crit)
    Base.display(p)
end

function test_kp_mi()
    img2=Images.load("imgs/simpleBW.png")
    img2=convert(Images.Image{Images.Gray},img2)   # on Unix the previous command gets us an RGB image 
    # img2=Images.load("imgs/case03-5-he-smallBW.png")
    # angle=15. ; shift=[11.;-5.]
    angle=0. ; shift=[3.;0.]
    #angle=0. ; shift=[0.;0.]

    opts = @options default = img2[1]
    def = deformation.RigidDeformation2D([size(img2)...] .* Images.pixelspacing(img2), [angle; shift])
    img1 = deformation.transform_image_lin(def, img2, size(img2), opts)
    # img1=Images.load("imgs/case03-3-psap-smallBW.png")
    
    quantQ = 3
    img1=segimgtools.quantize(img1, quantQ)
    img2=segimgtools.quantize(img2, quantQ)
    
    h = mil.joint_histogram(img1, img2, quantQ)
    ImageView.view(Images.sc(img1),name="img1")
    ImageView.view(Images.sc(img2),name="img2")
    # ImageView.view(h,name="joint histogram")
    
    spacing=10
    hmax=10
    #construct sampled keypoint (including sampled pixels)
    kptss = Array(keypoints.SampledKeypoint{UInt8},1)
    #kptss[1] = keypoints.SampledKeypoint{UInt8}([97-shift[1],57.5-shift[2]],[1.,0.],10.0,UInt8[0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03])
    kptss[1] = keypoints.SampledKeypoint{UInt8}([97-shift[1],57.5-shift[2]],[1.,0.],10.0,collect(chain(repeated(0x01,hmax),repeated(0x03,hmax))))
    kptssimple=finereg.transform_keypoints(kptss, Nullable())
    # v=keypoints.sample_normal(img1,kptss[1],5)        #check normal (is the keypoint at the border?)

    #construct sampled keypoint
    kptst = Array(keypoints.TwoclassKeypoint,1)
    kptst[1] = keypoints.TwoclassKeypoint([97-shift[1],57.5-shift[2]], 10., [1.,0.], 0x03, 0x01)
    kptstimple=finereg.transform_keypoints(kptst, Nullable())
    
    # img1kptss=keypoints.create_image_with_keypoints(img1,kptssimple)
    # ImageView.view(img1kptss/maximum(img1kptss),name="with Sampled keypoints")
    # img1kptst=keypoints.create_image_with_keypoints(img1,kptstimple)
    # ImageView.view(img1kptst/maximum(img1kptst),name="with Twoslass keypoints")

    crit_params=mil.MILParameters(quantQ)
    critstate::CriterionState{typeof(crit_params)}=criterion_init(img1, img2, crit_params)
    
    #compute contributions for both
    contributionss=finereg.precalculate_contributions(img2, kptss, kptssimple, critstate, hmax)
    contributionst=finereg.precalculate_contributions(img2, kptst, kptstimple, critstate, hmax)
    println("       sampled,          twoclass,        difference")
    println([contributionss.D'; contributionst.D'; contributionss.D'-contributionst.D']')
    Winston.figure()
    p=Winston.plot(contributionss.D,"or")    #sampled
    Winston.oplot(contributionst.D,"og")     #twoclass
    Winston.oplot(contributionss.D-contributionst.D,"b")     #sampled - twoclass
    Winston.title("twoclass-green, sampled-red, sam-tc -blue")
    Base.display(p)
    return
end

function test_register_mi()
    # img2=Images.load("imgs/simpleBW.png")
    # img2=Images.load("imgs/case03-5-he-smallBW.png")
    img2=convert(Images.Image{Images.Gray},Images.load("imgs/case03-5-he-small.png"))
    # img2 = readMetaimage.read_data("m:/RIRE/training_001/ct/training_001_ct.mhd");
    # img2 = Images.grayim(img2[:,:,10])
    angle=15. ; shift=[11.;-5.]
    #angle=0. ; shift=[5.;0]
    
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    #img1=Images.load("imgs/case03-3-psap-smallBW.png")
    # img1=convert(Images.Image{Images.Gray},Images.load("imgs/case03-3-psap-small.png"))

    quantQ = 5
    spacing=15
    hmax=10
    
    #create keypoints at high gradients
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=500,sigma=[1.;1.]),"find simple keypoints")
    
    img1=segimgtools.quantize(img1, quantQ)
    img2=segimgtools.quantize(img2, quantQ)
    #sample quantized image
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    # kptsampled, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(img1, spacing, 1),"find_keypoints_and_neighbors:")
    # kptsimple=finereg.transform_keypoints(kptsampled, Nullable())
    
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
    ImageView.view(segimgtools.overlay(img1/maximum(img1),img2/maximum(img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    crit_params=mil.MILParameters(quantQ)
    opt_state=optimizer.OptimizerMMA(abstol=1e-6,neval=1000,initstep=1.0)
    reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    # def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    bc = 4
    # def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), round(Int,[bc,bc]))
    # bc = 2
    def = deformation.BsplineDeformation(1,[size(img2)...] .* Images.pixelspacing(img2), round(Int,[bc,bc]))
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    
    # Profile.clear()
    # Profile.init(delay = 0.0001)
    # d= @profile finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,reg_params,@options resample=true hmax=hmax minsize=64 minKP=50000 largerotation=false)
    # aaa=open("c:/Temp/prof.txt","w")
    # Profile.print(aaa,format = :tree, combine = true, sortedby = :count)
    # close(aaa)
    # Profile.clear()
    
    @time d= finereg.register_fast_multiscale(img1,img2,kptsampled,def,nhoods,crit_params,opt_state,reg_params,@options resample=true hmax=hmax minsize=64 minKP=500 largerotation=false)
    
    r=deformation.get_theta(d)
    println("Registration result: $r true values: $angle $shift")
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)
    ImageView.view(segimgtools.overlay(img1/maximum(img1),warped/maximum(warped)),name="overlay after") 
end

function test_register_mi_hb()
    # img2=Images.load("imgs/simpleBW.png")
    # img2=convert(Images.Image{Images.Gray},img2)   # on Unix the previous command gets us an RGB image 
    # img2=Images.load("imgs/case03-5-he-smallBW.png")
    # img2=Images.load("m:/microscopy/Flagship/images-MultiGeneSignatureMaps_convert/scale-5pc/Case002/case02-2-ki67.jpg"); img2 = convert(Images.Image{Images.Gray}, img2)
    img2 = readMetaimage.read_data("m:/RIRE/training_001/ct/training_001_ct.mhd");
    img2 = Images.grayim(img2[:,:,10])
    angle=20. ; shift=[11.;-5.]
    angle=0. ; shift=[5.;0]
   
    img1=deformation.transform_rigid2D(img2,size(img2),angle,shift, @options default = img2[1])
    # img1=Images.load("imgs/case03-3-psap-smallBW.png")
    # img1=Images.load("m:/microscopy/Flagship/images-MultiGeneSignatureMaps_convert/scale-5pc/Case002/case02-1-he.jpg"); img1 = convert(Images.Image{Images.Gray}, img1)
    quantQ = 5
    spacing=30#15
    kpnum = 1000#500
    hmax=30
    
    #create keypoints at high gradients
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=kpnum,sigma=[1.;1.]),"find simple keypoints")
    
    img1=segimgtools.quantize(img1, quantQ)
    img2=segimgtools.quantize(img2, quantQ)
    #sample quantized image
    kptsampled=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    # kptsampled, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(img1, spacing, 1),"find_keypoints_and_neighbors:")
    # kptsimple=finereg.transform_keypoints(kptsampled, Nullable())
    
    # img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    # ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")

    # ImageView.view(segimgtools.overlay(img1/maximum(img1),img2/maximum(img2)),name="overlay before") 
    @debugprintln("Initialize criterion")
    crit_params=mil.MILParameters(quantQ)
    opt_state=optimizer.OptimizerMMA(abstol=1e-3,neval=5000,initstep=20.0)
    # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-3,neval=50000,initstep=20.0)
    # reg_params = regularization.NoneRegularizationParams()
    # reg_params = regularization.SumRegularizationParams([1e-9, 0])
    reg_params = regularization.NoneRegularizationParams()
    isize=[size(img1)...]
    pxsp=Images.pixelspacing(img1)
    # def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation2D)
    # def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [4,4])
    aa = [deformation.BsplineDeformation(3, isize .* pxsp, [11,11]);
          deformation.create_identity(isize .* pxsp, deformation.RigidDeformation2D)]
    def = deformation.HierarchicalDeformation(aa)
    # def=deformation.create_identity([size(img2)...] .* Images.pixelspacing(img2), deformation.RigidDeformation2D)
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")

    resampleFactor= [0,1,1,1,1]
    levelProg=      [1,0,0,0,0]

    d = @debugtimeline(finereg.register_fast_multiscaleHB( img1, img2, kptsampled, def, nhoods, crit_params,
                       opt_state, reg_params, resampleFactor, levelProg,
                       @options hmax = hmax minKP = 500 largerotation = false resample = true), "Multiscale registration:")

    r=deformation.get_theta(d)
    @time warped=deformation.transform_image_nn(d,img2,size(img1),@options)# default = img2[1])
    ImageView.view(segimgtools.overlay(img1/maximum(img1),warped/maximum(warped)),name="overlay after")
    # println("-------------------------")
    # println(def1.splines[1].theta-d.splines[1].theta)
    # println("-------------------------")
    # println(def1.splines[2].theta-d.splines[2].theta)
end

function test_mi_3D()
    basedir = "m:/RIRE/"
    if !isdir(basedir)
        basedir = "/datagrid/Medical/RIRE/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/RIRE/"
    end
    if !isdir(basedir)
        basedir = "c:/Temp/RIRE/"
    end
    # img2 = readMetaimage.read_data(string(basedir, "training_001/ct/training_001_ct.mhd"));
    img2 = Images.grayim(data(Images.load("c:/Temp/phantom3D512.tif"))); #img2["pixelspacing"] = [1., 1., 5.]
    # angle=15. ; shift=[11.;-5.]
    angle=[0.;0.;0.] ; shift=[0.;1.;0.]

    opts = @options default = img2[1]
    def = deformation.RigidDeformation3D([size(img2)...] .* Images.pixelspacing(img2), [angle; shift])
    img1 = deformation.transform_image_lin(def, img2, size(img2), opts)
    # img1 = readMetaimage.read_data(string(basedir, "training_001/mr_T1/training_001_mr_T1.mhd"));
    
    quantQ = 4
    spacing= 20
    hmax=20
    numberKP = 15
    
    kptsimple=@debugtime(keypoints.find_keypoints_from_gradients(img1;spacing=spacing,number=numberKP,sigma=[1.;1.;1.]),"find simple keypoints")

    # for i in kptsimple
        # i.normal = round(i.normal * 10) / 10
        # i.normal /= norm(i.normal)
    # end
    img1=segimgtools.quantize(img1, quantQ)
    img2=segimgtools.quantize(img2, quantQ)
    
    h = mil.joint_histogram(img1, img2, quantQ)
    # ImageView.view(img1/maximum(img1),name="img1")
    # ImageView.view(img2/maximum(img2),name="img2")
    # ImageView.view(h,name="joint histogram")

    kpts=keypoints.create_sampled_keypoints(img1,kptsimple,hmax;spacing=float(spacing))
    
    # kpts, nhoodKP = @debugtime( keypoints.find_keypoints_and_neighbors(img1, spacing, 1),"find_keypoints_and_neighbors:")
    # kptsimple=finereg.transform_keypoints(kpts, Nullable())
    
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple)
    ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
    
    crit_params=mil.MILParameters(quantQ)
    critstate::CriterionState{typeof(crit_params)}=criterion_init(img1, img2, crit_params)
# critstate=ssd.criterion_init(img1,img2,ssd.SSDParameters())
    # kptsimple=finereg.transform_keypoints(kpts, Nullable(def))
    contributions=finereg.precalculate_contributions(img2, kpts, kptsimple, critstate, hmax)
    Winston.figure()
    p=Winston.plot(contributions.D,"k")    #sampled
    Winston.oplot(sum(contributions.D, 2)./numberKP,"r")
    Base.display(p)

    isize=[size(img1)...]
    def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation3D)
    # theta = [-4.748616469812414,0.010172074477347408,-1.4622150967066656,-20.72835665333503,32.00905580555444,17.90059679012042]
    theta = [angle; shift]
    a = collect(-1:.25:10)
    crit = Array(Float64, length(a))
    for i =1:length(a)
        theta[5] = a[i]
        crit[i] = finereg.eval_contributions(theta, def, kpts, kptsimple, contributions, Float64[])
        println("$(a[i]): $(crit[i])")
    end
    Winston.figure()
    p=Winston.plot(a,crit)
    Base.display(p)
end

function test_kp_mi_3D()
    basedir = "m:/RIRE/"
    if !isdir(basedir)
        basedir = "/datagrid/Medical/RIRE/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/RIRE/"
    end
    if !isdir(basedir)
        basedir = "c:/Temp/RIRE/"
    end
    img2 = readMetaimage.read_data(string(basedir, "training_001/ct/training_001_ct.mhd"));
    # angle=15. ; shift=[11.;-5.]
    angle=[0.;0.;0.] ; shift=[2.;0.;0.]

    opts = @options default = img2[1]
    def = deformation.RigidDeformation3D([size(img2)...] .* Images.pixelspacing(img2), [angle; shift])
    img1 = deformation.transform_image_lin(def, img2, size(img2), opts)
    # img1 = readMetaimage.read_data(string(basedir, "training_001/mr_T1/training_001_mr_T1.mhd"));

    #quantize images
    quantQ = 3
    img1=segimgtools.quantize(img1, quantQ)
    img2=segimgtools.quantize(img2, quantQ)
    
    h = mil.joint_histogram(img1, img2, quantQ)
    # ImageView.view(img1,name="img1")
    # ImageView.view(img2,name="img2")
    # ImageView.view(h,name="joint histogram")

    spacing=10
    hmax=20
    #construct sampled keypoint (including sampled pixels)
    kptss = Array(keypoints.SampledKeypoint{UInt8},1)
    kptss[1] = keypoints.SampledKeypoint{UInt8}([92.8-shift[1],116.6667075-shift[2],30.0-shift[3]],[0.34946642633457126,0.24604756798143723,-3.026276967776032],10.0,collect(chain(repeated(0x01,hmax),repeated(0x02,hmax))))
    kptssimple=finereg.transform_keypoints(kptss, Nullable())
    
    #construct sampled keypoint
    kptst = Array(keypoints.TwoclassKeypoint,1)
    kptst[1] = keypoints.TwoclassKeypoint([92.8-shift[1],116.6667075-shift[2],30.0-shift[3]], 10., [0.34946642633457126,0.24604756798143723,-3.026276967776032], 0x02, 0x01)
    kptstimple=finereg.transform_keypoints(kptst, Nullable())
    
    # img1kptss=keypoints.create_image_with_keypoints(img1,kptssimple)
    # ImageView.view(img1kptss/maximum(img1kptss),name="with Sampled keypoints")
    # img1kptst=keypoints.create_image_with_keypoints(img1,kptstimple)
    # ImageView.view(img1kptst/maximum(img1kptst),name="with Twoslass keypoints")

    crit_params=mil.MILParameters(quantQ)
    critstate::CriterionState{typeof(crit_params)}=criterion_init(img1, img2, crit_params)
    
    #compute contributions for both
    contributionss=finereg.precalculate_contributions(img2, kptss, kptssimple, critstate, hmax)
    contributionst=finereg.precalculate_contributions(img2, kptst, kptstimple, critstate, hmax)
    println("       sampled,           twoclass,          difference")
    println([contributionss.D'; contributionst.D'; contributionss.D'-contributionst.D']')
    Winston.figure()
    p=Winston.plot(contributionss.D,"or")    #sampled
    Winston.oplot(contributionst.D,"og")     #twoclass
    Winston.oplot(contributionss.D-contributionst.D,"b")     #sampled - twoclass
    Winston.title("twoclass-green, sampled-red, sam-tc -blue")
    Base.display(p)
    return
end

function test_register_mi_3D()
#ct-t1[-4.748616469812414,0.010172074477347408,-1.4622150967066656,-20.72835665333503,32.00905580555444,17.90059679012042]
    basedir = "c:/Temp/"
    if !isdir(basedir)
        basedir = "/datagrid/Medical/"
    end
    if !isdir(basedir)
        basedir = "/home/kybic/data/Medical/"
    end
    if !isdir(basedir)
        basedir = "m:/"
    end
    filename2 = "POPI/02/00"
    img2 = readMetaimage.read_data(string(basedir, filename2, ".mhd"));
    # img2 = readMetaimage.read_data(string(basedir, "RIRE/training_001/ct/training_001_ct.mhd"));
    # img2 = readMetaimage.read_data(string(basedir, "RIRE/training_001/mr_PD/training_001_mr_PD.mhd"));
    # img2 = convert(Images.Image{Images.Gray},Images.load("c:/Temp/ph3d.tif")); img2["spatialorder"] = ["x" "y" "z"]; img2["pixelspacing"] = [1.; 1.; 1.]
    # img2 = convert(Images.Image{Images.Gray},Images.load("c:/Temp/phantom3D512.tif")); img2["spatialorder"] = ["x" "y" "z"]; img2["pixelspacing"] = [1.; 1.; 10.]

    quantQ = 6
    KPspacing = 15
    KPcount = 4000#1000#4000
    hmax = 20#30
    angle = [-15.;3.;1.];
    shift = [-5; 10.; 1];
    opts = @options default = img2[1, 1, 1]

    def = deformation.RigidDeformation3D([size(img2)...] .* Images.pixelspacing(img2), [angle; shift])
    # # img1 = Images.grayim(Array(eltype(img2),(div([size(img2)...],2)...)))
    # # img1["spatialorder"] = ["x", "y", "z"]
    # # img1["pixelspacing"] = Images.pixelspacing(img2).*2
    # # deformation.transform_image_lin!(def, img1, img2, opts)
    # img1 = deformation.transform_image_lin(def, img2, size(img2), opts)
    filename1 = "POPI/02/50"
    img1 = readMetaimage.read_data(string(basedir, filename1, ".mhd"))
    # img1=img2
    # img1 = readMetaimage.read_data(string(basedir, "RIRE/training_001/mr_T2/training_001_mr_T2.mhd"));
    # img1 = readMetaimage.read_data(string(basedir, "RIRE/training_001/mr_T1/training_001_mr_T1.mhd"));
    # img1 = readMetaimage.read_data(string(basedir, "RIRE/training_001/mr_T2_rectified"));
    # img1 = readMetaimage.read_data(string(basedir, "RIRE/training_001/mr_PD/training_001_mr_PD.mhd"));
# a=collect(.7:.01:1)#collect(.4:.01:3)#[.5 .6 .625 .7 .8 .9 1 1.1 1.25]
# m=zeros(Float64, length(a))
# for i=1:length(a)
# nlen = a[i]
pxsp1 = Images.pixelspacing(img2)
pxsp2 = Images.pixelspacing(img2)
img1 = Images.grayim(img1[1:470,1:365,1:155]); img1["spatialorder"] = ["x", "y", "z"]; img1["pixelspacing"] = pxsp1
img2 = Images.grayim(img2[1:470,1:365,1:155]); img2["spatialorder"] = ["x", "y", "z"]; img2["pixelspacing"] = pxsp2

# ImageView.view((img1-minimum(img1))/(maximum(img1)-minimum(img1)))
# ImageView.view((img2-minimum(img2))/(maximum(img2)-minimum(img2)))

    nlen = min(minimum(Images.pixelspacing(img1)), minimum(Images.pixelspacing(img2)))

    img2 = segimgtools.quantize(img2, quantQ)
    img1 = segimgtools.quantize(img1, quantQ)
    # ImageView.view((img1-minimum(img1))/(maximum(img1)-minimum(img1)))
    # ImageView.view((img2-minimum(img2))/(maximum(img2)-minimum(img2)))

    kptsimple = @debugtime(keypoints.find_keypoints_from_gradients(img1; spacing = KPspacing, number = KPcount, sigma = 2 ./ Images.pixelspacing(img1), nlength = nlen), "find simple keypoints")

    # nhood = keypoints.keypoint_neighbors(kptsimple)
    # kptsimple, nhood = subsample_keypoints_equidistant(kptsimple, nhood, [1,1,1])
    # kptsimple, nhood = subsample_keypoints_equidistant(kptsimple, nhood, [1,1,1])
    # nhood = keypoints.keypoint_neighbors(kptsimple)
    # kptsimple, nhood = subsample_keypoints_equidistant(kptsimple, nhood, [1,1,1])
    kptsampled=keypoints.create_sampled_keypoints(img1, kptsimple, hmax; spacing = float(KPspacing))
    
    img1kpts=keypoints.create_image_with_keypoints(img1,kptsimple,llen=hmax)
    ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
    # img1kpts["timedim"] = 2
    # ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
    # img1kpts["timedim"] = 1
    # ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
    # ImageView.view(segimgtools.overlay((img1 - minimum(img1)) / (maximum(img1) - minimum(img1)), (img2 - minimum(img2)) / (maximum(img2) - minimum(img2))), name="overlay before")

    @debugprintln("Initialize criterion")
    crit_params=mil.MILParameters(quantQ + 1)
    # opt_state=optimizer.OptimizerMMA(abstol=1e-12,neval=5000,initstep=5.0)
    # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-6,neval=50000,initstep=5.0)
    opt_state=optimizer.OptimizerGreedyDescent(abstol=1e-6,neval=4,initstep=2.0)
    reg_params = regularization.NoneRegularizationParams()
    # reg_params = regularization.SumRegularizationParams([1e-6])

    isize=[size(img1)...]
    # def=deformation.create_identity(isize .* Images.pixelspacing(img1), deformation.RigidDeformation3D)
    def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [11,11,7])
    # def = deformation.BsplineDeformation(3,[size(img2)...] .* Images.pixelspacing(img2), [19,19,11])
    # theta=-7+(14)*rand(length(deformation.lower_bound(def))); deformation.set_theta!(def,theta)
    @debugprintln("Finding keypoint neighbors")
    nhoods=keypoints.keypoint_neighbors(kptsampled)
    @debugprintln("Registering multiscale")
    @time d = finereg.register_fast_multiscale(img1, img2, kptsampled, def, nhoods, crit_params, opt_state, reg_params, @options resample=true hmax=hmax minsize=12800 minKP=3000 largerotation=false pwl=false)

    ############
    # # measure the error
    erroreval.measure_error_POPI(d, string(basedir, filename1, ".pts"), string(basedir, filename2, ".pts"), string(basedir, filename1, ".mhd"),  string(basedir, filename2, ".mhd"))
    # # _, m[i]=erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "RIRE/training_001/transformations.csv/ct_T1.csv"))
    # # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "RIRE/training_001/transformations.csv/ct_T2.csv"))
    # # erroreval.measure_error_RIRE(img1, img2, d, string(basedir, "RIRE/training_001/transformations.csv/ct_pd.csv"))
    # erroreval.measure_error_RIRE(img1, img2, d, angle, shift)
# end
# Winston.figure()
# p=Winston.plot(a,m)
# Winston.xlabel("Normal length [mm]")
# Winston.ylabel("Error [px]")
# Base.display(p)
    r=deformation.get_theta(d)
    # println("Registration result: $r true values: $angle $shift")
    warped = similar(img1)

    mos = segimgtools.overlay(Images.grayim((img2 - minimum(img2)) / (maximum(img2) - minimum(img2))), Images.grayim((img1 - minimum(img1)) / (maximum(img1) - minimum(img1))))
    ImageView.view(mos)
    mos["timedim"] = 2
    ImageView.view(mos)    
    mos["timedim"] = 1
    ImageView.view(mos)
    @time deformation.transform_image_lin!(d, warped, img2, @options)
    mos = segimgtools.overlay(Images.grayim((warped - minimum(warped)) / (maximum(warped) - minimum(warped))), Images.grayim((img1 - minimum(img1)) / (maximum(img1) - minimum(img1))))
    ImageView.view(Images.grayim((warped - minimum(warped)) / (maximum(warped) - minimum(warped))))
    # mos = segimgtools.overlay(warped, img1)
    ImageView.view(mos)
    mos["timedim"] = 2
    ImageView.view(mos)    
    mos["timedim"] = 1
    ImageView.view(mos)
end

function test_flagship()
    maxd = 30
    regular = 0
    kptsnum = 1000
    minimgsize = 128
    minkpts = 500
    kpdist = 30
    largerot = false
    quantQ = 5
    bc1 = 7
    
    tmeanmin = 70
    tmeanstep = 5

    crit_params=mil.MILParameters(quantQ + 1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-3,neval=5000,initstep=20.0)
    # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-9,neval=500000,initstep=2.0)
    if regular == 0
        reg_params = regularization.NoneRegularizationParams()
    else
        reg_params = regularization.SumRegularizationParams([regular, 0])
    end
    if !isdir("res/")
        mkdir("res/")
    end
    logdir = "c:/Temp/"
    logfile = string(logdir, "output.csv")
    if !isdir(logdir)
        logfile = "outputMINIMGSIZE64.csv"
    end
    basedir = "m:/microscopy/"
    if !isdir(basedir)
        basedir = "/datagrid/Medical/microscopy/"
    end
    listdir = string(basedir, "lists_regImgSets/flagship-scale-5pc/")
    listfiles = readdir(listdir)
    sort!(listfiles)
    # Init log file
    fstr = open(logfile, "w")
    write(fstr, "Base file, Reg. file, mean dist. before [px], mean dist. after [px], std dist.after [px], median dist. after [px], ")
    for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
        write(fstr, "mean$(rtm)\% dist. after [px], ")
    end
    write(fstr, "min dist. before [px], min dist. after [px], max dist. before [px], max dist. after [px]\n")
    close(fstr)
    # Init output
    totmin0 = 1.11e5
    totmax0 = zero(Float64)
    totlen0 = zero(Float64)
    totTcnt = zero(Float64)
    totdistv = Float64[]
    totRdistv = Float64[]
    # Iterate all the possible file combinations
    for casenum = 1:length(listfiles)
        fstr = open(string(listdir, listfiles[casenum]), "r")
        files = readcsv(fstr)
        close(fstr)
        if (files[1][1:4] == "CIMA")
            continue
        end
        # Init output
        casemin0 = 1.11e5
        casemax0 = zero(Float64)
        caselen0 = zero(Float64)
        caseTcnt = zero(Float64)
        casedistv = Float64[]
        caseRdistv = Float64[]
        for filnum = 1:2:length(files)
            # Read base image and anotations
            println(string("Opening base: ", files[filnum]))
            imgbase = Images.load(string(basedir, files[filnum]))
            # imgbase = convert(Images.Image{Images.Gray}, imgbase)
            fstr = open(string(basedir, files[filnum + 1]), "r")
            landbase = readdlm(fstr, Float64; skipstart = 2)
            close(fstr)
            
            # Find keypoints
            kptsimple=keypoints.find_keypoints_from_gradients(imgbase; spacing=kpdist, number=kptsnum, sigma=[1.;1.])
            imgbase=segimgtools.quantize(imgbase, quantQ)
            kpts=keypoints.create_sampled_keypoints(imgbase, kptsimple, maxd; spacing=float(kpdist))
            nhoods=keypoints.keypoint_neighbors(kpts)

            # img1kpts=keypoints.create_image_with_keypoints(imgbase,kptsimple)
            # ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
            # For all the rest of the set
            for filnum2 = filnum + 2:2:length(files)
                println(string("   Opening second: ", files[filnum2]))
                imgreg = Images.load(string(basedir, files[filnum2]))
                # imgreg = convert(Images.Image{Images.Gray}, imgreg)
                fstr = open(string(basedir, files[filnum2 + 1]), "r")
                landreg = readdlm(fstr, Float64; skipstart = 2)
                close(fstr)
                imgreg=segimgtools.quantize(imgreg, quantQ)

                # Init registration
                vlen = ceil(Int, log(minimum(min(size(imgreg),size(imgbase))) / minimgsize) / log(2)) + 1
                resampleFactor = ones(Int, vlen)
                resampleFactor[1] = 0
                levelProg = zeros(Int, vlen)
                levelProg[1]  = 1
                sizemm = Images.pixelspacing(imgreg) .* [size(imgreg)...]
                # def = deformation.HierarchicalDeformation([ deformation.BsplineDeformation(1,sizemm, [bc1,bc1]);
                                                        # deformation.BsplineDeformation(1,sizemm, [4,4])])
                def = deformation.HierarchicalDeformation([ deformation.BsplineDeformation(3, sizemm, [bc1,bc1]);
                                                            deformation.create_identity(sizemm, deformation.RigidDeformation2D)
                                                         ])
                # def = deformation.create_identity(sizemm, deformation.RigidDeformation2D)
                # Register
                d = finereg.register_fast_multiscaleHB(imgbase, imgreg, kpts, def, nhoods,
                        crit_params, opt_state, reg_params, resampleFactor, levelProg,
                        @options hmax = maxd minKP = minkpts resample = true largerotation = largerot)

                # Evaluate the registration
                y  = zeros(Float64, 2)
                # Init output
                tupmin0 = 1.11e5
                tupmax0 = zero(Float64)
                tuplen0 = zero(Float64)
                setdistv = Array(Float64, size(landreg, 1))
                for poin = 1:size(landreg, 1)
                    deformation.transform_point!(d, vec(landbase[poin, :]), y)
                    setdistv[poin] = sqrt(sum((vec(landreg[poin, :]) - y).^2))
                    len0 = sqrt(sum((vec(landreg[poin, :]) - vec(landbase[poin, :])).^2))
                    tupmin0 = min(tupmin0, len0)
                    tupmax0 = max(tupmax0, len0)
                    tuplen0 += len0
                end
                sort!(setdistv)
                casemin0 = min(casemin0, tupmin0)
                casemax0 = max(casemax0, tupmax0)
                caselen0 += tuplen0
                caseTcnt += 1
                casedistv = [casedistv; setdistv]
                if (0.5 * tuplen0 > sum(setdistv))
                    caseRdistv = [caseRdistv; setdistv]
                end
                # Write image pair result into log file
                fstr = open(logfile, "a")
                write(fstr, string(files[filnum], ", ", files[filnum2], ", "))
                print_shortest(fstr, tuplen0 / size(landreg, 1))
                write(fstr, ", ")
                print_shortest(fstr, mean(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, std(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, median!(setdistv))
                for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
                    write(fstr, ", ")
                    print_shortest(fstr, mean(setdistv[1:round(Int, length(setdistv) * rtm / 100)]))
                end
                write(fstr, ", ")
                print_shortest(fstr, tupmin0)
                write(fstr, ", ")
                print_shortest(fstr, minimum(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, tupmax0)
                write(fstr, ", ")
                print_shortest(fstr, maximum(setdistv))
                write(fstr, "\n")
                close(fstr)
                r=deformation.get_theta(d)
                @debugprintln_with_color(:green, "Registration result: len: $(tuplen0/size(landreg, 1)) => $(sum(setdistv)/size(landreg, 1))")
                # ImageView.view(segimgtools.overlay(imgbase/maximum(imgbase),imgreg/maximum(imgreg)),name="overlay before")
                # warped=deformation.transform_image_nn(d.def[2],imgreg,size(imgbase),@options)
                # ImageView.view(segimgtools.overlay(imgbase/maximum(imgbase),warped/maximum(warped)),name="overlay after 1")
                # warped=deformation.transform_image_nn(d,imgreg,size(imgbase),@options)
                # ImageView.view(segimgtools.overlay(imgbase/maximum(imgbase),warped/maximum(warped)),name="overlay after2")
            end
        end
        sort!(casedistv)
        totmin0 = min(totmin0, casemin0)
        totmax0 = max(totmax0, casemax0)
        totlen0 += caselen0
        totTcnt += caseTcnt
        totdistv = [totdistv; casedistv]
        totRdistv = [totRdistv; caseRdistv]
        fstr = open(logfile, "a")
        write(fstr, listfiles[casenum])
        write(fstr, ", , ")
        print_shortest(fstr, caselen0 / length(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, mean(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, std(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, median!(casedistv))
        for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
            write(fstr, ", ")
            print_shortest(fstr, mean(casedistv[1:round(Int, length(casedistv) * rtm / 100)]))
        end
        write(fstr, ", ")
        print_shortest(fstr, casemin0)
        write(fstr, ", ")
        print_shortest(fstr, minimum(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, casemax0)
        write(fstr, ", ")
        print_shortest(fstr, maximum(casedistv))
        write(fstr, "\nRobustness = ")
        print_shortest(fstr, length(caseRdistv) / length(casedistv))
        write(fstr, ", , , ")
        print_shortest(fstr, mean(caseRdistv))
        write(fstr, ", ")
        if isempty(caseRdistv)
            write(fstr, "N/A")
            write(fstr, ", ")
            write(fstr, "N/A")
        else
            print_shortest(fstr, std(caseRdistv))
            write(fstr, ", ")
            print_shortest(fstr, median!(caseRdistv))
        end
        write(fstr, "\n\n")
        close(fstr)
    end
    sort!(totdistv)
    fstr = open(logfile, "a")
    write(fstr, "total, , ")
    print_shortest(fstr, totlen0 / length(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, mean(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, std(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, median!(totdistv))
    for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
        write(fstr, ", ")
        print_shortest(fstr, mean(totdistv[1:round(Int, length(totdistv) * rtm / 100)]))
    end
    write(fstr, ", ")
    print_shortest(fstr, totmin0)
    write(fstr, ", ")
    print_shortest(fstr, minimum(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, totmax0)
    write(fstr, ", ")
    print_shortest(fstr, maximum(totdistv))
    write(fstr, "\nRobustness = ")
    print_shortest(fstr, length(totRdistv) / length(totdistv))
    write(fstr, ", , , ")
    print_shortest(fstr, mean(totRdistv))
    write(fstr, ", ")
    print_shortest(fstr, std(totRdistv))
    write(fstr, ", ")
    print_shortest(fstr, median!(totRdistv))
    write(fstr, "\n")
    close(fstr)
end

function test_popi()
    maxd = 10
    kptsnum = 5700
    minimgsize = 1024
    minkpts = 6000
    kpdist = 10
    largerot = false
    pwl = false
    quantQ = 4
    initstep = 2.
    regular = 0

    tmeanmin = 70
    tmeanstep = 5
    crit_params=mil.MILParameters(quantQ + 1)
    opt_state=optimizer.OptimizerMMA(abstol=1e-2,neval=5000,initstep=initstep)
    # opt_state=optimizer.OptimizerCOBYLA(abstol=1e-9,neval=500000,initstep=2.0)
    if regular == 0
        reg_params = regularization.NoneRegularizationParams()
    else
        reg_params = regularization.SumRegularizationParams([regular])
    end
    logdir = "c:/Temp/"
    logfile = string(logdir, "outputPOPI.csv")
    if !isdir(logdir)
        logfile = "outputPOPIMINIMGSIZE64.csv"
    end
    basedir = "c:/Temp/POPI/"
    if !isdir(basedir)
        basedir = "/datagrid/POPI/"
    end
    listset = readdir(basedir)
    sort!(listset, by=lowercase)
    # Init log file
    fstr = open(logfile, "w")
    write(fstr, "Base file, Reg. file, mean dist. before [mm], mean dist. after [mm], std dist.after [mm], median dist. after [mm], ")
    for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
        write(fstr, "mean$(rtm)\% dist. after [mm], ")
    end
    write(fstr, "min dist. before [mm], min dist. after [mm], max dist. before [mm], max dist. after [mm]\n")
    close(fstr)
    # Init output
    totmin0 = 1.11e5
    totmax0 = zero(Float64)
    totlen0 = zero(Float64)
    totTcnt = zero(Float64)
    totdistv = Float64[]
    totRdistv = Float64[]
    # Iterate all the possible file combinations
    #TODO add dataset 07
    for casenum = 1:length(listset)-1   #dataset 07 has dirrerent name conventions and different landmark format
        files = readdir(string(basedir, listset[casenum]))
        # Init output
        casemin0 = 1.11e5
        casemax0 = zero(Float64)
        caselen0 = zero(Float64)
        caseTcnt = zero(Float64)
        casedistv = Float64[]
        caseRdistv = Float64[]
        filnum = 1
        while filnum <= length(files)
            filename = files[filnum][1:end-4]
            pts = false
            while filnum <= length(files) && filename == files[filnum][1:end-4]
                if (files[filnum][end-2:end] == "pts")
                    pts=true
                end
                filnum += 1
            end
            if !pts
                continue
            end

            # Read base image and anotations
            println(string("Opening base: ", listset[casenum], "/", filename))
            imgbase = readMetaimage.read_data(string(basedir, listset[casenum], "/", filename, ".mhd"))
            # imgbase = convert(Images.Image{Images.Gray}, imgbase)
            landbase = erroreval.read_landmarks_POPI(imgbase, string(basedir, listset[casenum], "/", filename, ".pts"), string(basedir, listset[casenum], "/", filename, ".mhd"))
            # Find keypoints
            imgbase=segimgtools.quantize(imgbase, quantQ)
            kptsimple=keypoints.find_keypoints_from_gradients(imgbase; spacing=kpdist, number=kptsnum, sigma=[1.; 1.; 1.])
            kpts=keypoints.create_sampled_keypoints(imgbase, kptsimple, maxd; spacing=float(kpdist))
            nhoods=keypoints.keypoint_neighbors(kpts)

            # img1kpts=keypoints.create_image_with_keypoints(imgbase,kptsimple)
            # ImageView.view(img1kpts/maximum(img1kpts),name="with keypoints")
            # For all the rest of the set
            filnum2 = filnum
            while filnum2 <= length(files)
                filename2 = files[filnum2][1:end-4]
                pts = false
                while filnum2 <= length(files) && filename2 == files[filnum2][1:end-4]
                    if (files[filnum2][end-2:end] == "pts")
                        pts=true
                    end
                    filnum2 += 1
                end
                if !pts
                    continue
                end
                println(string("   Opening second: ", listset[casenum], "/", filename2))
                imgreg = readMetaimage.read_data(string(basedir, listset[casenum], "/", filename2, ".mhd"))
                # imgreg = convert(Images.Image{Images.Gray}, imgreg)
                landreg = erroreval.read_landmarks_POPI(imgreg, string(basedir, listset[casenum], "/", filename2, ".pts"), string(basedir, listset[casenum], "/", filename2, ".mhd"))
                imgreg=segimgtools.quantize(imgreg, quantQ)

                # Init registration
                sizemm = Images.pixelspacing(imgreg) .* [size(imgreg)...]
                def = deformation.BsplineDeformation(3,sizemm, [11,11,7])
                # Register
                d = finereg.register_fast_multiscale(imgbase, imgreg, kpts, def, nhoods,
                        crit_params, opt_state, reg_params,
                        @options hmax = maxd minsize = minimgsize minKP = minkpts resample = true largerotation = largerot pwl = pwl)

                # Evaluate the registration
                y  = zeros(Float64, 3)
                # Init output
                tupmin0 = 1.11e5
                tupmax0 = zero(Float64)
                tuplen0 = zero(Float64)
                setdistv = Array(Float64, size(landreg, 1))
                for poin = 1:size(landreg, 1)
                    deformation.transform_point!(d, vec(landbase[poin, :]), y)
                    setdistv[poin] = sqrt(sum((vec(landreg[poin, :]) - y).^2))
                    len0 = sqrt(sum((vec(landreg[poin, :]) - vec(landbase[poin, :])).^2))
                    tupmin0 = min(tupmin0, len0)
                    tupmax0 = max(tupmax0, len0)
                    tuplen0 += len0
                end
                sort!(setdistv)
                casemin0 = min(casemin0, tupmin0)
                casemax0 = max(casemax0, tupmax0)
                caselen0 += tuplen0
                caseTcnt += 1
                casedistv = [casedistv; setdistv]
                if (0.5 * tuplen0 > sum(setdistv))
                    caseRdistv = [caseRdistv; setdistv]
                end
                # Write image pair result into log file
                fstr = open(logfile, "a")
                write(fstr, string(filename, ", ", filename2, ", "))
                print_shortest(fstr, tuplen0 / size(landreg, 1))
                write(fstr, ", ")
                print_shortest(fstr, mean(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, std(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, median!(setdistv))
                for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
                    write(fstr, ", ")
                    print_shortest(fstr, mean(setdistv[1:round(Int, length(setdistv) * rtm / 100)]))
                end
                write(fstr, ", ")
                print_shortest(fstr, tupmin0)
                write(fstr, ", ")
                print_shortest(fstr, minimum(setdistv))
                write(fstr, ", ")
                print_shortest(fstr, tupmax0)
                write(fstr, ", ")
                print_shortest(fstr, maximum(setdistv))
                write(fstr, "\n")
                close(fstr)
                r=deformation.get_theta(d)
                @debugprintln_with_color(:green, "Registration result: len: $(tuplen0/size(landreg, 1)) => $(sum(setdistv)/size(landreg, 1))")
                # ImageView.view(segimgtools.overlay(imgbase/maximum(imgbase),imgreg/maximum(imgreg)),name="overlay before")
                # warped=deformation.transform_image_nn(d.def[2],imgreg,size(imgbase),@options)
                # ImageView.view(segimgtools.overlay(imgbase/maximum(imgbase),warped/maximum(warped)),name="overlay after 1")
                # warped=deformation.transform_image_nn(d,imgreg,size(imgbase),@options)
                # ImageView.view(segimgtools.overlay(imgbase/maximum(imgbase),warped/maximum(warped)),name="overlay after2")
            end
        end
        sort!(casedistv)
        totmin0 = min(totmin0, casemin0)
        totmax0 = max(totmax0, casemax0)
        totlen0 += caselen0
        totTcnt += caseTcnt
        totdistv = [totdistv; casedistv]
        totRdistv = [totRdistv; caseRdistv]
        fstr = open(logfile, "a")
        write(fstr, listset[casenum])
        write(fstr, ", , ")
        print_shortest(fstr, caselen0 / length(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, mean(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, std(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, median!(casedistv))
        for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
            write(fstr, ", ")
            print_shortest(fstr, mean(casedistv[1:round(Int, length(casedistv) * rtm / 100)]))
        end
        write(fstr, ", ")
        print_shortest(fstr, casemin0)
        write(fstr, ", ")
        print_shortest(fstr, minimum(casedistv))
        write(fstr, ", ")
        print_shortest(fstr, casemax0)
        write(fstr, ", ")
        print_shortest(fstr, maximum(casedistv))
        write(fstr, "\nRobustness = ")
        print_shortest(fstr, length(caseRdistv) / length(casedistv))
        write(fstr, ", , , ")
        if isempty(caseRdistv)
            write(fstr, "N/A")
            write(fstr, ", ")
            write(fstr, "N/A")
            write(fstr, ", ")
            write(fstr, "N/A")
        else
            print_shortest(fstr, mean(caseRdistv))
            write(fstr, ", ")
            print_shortest(fstr, std(caseRdistv))
            write(fstr, ", ")
            print_shortest(fstr, median!(caseRdistv))
        end
        write(fstr, "\n\n")
        close(fstr)
    end
    sort!(totdistv)
    fstr = open(logfile, "a")
    write(fstr, "total, , ")
    print_shortest(fstr, totlen0 / length(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, mean(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, std(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, median!(totdistv))
    for rtm = 100 - tmeanstep:-tmeanstep:tmeanmin
        write(fstr, ", ")
        print_shortest(fstr, mean(totdistv[1:round(Int, length(totdistv) * rtm / 100)]))
    end
    write(fstr, ", ")
    print_shortest(fstr, totmin0)
    write(fstr, ", ")
    print_shortest(fstr, minimum(totdistv))
    write(fstr, ", ")
    print_shortest(fstr, totmax0)
    write(fstr, ", ")
    print_shortest(fstr, maximum(totdistv))
    write(fstr, "\nRobustness = ")
    print_shortest(fstr, length(totRdistv) / length(totdistv))
    write(fstr, ", , , ")
    if isempty(totRdistv)
        write(fstr, "N/A")
        write(fstr, ", ")
        write(fstr, "N/A")
        write(fstr, ", ")
        write(fstr, "N/A")
    else
        print_shortest(fstr, mean(totRdistv))
        write(fstr, ", ")
        print_shortest(fstr, std(totRdistv))
        write(fstr, ", ")
        print_shortest(fstr, median!(totRdistv))
    end
    write(fstr, "\n")
    close(fstr)
end

end
