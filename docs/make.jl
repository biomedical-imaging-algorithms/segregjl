push!(LOAD_PATH,"../")
using Documenter, admm, batchfunctions, bsplines, coarsereg, criterion, deformation, eqcrit, erroreval
using finereg, gaussian, gradientdescent, keypoints, linprog_register, mil, optimizer, readMetaimage
using readRIRE, register, regularization, segimgtools, SLICsuperpixels, softmax, ssd
using test_admm, test_linprog_register, testreg

makedocs(
    format = :html,
    sitename = "segreg",
    authors = "Jan Kybic and Martin dolejsi.",
    pages = [   "Home" => "index.md",
                "Test modules" => [
                    "test_admm.md",
                    "test_linprog_register.md",
                    "testreg.md"
                ],
                "Modules" => [
                    "admm.md",
                    "batchfunctions.md",
                    "bsplines.md",
                    "coarsereg.md",
                    "criterion.md",
                    "deformation.md",
                    "eqcrit.md",
                    "erroreval.md",
                    "finereg.md",
                    "gaussian.md",
                    "gradientdescent.md",
                    "keypoints.md",
                    "linprog_register.md",
                    "mil.md",
                    "optimizer.md",
                    "readMetaimage.md",
                    "readRIRE.md",
                    "register.md",
                    "regularization.md",
                    "segimgtools.md",
                    "SLICsuperpixels.md",
                    "softmax.md",
                    "ssd.md"
                ]
            ]
    )
