# bsplines

```@autodocs
Modules = [bsplines]
Private = false
Order   = [:module]
```

# Exported functions ##

```@autodocs
Modules = [bsplines]
Private = false
Order   = [:function, :type]
```

# Internal functions ##

```@autodocs
Modules = [bsplines]
Public = false
Order   = [:function, :type]
```