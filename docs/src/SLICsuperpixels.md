# SLICsuperpixels

```@meta
CurrentModule = SLICsuperpixels
DocTestSetup  = quote
    using SLICsuperpixels
    using Colors
end
```

```@autodocs
Modules = [SLICsuperpixels]
Private = false
Order   = [:module]
```

# Exported functions ##

```@autodocs
Modules = [SLICsuperpixels]
Private = false
Order   = [:function, :type]
```

# Internal functions ##

```@autodocs
Modules = [SLICsuperpixels]
Public = false
Order   = [:function, :type]
```