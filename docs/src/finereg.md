# finereg

```@autodocs
Modules = [finereg]
Private = false
Order   = [:module]
```

# Exported functions ##

```@autodocs
Modules = [finereg]
Private = false
Order   = [:function, :type]
```

# Internal functions ##

```@autodocs
Modules = [finereg]
Public = false
Order   = [:function, :type]
```