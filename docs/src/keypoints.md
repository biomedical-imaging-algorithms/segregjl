# keypoints

```@autodocs
Modules = [keypoints]
Private = false
Order   = [:module]
```

# Exported functions ##

```@autodocs
Modules = [keypoints]
Private = false
Order   = [:function, :type]
```

# Internal functions ##

```@autodocs
Modules = [keypoints]
Public = false
Order   = [:function, :type]
```