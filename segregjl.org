Notes for the segmentation and registration project in Julia

best edited with Emacs Org-mode

* Log

** <2014-11-11 Tue> Jan

I am importing code from the segreg Python implementation 
~kybic/work/segreg
~kybic/work/BIA/code/segreg

I am creating a superpixel.py module in Python
by extracting the necessary parts from other Python code to make it standalone

Images are in ~kybic/work/segreg/imgs and on /datagrid/Medical

I will not push them to the git repository.

Just make a link or directory imgs in segregjl/

The scikit shipped with Ubuntu is too old, installing current using
PIP from http://scikit-image.org/download.html

created the INSTALL file with installation instructions

** <2014-11-12 Wed>

the superpixel code in skimage.segmentation seems to be buggy
will go back to the standalone


** <2014-11-15 Sat>

I have corrected a mistake when turning an image in
test_register_fast_rigid

Correcting the case when the samples go outside of image

test_register_fast_rigid now works. git commit 

Starting a new branch new-images
We will work directly with Julia's Images, instead of converting them
to Arrays. It should also support 3D. Namely, we will correct

*** DONE update transform_rigid to handle Images
    - State "DONE"       from "NEXT"       [2014-12-14 Sun 21:17]
*** DONE update superpixels to hangle Images
    - State "DONE"       from "NEXT"       [2014-12-14 Sun 21:27]
*** DONE update kmeans_segmentation to hangle Images
    - State "DONE"       from "NEXT"       [2014-12-14 Sun 21:27]
*** DONE update find_keypoints to hangle Images
    - State "DONE"       from "NEXT"       [2014-12-14 Sun 21:27]

** <2014-11-16 Sun>


interfacing Python and Julia is tricky because of the type  

I am moving to 'x','y' spatial order in images but it requires showing
them with e.g. imshow(convert(Array,p))

new module "deformation", now testing on
testreg.test_register_fast_rigid()

** <2014-11-23 Sun>

testreg.test_superpixel_segmentation() works
test_affine_segmreg seems to work, just slow, probably due to deformation.transform_rigid2D

next step, test_register_rigid(), change coarsereg to make it compatible with deformation.jl

** <2014-11-25 Tue>

introducing register.jl

** <2014-11-26 Wed>

working on testreg.test_register_rigid

** <2014-12-02 Tue>

** <2014-12-06 Sat>

NLOpt kept failing with incorrect argument error. It turns out that
disabling precompilation (userimg.jl) gets rid off the problem.

We now need to find out, what combination of packages causes the problem.

Seems to have been solved by the package maintainer.


test_register_rigid compiles but 
   - takes a very long time (76s!!, lot of garbage collection?)

find out why it is slow - image warping?
 - use anonymous functions
 - use multidimensional cycles

** <2014-12-07 Sun>

*** Profiling test_register_rigid

Register.register: Optimization finished after 41 iterations, crit=-0.8806519235350478
elapsed time: 98.692118235 seconds (12986263048 bytes allocated, 35.02% gc time)

1099 ...ybic/.julia/v0.3/NLopt/src/NLopt.jl; nlopt_callback_wrapper; line: 410
 1099 ...c_homedir/work/segregjl/register.jl; critwarped; line: 36
  8   ...omedir/work/segregjl/deformation.jl; transform_image_nn; line: 140
   1 ./abstractarray.jl; vcat; line: 0
   4 ./abstractarray.jl; vcat; line: 518
  6   ...omedir/work/segregjl/deformation.jl; transform_image_nn; line: 141
   1 ...omedir/work/segregjl/deformation.jl; transform_point; line: 110
   2 ...omedir/work/segregjl/deformation.jl; transform_point; line: 113
     1 ./array.jl; fill!; line: 0
   1 abstractarray.jl; copy!; line: 149
   1 abstractarray.jl; integer; line: 288
   1 abstractarray.jl; integer; line: 291
  727 ...omedir/work/segregjl/deformation.jl; transform_image_nn; line: 142
   2   ./bitarray.jl; BitArray; line: 0
   2   ./bitarray.jl; BitArray; line: 28
   370 broadcast.jl; broadcast!; line: 227
 
** <2014-12-08 Mon>

testreg.time_deformation() for benchmarking
elapsed time: 1.168985875 seconds (316855352 bytes allocated, 69.19% gc time)

installing Devectorize

does not help much. specializing to 2D. I would like to do it with
Cartesian but it does not seem to be easy

** <2014-12-09 Tue>

rewritting transform_image_nn for a fixed dimension and in-place calculation
brings the time down to
testreg.time_deformation()
elapsed time: 0.011574671 seconds (197532 bytes allocated)

Great.

I have added the 3D version but have not tested it yet.

** <2014-12-10 Wed>

test_register_rigid now works and is faster
Register.register: Optimization finished after 41 iterations, crit=-0.8806519235350478
elapsed time: 0.22304628 seconds (3499112 bytes allocated)

skipping starting to work on test_register_rigid_global so far, we can
add it afterwards

creating register.register_images_multiscale
seems to work, git push

test_register_case03 works, git push
test_superpixel works

changing finereg.find_keypoints to accept LabelImage

transposing in segimgtools.classimage2rb(Images.Image)

test_find_keypoints works

test_MIL_contributions works

eval_sampled_MIL_criterion works

testreg.test_register_fast_rigid() works

** <2014-12-13 Sat>

SLIC implementation in Julia

** <2014-12-14 Sun>

new branch speedup-register-fast to find out why the registration is
not faster than the standard pixel-based one.

What takes time seems to be sample_normals
I will make a specialized 2D version
Replaced exceptions with 'if'

register_fast in test_register_fast_rigid now takes about 16ms. committing

some allocation avoided in MIL_contributions but there is still some
allocation going on. 

*** NEXT Eliminate allocations in MIL_contributions and sample_normals


** <2014-12-18 Thu>

optimization using gradients works, merging to master
penalty function added in eval_sampled_MIL_criterion_and_grad!

** <2015-02-06 Fri>

some work in the Debugging branch. Conclusions - the displacement
should be small, otherwise the accuracy suffers

updated packages

in testreg.test_register_fast_rigid, xtol=0.1
  11 iterations are needed to converge with the LBFGS gradient  alg. crit=-0.101  (worst)
  27 iterations with BOBYQA crit=-0.144
  28 iterations with COBYLA crit=-0.1437

pushing to git origin

working on multires in branch multires
entry point:    testreg.test_register_rigid_multiscale()
main function:  register.register_images_multiscale()

** <2015-02-07 Sat>

test_register_rigid_multiscale seems to work but does not save any
time (this might be due to garbage collection). Checking in.

now trying to merge master into the multires branch

** <2015-02-10 Tue>

register_fast_resample + test_register_fast_resample_rigid works
(Martin)
somewhat inefficient, lot of allocations

** <2015-02-18 Wed>

register_fast_multiscale works

** <2015-06-25 Thu>

new branch "affine"
trying test_register_fast_multiscale_rigid - seems to work
superpixels take 2x 5s, registration takes 13.1s, a lot of allocation (2,040,949,024) bytes

updating Julia to 0.3.11
registration time reduced to 8s, a lot of allocations (2147549592 bytes allocated, 42.25% gc time)
precompilation (userimg.jl) does not seem to work

profiling finereg.register_fast_multiscale
what seems to take time is "transform_point!" and the dot product in
eval_sampled_MIL_criterion_and_grad!,
also downsample_majority

Multiscale registration:
elapsed time: 8.086415231 seconds (2091067888 bytes allocated, 35.19% gc time)
522  /home/kybic/.julia/v0.3/NLopt/src/NLopt.jl; nlopt_callback_wrapper; line: 414
 520 /local/kybic_homedir/work/segregjl/finereg.jl; crit!; line: 869
  64  /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 759
   47 /local/kybic_homedir/work/segregjl/deformation.jl; transform_point!; line: 247
   3  /local/kybic_homedir/work/segregjl/deformation.jl; transform_point!; line: 248
  221 /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 761
   166 array.jl; -; line: 723
   1   array.jl; -; line: 724
   8   array.jl; -; line: 725
   1   array.jl; -; line: 727
   8   linalg/blas.jl; dot; line: 146
   2   linalg/blas.jl; dot; line: 147
  2   /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 762
  4   /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 764
  2   /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 766
  1   /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 777
  9   /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 782
  42  /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 783
  141 /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 785
  33  /local/kybic_homedir/work/segregjl/finereg.jl; eval_sampled_MIL_criterion_and_grad!; line: 790
   12 /local/kybic_homedir/work/segregjl/deformation.jl; add_grad_contrib!; line: 297


There is some problem in PyEval, core dumps when the window gets focus


Individual iterations seem to be fast.

I have improved majority subsampling for 2D very dramatically, from 2s
to 0.02s. 

Still the registration takes about 2.8s. A lot of time is taken by MIL_contributions
Accelerated by taking away "slice" and unrolling. MIL_contribution is
now <40ms but still a lot of allocations. 

Registration proper now takes about 2s.

** <2015-06-26 Fri>

To avoid crashes when a PyPlot window is selected, use
using PyCall
pygui(:qt) # to avoid a crash when a PyPlot window is selected
using PyPlot

"correct" results are [5.405908372580256,-46.47292611957964,-65.2872570701771]

Let's have a look at the superpixels: SLICsuperpixels.slicSize

superpixels contain about 250 pixels (15x15) - parameter spedge

** <2015-06-29 Mon>

in branch PhysicalUnits, trying to find out, why it does not work

test_register_fast_multiscale_rigid_3D()

implement landmark fitting (deformation.fit_landmarks) using
optimization (could be fitted directly)

tested - landmarks work.

new we should implement reading the image files

** <2015-06-30 Tue>

new module readRIRE

to read RIRE files directly
had to convert from little endian
works but is slow

** <2015-07-02 Thu>
Branches affine and PhysicalUnits merged into master

** <2015-07-17 Fri>

in branch affine, trying to find out, why the registration is not
properly updated from level to level

running test_register_fast_multiscale_rigid_3D

JK->MD: Pisme komentare ke kazde funkci, co dela. Napr. get_resample_factor

** <2015-07-21 Tue>

modifying test_register_fast_rigid_3D() to use the same images as
test_register_fast_multiscale_rigid_3D()

I get ERROR: BoundsError()
 in interpolate_recursive at /local/kybic_homedir/work/segregjl/deformation.jl:128
there seems to be an error in transform_image_lin!

creating a function show3Doverlay

** <2015-08-06 Thu>

for some reason, canvasgrid in show3Doverlay does not support
interactivity, abandoning

problems in transform_image_lin!, does not handle the singular case of
the deformation giving back integer coordinates. Corrected.

with good segmentation, test_register_fast_rigid_3D gives correct results

* Notes

** How to reload a module in Julia

reload("TstMod")

** To speedup loading, 

defining ~/builds/julia/base/userimg.jl to
precompile often used packages
https://groups.google.com/forum/#!topic/julia-users/uQfBNtJksRo

My base/userimg.jl:

#Base.require("PyCall.jl")
#Base.require("PyPlot.jl")
#Base.require("Images.jl")
#Base.require("ImageView.jl")
Base.require("Clustering.jl")
Base.require("Debug.jl")
Base.require("Color.jl")
Base.require("GSL.jl")
Base.require("NLopt.jl")
Base.require("ImmutableArrays.jl")
Base.require("FixedPointNumbers.jl")
Base.require("DataFrames.jl")

** To speedup anonymous functions
https://github.com/timholy/FastAnonymous.jl

* ToDo


** NEXT accelerate transform_image_lin, probably by writing specialized versions
** NEXT accelerate segmentation
** NEXT avoid allocation of the Jacobian matrix in deformation.get_jacobian

** DONE Get rid of the superpixel module in Python
   - State "DONE"       from "NEXT"       [2014-12-23 Tue 21:50]

  Make native Julia superpixels 
or interface to the C superpixel code (SLIC)

** DONE add gradient to finereg.eval_sampled_MIL_criterion and
   - State "DONE"       from "NEXT"       [2014-12-23 Tue 21:50]
   finereg.register_fast It should be easy, just interpolate from the
   stored values. 

** NEXT combine multiscale + register_fast

  combine the multiresolution mechanism (see
  register.register_images_multiscale) and fast registration based on
  sampling around boundary points (finereg.register_fast), so that it
  is even faster
    
input: segmentations f,g at level 'l' sets of keypoints at level 'l',
       initial transformation 'T0' at level 'l'

if images are small enough
   optimize 'T' directly at level 'l' starting from initial
      transformation 'T0' (this we have already, except for the initial transf.)
else
   represent 'T0' at the coarser level 'l+1' (twice as small images,
                                              change of coordinates)

   subsample keypoints twice

   represent keypoints at level 'l+1' (change of coordinates)

   reduce segmentation sizes twice to get level 'l+1' by majority voting  (implemented)
      (Note: if this turns out to be the bottleneck, we can do the
      subsampling only at pixels we need, which should reduce the
     complexity significantly at the cost of some book-keeping)

   recursively find transformation 'T' at level 'l+1' starting from reduced 'T0'
     
   represent 'T' at the original level 'l'

   optimize 'T' directly at level 'l'
end

** NEXT resample normals during optimization

   we need that sample_normals take into account the current   transformation
   the new transformation then needs to be composed with the old one

   alternatively, the MIL_contribution should take into account that
   the normals were sampled away from the identity transformation.

** DONE extend to 3D
   - State "DONE"       from "NEXT"       [2015-02-07 Sat 23:44]
** NEXT profile and further acceleration, I believe there is a multiplicative factor to gain.
** NEXT accept anisotropic voxels
** NEXT parallelize to accelerate
** NEXT elastic deformation. Replace rigid transformation by B-splines.
** NEXT make different similarity criterion possible. Test case: percentage/number of matching labels
** DONE in finereg - handle displacement outside of the precalculated range.
   - State "DONE"       from "NEXT"       [2015-01-01 Thu 21:34]
   Perhaps a barrier function to lead the parameters back into the range.   


** DONE make register_fast_resample more efficient
   - State "DONE"       from "NEXT"       [2015-02-09 Sat 14:15]
   use trust region optimization, do not go outside the precomputed regions
   only resample, if the transformation has changed significantly

** DONE subsample keypoints
   - State "DONE"       from "NEXT"       [2015-02-18 Wed 00:33]

   preprocessing:
      - for each superpixel, keep a set of its neighbors
      - for each keypoint, know which superpixels it borders
      - for each superpixel, know which keypoints are incident with it
      - for each keypoint, know what are the 'neighboring' keypoints 
            = their superpixels must be neighbors and the classes must be the same. 
     
   procedure (linear time):
      - Mark all keypoints as unseen
      - While there is an unseen keypoint
          - put the keypoint into a queue
          - while the queue is not empty
              - get a keypoint K from the queue
              - if K is unseen 
                  -  mark K as seen
                  -  eliminate K's neighbors and mark them as seen
                  -  put the unseen neighbors of unseen neighbors of K into the queue
                     and mark them as seen
** NEXT test and hangle the two conditions separately s <= minsize || lKP <= minKP 
        in register_fast_multiscale
** NEXT eliminate downsampling of 's1ln' and 'g' in register_fast_multiscale

