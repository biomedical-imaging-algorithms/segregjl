# Segmentation and registration in Julia

Jan Kybic, November 2014

## How to install

1. Install Julia (julialang.org). Used version 0.6.x
   Read the manual
2. Install Python with various packages, namely 
     - ScientificPytho (scipy), version 0.13.3
     - Matplotlib, version 1.3.1
     - optional: Scikit-image, version 0.10.1.
         Scikit contains SLIC but it does not work for me
     - SLIC interface from https://github.com/amueller/slic-python
   Julia can call Python, so for the moment, I am using Python for
   example for visualization and superpixel calculation. We should
   gradually move away from it.
3. Start `julia`
4. Install packages in Julia from the package manager 
   http://julia.readthedocs.org/en/latest/manual/packages/
   e.g. by saying `Pkg.add("PyCall")`

   Currently used packages:

```
    julia> Pkg.status()
    9 required packages:
     - Clustering                    0.9.1
     - ImageMagick                   0.5.0
     - ImageView                     0.6.0
     - Images                        0.12.0
     - MicroLogging                  0.2.0
     - NLopt                         0.3.6
     - NearestNeighbors              0.3.0
     - PyCall                        1.15.0
     - PyPlot                        2.3.2
    65 additional packages:
    ...
```
   
**Note**, do not install Options package because the local should be used

5. Launch some of the tests from testreg.jl, for example by

   include("testreg.jl")
   testreg.test_register_fast_rigid()
